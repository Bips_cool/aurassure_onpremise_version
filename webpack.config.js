/*eslint no-unused-vars: "off"*/

const webpack = require('webpack'),
	poststylus = require('poststylus'),

	config = require('./config');

const is_production_build = (config.env === 'production');
const build_type = config.type;

module.exports = {
	entry: './src/js/index.js',
	output: {
		path: 'dist',
		filename: 'bundle.js'
	},
	devServer: {
		inline: true,
		contentBase: './dist',
		port: 9090
	},
	module: {
		preLoaders: [
			{
				test: /\.js$/,
				loader: 'eslint-loader',
				exclude: /node_modules/
			}
		],
		loaders: [
			{
				test: /\.js$/,
				loaders: [
					'babel',
					'string-replace?' + JSON.stringify({
						multiple: [
							{
								search: '##PR_STRING_REPLACE_ACCOUNT_BASE_PATH##',
								replace: (build_type === 'deploy') ? '' : 'https://dev.datoms.io/aurassure-webapp/accounts',
								flags: 'g'
							},
							{
								search: '##PR_STRING_REPLACE_API_BASE_PATH##',
								replace: is_production_build ? (build_type === 'stage' ? 'https://dev.datoms.io/aurassure-webapp/smart_city_platform/api/' : 'https://api.aurassure.com/smart_city_platform/') : 'https://dev.datoms.io/api/aurassure-webapp/smart_city_platform/',
								flags: 'g'
							},
							{
								search: '##PR_STRING_REPLACE_SOCKET_BASE_PATH##',
								replace: is_production_build ? (build_type === 'stage' ? 'https://stage-rtapp.datoms.io' : 'https://rtapp.datoms.io') : 'https://dev-rtapp.datoms.io',
								flags: 'g'
							}
						]
					})
				],
				exclude: /node_modules/
			},
			{
				test: /\.json$/,
				loader: 'json'
			},
			{
				test: /\.styl$/,
				loaders: [
					'style',
					'css',
					'stylus'
				]
			}
		]
	},
	stylus: {
		use: [
			poststylus([ 'postcss-cssnext' ])
		]
	},
	plugins: is_production_build ? [
		new webpack.DefinePlugin({ //this is recomended by React - https://facebook.github.io/react/docs/optimizing-performance.html#use-the-production-build
			'process.env': {
				NODE_ENV: JSON.stringify('production'),
				BUILD_TYPE: JSON.stringify(build_type)
			}
		}),
		new webpack.optimize.AggressiveMergingPlugin(),
		new webpack.optimize.OccurrenceOrderPlugin(),
		new webpack.optimize.DedupePlugin(),
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				booleans: true, // Various optimizations for boolean context. E.g.: !!a ? b : c → a ? b : c
				warnings: false, // Display warnings when dropping unreachable code or unused declarations etc.
				pure_getters: true, // It will assume that object property access. (E.g.: foo.bar or foo["bar"])
				unsafe: true, // It enables some transformations that might break code logic in certain contrived cases, but should be fine for most code. E.g.: new Array(1, 2, 3) or Array(1, 2, 3) → [1, 2, 3]
				screw_ie8: true, // Pass this flag if you don't care about full compliance with IE 6-8 quirks.
				conditionals: true, // Apply optimizations for if-s and conditional expressions.
				unused: true, // Drop unreferenced functions and variables.
				comparisons: true, // Apply optimizations to binary nodes. E.g.: !(a <= b) → a > b (only when unsafe)
				sequences: true, // Join consecutive simple statements using the comma operator
				drop_debugger: true, // Remove debugger; statements.
				drop_console: (build_type === 'stage') ? false : true, // Pass true to discard calls to console.* functions.
				dead_code: true, // Remove unreachable code.
				evaluate: true, // Attempt to evaluate constant expressions.
				if_return: true, // Optimizations for if/return and if/continue.
				join_vars: true, // Join consecutive var statements.
				properties: true, // Rewrite property access using the dot notation. E.g.: foo["bar"] → foo.bar
			},
			output: {
				comments: false
			},
			exclude: [/\.min\.js$/gi] // skip pre-minified libs
		}),
		new webpack.IgnorePlugin(/^\.\/locale$/, [/moment$/])
	] : []
};
