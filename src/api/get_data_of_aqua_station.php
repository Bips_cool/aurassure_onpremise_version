<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_data_of_station.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    station_id: 1
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(24, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$station_id = $received_data['station_id'];

if(isset($station_id) && $station_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$station_id = mysqli_real_escape_string($aurassure_db->connection, $station_id);

	if($api_utilities->session_handler->user_session_data['ci'] == 5) {
		//send all devices for admin portal
		$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id;
	} else {
		$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, the requested station wasn\'t found.');
	}

	$result = mysqli_fetch_assoc($result_set);
	$json_response['id'] = $result['dvcloc_id'];
	$temp_station_name = '';
	if($result['dvcloc_name']) {
		$temp_station_name .= $result['dvcloc_name'].', ';
	}
	if($result['dvcloc_city']) {
		$temp_station_name .= $result['dvcloc_city'];
	}
	$temp_station_name = rtrim($temp_station_name, ', ');
	$json_response['name'] = $temp_station_name;
	$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
	$json_response['last_data_update_time'] = $result['dvcloc_last_data_update_time'];
	$json_response['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
	$json_response['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);
	$json_response['aqi_status'] = EndUserResult::getAQIStatus($result['dvcloc_aqi']);
	$json_response['temp'] = $result['dvcloc_temp'];
	$json_response['humid'] = $result['dvcloc_humid'];
	$json_response['suggestions'] = EndUserResult::getSuggestions($result['dvcloc_aqi'], $result['dvcloc_responsible_param']);

	$sql = "SELECT dhad_aqi, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=$station_id AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME'];
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_hourly_aqis = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$station_hourly_aqis[$result['dhad_upto_time']] = $result['dhad_aqi'];
	}

	$json_response['hourly_aqis'] = [];
	$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
	for ($i=0; $i<24; $i++) {
		if($station_hourly_aqis[$latest_hour]) {
			$json_response['hourly_aqis'] []= $station_hourly_aqis[$latest_hour];
		} else {
			$json_response['hourly_aqis'] []= null;
		}

		$latest_hour -= 3600;
	}

	//get params and param units of station
	$sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=".$station_id." ORDER BY dvcdat_time DESC LIMIT 1";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, no data has been received from the station yet.');
	}
	$result = $result_set->fetch_assoc();
	$station_param_keys = [];
	$station_latest_data = json_decode($result['dvcdat_data'], true);
	foreach ($station_latest_data as $param_key => $param_value) {
		$station_param_keys []= $param_key;
	}
	
	$sql = "SELECT dvcprm_name, dvcprm_name_noformat, dvcprm_key, dvcprm_unit FROM dvc_params WHERE dvcprm_key IN ('".implode('\',\'', $station_param_keys)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_params = [];
	$json_response['params'] = [];
	$json_response['param_units'] = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$station_params []= $result['dvcprm_key'];
		$json_response['params'] []= [
			'name' => $result['dvcprm_name'],
			'key' => $result['dvcprm_key']
		];
		$json_response['param_units'] []= $result['dvcprm_unit'];
	}

	$sql = "SELECT dvcdvc_last_raw_data, dvcdvc_last_data_receive_time FROM dvc_devices WHERE dvcloc_id=".$station_id;
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$result = $result_set->fetch_assoc();
	$json_response['latest_param_value'] = json_decode($result['dvcdvc_last_raw_data'], true);
	$json_response['last_data_receive_time'] = $result['dvcdvc_last_data_receive_time'];
	$json_response['data_receive_connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcdvc_last_data_receive_time']) <= 7200 ? 'online' : 'offline';

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(24, $access_time, 'update', $user_id, $log_id);