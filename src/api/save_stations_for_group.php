<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/save_group_stations.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		group_id: 2,
		group_stations: ["1","2","3"],
		group_stations_unselected: ["4","5"]
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(240, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$group_id = $received_data['group_id'];
$group_stations = $received_data['group_stations'];
$group_stations_unselected = $received_data['group_stations_unselected'];

if (isset($group_id) && $group_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$group_id = mysqli_real_escape_string($aurassure_db->connection, $group_id);

	if ($group_stations && count($group_stations)) {
		if (count($group_stations)) {
			$add_station_sql = '';
			for ($i = 0; $i < count($group_stations); $i++) {
				$add_station_sql = "INSERT INTO `stations_under_group`(`grp_id`, `dvcloc_id`) VALUES ('".$group_id."','".$group_stations[$i]."');";
				$add_station_result_set = $aurassure_db->query($add_station_sql);
			}
		}
	}
	if ($group_stations_unselected && count($group_stations_unselected)) {
		$delete_sql = '';
		$aaaa = [];
		for ($i = 0; $i < count($group_stations_unselected); $i++) {
			$delete_sql = "DELETE FROM stations_under_group WHERE grp_id='".$group_id."' AND dvcloc_id=".$group_stations_unselected[$i];
			$aaaa []= $delete_sql;
			$delete_station_result_set = $aurassure_db->query($delete_sql);
		}
	}

	if ($add_station_result_set || $delete_station_result_set) {
		// $json_response['sql'] = $aaaa;
		$api_utilities->set_success_ststus_in_response();
	} else{
		// $json_response['sql'] = $aaaa;
		$api_utilities->throw_error('Sorry, unable to Add Stations!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(240, $access_time, 'update', $user_id, $log_id);