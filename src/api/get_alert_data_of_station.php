<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_alert_data_of_station.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		station_id: 2
	})
}).then(function(response) {
	return response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(17, $access_time, 'insert', $user_id);
use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$station_id = $received_data['station_id'];

if (isset($station_id) && is_numeric($station_id)) {
	$sql = "SELECT `dvcprm_id`, `dvcprm_name`, `dvcprm_name_noformat` FROM `dvc_params`";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_params = [];
	$json_response['params'] = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$json_response['params'] []= [
			'id' => $result['dvcprm_id'],
			'name' => $result['dvcprm_name'],
			'name_noformat' => $result['dvcprm_name_noformat'],
			'name_noformat_lower' => strtolower($result['dvcprm_name_noformat'])
		];
	}

	$sql = "SELECT `dpu_id`, `dpu_unit`, `dpu_unit_noformat` FROM `dvc_param_units`";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_params = [];
	$json_response['param_units'] = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$json_response['param_units'] []= [
			'id' => $result['dpu_id'],
			'unit' => $result['dpu_unit'],
			'unit_noformat' => $result['dpu_unit_noformat']
		];
	}

	$sql = "SELECT `dvcloc_params`, `dvcloc_param_units`, `dvcloc_param_thresholds`, `dvcloc_parameter_thresholds`, `ct_id`, `dvcloc_id` FROM `dvc_locations` WHERE `dvcloc_id`=".$station_id;
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$result = $result_set->fetch_assoc();
	$station_data = [
		'allowed_parameters' => json_decode($result['dvcloc_params'], true),
		'allowed_parameter_units' => json_decode($result['dvcloc_param_units'], true),
		'parameter_thresholds' => json_decode($result['dvcloc_parameter_thresholds'], true)
	];
	$json_response['station_data'] = $station_data;

	/*$sql = "SELECT `dvcprm_id`, `dvcprm_name`, `dvcprm_unit` FROM `dvc_params`";
	$city_result_set = $aurassure_db->query($sql);
	if(!$city_result_set) {
		$api_utilities->db_error();
	}
	$json_response['param_details'] = [];
	while ($result = $city_result_set->fetch_assoc()) {
		$json_response['param_details'][$result['dvcprm_id']] = [];
		$json_response['param_details'][$result['dvcprm_id']]['name'] = $result['dvcprm_name'];
		$json_response['param_details'][$result['dvcprm_id']]['unit'] = $result['dvcprm_unit'];
	}*/

	$api_utilities->set_success_ststus_in_response();
	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(17, $access_time, 'update', $user_id, $log_id);