<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_cities.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(230, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
$sql = "SELECT cities.ct_id, cities.ct_name, cities.ct_subdomain, cities.ct_send_alerts, cities.ct_mail_ids, (select COUNT( * ) from dvc_locations where dvc_locations.ct_id = cities.ct_id) AS locations_for_city FROM cities";
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$cities = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['ct_id'];
		$temp_location_array['name'] = $result['ct_name'];
		$temp_location_array['subdomain'] = $result['ct_subdomain'];
		$temp_location_array['ct_alerts'] = $result['ct_send_alerts'];
		$temp_location_array['ct_mail_id'] = $result['ct_mail_ids'];
		$temp_location_array['user_count'] = $result['total_no_of_users_for_city'];
		$temp_location_array['locations_count'] = $result['locations_for_city'];

		$cities []= $temp_location_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response['cities'] = $cities;
} else {
	$api_utilities->db_error();
	// $json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(230, $access_time, 'update', $user_id, $log_id);