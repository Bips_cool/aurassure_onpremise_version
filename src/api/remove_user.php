<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/remove_user.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		user_id: that.state.remove_user
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(33, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("_includes/form_data_validation_functions.php");

	// Check the value of required fields are set or not
	$data = json_decode(file_get_contents('php://input'), true);
	if(!has_presence($data['user_id'])) {
		$api_utilities->incomplete_request("Please select a Valid User!");
	}

	// Sanitize the fields
	$user_id = mysqli_real_escape_string($aurassure_db->connection, $data['user_id']);

	// Remove user
	$sql = "INSERT INTO `usr_deleted_accounts`(`usr_id`, `usrdel_first_name`, `usrdel_middle_name`, `usrdel_last_name`, `usrdel_email`, `usrdel_mobile`, `usrdel_dob`, `usrdel_gender`, `usrdel_pass`, `usrdel_date_created`, `usrdel_force_pw_change`, `usrdel_gcm_device_id`, `usrdel_logged_in_from_os`, `usrdel_app_settings`, `usrdel_type`, `usrdel_for_city`, `usrdel_for_grp`, `usrdel_source`, `usrdel_social_id`, `usrdel_role`, `usrdel_designation`, `usrdel_department`,`usrdel_access`, `usrdel_under`, `usrdel_deleted_by`, `usrdel_deleted_at`, `usrdel_deleted_from_ua`) SELECT `usr_id`, `usr_first_name`, `usr_middle_name`, `usr_last_name`, `usr_email`, `usr_mobile`, `usr_dob`, `usr_gender`, `usr_pass`, `usr_date_created`, `usr_force_pw_change`, `usr_gcm_device_id`, `usr_logged_in_from_os`, `usr_app_settings`, `usr_type`, `usr_for_city`, `usr_for_grp`, `usr_source`, `usr_social_id`, `usr_role`, `usr_designation`, `usr_department`,`usr_access`, `usr_under`, ".$api_utilities->session_handler->user_session_data['ui'].", ".$_SERVER['REQUEST_TIME'].", '".$_SERVER['HTTP_USER_AGENT']."' FROM `usr_login` WHERE `usr_id`=".$user_id;
	$result_set =  $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->throw_error('Sorry, unable to process user!');
		// $api_utilities->throw_error('SQL Error: '.$sql);
	} else {
		$sql = "DELETE FROM `usr_login` WHERE `usr_id`=".$user_id;
		$del_result_set =  $aurassure_db->query($sql);
		if(!$del_result_set) {
			$api_utilities->throw_error('Sorry, unable to delete user!');
			// $api_utilities->throw_error('SQL Error: '.$sql);
		}
		if($del_result_set) {
			$api_utilities->set_success_ststus_in_response();
		}
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(33, $access_time, 'update', $user_id, $log_id);