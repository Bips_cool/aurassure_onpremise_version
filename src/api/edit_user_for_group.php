<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/edit_user_for_group.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		user_id: that.state.edit_user,
		user_fname: document.getElementById('edit_fname').value,
		user_mname: document.getElementById('edit_mname').value,
		user_lname: document.getElementById('edit_lname').value,
		designation: document.getElementById('edit_designation').value,
		department: document.getElementById('edit_department').value,
		user_email: document.getElementById('edit_email').value,
		user_phone: document.getElementById('edit_phone').value,
		user_role: document.getElementById('edit_role').value
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(15, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("_includes/form_data_validation_functions.php");

	// Check the value of required fields are set or not
	$data = json_decode(file_get_contents('php://input'), true);
	if(!has_presence($data['user_id'])) {
		$api_utilities->incomplete_request("Please select a valid user!");
	} else if(!has_presence($data['user_fname'])) {
		$api_utilities->incomplete_request("Please give a valid First Name!");
	} else if(!has_presence($data['user_email'])) {
		$api_utilities->incomplete_request("Please give a valid Email ID!");
	} else if(!has_presence($data['user_phone'])) {
		$api_utilities->incomplete_request("Please give a valid Phone Number!");
	} else if(!has_presence($data['user_role'])) {
		$api_utilities->incomplete_request("Please give a valid User Role!");
	} 

	// Sanitize the fields
	$user_id = mysqli_real_escape_string($aurassure_db->connection, $data['user_id']);
	$user_fname = mysqli_real_escape_string($aurassure_db->connection, $data['user_fname']);
	$user_mname = mysqli_real_escape_string($aurassure_db->connection, $data['user_mname']);
	$user_lname = mysqli_real_escape_string($aurassure_db->connection, $data['user_lname']);
	$designation = mysqli_real_escape_string($aurassure_db->connection, $data['designation']);
	$department = mysqli_real_escape_string($aurassure_db->connection, $data['department']);
	$user_email = mysqli_real_escape_string($aurassure_db->connection, $data['user_email']);
	$user_phone = mysqli_real_escape_string($aurassure_db->connection, $data['user_phone']);
	$user_role = mysqli_real_escape_string($aurassure_db->connection, $data['user_role']);
	$user_type = mysqli_real_escape_string($aurassure_db->connection, $data['user_type']);
	$user_group = mysqli_real_escape_string($aurassure_db->connection, $data['group_id']);

	// Check other user with same email or phone is registered or not
	$sql = "SELECT `usr_id` FROM `usr_login` WHERE (`usr_email`='".$user_email."' OR `usr_mobile`='".$user_phone."') AND `usr_id`!=".$user_id;
	$result_set = $aurassure_db->query($sql);
	if(mysqli_num_rows($result_set)) {
		$api_utilities->throw_error("User with this Email/Phone is already registered!");
		// $json_response['sql'] = $sql;
	} else {
		// Update data of user
		$edit_sql = "UPDATE `usr_login` SET `usr_first_name`='".$user_fname."', `usr_middle_name`='".$user_mname."', `usr_last_name`='".$user_lname."', `usr_email`='".$user_email."', `usr_mobile`='".$user_phone."', `usr_role`=".$user_role.", `usr_type`='".$user_type."', `usr_for_grp`='".$user_group."' , `usr_designation`='".$designation."', `usr_department`='".$department."' WHERE `usr_id`=".$user_id." AND usr_for_grp=".$user_group;
		$edit_result_set = $aurassure_db->query($edit_sql);
		if($edit_result_set) {
			$api_utilities->set_success_ststus_in_response();
		} else {
			$api_utilities->throw_error('Sorry, unable to submit data!');
		}
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(15, $access_time, 'update', $user_id, $log_id);