<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_stations_added_for_cities.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
	body: JSON.stringify({
		city_id: 2
	})
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(241, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$city_id = $received_data['city_id'];

if (isset($city_id) && $city_id != '') {

	//sanitize received data
	$city_id = mysqli_real_escape_string($aurassure_db->connection, $city_id);

	$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id=".$city_id;

	$result_set = $aurassure_db->query($sql);
	// if(!$result_set->num_rows) {
	// 	$api_utilities->throw_error('Sorry, no stations found under this city.');
	// }

	$city_stations = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['dvcloc_id'];
		$temp_location_array['name'] = $result['dvcloc_name'];
		$temp_location_array['city_name'] = $result['dvcloc_city'];
		$temp_location_array['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
		$city_stations []= $temp_location_array;
	}
	$api_utilities->set_success_ststus_in_response();
	$json_response['city_stations'] = $city_stations;
	$json_response['cities'] = '';

	$cities_sql = "SELECT ct_id, ct_name FROM cities";
	$cities_result_set = $aurassure_db->query($cities_sql);
	if($cities_result_set) {
		$cities = [];
		while($cities_result = mysqli_fetch_assoc($cities_result_set)) {
			$temp_cities_array = [];
			$temp_cities_array['id'] = $cities_result['ct_id'];
			$temp_cities_array['name'] = $cities_result['ct_name'];

			$cities []= $temp_cities_array;
		}

		$json_response['cities'] = $cities;
	}

} else {
	$api_utilities->incomplete_request();
	// $json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(241, $access_time, 'update', $user_id, $log_id);