<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/add_station_to_compare_list_of_user.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		station_id: 1
		OR
		station_array: [1,2,3]
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];

use Aurassure\DataProcessor\EndUserResult;
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(11, $access_time, 'insert', $user_id);

$data = json_decode(file_get_contents('php://input'), true);
$station_id = $data['station_id'];
$station_array = $data['station_array'];

if((isset($station_id) && $station_id != '') || (isset($station_array) && is_array($station_array))) {
	$sql = "SELECT `usr_id`, `compared_locations_id` FROM `usr_compare_locations` WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui']." LIMIT 1";
	$result_set = $aurassure_db->query($sql);
	if($result_set && mysqli_num_rows($result_set)) {
		if ($station_id) {
			$compare_data = mysqli_fetch_assoc($result_set);
			$station_ids = json_decode($compare_data['compared_locations_id']);
			$station_id = mysqli_real_escape_string($aurassure_db->connection, $station_id);
			array_push($station_ids, intval($station_id));
			$sql = "UPDATE `usr_compare_locations` SET `compared_locations_id`='".json_encode(array_unique($station_ids))."' WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui'];
		} else if ($station_array) {
			$station_array = array_filter($station_array);
			$sql = "UPDATE `usr_compare_locations` SET `compared_locations_id`='".json_encode($station_array)."' WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui'];
		}
		if($result_set = $aurassure_db->query($sql)) {
			$api_utilities->set_success_ststus_in_response();
		} else {
			$api_utilities->db_error();
		}
	} else if($result_set && !mysqli_num_rows($result_set)) {
		if ($station_id) {
			$station_id = mysqli_real_escape_string($aurassure_db->connection, $station_id);
			$sql = "INSERT INTO `usr_compare_locations`(`usr_id`, `compared_locations_id`) VALUES (".$api_utilities->session_handler->user_session_data['ui'].",'[".intval($station_id)."]')";
		} else if ($station_array) {
			$station_array = array_filter($station_array);
			$sql = "INSERT INTO `usr_compare_locations`(`usr_id`, `compared_locations_id`) VALUES (".$api_utilities->session_handler->user_session_data['ui'].",'".json_encode($station_array)."')";
		}
		if($result_set = $aurassure_db->query($sql)) {
			$api_utilities->set_success_ststus_in_response();
		} else {
			$api_utilities->db_error();
		}
	} else {
		$api_utilities->db_error();
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(11, $access_time, 'update', $user_id, $log_id);