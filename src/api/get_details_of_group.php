<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_details_of_group.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    group_id: 1
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(239, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$group_id = $received_data['group_id'];

if(isset($group_id) && $group_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$group_id = mysqli_real_escape_string($aurassure_db->connection, $group_id);

	$sql = "SELECT grp_id, grp_name, grp_subdomain, grp_lat, grp_long, grp_zoom_lvl, grp_has_public, grp_is_premium FROM groups WHERE grp_id=".$group_id;
	// if($api_utilities->session_handler->user_session_data['ci'] == 5) {
	// 	//send all devices for admin portal
	// } else {
	// 	$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
	// }
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, the requested station wasn\'t found.');
	}

	$result = mysqli_fetch_assoc($result_set);
	$json_response['id'] = $result['grp_id'];
	$json_response['name'] = $result['grp_name'];
	$json_response['subdomain'] = $result['grp_subdomain'];
	$json_response['lat'] = $result['grp_lat'];
	$json_response['long'] = $result['grp_long'];//ToDo -> calculate the AQI and send it
	$json_response['zoom_lvl'] = $result['grp_zoom_lvl'];
	$json_response['is_public'] = ($result['grp_has_public'] == 1 ? true : false);
	$json_response['is_premium'] = ($result['grp_is_premium'] == 1 ? true : false);
	
	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(239, $access_time, 'update', $user_id, $log_id);