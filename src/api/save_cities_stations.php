<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/save_cities_stations.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		city_id: 2,
		city_stations: ["1","2","3"],
		city_stations_unselected: ["4","5"]
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(240, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$city_id = $received_data['city_id'];
$city_stations = $received_data['city_stations'];
$city_stations_unselected = $received_data['city_stations_unselected'];

if (isset($city_id) && $city_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$city_id = mysqli_real_escape_string($aurassure_db->connection, $city_id);

	if ($city_stations && count($city_stations)) {
		$add_station_sql = '';
		for ($i = 0; $i < count($city_stations); $i++) {
			$add_station_sql = "UPDATE `dvc_locations` SET `ct_id`='".$city_id."' WHERE `dvcloc_id`='".$city_stations[$i]."'";
			$add_station_result_set = $aurassure_db->query($add_station_sql);
		}
	}
	if ($city_stations_unselected && count($city_stations_unselected)) {
		$remove_station_sql = '';
		for ($i = 0; $i < count($city_stations_unselected); $i++) {
			$remove_station_sql = "UPDATE `dvc_locations` SET `ct_id`=0 WHERE `dvcloc_id`='".$city_stations_unselected[$i]."'";
			$remove_station_result_set = $aurassure_db->query($remove_station_sql);
		}
	}
	
	if ($add_station_result_set || $remove_station_result_set) {
		$api_utilities->set_success_ststus_in_response();
	} else{
		$api_utilities->throw_error('Sorry, unable to Add Stations!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(240, $access_time, 'update', $user_id, $log_id);