<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_groups.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(229, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
$sql = "SELECT groups.grp_id, groups.grp_name, groups.grp_subdomain, groups.grp_lat, groups.grp_long, (select COUNT( * ) from stations_under_group where stations_under_group.grp_id = groups.grp_id) AS locations_for_group, groups.grp_has_public, groups.grp_is_premium, (select COUNT( * ) from usr_login where usr_login.usr_for_grp = groups.grp_id) AS total_no_of_users_for_grp FROM groups";
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$groups = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['grp_id'];
		$temp_location_array['name'] = $result['grp_name'];
		$temp_location_array['subdomain'] = $result['grp_subdomain'];
		$temp_location_array['lat'] = $result['grp_lat'];
		$temp_location_array['long'] = $result['grp_long'];
		$temp_location_array['user_count'] = $result['total_no_of_users_for_grp'];
		$temp_location_array['locations_count'] = $result['locations_for_group'];
		$temp_location_array['has_public'] = ($result['grp_has_public'] == 1 ? 'public' : 'no');

		$groups []= $temp_location_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response['groups'] = $groups;
} else {
	$api_utilities->db_error();
	// $json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(229, $access_time, 'update', $user_id, $log_id);