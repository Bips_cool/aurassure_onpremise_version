<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_unassaigned_stations_for_cities.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(238, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id=0";
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$stations = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['dvcloc_id'];
		$temp_location_array['name'] = $result['dvcloc_name'];
		$temp_location_array['city_id'] = $result['ct_id'];
		$temp_location_array['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';

		$stations []= $temp_location_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response['stations'] = $stations;
} else {
	$api_utilities->db_error();
	// $json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(238, $access_time, 'update', $user_id, $log_id);