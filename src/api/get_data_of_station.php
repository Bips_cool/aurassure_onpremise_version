<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_data_of_station.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    station_id: 1
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(26, $access_time, 'insert', $user_id);
function json_validator($data=NULL) {
	if (!empty($data)) {
		json_decode($data);
		return (json_last_error() === JSON_ERROR_NONE);
	}
	return false;
}
use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$station_id = $received_data['station_id'];

$sql = "SELECT `usr_id`, `usr_access` FROM `usr_login` WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui'];
$result_set = $aurassure_db->query($sql);
$result = mysqli_fetch_assoc($result_set);
if ($result['usr_access'] != "*") {
	if (json_validator($result['usr_access'])) {
		$user_access = json_decode($result['usr_access'], true);
	} else {
		$user_access = [];
	}
}

if(isset($station_id) && $station_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$station_id = mysqli_real_escape_string($aurassure_db->connection, $station_id);

	if($api_utilities->session_handler->user_session_data['ci'] == 5) {
		//send all devices for admin portal
		
		if ($result['usr_access'] == "*") {
			$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id;
		} else if (in_array($station_id, $user_access)) {
			$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id;
		} else {
			$api_utilities->throw_error('Sorry, you have not access to the requested station.');
		}
	} else {
		if ($api_utilities->session_handler->user_session_data['ci'] == 22) {
			if ($station_id == 231) {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=179 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=231";
			} else if ($station_id == 232) {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=153 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=232";
			} else {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 16 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 19 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 20 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 22";
			}
		} else {
			if ($result['usr_access'] == "*") {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
			} else if (in_array($station_id, $user_access)) {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
			} else {
				$api_utilities->throw_error('Sorry, you have not access to the requested station.');
			}
		}
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, the requested station wasn\'t found.');
	}

	if ($result_set->num_rows > 1) {
		if ($station_id == 231) {
			$station_data = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_name' => $result['dvcloc_name'],
					'dvcloc_city' => $result['dvcloc_city'],
					'loc_type' => $result['dvcloc_type'],
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}
			$temp_station_name = '';
			$connection_status = '';
			$aqi_val = '';
			$responsible_param = '';
			if($station_data['231']['dvcloc_name']) {
				$temp_station_name .= $station_data['231']['dvcloc_name'].', ';
			}
			if($station_data['231']['dvcloc_city']) {
				$temp_station_name .= $station_data['231']['dvcloc_city'];
			}
			if (intval($station_data['231']['dvcloc_last_data_update_time']) > intval($station_data['179']['dvcloc_last_data_update_time'])) {
				$connection_status = $station_data['231']['dvcloc_last_data_update_time'];
			} else {
				$connection_status = $station_data['179']['dvcloc_last_data_update_time'];
			}
			if (intval($station_data['231']['aqi']) > intval($station_data['179']['aqi'])) {
				$aqi_val = $station_data['231']['aqi'];
				$responsible_param = $station_data['231']['dvcloc_responsible_param'];
			} else {
				$aqi_val = $station_data['179']['aqi'];
				// $json_response['station_data']=$station_data['179']['dvcloc_responsible_param'];
				$responsible_param = $station_data['179']['dvcloc_responsible_param'];
				// $json_response['station_data1']=$responsible_param;
			}
			$json_response['id'] = 231;
			$temp_station_name = rtrim($temp_station_name, ', ');
			$json_response['loc_type'] = $station_data['231']['loc_type'];
			$json_response['name'] = $temp_station_name;
			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['179']['temp'];
			$json_response['humid'] = $station_data['179']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);
		} else if ($station_id == 232) {
			$station_data = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_name' => $result['dvcloc_name'],
					'dvcloc_city' => $result['dvcloc_city'],
					'loc_type' => $result['dvcloc_type'],
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}
			$temp_station_name = '';
			$connection_status = '';
			$aqi_val = '';
			$responsible_param = '';
			if($station_data['232']['dvcloc_name']) {
				$temp_station_name .= $station_data['232']['dvcloc_name'].', ';
			}
			if($station_data['232']['dvcloc_city']) {
				$temp_station_name .= $station_data['232']['dvcloc_city'];
			}
			if (intval($station_data['232']['dvcloc_last_data_update_time']) > intval($station_data['153']['dvcloc_last_data_update_time'])) {
				$connection_status = $station_data['232']['dvcloc_last_data_update_time'];
			} else {
				$connection_status = $station_data['153']['dvcloc_last_data_update_time'];
			}
			if (intval($station_data['232']['aqi']) > intval($station_data['153']['aqi'])) {
				$aqi_val = $station_data['232']['aqi'];
				$responsible_param = $station_data['232']['dvcloc_responsible_param'];
			} else {
				$aqi_val = $station_data['153']['aqi'];
				// $json_response['station_data']=$station_data['153']['dvcloc_responsible_param'];
				$responsible_param = $station_data['153']['dvcloc_responsible_param'];
				// $json_response['station_data1']=$responsible_param;
			}
			$json_response['id'] = 232;
			$temp_station_name = rtrim($temp_station_name, ', ');
			$json_response['loc_type'] = $station_data['232']['loc_type'];
			$json_response['name'] = $temp_station_name;
			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['153']['temp'];
			$json_response['humid'] = $station_data['153']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);
		}
	} else {
		$result = mysqli_fetch_assoc($result_set);
		$json_response['id'] = $result['dvcloc_id'];
		$temp_station_name = '';
		if($result['dvcloc_name']) {
			$temp_station_name .= $result['dvcloc_name'].', ';
		}
		if($result['dvcloc_city']) {
			$temp_station_name .= $result['dvcloc_city'];
		}
		$temp_station_name = rtrim($temp_station_name, ', ');
		$json_response['loc_type'] = $result['dvcloc_type'];
		$json_response['name'] = $temp_station_name;
		$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
		$json_response['last_data_update_time'] = $result['dvcloc_last_data_update_time'];
		$json_response['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
		$json_response['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);
		$json_response['aqi_status'] = EndUserResult::getAQIStatus($result['dvcloc_aqi']);
		$json_response['temp'] = $result['dvcloc_temp'];
		$json_response['humid'] = $result['dvcloc_humid'];
		$json_response['suggestions'] = EndUserResult::getSuggestions($result['dvcloc_aqi'], $result['dvcloc_responsible_param']);
	}


	if ($station_id == 231) {
		$sql = "SELECT dvcloc_id, dhad_aqi, dhad_responsible_param, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=231 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." UNION SELECT dvcloc_id, dhad_aqi, dhad_responsible_param, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=179 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME'];

		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		$multi_stations_hourly_aqis = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$multi_stations_hourly_aqis []= [
				'dhad_upto_time' => $result['dhad_upto_time'],
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param']
			];
		}

		for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
			for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
				if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
					if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$i]['aqi'],
							'param' => $multi_stations_hourly_aqis[$i]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
					} else {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$j]['aqi'],
							'param' => $multi_stations_hourly_aqis[$j]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
					}
					
				}
			}
		}

		$json_response['hourly_aqis'] = [];
		$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
		for ($i=0; $i<24; $i++) {
			if($station_hourly_aqis[$latest_hour]['aqi']) {
				$json_response['hourly_aqis'] []= [
					'aqi' => $station_hourly_aqis[$latest_hour]['aqi'],
					'param' => $station_hourly_aqis[$latest_hour]['param']
				];
			} else {
				$json_response['hourly_aqis'] []= [
					'aqi' => null,
					'param' => null
				];
			}

			$latest_hour -= 3600;
		}
	} else if ($station_id == 232) {
		$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=232 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=153 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME'];

		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		$multi_stations_hourly_aqis = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$multi_stations_hourly_aqis []= [
				'dhad_upto_time' => $result['dhad_upto_time'],
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param']
			];
		}

		for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
			for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
				if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
					if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$i]['aqi'],
							'param' => $multi_stations_hourly_aqis[$i]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
					} else {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$j]['aqi'],
							'param' => $multi_stations_hourly_aqis[$j]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
					}
					
				}
			}
		}

		$json_response['hourly_aqis'] = [];
		$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
		for ($i=0; $i<24; $i++) {
			if($station_hourly_aqis[$latest_hour]['aqi']) {
				$json_response['hourly_aqis'] []= [
					'aqi' => $station_hourly_aqis[$latest_hour]['aqi'],
					'param' => $station_hourly_aqis[$latest_hour]['param']
				];
			} else {
				$json_response['hourly_aqis'] []= [
					'aqi' => null,
					'param' => null
				];
			}

			$latest_hour -= 3600;
		}

	} else {
		$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=$station_id AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME'];
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$station_hourly_aqis[$result['dhad_upto_time']] = [
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param']
			];
			$json_response['responsible_param'] = $result['dhad_responsible_param'];
		}

		$json_response['hourly_aqis'] = [];
		$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
		for ($i=0; $i<24; $i++) {
			if($station_hourly_aqis[$latest_hour]['aqi']) {
				$json_response['hourly_aqis'] []= [
					'aqi' => $station_hourly_aqis[$latest_hour]['aqi'],
					'param' => $station_hourly_aqis[$latest_hour]['param']
				];
			} else {
				$json_response['hourly_aqis'] []= [
					'aqi' => null,
					'param' => null
				];
			}

			$latest_hour -= 3600;
		}
	}

	//get params and param units of station
	$sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=".$station_id." ORDER BY dvcdat_time DESC LIMIT 1";
	$station_param_keys = [];
	if ($station_id == 220) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=168 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				if ($param_key == 'lint' || $param_key == 'uv') {
					$station_param_keys []= $param_key;
				}
			}
		}
	} else if ($station_id == 221) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=162 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				if ($param_key == 'lint' || $param_key == 'uv') {
					$station_param_keys []= $param_key;
				}
			}
		}
	} else if ($station_id == 231) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=179 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				$station_param_keys []= $param_key;
			}
		}
	} else if ($station_id == 232) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=153 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				$station_param_keys []= $param_key;
			}
		}
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, no data has been received from the station yet.');
	}
	$result = $result_set->fetch_assoc();
	$station_latest_data = json_decode($result['dvcdat_data'], true);
	if ($station_id == 220 || $station_id == 221) {
		foreach ($station_latest_data as $param_key => $param_value) {
			if ($param_key != 'rain') {
				$station_param_keys []= $param_key;
			}
		}
	} else {
		foreach ($station_latest_data as $param_key => $param_value) {
			$station_param_keys []= $param_key;
		}
	}
	
	$sql = "SELECT dvcprm_name, dvcprm_name_noformat, dvcprm_key, dvcprm_unit FROM dvc_params WHERE dvcprm_key IN ('".implode('\',\'', $station_param_keys)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_params = [];
	$json_response['params'] = [];
	$json_response['param_units'] = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$station_params []= $result['dvcprm_key'];
		$json_response['params'] []= [
			'name' => $result['dvcprm_name'],
			'key' => $result['dvcprm_key']
		];
		$json_response['param_units'] []= $result['dvcprm_unit'];
	}

	$sql = "SELECT dvcdvc_last_raw_data, dvcdvc_last_data_receive_time FROM dvc_devices WHERE dvcloc_id=".$station_id;
	$custom_parameters = [];
	if ($station_id == 220) {
		$post_sql = "SELECT dvcdvc_last_raw_data FROM dvc_devices WHERE dvcloc_id=168";
		$result_set = $aurassure_db->query($post_sql);
		$result = $result_set->fetch_assoc();
		$post_dvcdvc_last_raw_data = json_decode($result['dvcdvc_last_raw_data'], true);
		foreach ($post_dvcdvc_last_raw_data as $k => $val) {
			if ($k == 'lint' || $k == 'uv') {
				$custom_parameters[$k] = strval(abs($val));
			}
		}
	} else if ($station_id == 221) {
		$post_sql = "SELECT dvcdvc_last_raw_data FROM dvc_devices WHERE dvcloc_id=162";
		$result_set = $aurassure_db->query($post_sql);
		$result = $result_set->fetch_assoc();
		$post_dvcdvc_last_raw_data = json_decode($result['dvcdvc_last_raw_data'], true);
		foreach ($post_dvcdvc_last_raw_data as $key => $value) {
			if ($key == 'lint' || $key == 'uv') {
				$custom_parameters[$key] = strval(abs($value));
			}
		}
	} else if ($station_id == 231) {
		$post_sql = "SELECT dvcdvc_last_raw_data FROM dvc_devices WHERE dvcloc_id=179";
		$result_set = $aurassure_db->query($post_sql);
		$result = $result_set->fetch_assoc();
		$post_dvcdvc_last_raw_data = json_decode($result['dvcdvc_last_raw_data'], true);
		foreach ($post_dvcdvc_last_raw_data as $param_name => $param_value) {
			$post_dvcdvc_last_raw_data[$param_name] = strval(abs($param_value));
		}
	} else if ($station_id == 232) {
		$post_sql = "SELECT dvcdvc_last_raw_data FROM dvc_devices WHERE dvcloc_id=153";
		$result_set = $aurassure_db->query($post_sql);
		$result = $result_set->fetch_assoc();
		$post_dvcdvc_last_raw_data = json_decode($result['dvcdvc_last_raw_data'], true);
		foreach ($post_dvcdvc_last_raw_data as $param_name => $param_value) {
			$post_dvcdvc_last_raw_data[$param_name] = strval(abs($param_value));
		}
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$result = $result_set->fetch_assoc();
	$dvcdvc_last_raw_data = json_decode($result['dvcdvc_last_raw_data'], true);
	foreach ($dvcdvc_last_raw_data as $param_name => $param_value) {
		$dvcdvc_last_raw_data[$param_name] = strval(abs($param_value));
	}
	if ($station_id == 220 || $station_id == 221) {
		$json_response['latest_param_value'] = array_merge($dvcdvc_last_raw_data, $custom_parameters);
	} else if ($station_id == 231 || $station_id == 232) {
		$json_response['latest_param_value'] = array_merge($dvcdvc_last_raw_data, $post_dvcdvc_last_raw_data );
	} else {
		$json_response['latest_param_value'] = $dvcdvc_last_raw_data;
	}
	$json_response['last_data_receive_time'] = $result['dvcdvc_last_data_receive_time'];
	$json_response['data_receive_connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcdvc_last_data_receive_time']) <= 7200 ? 'online' : 'offline';
	$json_response['station_param_keys'] = $custom_parameters;
	
	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(26, $access_time, 'update', $user_id, $log_id);