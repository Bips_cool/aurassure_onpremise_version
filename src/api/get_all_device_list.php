<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_device_list.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include'
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(19, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("_includes/form_data_validation_functions.php");
	
	// Get Device List
	$sql = "SELECT `dvcloc_id`,`dvcloc_name`,`ct_id` FROM `dvc_locations` WHERE `dvcloc_id` IN (SELECT `dvcloc_id` FROM `dvc_devices`) ORDER BY `dvcloc_name` ASC";
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		// $json_response['sql'] = $sql;
		$json_response['devices'] = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$json_response['devices'] [] = [
				'id' => $result['dvcloc_id'],
				'name' => $result['dvcloc_name'],
				'ct_id' => $result['ct_id']
			];
		}
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to load data!');
	}

	// Get Device Cities
	$sql = "SELECT `ct_id`,`ct_name` FROM `cities` ORDER BY `ct_name` ASC";
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		// $json_response['sql'] = $sql;
		$json_response['cities'] = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$json_response['cities'] [] = [
				'id' => $result['ct_id'],
				'name' => $result['ct_name']
			];
		}
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to load data!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(19, $access_time, 'update', $user_id, $log_id);