<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_detailed_analytics_of_station.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    station_id: 1,
    time_interval: [1510120000, 1510125000]
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");
require_once(getcwd().'/_generate_raw_data_json.php');

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(27, $access_time, 'insert', $user_id);
function json_validator($data=NULL) {
	if (!empty($data)) {
		json_decode($data);
		return (json_last_error() === JSON_ERROR_NONE);
	}
	return false;
}
use Aurassure\DataProcessor\EndUserResult;
use Aurassure\DataProcessor\Tools;

$sql = "SELECT `usr_id`, `usr_access` FROM `usr_login` WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui'];
$result_set = $aurassure_db->query($sql);
$result = mysqli_fetch_assoc($result_set);
if ($result['usr_access'] != "*") {
	if (json_validator($result['usr_access'])) {
		$user_access = json_decode($result['usr_access'], true);
	} else {
		$user_access = [];
	}
}

$received_data = json_decode(file_get_contents('php://input'), true);
$station_id = $received_data['station_id'];
$time_interval = $received_data['time_interval'];

if(isset($station_id) && $station_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$station_id = mysqli_real_escape_string($aurassure_db->connection, $station_id);
	$from_time = $time_interval[0];
	$upto_time = $time_interval[1];

	if($api_utilities->session_handler->user_session_data['ci'] == 5) {
		//send all devices for admin portal
		
		if ($result['usr_access'] == "*") {
			$sql = "SELECT dvcloc_name, dvcloc_city, dvcloc_params, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id;
		} else if (in_array($station_id, $user_access)) {
			$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id;
		} else {
			$api_utilities->throw_error('Sorry, you have not access to the requested station.');
		}
		
	} else {
		if ($api_utilities->session_handler->user_session_data['ci'] == 22) {
			if ($station_id == 231) {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=179 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=231";
			} else if ($station_id == 232) {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=153 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=232";
			} else {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 16 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 19 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 20 UNION SELECT dvcloc_id, dvcloc_name, dvcloc_params, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id= 22";
			}
		} else {
			if ($result['usr_access'] == "*") {
				$sql = "SELECT dvcloc_name, dvcloc_city, dvcloc_params, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
			} else if (in_array($station_id, $user_access)) {
				$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
			} else {
				$api_utilities->throw_error('Sorry, you have not access to the requested station.');
			}
			
		}
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, the requested station wasn\'t found.');
	}

	if ($result_set->num_rows > 1) {
		if ($station_id == 231) {
			$station_data = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_name' => $result['dvcloc_name'],
					'dvcloc_city' => $result['dvcloc_city'],
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}
			// $json_response['station_data12']=$station_data;
			$temp_station_name = '';
			$connection_status = '';
			$aqi_val = '';
			$responsible_param = '';
			if($station_data['231']['dvcloc_name']) {
				$temp_station_name .= $station_data['231']['dvcloc_name'].', ';
			}
			if($station_data['231']['dvcloc_city']) {
				$temp_station_name .= $station_data['231']['dvcloc_city'];
			}
			if (intval($station_data['231']['dvcloc_last_data_update_time']) > intval($station_data['179']['dvcloc_last_data_update_time'])) {
				$connection_status = $station_data['231']['dvcloc_last_data_update_time'];
			} else {
				$connection_status = $station_data['179']['dvcloc_last_data_update_time'];
			}
			if (intval($station_data['231']['aqi']) > intval($station_data['179']['aqi'])) {
				$aqi_val = $station_data['231']['aqi'];
				$responsible_param = $station_data['231']['dvcloc_responsible_param'];
			} else {
				$aqi_val = $station_data['179']['aqi'];
				// $json_response['station_data2']=$station_data['179']['dvcloc_responsible_param'];
				$responsible_param = $station_data['179']['dvcloc_responsible_param'];
				// $json_response['station_data1']=$responsible_param;
			}
			$json_response['id'] = 231;
			$temp_station_name = rtrim($temp_station_name, ', ');
			$json_response['name'] = $temp_station_name;
			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['179']['temp'];
			$json_response['humid'] = $station_data['179']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);
		} else if ($station_id == 232) {
			$station_data = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_name' => $result['dvcloc_name'],
					'dvcloc_city' => $result['dvcloc_city'],
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}
			$temp_station_name = '';
			$connection_status = '';
			$aqi_val = '';
			$responsible_param = '';
			if($station_data['232']['dvcloc_name']) {
				$temp_station_name .= $station_data['232']['dvcloc_name'].', ';
			}
			if($station_data['232']['dvcloc_city']) {
				$temp_station_name .= $station_data['232']['dvcloc_city'];
			}
			if (intval($station_data['232']['dvcloc_last_data_update_time']) > intval($station_data['153']['dvcloc_last_data_update_time'])) {
				$connection_status = $station_data['232']['dvcloc_last_data_update_time'];
			} else {
				$connection_status = $station_data['153']['dvcloc_last_data_update_time'];
			}
			if (intval($station_data['232']['aqi']) > intval($station_data['153']['aqi'])) {
				$aqi_val = $station_data['232']['aqi'];
				$responsible_param = $station_data['232']['dvcloc_responsible_param'];
			} else {
				$aqi_val = $station_data['153']['aqi'];
				// $json_response['station_data']=$station_data['153']['dvcloc_responsible_param'];
				$responsible_param = $station_data['153']['dvcloc_responsible_param'];
				// $json_response['station_data1']=$responsible_param;
			}
			$json_response['id'] = 232;
			$temp_station_name = rtrim($temp_station_name, ', ');
			$json_response['name'] = $temp_station_name;
			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['153']['temp'];
			$json_response['humid'] = $station_data['153']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);
		}
	} else {
		$result = mysqli_fetch_assoc($result_set);
		$temp_station_name = '';
		if($result['dvcloc_name']) {
			$temp_station_name .= $result['dvcloc_name'].', ';
		}
		if($result['dvcloc_city']) {
			$temp_station_name .= $result['dvcloc_city'];
		}
		$temp_station_name = rtrim($temp_station_name, ', ');
		$json_response['name'] = $temp_station_name;
		$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
		$json_response['last_data_update_time'] = $result['dvcloc_last_data_update_time'];
		$json_response['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
		$json_response['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);
		$json_response['aqi_status'] = EndUserResult::getAQIStatus($result['dvcloc_aqi']);
		$json_response['temp'] = $result['dvcloc_temp'];
		$json_response['humid'] = $result['dvcloc_humid'];
		$json_response['suggestions'] = EndUserResult::getSuggestions($result['dvcloc_aqi'], $result['dvcloc_responsible_param']);
		// $station_param_ids = json_decode($result['dvcloc_params'], true);
	}

	//get params and param units of station
	$sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=".$station_id." ORDER BY dvcdat_time DESC LIMIT 1";
	$station_param_keys = [];
	$station_temp_param_keys = [];
	if ($station_id == 220) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=168 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				if ($param_key == 'lint' || $param_key == 'uv') {
					$station_param_keys []= $param_key;
				}
			}
		}
	} else if ($station_id == 221) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=162 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				if ($param_key == 'lint' || $param_key == 'uv') {
					$station_param_keys []= $param_key;
				} 
			}
		}
	} else if ($station_id == 231) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=179 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				$station_param_keys []= $param_key;
				$station_temp_param_keys []= $param_key;
			}
		}
	} else if ($station_id == 232) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=153 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				$station_param_keys []= $param_key;
				$station_temp_param_keys []= $param_key; 
			}
		}
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, no data has been received from the station yet.');
	}
	$result = $result_set->fetch_assoc();
	$station_latest_data = json_decode($result['dvcdat_data'], true);
	if ($station_id == 220 || $station_id == 221) {
		foreach ($station_latest_data as $param_key => $param_value) {
			if ($param_key != 'rain') {
				$station_param_keys []= $param_key;
				$station_temp_param_keys []= $param_key;
			}
		}
	} else {
		foreach ($station_latest_data as $param_key => $param_value) {
			$station_param_keys []= $param_key;
		}
	}
	if (in_array('debug', $station_param_keys)) {
		$station_param_keys = array_diff($station_param_keys, ["debug"]);
	}

	$json_response['station_param_keys'] = $station_param_keys;

	$sql = "SELECT dvcprm_name, dvcprm_name_noformat, dvcprm_key, dvcprm_unit FROM dvc_params WHERE dvcprm_key IN ('".implode('\',\'', $station_param_keys)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_params = [];
	$json_response['params'] = [];
	$json_response['param_units'] = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$station_params []= $result['dvcprm_key'];
		$json_response['params'] []= [
			'name' => $result['dvcprm_name'],
			'key' => $result['dvcprm_key']
		];
		$json_response['param_units'] []= $result['dvcprm_unit'];
	}

	if($from_time && $upto_time && $from_time != 0 && $upto_time != 0) {
		$query_latest_data_table = true;
	} else {
		$query_latest_data_table = false;
	}

	// $json_response['station_params'] = $station_params;
	$json_response['param_key_unit'] = $from_time;

	//get raw data of station for last 24 hours
	$station_params_temp = ['uv', 'lint'];
	$station_params_temp_dust = ['pm1', 'pm2.5', 'pm10'];
	if ($query_latest_data_table) {
		if ($station_id == 220) {
			$required_data_station_one = get_raw_data_json(220, $from_time, $upto_time, $station_params, false);
			$required_data_station_two = get_raw_data_json(168, $from_time, $upto_time, $station_params_temp, false);
			$required_data_view_new_params = [];
			$required_data_view_existing_params = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_existing_params[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_params_temp as $param) {
					$required_data_view_new_params[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$existing_params_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$existing_params_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_existing_params[$index]
					];
				}
			}
			
			$new_param_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$new_param_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_new_params[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($existing_params_station_value_with_timestamp, $new_param_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}

			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else if ($station_id == 221) {
			$required_data_station_one = get_raw_data_json(221, $from_time, $upto_time, $station_params, false);
			$required_data_station_two = get_raw_data_json(162, $from_time, $upto_time, $station_params_temp, false);
			$required_data_view_new_params = [];
			$required_data_view_existing_params = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_existing_params[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_params_temp as $param) {
					$required_data_view_new_params[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$existing_params_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$existing_params_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_existing_params[$index]
					];
				}
			}
			
			$new_param_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$new_param_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_new_params[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($existing_params_station_value_with_timestamp, $new_param_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}

			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else if ($station_id == 231) {
			$required_data_station_one = get_raw_data_json(231, $from_time, $upto_time, $station_params_temp_dust, false);
			$required_data_station_two = get_raw_data_json(179, $from_time, $upto_time, $station_params, false);

			$required_data_view_dust = [];
			$required_data_view_other = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_params_temp_dust as $param) {
					$required_data_view_dust[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_other[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$dust_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$dust_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_dust[$index]
					];
				}
			}
			
			$other_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$other_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_other[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($dust_station_value_with_timestamp, $other_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}

			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else if ($station_id == 232) {
			$required_data_station_one = get_raw_data_json(232, $from_time, $upto_time, $station_params_temp_dust, false);
			$required_data_station_two = get_raw_data_json(153, $from_time, $upto_time, $station_params, false);

			$required_data_view_dust = [];
			$required_data_view_other = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_params_temp_dust as $param) {
					$required_data_view_dust[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_other[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$dust_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$dust_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_dust[$index]
					];
				}
			}
			
			$other_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$other_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_other[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($dust_station_value_with_timestamp, $other_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}

			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else {
			$required_data = get_raw_data_json($station_id, $from_time, $upto_time, $station_params, false);
		}
	} else {
		if ($station_id == 220) {
			$required_data_station_one = get_raw_data_json(220, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'],$station_params, false);
			$required_data_station_two = get_raw_data_json(168, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params_temp, false);
			$required_data_view_new_params = [];
			$required_data_view_existing_params = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_existing_params[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_params_temp as $param) {
					$required_data_view_new_params[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$existing_params_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$existing_params_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_existing_params[$index]
					];
				}
			}
			
			$new_param_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$new_param_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_new_params[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($existing_params_station_value_with_timestamp, $new_param_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}

			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else if ($station_id == 221) {
			$required_data_station_one = get_raw_data_json(221, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params, false);
			$required_data_station_two = get_raw_data_json(162, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params_temp, false);
			$required_data_view_new_params = [];
			$required_data_view_existing_params = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_existing_params[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_params_temp as $param) {
					$required_data_view_new_params[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$existing_params_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$existing_params_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_existing_params[$index]
					];
				}
			}
			
			$new_param_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$new_param_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_new_params[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($existing_params_station_value_with_timestamp, $new_param_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}

			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else if ($station_id == 231) {
			$required_data_station_one = get_raw_data_json(231, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params_temp_dust, false);
			$required_data_station_two = get_raw_data_json(179, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params, false);

			$required_data_view_dust = [];
			$required_data_view_other = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_params_temp_dust as $param) {
					$required_data_view_dust[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_other[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$dust_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$dust_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_dust[$index]
					];
				}
			}
			
			$other_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$other_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_other[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($dust_station_value_with_timestamp, $other_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}

			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else if ($station_id == 232) {
			$required_data_station_one = get_raw_data_json(232, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params_temp_dust, false);
			$required_data_station_two = get_raw_data_json(153, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params, false);

			$required_data_view_dust = [];
			$required_data_view_other = [];
			for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
				foreach ($station_params_temp_dust as $param) {
					$required_data_view_dust[$i][$param] = $required_data_station_one['param_values'][$param][$i];
				}
			}
			for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
				foreach ($station_temp_param_keys as $param) {
					$required_data_view_other[$i][$param] = $required_data_station_two['param_values'][$param][$i];
				}
			}

			$dust_station_value_with_timestamp = [];
			foreach ($required_data_station_one['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$dust_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_one['time_stamps'][$index],
						'value' => $required_data_view_dust[$index]
					];
				}
			}
			
			$other_station_value_with_timestamp = [];
			foreach ($required_data_station_two['param_values'] as $key => $value) {
				foreach ($value as $index => $val) {
					$other_station_value_with_timestamp[$index]= [
						'time' => $required_data_station_two['time_stamps'][$index],
						'value' => $required_data_view_other[$index]
					];
				}
			}

			// receive merged data from function
			$all_station_length_data = Tools::getMergedDataOfStations($dust_station_value_with_timestamp, $other_station_value_with_timestamp);

			$all_parameter_data_packet = [];
			$all_parameter_timestamp_view = [];
			$all_parameter_data_view = [];
			foreach ($all_station_length_data as $key => $value) {
				$all_parameter_data_packet []= $value;
				$all_parameter_timestamp_view []= $key;
			}
			for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
				foreach ($all_parameter_data_packet[$i] as $key => $value) {
					$all_parameter_data_view[$key] []= $value;
				}
			}
			// $json_response['test_param_value'] = $required_data_station_two['param_values'];
			$required_data['param_values'] = $all_parameter_data_view;
			$required_data['time_stamps'] = $all_parameter_timestamp_view;
		} else {
			$required_data = get_raw_data_json($station_id, $_SERVER['REQUEST_TIME']-86400, $_SERVER['REQUEST_TIME'], $station_params, false);
		}
	}
	$json_response['param_values'] = $required_data['param_values'];
	$json_response['time_stamps'] = $required_data['time_stamps'];
	// $json_response['demo'] = $required_data['demo'];
	// $json_response['param'] = $required_data['param'];

	//get hourly param conc.s and indices of station
	
	if ($station_id == 220) {
		if ($query_latest_data_table) {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=220 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=168 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." ORDER BY dhad_upto_time DESC";
		} else {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=220 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=168 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." ORDER BY dhad_upto_time DESC";
		}
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		$station_param_hourly_concs = [];
		$station_param_hourly_aqis = [];
		foreach ($station_params as $param) {
			$station_param_hourly_concs[$param] = [];
			$station_param_hourly_aqis[$param] = [];
		}
		$multi_stations_hourly_aqis = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$multi_stations_hourly_aqis []= [
				'dhad_upto_time' => $result['dhad_upto_time'],
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param'],
				'param_concs' => json_decode($result['dhad_param_concs'], true),
				'param_aqis' => json_decode($result['dhad_param_aqis'], true)
			];
		}

		// $json_response['test'] = $multi_stations_hourly_aqis;

		for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
			for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
				if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
					if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$i]['aqi'],
							'param' => $multi_stations_hourly_aqis[$i]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
					} else {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$j]['aqi'],
							'param' => $multi_stations_hourly_aqis[$j]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
					}

					$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$j]['param_concs'], $multi_stations_hourly_aqis[$i]['param_concs']);
					$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$j]['param_aqis'], $multi_stations_hourly_aqis[$i]['param_aqis']);
					foreach ($station_params as $param) {
						if($temp_station_param_hourly_concs[$param]) {
							$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
						}
						if($temp_station_param_hourly_aqis[$param]) {
							$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
						}
					}
					// $json_response['test2'] []= $station_param_hourly_concs;
					// $json_response['test3'] []= $station_param_hourly_aqis;
				}
			}
		}
	} else if ($station_id == 221) {
		if ($query_latest_data_table) {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=221 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=162 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." ORDER BY dhad_upto_time DESC";
		} else {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=221 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=162 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." ORDER BY dhad_upto_time DESC";
		}
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		$station_param_hourly_concs = [];
		$station_param_hourly_aqis = [];
		foreach ($station_params as $param) {
			$station_param_hourly_concs[$param] = [];
			$station_param_hourly_aqis[$param] = [];
		}
		$multi_stations_hourly_aqis = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$multi_stations_hourly_aqis []= [
				'dhad_upto_time' => $result['dhad_upto_time'],
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param'],
				'param_concs' => json_decode($result['dhad_param_concs'], true),
				'param_aqis' => json_decode($result['dhad_param_aqis'], true)
			];
		}

		// $json_response['test'] = $multi_stations_hourly_aqis;

		for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
			for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
				if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
					if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$i]['aqi'],
							'param' => $multi_stations_hourly_aqis[$i]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
					} else {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$j]['aqi'],
							'param' => $multi_stations_hourly_aqis[$j]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
					}

					$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$j]['param_concs'], $multi_stations_hourly_aqis[$i]['param_concs']);
					$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$j]['param_aqis'], $multi_stations_hourly_aqis[$i]['param_aqis']);
					foreach ($station_params as $param) {
						if($temp_station_param_hourly_concs[$param]) {
							$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
						}
						if($temp_station_param_hourly_aqis[$param]) {
							$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
						}
					}
					// $json_response['test2'] []= $station_param_hourly_concs;
					// $json_response['test3'] []= $station_param_hourly_aqis;
				}
			}
		}
	} else if ($station_id == 231) {
		if ($query_latest_data_table) {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=231 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=179 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." ORDER BY dhad_upto_time DESC";
		} else {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=231 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=179 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." ORDER BY dhad_upto_time DESC";
		}
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		$station_param_hourly_concs = [];
		$station_param_hourly_aqis = [];
		foreach ($station_params as $param) {
			$station_param_hourly_concs[$param] = [];
			$station_param_hourly_aqis[$param] = [];
		}
		$multi_stations_hourly_aqis = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$multi_stations_hourly_aqis []= [
				'dhad_upto_time' => $result['dhad_upto_time'],
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param'],
				'param_concs' => json_decode($result['dhad_param_concs'], true),
				'param_aqis' => json_decode($result['dhad_param_aqis'], true)
			];
		}

		// $json_response['test'] = $multi_stations_hourly_aqis;

		for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
			for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
				if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
					if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$i]['aqi'],
							'param' => $multi_stations_hourly_aqis[$i]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
					} else {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$j]['aqi'],
							'param' => $multi_stations_hourly_aqis[$j]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
					}

					$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$j]['param_concs'], $multi_stations_hourly_aqis[$i]['param_concs']);
					$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$j]['param_aqis'], $multi_stations_hourly_aqis[$i]['param_aqis']);
					foreach ($station_params as $param) {
						if($temp_station_param_hourly_concs[$param]) {
							$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
						}
						if($temp_station_param_hourly_aqis[$param]) {
							$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
						}
					}
					// $json_response['test2'] []= $station_param_hourly_concs;
					// $json_response['test3'] []= $station_param_hourly_aqis;
				}
			}
		}
	} else if ($station_id == 232) {
		if ($query_latest_data_table) {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=232 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=153 AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." ORDER BY dhad_upto_time DESC";
		} else {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=232 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." UNION SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=153 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." ORDER BY dhad_upto_time DESC";

		}
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		$station_param_hourly_concs = [];
		$station_param_hourly_aqis = [];
		foreach ($station_params as $param) {
			$station_param_hourly_concs[$param] = [];
			$station_param_hourly_aqis[$param] = [];
		}
		$multi_stations_hourly_aqis = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$multi_stations_hourly_aqis []= [
				'dhad_upto_time' => $result['dhad_upto_time'],
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param'],
				'param_concs' => json_decode($result['dhad_param_concs'], true),
				'param_aqis' => json_decode($result['dhad_param_aqis'], true)
			];
		}

		// $json_response['test'] = $multi_stations_hourly_aqis;

		for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
			for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
				if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
					if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$i]['aqi'],
							'param' => $multi_stations_hourly_aqis[$i]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
					} else {
						$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
							'aqi' => $multi_stations_hourly_aqis[$j]['aqi'],
							'param' => $multi_stations_hourly_aqis[$j]['param']
						];
						// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
					}

					$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$i]['param_concs'], $multi_stations_hourly_aqis[$j]['param_concs']);
					$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$i]['param_aqis'], $multi_stations_hourly_aqis[$j]['param_aqis']);
					foreach ($station_params as $param) {
						if($temp_station_param_hourly_concs[$param]) {
							$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
						}
						if($temp_station_param_hourly_aqis[$param]) {
							$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
						}
					}
					// $json_response['test2'] []= $station_param_hourly_concs;
					// $json_response['test3'] []= $station_param_hourly_aqis;
				}
			}
		}
	} else {
		if ($query_latest_data_table) {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=$station_id AND dhad_upto_time>=".$from_time." AND dhad_upto_time<=".$upto_time." ORDER BY dhad_upto_time DESC";
		} else {
			$sql = "SELECT dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=$station_id AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." ORDER BY dhad_upto_time DESC";
		}
		// $json_response['sql'] = $sql;
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_hourly_aqis = [];
		$station_param_hourly_concs = [];
		$station_param_hourly_aqis = [];
		foreach ($station_params as $param) {
			$station_param_hourly_concs[$param] = [];
			$station_param_hourly_aqis[$param] = [];
		}
		while($result = mysqli_fetch_assoc($result_set)) {
			$station_hourly_aqis[$result['dhad_upto_time']] = [
				'aqi' => $result['dhad_aqi'],
				'param' => $result['dhad_responsible_param']
			];

			$temp_station_param_hourly_concs = json_decode($result['dhad_param_concs'], true);
			$temp_station_param_hourly_aqis = json_decode($result['dhad_param_aqis'], true);
			foreach ($station_params as $param) {
				if($temp_station_param_hourly_concs[$param]) {
					$station_param_hourly_concs[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
				}
				if($temp_station_param_hourly_aqis[$param]) {
					$station_param_hourly_aqis[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
				}
			}
		}
	}


	$json_response['hourly_aqis'] = [];
	$json_response['hourly_param_concs'] = [];
	$json_response['hourly_param_aqis'] = [];
	foreach ($station_params as $param) {
		$json_response['hourly_param_concs'][$param] = [];
		$json_response['hourly_param_aqis'][$param] = [];
	}

	// $json_response['check'] = $station_params;
	if ($query_latest_data_table) {
		while ($from_time <= $upto_time) {
			if($station_hourly_aqis[$latest_hour]['aqi']) {
				$json_response['hourly_aqis'] []= [
					'aqi' => $station_hourly_aqis[$latest_hour]['aqi'],
					'param' => $station_hourly_aqis[$latest_hour]['param']
				];
			} else {
				$json_response['hourly_aqis'] []= [
					'aqi' => null,
					'param' => null
				];
			}
			foreach ($station_params as $param) {
				if($station_param_hourly_concs[$param]) {
					$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$from_time];
				} else {
					$json_response['hourly_param_concs'][$param] []= null;
				}
				if($station_param_hourly_aqis[$param]) {
					$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$from_time];
				} else {
					$json_response['hourly_param_aqis'][$param] []= null;
				}
			}
			$from_time += 3600;
		}
	} else {
		$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
		for ($i=0; $i<24; $i++) {
			if($station_hourly_aqis[$latest_hour]['aqi']) {
				$json_response['hourly_aqis'] []= [
					'aqi' => $station_hourly_aqis[$latest_hour]['aqi'],
					'param' => $station_hourly_aqis[$latest_hour]['param']
				];
			} else {
				$json_response['hourly_aqis'] []= [
					'aqi' => null,
					'param' => null
				];
			}

			foreach ($station_params as $param) {
				if($station_param_hourly_concs[$param][$latest_hour]) {
					$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$latest_hour];
				} else {
					$json_response['hourly_param_concs'][$param] []= null;
				}
				if($station_param_hourly_aqis[$param][$latest_hour]) {
					$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$latest_hour];
				} else {
					$json_response['hourly_param_aqis'][$param] []= null;
				}
			}

			$latest_hour -= 3600;
		}
	}

	if ($station_id == 231) {
		$sql = "SELECT dvcdvc_last_raw_data, dvcdvc_last_data_receive_time FROM dvc_devices WHERE dvcloc_id=231 UNION SELECT dvcdvc_last_raw_data, dvcdvc_last_data_receive_time FROM dvc_devices WHERE dvcloc_id=179";
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_latest_param_data = [];
		while ($result = mysqli_fetch_assoc($result_set)) {
			$station_latest_param_data []= [
				'latest_param_value' => json_decode($result['dvcdvc_last_raw_data'], true),
				'last_data_receive_time' => $result['dvcdvc_last_data_receive_time']
			];
		}
		$json_response['station_latest_param_data'] = $station_latest_param_data;
		$json_response['latest_param_value'] = array_merge($station_latest_param_data[0]['latest_param_value'], $station_latest_param_data[1]['latest_param_value']);
		$json_response['last_data_receive_time'] = $station_latest_param_data[1]['last_data_receive_time'];
		$json_response['data_receive_connection_status'] = ($_SERVER['REQUEST_TIME'] - $station_latest_param_data[1]['last_data_receive_time']) <= 7200 ? 'online' : 'offline';
	} else if ($station_id == 232) {
		$sql = "SELECT dvcdvc_last_raw_data, dvcdvc_last_data_receive_time FROM dvc_devices WHERE dvcloc_id=232 UNION SELECT dvcdvc_last_raw_data, dvcdvc_last_data_receive_time FROM dvc_devices WHERE dvcloc_id=153";
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$station_latest_param_data = [];
		while ($result = mysqli_fetch_assoc($result_set)) {
			$station_latest_param_data []= [
				'latest_param_value' => json_decode($result['dvcdvc_last_raw_data'], true),
				'last_data_receive_time' => $result['dvcdvc_last_data_receive_time']
			];
		}

		$json_response['latest_param_value'] = array_merge($station_latest_param_data[0]['latest_param_value'], $station_latest_param_data[1]['latest_param_value']);
		$json_response['last_data_receive_time'] = $station_latest_param_data[1]['last_data_receive_time'];
		$json_response['data_receive_connection_status'] = ($_SERVER['REQUEST_TIME'] - $station_latest_param_data[1]['last_data_receive_time']) <= 7200 ? 'online' : 'offline';
	} else {
		$sql = "SELECT dvcdvc_last_raw_data, dvcdvc_last_data_receive_time FROM dvc_devices WHERE dvcloc_id=".$station_id;
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}
		$result = $result_set->fetch_assoc();
		$json_response['latest_param_value'] = json_decode($result['dvcdvc_last_raw_data'], true);
		$json_response['last_data_receive_time'] = $result['dvcdvc_last_data_receive_time'];
		$json_response['data_receive_connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcdvc_last_data_receive_time']) <= 7200 ? 'online' : 'offline';
	}


	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(27, $access_time, 'update', $user_id, $log_id);