<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/add_new_user.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		user_fname: document.getElementById('fname').value,
		user_mname: document.getElementById('mname').value,
		user_lname: document.getElementById('lname').value,
		designation: document.getElementById('designation').value,
		department: document.getElementById('department').value,
		user_email: document.getElementById('email').value,
		user_phone: document.getElementById('phone').value,
		user_pass: document.getElementById('password').value,
		user_role: document.getElementById('role').value
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(10, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("_includes/form_data_validation_functions.php");

	// Check the value of required fields are set or not
	$data = json_decode(file_get_contents('php://input'), true);
	if(!has_presence($data['user_fname'])) {
		$api_utilities->incomplete_request("Please give a valid First Name!");
	} else if(!has_presence($data['user_email'])) {
		$api_utilities->incomplete_request("Please give a valid Email ID!");
	} else if(!has_presence($data['user_phone'])) {
		$api_utilities->incomplete_request("Please give a valid Phone Number!");
	} else if(!has_presence($data['user_pass'])) {
		$api_utilities->incomplete_request("Please give a valid Password!");
	} else if(!has_presence($data['user_role'])) {
		$api_utilities->incomplete_request("Please give a valid User Role!");
	} 

	// Sanitize the fields
	$user_fname = mysqli_real_escape_string($aurassure_db->connection, $data['user_fname']);
	$user_mname = mysqli_real_escape_string($aurassure_db->connection, $data['user_mname']);
	$user_lname = mysqli_real_escape_string($aurassure_db->connection, $data['user_lname']);
	$designation = mysqli_real_escape_string($aurassure_db->connection, $data['designation']);
	$department = mysqli_real_escape_string($aurassure_db->connection, $data['department']);
	$user_email = mysqli_real_escape_string($aurassure_db->connection, $data['user_email']);
	$user_phone = mysqli_real_escape_string($aurassure_db->connection, $data['user_phone']);
	$user_role = mysqli_real_escape_string($aurassure_db->connection, $data['user_role']);
	// $user_type = mysqli_real_escape_string($aurassure_db->connection, $data['user_type']);
	$user_type = 1;
	$user_group = mysqli_real_escape_string($aurassure_db->connection, $data['group_id']);
	$user_pass = password_hash($data['user_pass'], PASSWORD_BCRYPT);
	$user_master_access = $data['master_access'];
	$user_selected_stations = $data['selected_stations'];
	$usr_access = $user_master_access ? '*' : json_encode($user_selected_stations);

	if(mysqli_num_rows($result_set)) {
		$api_utilities->throw_error("User with this Email/Phone is already registered!");
	} else {
		// Insert data of user
		$add_sql = "INSERT INTO `usr_login`(`usr_first_name`, `usr_middle_name`, `usr_last_name`, `usr_email`, `usr_mobile`, `usr_pass`, `usr_date_created`, `usr_type`, `usr_for_city`, `usr_for_grp`, `usr_role`, `usr_designation`, `usr_department`, `usr_under`, `usr_access`) VALUES ('".$user_fname."','".$user_mname."','".$user_lname."','".$user_email."','".$user_phone."','".$user_pass."',".$_SERVER['REQUEST_TIME'].",'".$user_type."','".$api_utilities->session_handler->user_session_data['ci']."','".$user_group."','".$user_role."','".$designation."','".$department."',".$api_utilities->session_handler->user_session_data['ui'].", '".$usr_access."')";
		$add_result_set = $aurassure_db->query($add_sql);
		if($add_result_set) {
			$json_response['id'] = $aurassure_db->connection->insert_id;
			$json_response['uid'] = $api_utilities->session_handler->user_session_data['ui'];
			$api_utilities->set_success_ststus_in_response();
		} else {
			$api_utilities->throw_error('Sorry, unable to submit data!');
		}
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(10, $access_time, 'update', $user_id, $log_id);