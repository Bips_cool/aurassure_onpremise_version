<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_details_of_city.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    city_id: 1
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(239, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$city_id = $received_data['city_id'];

if(isset($city_id) && $city_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$city_id = mysqli_real_escape_string($aurassure_db->connection, $city_id);

	$sql = "SELECT ct_id, ct_name, ct_mail_ids FROM cities WHERE ct_id=".$city_id;
	// if($api_utilities->session_handler->user_session_data['ci'] == 5) {
	// 	//send all devices for admin portal
	// } else {
	// 	$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
	// }
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, the requested city wasn\'t found.');
	}

	$result = mysqli_fetch_assoc($result_set);
	$json_response['id'] = $result['ct_id'];
	$json_response['name'] = $result['ct_name'];
	$json_response['email'] = json_decode($result['ct_mail_ids']);
	
	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(239, $access_time, 'update', $user_id, $log_id);