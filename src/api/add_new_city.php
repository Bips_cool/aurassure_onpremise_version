<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/add_new_city.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    ct_name: 'Demo',
    station_ids: ['1','2','3','4','5','6']
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(236, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
$data = json_decode(file_get_contents('php://input'), true);
$ct_name = $data['ct_name'];
$station_ids = $data['station_ids'];

//sanitize received data
$ct_name = mysqli_real_escape_string($aurassure_db->connection, $ct_name);
$ct_subdomain = $api_utilities->slug($ct_name);

// Check Group is added or not
$sql = "SELECT ct_name, ct_subdomain FROM cities WHERE ct_name='".$ct_name."' OR ct_subdomain='".$ct_subdomain."'";
$result_set = $aurassure_db->query($sql);
if(mysqli_num_rows($result_set)) {
	// $json_response['sql'] = $sql;
	$api_utilities->throw_error("City already exists!");
} else {
	// $json_response['sql'] = $sql;
	$add_sql = "INSERT INTO `cities`(`ct_name`, `ct_subdomain`) VALUES ('".$ct_name."','".$ct_subdomain."')";
	$add_result_set = $aurassure_db->query($add_sql);
	if($add_result_set) {
		$new_city_id = $aurassure_db->connection->insert_id;
		if ($station_ids && count($station_ids)) {
			foreach ($station_ids as $key => $id) {
				$update_station_sql = "UPDATE `dvc_locations` SET `ct_id`='".$new_city_id."' WHERE `dvcloc_id`=".$id;
				$update_result_set = $aurassure_db->query($update_station_sql);
			}
		}
		$json_response['id'] = $new_city_id;
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to Insert City Data!');
	}
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(236, $access_time, 'update', $user_id, $log_id);