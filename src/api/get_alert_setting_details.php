<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_alert_setting_details.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    city_id: 1
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(239, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$city_id = $received_data['city_id'];

if(isset($city_id) && $city_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$city_id = mysqli_real_escape_string($aurassure_db->connection, $city_id);

	$allowed_station_param_keys = ['pm2.5', 'pm10', 'so2', 'no2', 'co', 'o3', 'co2', 'temperature', 'humidity', 'rain', 'wspeed', 'wdir', 'noise', 'press'];
	$sql = "SELECT `dvcprm_name`, `dvcprm_key`, `dvcprm_unit`, `dvcprm_unit_noformat` FROM `dvc_params` WHERE dvcprm_key IN ('".implode('\',\'', $allowed_station_param_keys)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$json_response['params'] = [];
	$json_response['stations'] = [];
	$json_response['param_threshold'] = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$station_params []= $result['dvcprm_key'];
		$json_response['params'] []= [
			'name' => $result['dvcprm_name'],
			'key' => $result['dvcprm_key'],
			'unit' => $result['dvcprm_unit'],
			'unit_no_format' => $result['dvcprm_unit_noformat']
		];
	}

	$sql = "SELECT `ct_id`, `ct_threshold_limits` FROM `cities` WHERE ct_id=".$city_id;
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$results = mysqli_fetch_assoc($result_set);
	$json_response['param_threshold'] = json_decode($results['ct_threshold_limits'], true);

	$sql = "SELECT dvcloc_id, dvcloc_name, sad_send_alerts, sad_mobile_nos, sad_mail_ids FROM dvc_locations WHERE ct_id=".$city_id;
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		$stations = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$temp_location_array = [];
			$temp_location_array['id'] = $result['dvcloc_id'];
			$temp_location_array['name'] = $result['dvcloc_name'];
			$temp_location_array['alert_status'] = $result['sad_send_alerts'] == 1 ? true : false;
			$temp_location_array['mobile_nos'] = json_decode($result['sad_mobile_nos'], true);
			$temp_location_array['mail_ids'] = json_decode($result['sad_mail_ids'], true);

			$stations []= $temp_location_array;
		}
		$json_response['stations'] = $stations;
	}
	
	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(239, $access_time, 'update', $user_id, $log_id);