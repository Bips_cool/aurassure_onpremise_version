<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_device_error_log.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		device_id: 18,
		time_interval: []
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(28, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("_includes/form_data_validation_functions.php");

	// Check the value of required fields are set or not
	$data = json_decode(file_get_contents('php://input'), true);
	if(!has_presence($data['device_id'])) {
		$api_utilities->incomplete_request("Please select a valid device!");
	}

	// Sanitize the fields
	$device_id = mysqli_real_escape_string($aurassure_db->connection, $data['device_id']);
	if (sizeof($data['time_interval'])) {
		$time_interval = [];
		$time_interval [] = mysqli_real_escape_string($aurassure_db->connection, $data['time_interval'][0]);
		$time_interval [] = mysqli_real_escape_string($aurassure_db->connection, $data['time_interval'][1]);
		$sql = "SELECT dvcdat_data,dvcdat_time, dvcdat_server_time FROM dvc_raw_data WHERE dvcloc_id=".$device_id." AND dvcdat_time>=".$time_interval[0]." AND dvcdat_time<=".$time_interval[1]." ORDER BY dvcdat_time DESC";
	} else {
		$sql = "SELECT dvcdat_data, dvcdat_time, dvcdat_server_time FROM dvc_raw_data WHERE dvcloc_id=".$device_id." ORDER BY dvcdat_time DESC LIMIT 200";
	}
	
	// Get Device Errors
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		// $json_response['sql'] = $sql;
		$json_response['error_logs'] = [];
		while($result = mysqli_fetch_assoc($result_set)) {
			$err_data = json_decode($result['dvcdat_data'], true);
			if ($device_id == 243) {
				$json_response['error_logs'] [] = [
					'error' => json_encode($err_data),
					'timestamp' => $result['dvcdat_time'],
					'server_time' => $result['dvcdat_server_time'],
					'color' => $color
				];
			} else {
				$debug_data = ($err_data['Error'] ? $err_data['Error'] : ($err_data['debug'] ? $err_data['debug'] : ''));
				if (!isset($debug_data['errno']) || !isset($debug_data['errbyte1']) || !isset($debug_data['errbyte2'])) {
					$color = 7;
				} else if ($debug_data['errbyte2'] >= 128) {
					$color = 1;
				} else if ($debug_data['errbyte2'] >= 64) {
					$color = 2;
				} else if ($debug_data['errbyte2'] >= 32) {
					$color = 3;
				} else if (($debug_data['errno'] != 0) || ($debug_data['errbyte1'] != 0) || ($debug_data['errbyte2'] != 0)) {
					$color = 4;
				} else if (isset($debug_data['power']) && $debug_data['power'] < 8.5) {
					$color = 5;
				} else if (isset($debug_data['rssi']) && $debug_data['rssi'] < 12) {
					$color = 6;
				} else {
					$color = 0;
				}
				$error_text = '';
				if (isset($debug_data['rssi']) && $debug_data['rssi'] <= 40) {
					$error_text = 'Signal Strength:'.$debug_data['rssi'];
				} else if (isset($debug_data['rssi']) && $debug_data['rssi'] == 100) {
					$error_text = 'Network OK';
				}
				if (isset($debug_data['prc']) && $debug_data['prc'] > 0 ) {
					$error_text .= ', Power Failure';
				}
				if (isset($debug_data['crc']) && $debug_data['crc'] > 0 ) {
					$error_text .= ', Device Reboot';
				}
				$json_response['error_logs'] [] = [
					'error' => json_encode($debug_data),
					'timestamp' => $result['dvcdat_time'],
					'server_time' => $result['dvcdat_server_time'],
					'color' => $color
				];
			}
		}

		if (!isset($time_interval)) {
			$json_response['time_interval'] = [$json_response['error_logs'][(sizeof($json_response['error_logs'])-1)]['timestamp'],$json_response['error_logs'][0]['timestamp']];
		}
		
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to load data!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(28, $access_time, 'update', $user_id, $log_id);