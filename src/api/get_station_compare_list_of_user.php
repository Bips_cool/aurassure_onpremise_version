<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_station_compare_list_of_user.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include'
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(29, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

if($_SERVER['REQUEST_METHOD'] === "POST") {
	$sql = "SELECT `compared_locations_id` FROM `usr_compare_locations` WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui']." LIMIT 1";
	$result_set = $aurassure_db->query($sql);
	$api_utilities->set_success_ststus_in_response();
	if($result_set && mysqli_num_rows($result_set)) {
		$data = mysqli_fetch_assoc($result_set);
		$json_response['station_ids'] = json_decode($data['compared_locations_id']);
	} else if($result_set && !mysqli_num_rows($result_set)) {
		$json_response['station_ids'] = null;
	} else {
		$api_utilities->db_error();
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(29, $access_time, 'update', $user_id, $log_id);