<?php

function get_raw_data_json($station_id, $from_time, $upto_time, $required_params=[], $data_in_descending_order = true, $unit_conv_type) {
	//global variables
	global $aurassure_db, $api_utilities;
	$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
	$user_id = $api_utilities->session_handler->user_session_data['ui'];
	$log_id = $api_utilities->log_api_tracking_data(9, $access_time, 'insert', $user_id);

	//initialize the final json result to be returned
	$result_json = [];
	// $result_json['param_values'] = [];
	// $result_json['time_stamps'] = [];
	$convert_type = ($unit_conv_type == 'naqi') ? true : false;

	//get data within the given interval
	// "SELECT dvcloc_id, dvcdat_time, dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=$station_id AND dvcdat_time<=".($_SERVER['REQUEST_TIME']-86400)." AND dvcdat_time>=".$_SERVER['REQUEST_TIME'];
	$sql = "SELECT dvcloc_id, dvcdat_time, dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=". $station_id;
	if($from_time) {
		if($upto_time) {
			$sql .= " AND dvcdat_time>=". $from_time ." AND dvcdat_time<=". $upto_time;
		} else {
			$sql .= " AND dvcdat_time>". $from_time;
		}
		$sql .= " ORDER BY dvcdat_time";
		if($data_in_descending_order) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}
	} else {
		$sql .= " ORDER BY dvcdat_time DESC LIMIT 1";
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}

	//Vadodara POC
	if($station_id == 139) {
		$sql = "SELECT dvcloc_id, dvcdat_time, dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=140";
		if($from_time) {
			if($upto_time) {
				$sql .= " AND dvcdat_time>=". $from_time ." AND dvcdat_time<=". $upto_time;
			} else {
				$sql .= " AND dvcdat_time>". $from_time;
			}
			$sql .= " ORDER BY dvcdat_time";
			if($data_in_descending_order) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}
		} else {
			$sql .= " ORDER BY dvcdat_time DESC LIMIT 1";
		}

		$weather_result_set = $aurassure_db->query($sql);
		if(!$weather_result_set) {
			$api_utilities->db_error();
		}

		$weather_data = [];
		while($weather_result = $weather_result_set->fetch_assoc()) {
			$weather_data[$weather_result['dvcloc_id']][$weather_result['dvcdat_time']] = json_decode($weather_result['dvcdat_data'], true);
		}
	}

	while($result = mysqli_fetch_assoc($result_set)) {
		$result_data = json_decode($result['dvcdat_data'], true);
		foreach ($required_params as $parameter) {
			$required_parameter_values[$parameter] = null;
		}
		foreach($result_data as $parameter => $value) {
			// echo $parameter;
			if(in_array($parameter, $required_params)) {
				$filtered_parameter_value = null;
				if($value > 0) {
					$filtered_parameter_value = convert_raw_value($parameter, $value, $convert_type);
				} else {
					$filtered_parameter_value = convert_raw_value($parameter, -$value, $convert_type);
				}

				if($parameter == 'co') {
					$required_parameter_values[$parameter] = number_format($filtered_parameter_value, 4, '.', '');
				} else {
					$required_parameter_values[$parameter] = number_format($filtered_parameter_value, 2, '.', '');
				}
			}
		}

		//Vadodara POC
		if($station_id == 139) {
			$latest_weather_data_time = array_keys($weather_data)[0];
			foreach ($weather_data as $weather_data_time => $weather_data_value) {
				if($weather_data_time <= $result['dvcdat_time']) {
					$latest_weather_data_time = $weather_data_time;
				} else {
					break;
				}
			}
			$weather_data_values = $weather_data[$latest_weather_data_time];
			foreach($weather_data_values as $parameter => $value) {
				// echo $parameter;
				if(in_array($parameter, $required_params)) {
					$filtered_parameter_value = null;
					if($value > 0) {
						$filtered_parameter_value = convert_raw_value($parameter, $value, $convert_type);
					} else {
						$filtered_parameter_value = convert_raw_value($parameter, -$value, $convert_type);
					}

					$required_parameter_values[$parameter] = number_format($filtered_parameter_value, 2, '.', '');
				}
			}
		}

		foreach ($required_params as $parameter) {
			$result_json['param_values'][$parameter] []= $required_parameter_values[$parameter];
		}
		$result_json['time_stamps'] []= $result['dvcdat_time'];
	}

	return $result_json;
	$api_utilities->log_api_tracking_data(9, $access_time, 'update', $user_id, $log_id);
}

function convert_raw_value($param, $value, $convert_type) {
	// Conversion factors for parameters
	$conv_factors = [
		'so2' => 2.6202863,
		'no2' => 1.88161554,
		'co' => 1.14560327,
		'o3' => 1.96319018
	];

	if ($convert_type) {
		switch ($param) {
			case 'so2':
				return (floatval($value) * $conv_factors['so2']);
				break;
			case 'no2':
				return (floatval($value) * $conv_factors['no2']);
				break;
			case 'co':
				return (floatval($value) * $conv_factors['co']);
				break;
			case 'o3':
				return (floatval($value) * $conv_factors['o3']);
				break;
			default:
				return floatval($value);
				break;
		}
	} else {
		return floatval($value);
	}
}