<?php
set_include_path("/var/www/aurassure/");
require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(12, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	require_once("_includes/random_string_generator.php");
	require_once("_includes/form_data_validation_functions.php");

	$decoded_posted_data = json_decode($_POST['d'], true);
	$old_password = $decoded_posted_data['old_password'];
	$new_password = $decoded_posted_data['new_password'];

	if(!has_presence($old_password)) {
		$api_utilities->incomplete_request("Please give your last used password.");
	} elseif(!has_presence($new_password)) {
		$api_utilities->incomplete_request("Please give a valid password.");
	}

	//sanitize the fields
	$encrypted_password = password_hash($new_password, PASSWORD_BCRYPT);

	//check if the password already exists or not
	$sql = "SELECT usr_pass FROM usr_login WHERE usr_id=".$api_utilities->session_handler->user_session_data['ui']." AND usr_type=1";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(mysqli_num_rows($result_set)) {
		$result = mysqli_fetch_assoc($result_set);
		if(password_verify($old_password, $result['usr_pass'])) {
			$pass_sql = "UPDATE usr_login SET usr_pass='".$encrypted_password."' WHERE usr_id='".$api_utilities->session_handler->user_session_data['ui']."' AND usr_type=1";
			$pass_result_set = $aurassure_db->query($pass_sql);
			if(!$pass_result_set) {
				$api_utilities->db_error();
			}
			$api_utilities->set_success_ststus_in_response();
		} else {
			$api_utilities->throw_error('Your recent password is incorrect.');
		}
	}
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(12, $access_time, 'update', $user_id, $log_id);