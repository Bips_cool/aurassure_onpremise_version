<?php
/*
	Testing Code
*/
/*fetch('https://api.aurassure.com/dev/get_archive_data_of_station.php', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	credentials: 'include',
	body: JSON.stringify({"station_id":"177",
		"parameters":["temperature","humidity","co2","o2","co","so2","lint","uv","noise","rain","nox"],
		"from_time":"12-10-2017 12:48",
		"upto_time":"14-10-2017 12:48",
		"data_type":"avg_over_time",
		"duration":"1_hour",
		"conversion_type": "naqi"
	})
})
.then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json)
})*/


require_once("/var/www/aurassure/_includes/vendor/autoload.php");
require_once('./_generate_raw_data_json.php');
require_once('./_generate_avg_data_json.php');

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
use Aurassure\DataProcessor\AQICalculator;
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(22, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$city_id = $received_data['city_id'];
$station_ids = $received_data['station_id'];
$data_type = $received_data['data_type'];
$parameters = $received_data['parameters'];
$start_time = "00:00:00";
$end_time = date('G:H:s');
$now = date('d-m-Y');
if($received_data['upto_time'] < $now) {
	$end_time = "00:00:00";
} else {
	$end_time = date('G:H:s');
}
$from_time = ($received_data['from_time']." ".$start_time);
$upto_time = ($received_data['upto_time']." ".$end_time);
$duration = $received_data['duration'];
$conversion_type = $received_data['conversion_type'];
$aqi_duration = $duration;
switch($duration) {
	case 1:
		$duration = 3600;
		break;
	case 8:
		$duration = 8 * 3600;
		break;
	case 24:
		$duration = 24 * 3600;
		break;
}

$merged_city_ids = [11,16,19,20,22];
$all_parameters = ['pm2.5','humidity','temperature','co2','voc','no2','nh3','co','pm10','so2','o3','pm1','noise','rain','uv','lint','o2','no','nox','ch4','lead','wspeed','wdir','press','no3','ph','do','nh4','orp','us_mb','cur','us_hc04'];

if (isset($city_id) && $city_id != null) {

	//check if user has access to the requested city
	if ($city_id == 22) {
		$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id IN (".implode(',', $merged_city_ids).")";
	} else {
		$sql = "SELECT dvcloc_id FROM dvc_locations  WHERE ct_id =".$city_id;
	}

	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// $api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		// $api_utilities->throw_error('Sorry, no Station found in the city.');
	}

	$api_utilities->set_success_ststus_in_response();
	$city_station_ids = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$city_station_ids []= $result['dvcloc_id'];
	}
	$city_station_data = []; $city_param = [];
	$required_data = get_avg_data_json($city_station_ids, strtotime($from_time), strtotime($upto_time), $duration, $all_parameters, true, $conversion_type);

	foreach ($required_data['station_data'] as $stn_id => $parameters_value) {
		$stn_counter[$stn_id] = 0;
		foreach ($parameters_value as $param_key => $value) {
			$param_counter[$param_key] = 0;
			if ($value != null || $value != []) {
				$avg_val[$param_key] = 0;
				$station_parameters[$stn_id] []= $param_key;
				$city_station_data[$stn_id][$param_key] = $value;
			}
		}
	}

	foreach ($station_parameters as $stn_id => $param) {
		$city_param = array_merge($param);
	}

	$required_params = [];
	if (isset($city_param) && (in_array('pm2.5', $city_param) || in_array('pm10', $city_param) || in_array('so2', $city_param) || in_array('no2', $city_param) || in_array('o3', $city_param) || in_array('co', $city_param))) {
		$required_params []= [
			'name' => 'AQI',
			'key' => 'hourly_aqi',
			'unit' => ''
		];
		$city_param []= 'hourly_aqi';
	}

	$sql = "SELECT dvcprm_name, dvcprm_name_noformat, dvcprm_key, dvcprm_unit_noformat FROM dvc_params WHERE dvcprm_key IN ('".implode('\',\'', $city_param)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// $db_error();
	}

	while($result = mysqli_fetch_assoc($result_set)) {
		if ($conversion_type == 'naqi') {
			if ($result['dvcprm_key'] == 'so2' || $result['dvcprm_key'] == 'no2' || $result['dvcprm_key'] == 'o3') {
				$required_params []= [
					'name' => $result['dvcprm_name'],
					'key' => $result['dvcprm_key'],
					'unit' => 'μg / m3'
				];
			} else if ($result['dvcprm_key'] == 'co') {
				$required_params []= [
					'name' => $result['dvcprm_name'],
					'key' => $result['dvcprm_key'],
					'unit' => 'mg / m3'
				];
			} else {
				$required_params []= [
					'name' => $result['dvcprm_name'],
					'key' => $result['dvcprm_key'],
					'unit' => $result['dvcprm_unit_noformat']
				];
			}
		} else {
			$required_params []= [
				'name' => $result['dvcprm_name'],
				'key' => $result['dvcprm_key'],
				'unit' => $result['dvcprm_unit_noformat']
			];
		}
	}

	foreach ($city_param as $value) {
		$city_wise_param_data[$value] =	[];
	}

	// $required_station_data = $required_data;
	// $json_response['all_received_city_data'] = $required_station_data;
	// $json_response['city_station_ids'] = $city_station_ids;
	// $json_response['city_data_check'] = $city_station_data;
	// $json_response['city_param'] = $city_param;
	// $json_response['city_wise_param_data'] = $city_wise_param_data;
	// $json_response['station_parameters'] = $station_parameters;
	$json_response['required_params'] = $required_params;
	$json_response['upto_time'] = strtotime($upto_time);
	$json_response['start_time'] = $duration == 86400 ? strtotime($from_time)-86400 : strtotime($from_time);
	$json_response['duration'] = $duration;

	$city_data = [];
	
	foreach ($city_param as $param) {
		foreach ($city_station_data as $stn_id => $parameters_key_value) {
			if ($city_station_data[$stn_id][$param]) {
				for ($i = 0; $i < count($city_station_data[$stn_id][$param]); $i++) {
					$city_wise_param_data[$param][$i] []=$city_station_data[$stn_id][$param][$i];
				}
			}
		}
	}
	// $city_data = $city_wise_param_data;
	$last_param;
	foreach ($city_wise_param_data as $param => $param_value) {
		// $param_counter[$param] = 0;
		foreach ($param_value as $index => $value) {
			$avg_val[$param] = 0;
			$max_value[$param] = 0;
			$min_value[$param] = 999999;
			$param_counter[$param] = 0;
			for ($i = 0; $i < count($value); $i++) {
				if ($value[$i]['avg'] != null && $value[$i]['avg'] != "0.00") {
					$avg_val[$param] += abs($value[$i]['avg']);
					$param_counter[$param]++;

					if ($value[$i]['max'] != null && $max_value[$param] <= $value[$i]['max']) {
						$max_value[$param] = $value[$i]['max'];
					}

					if ($value[$i]['min'] != null && $min_value[$param] >= $value[$i]['min']) {
						$min_value[$param] = $value[$i]['min'];
					}
				}
			}
			if ($avg_val[$param] != null) {
				if ($param == 'co') {
					$city_data[$param] []= [
						'avg' => number_format(($avg_val[$param]/$param_counter[$param]), 4, '.', ''),
						'min' => $min_value[$param],
						'max' => $max_value[$param]
					];
				} else {
					$city_data[$param] []= [
						'avg' => number_format(($avg_val[$param]/$param_counter[$param]), 2, '.', ''),
						'min' => $min_value[$param],
						'max' => $max_value[$param]
					];
				}
			} else {
				$city_data[$param] []= [
					'avg' => null,
					'min' => null,
					'max' => null
				];
			}
		}
		if (!isset($last_param)) {
			$last_param = $param;
		}
	}

	// $check = [];
	$param_avg_values = [];
	for ($i = 0; $i < count($city_data[$last_param]); $i++) {
		foreach ($city_data as $param => $param_value) {
			$param_avg_values[$param] = $param_value[$i]['avg'];
		}
		$station_aqi_data = Aurassure\DataProcessor\AQICalculator::getNAQIFromConc($param_avg_values, true);
		$station_aqi = ($station_aqi_data['aqi'] ? $station_aqi_data['aqi'] : 0);
		$station_responsible_parameter = $station_aqi_data['responsible_param'];
		$city_data['hourly_aqi'] []= [
			'avg' => $station_aqi,
			'param' => $station_responsible_parameter,
		];
		// $check []= $param_avg_values;
	}
	
	$json_response['city_wise_param_data_check'] = $city_wise_param_data;
	$json_response['city_data'] = $city_data;
	// $json_response['check_avg'] = $check;
	// $json_response['param_counter'] = $param_counter;

	echo json_encode($json_response);

} else if(isset($station_ids) && is_array($station_ids) && count($station_ids)) {
	$api_utilities->set_success_ststus_in_response();

	//check if user has access to the requested station
	if($api_utilities->session_handler->user_session_data['ci'] == 5) {
		// admin has access to all stations
		$sql = "SELECT dvcloc_id FROM dvc_locations  WHERE dvcloc_id IN (".implode(',', $station_ids).") AND dvcloc_data_source=0";
	} else {
		$sql = "SELECT dvcloc_id FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $station_ids).") AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// $api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		// $api_utilities->throw_error('Sorry, the requested Station wasn\'t found.');
	}

	//get raw data of station for last 24 hours
	if ($data_type === 'raw_data') {
		$required_data = get_raw_data_json($station_ids[0], strtotime($from_time), strtotime($upto_time), $parameters, false, $conversion_type);
		$json_response['param_values'] = $required_data['param_values'];
		$json_response['time_stamps'] = $required_data['time_stamps'];
	} else {
		$required_data = get_avg_data_json($station_ids, strtotime($from_time), strtotime($upto_time), $duration, $parameters, true, $conversion_type);
		$json_response['station_data'] = $required_data['station_data'];
		$json_response['station_summary'] = $required_data['station_summary'];
		$json_response['upto_time'] = strtotime($upto_time);
		$json_response['start_time'] = $duration == 86400 ? strtotime($from_time)-86400 : strtotime($from_time);
		$json_response['duration'] = $duration;
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(22, $access_time, 'update', $user_id, $log_id);