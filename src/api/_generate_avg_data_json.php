<?php

function get_avg_data_json($station_ids, $from_time, $upto_time, $duration, $required_params=[], $data_in_descending_order = true, $unit_conv_type) {
	//global variables
	date_default_timezone_set("Asia/Kolkata");
	global $aurassure_db, $api_utilities;
	$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
	$user_id = $api_utilities->session_handler->user_session_data['ui'];
	$log_id = $api_utilities->log_api_tracking_data(8, $access_time, 'insert', $user_id);

	//initialize the final json result to be returned
	$all_params = [];
	$result_to_return = [];
	$result_to_return['station_data'] = [];
	$result_to_return['station_summary'] = [];
	$convert_type = ($unit_conv_type == 'naqi') ? true : false;
	//get data within the given interval
	//
	if ($duration == 3600) {
		$sql = "SELECT dvcloc_id, dhad_aqi, dhad_responsible_param, dhad_upto_time, dhad_param_concs, dhad_param_min_max_data FROM dvc_hourly_avg_data WHERE dvcloc_id IN (".implode(',', $station_ids).") AND dhad_upto_time>". $from_time ." AND dhad_upto_time<=". $upto_time ." ORDER BY dhad_upto_time ASC";
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}

		if ($result_set && mysqli_num_rows($result_set)) {
			while ($db_result = mysqli_fetch_assoc($result_set)) {
				$all_params[$db_result['dvcloc_id']] = [];
				$current_data[$db_result['dvcloc_id']] = json_decode($db_result['dhad_param_concs'], true);
				$recent_data[$db_result['dvcloc_id']] []= json_decode($db_result['dhad_param_concs'], true);
				$db_result_set_avg_data[$db_result['dvcloc_id']][$db_result['dhad_upto_time']] = json_decode($db_result['dhad_param_concs'], true);
				$db_result_set_min_max_data[$db_result['dvcloc_id']][$db_result['dhad_upto_time']] = json_decode($db_result['dhad_param_min_max_data'], true);
				$db_result_set_aqi_data[$db_result['dvcloc_id']][$db_result['dhad_upto_time']] = $db_result['dhad_aqi'];
				$db_result_set_responsible_param_data[$db_result['dvcloc_id']][$db_result['dhad_upto_time']] = $db_result['dhad_responsible_param'];
				foreach ($current_data[$db_result['dvcloc_id']] as $key => $value) {
					$all_params[$db_result['dvcloc_id']] []= $key;
				}

				if (in_array('debug', $all_params[$db_result['dvcloc_id']])) {
					$all_params[$db_result['dvcloc_id']] = array_diff($all_params[$db_result['dvcloc_id']], ["debug"]);
				}
			}

			// $result_to_return['SQL'] = $sql;

			$no_data_array = [
				'avg' => null,
				'min' => null,
				'max' => null
			];
			$no_data_aqi_array = [
				'avg' => null,
				'param' => null
			];

			$demo_no_data = 0;
			foreach ($station_ids as $id) {
				$final_data_set[$id] = [];
			}
			foreach($all_params as $station_id => $params) {
				$current_upto_time[$station_id] = $from_time + $duration - 1;
				foreach ($params as $param) {
					if ($recent_data[$station_id]) {
						$sum[$station_id][$param] = 0;
						$no_of_data_points[$station_id][$param] = 0;
						$max[$station_id][$param] = $recent_data[$station_id][0][$param];
						$min[$station_id][$param] = $recent_data[$station_id][0][$param];

						if($param == 'rain') {
							$last_rain[$station_id] = $recent_data[$station_id][0]['rain'];
							$agg_rain[$station_id] = 0;
						}
					}

					$final_data_set[$station_id][$param] = [];
				}
			}
		}
		// 8 Hour data
	} else if ($duration == 28800) {
		$sql = "SELECT dvcloc_id, dhavgd_aqi, dhavgd_responsible_param, dhavgd_upto_time, dhavgd_param_concs, dhavgd_param_min_max_data FROM dvc_8_hour_avg_data WHERE dvcloc_id IN (".implode(',', $station_ids).") AND dhavgd_upto_time>". $from_time ." AND dhavgd_upto_time<=". $upto_time ." ORDER BY dhavgd_upto_time ASC";
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}

		if ($result_set && mysqli_num_rows($result_set)) {
			while ($db_result = mysqli_fetch_assoc($result_set)) {
				$all_params[$db_result['dvcloc_id']] = [];
				$current_data[$db_result['dvcloc_id']] = json_decode($db_result['dhavgd_param_concs'], true);
				$recent_data[$db_result['dvcloc_id']] []= json_decode($db_result['dhavgd_param_concs'], true);
				$db_result_set_avg_data[$db_result['dvcloc_id']][$db_result['dhavgd_upto_time']] = json_decode($db_result['dhavgd_param_concs'], true);
				$db_result_set_min_max_data[$db_result['dvcloc_id']][$db_result['dhavgd_upto_time']] = json_decode($db_result['dhavgd_param_min_max_data'], true);
				$db_result_set_aqi_data[$db_result['dvcloc_id']][$db_result['dhavgd_upto_time']] = $db_result['dhavgd_aqi'];
				$db_result_set_responsible_param_data[$db_result['dvcloc_id']][$db_result['dhavgd_upto_time']] = $db_result['dhavgd_responsible_param'];
				foreach ($current_data[$db_result['dvcloc_id']] as $key => $value) {
					$all_params[$db_result['dvcloc_id']] []= $key;
				}

				if (in_array('debug', $all_params[$db_result['dvcloc_id']])) {
					$all_params[$db_result['dvcloc_id']] = array_diff($all_params[$db_result['dvcloc_id']], ["debug"]);
				}
			}

			// $result_to_return['SQL'] = $sql;

			$no_data_array = [
				'avg' => null,
				'min' => null,
				'max' => null
			];
			$no_data_aqi_array = [
				'avg' => null,
				'param' => null
			];

			$demo_no_data = 0;
			foreach ($station_ids as $id) {
				$final_data_set[$id] = [];
			}
			foreach($all_params as $station_id => $params) {
				$current_upto_time[$station_id] = $from_time + $duration - 1;
				foreach ($params as $param) {
					if ($recent_data[$station_id]) {
						$sum[$station_id][$param] = 0;
						$no_of_data_points[$station_id][$param] = 0;
						$max[$station_id][$param] = $recent_data[$station_id][0][$param];
						$min[$station_id][$param] = $recent_data[$station_id][0][$param];

						if($param == 'rain') {
							$last_rain[$station_id] = $recent_data[$station_id][0]['rain'];
							$agg_rain[$station_id] = 0;
						}
					}

					$final_data_set[$station_id][$param] = [];
				}
			}
		}
		// 24 Hour data
	} else if ($duration == 86400) {
		$sql = "SELECT dvcloc_id, ddad_aqi, ddad_responsible_param, ddad_upto_time, ddad_param_concs, ddad_param_min_max_data FROM dvc_daily_avg_data WHERE dvcloc_id IN (".implode(',', $station_ids).") AND ddad_upto_time>". $from_time ." AND ddad_upto_time<=". $upto_time ." ORDER BY ddad_upto_time ASC";
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			$api_utilities->db_error();
		}

		if ($result_set && mysqli_num_rows($result_set)) {
			while ($db_result = mysqli_fetch_assoc($result_set)) {
				$all_params[$db_result['dvcloc_id']] = [];
				$current_data[$db_result['dvcloc_id']] = json_decode($db_result['ddad_param_concs'], true);
				$recent_data[$db_result['dvcloc_id']] []= json_decode($db_result['ddad_param_concs'], true);
				$db_result_set_avg_data[$db_result['dvcloc_id']][$db_result['ddad_upto_time']] = json_decode($db_result['ddad_param_concs'], true);
				$db_result_set_min_max_data[$db_result['dvcloc_id']][$db_result['ddad_upto_time']] = json_decode($db_result['ddad_param_min_max_data'], true);
				$db_result_set_aqi_data[$db_result['dvcloc_id']][$db_result['ddad_upto_time']] = $db_result['ddad_aqi'];
				$db_result_set_responsible_param_data[$db_result['dvcloc_id']][$db_result['ddad_upto_time']] = $db_result['ddad_responsible_param'];
				foreach ($current_data[$db_result['dvcloc_id']] as $key => $value) {
					$all_params[$db_result['dvcloc_id']] []= $key;
				}

				if (in_array('debug', $all_params[$db_result['dvcloc_id']])) {
					$all_params[$db_result['dvcloc_id']] = array_diff($all_params[$db_result['dvcloc_id']], ["debug"]);
				}
			}

			// $result_to_return['SQL'] = $sql;

			$no_data_array = [
				'avg' => null,
				'min' => null,
				'max' => null
			];
			$no_data_aqi_array = [
				'avg' => null,
				'param' => null
			];

			$demo_no_data = 0;
			foreach ($station_ids as $id) {
				$final_data_set[$id] = [];
			}
			foreach($all_params as $station_id => $params) {
				$current_upto_time[$station_id] = $from_time + $duration - 1;
				foreach ($params as $param) {
					if ($recent_data[$station_id]) {
						$sum[$station_id][$param] = 0;
						$no_of_data_points[$station_id][$param] = 0;
						$max[$station_id][$param] = $recent_data[$station_id][0][$param];
						$min[$station_id][$param] = $recent_data[$station_id][0][$param];

						if($param == 'rain') {
							$last_rain[$station_id] = $recent_data[$station_id][0]['rain'];
							$agg_rain[$station_id] = 0;
						}
					}

					$final_data_set[$station_id][$param] = [];
				}
			}
		}
	}

	// Station Data Calculation
	foreach ($station_ids as $stn_id) {
		$final_param_values_array = [];
		$temp_param_min_value = 999999;
		$temp_param_max_value = 0;
		$calm_data_count[$stn_id] = 0;
		// Hourly Data
		$current_upto_time = $from_time + $duration;
		$current_wind_time = $from_time + $duration;
		$current_pollution_time = $from_time + $duration;
		while ($current_upto_time <= $upto_time) {
			$temp_param_values_array = [];
			if (isset($db_result_set_avg_data[$stn_id][$current_upto_time])) {
				//All parameter data
				foreach ($all_params[$stn_id] as $param_index => $param_key) {
					$temp_param_avg_value = convert_avg_value($param_key, $db_result_set_avg_data[$stn_id][$current_upto_time][$param_key], $convert_type);
					$temp_param_min_value = convert_avg_value($param_key, $db_result_set_min_max_data[$stn_id][$current_upto_time][$param_key]['min'], $convert_type);
					$temp_param_max_value = convert_avg_value($param_key, $db_result_set_min_max_data[$stn_id][$current_upto_time][$param_key]['max'], $convert_type);
					if ($param_key == 'co') {
						$temp_param_avg_value = number_format($temp_param_avg_value, 4, '.', '');
						$temp_param_min_value = number_format($temp_param_min_value, 4, '.', '');
						$temp_param_max_value = number_format($temp_param_max_value, 4, '.', '');
					} else {
						$temp_param_avg_value = number_format($temp_param_avg_value, 2, '.', '');
						$temp_param_min_value = number_format($temp_param_min_value, 2, '.', '');
						$temp_param_max_value = number_format($temp_param_max_value, 2, '.', '');
					}

					$temp_param_values_array[$param_key] = [
						'avg' => $temp_param_avg_value,
						'min' => $temp_param_min_value,
						'max' => $temp_param_max_value
					];
				}

				// AQI data
				if (in_array('hourly_aqi', $required_params)) {
					$temp_aqi_val = $db_result_set_aqi_data[$stn_id][$current_upto_time];
					$temp_param_values_array['hourly_aqi'] = [
						'avg' => intval($temp_aqi_val),
						'param' => ($db_result_set_responsible_param_data[$stn_id][$current_upto_time]) ? $db_result_set_responsible_param_data[$stn_id][$current_upto_time] : 'NA'
					];
				}

			} else {
				if (in_array('hourly_aqi', $required_params)) {
					$temp_param_values_array['hourly_aqi'] = $no_data_aqi_array;
				}

				foreach($all_params[$stn_id] as $param_index => $param_key) {
					$temp_param_values_array[$param_key] = $no_data_array;
				}
			}

			if (in_array('hourly_aqi', $required_params)) {
				$final_param_values_array['hourly_aqi'] []= $temp_param_values_array['hourly_aqi'];
			}

			foreach ($all_params[$stn_id] as $param_index => $param_key) {
				$final_param_values_array[$param_key] []= $temp_param_values_array[$param_key];
			}

			$current_upto_time += $duration;
		}

		// Wind Rose Data
		if (in_array('wind_rose', $required_params)) {
			if (in_array('wspeed', $all_params[$stn_id]) && in_array('wdir', $all_params[$stn_id])) {
				$final_param_values_array['wind_rose'] = [
					[
						'name' => '0 mph (calm)',
						'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						'color' => '#fff'
					],
					[
						'name' => '0-5 mph',
						'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						'color' => 'yellow'
					],
					[
						'name' => '5-10 mph',
						'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						'color' => 'green'
					],
					[
						'name' => '10-15 mph',
						'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						'color' => 'orange'
					],
					[
						'name' => '15-20 mph',
						'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						'color' => 'red'
					],
					[
						'name' => '>20 mph',
						'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
						'color' => 'purple'
					]
				];

				while ($current_wind_time <= $upto_time) {
					if ($wind_data[$stn_id]['wspeed'] > 0) {
						if ($wind_data[$stn_id]['wspeed'] <= 5) {
							if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 33.75) {
								$final_param_values_array['wind_rose'][1]['data'][1]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 56.25) {
								$final_param_values_array['wind_rose'][1]['data'][2]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 78.75) {
								$final_param_values_array['wind_rose'][1]['data'][3]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 101.25) {
								$final_param_values_array['wind_rose'][1]['data'][4]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 123.75) {
								$final_param_values_array['wind_rose'][1]['data'][5]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 146.25) {
								$final_param_values_array['wind_rose'][1]['data'][6]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 168.75) {
								$final_param_values_array['wind_rose'][1]['data'][7]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 191.25) {
								$final_param_values_array['wind_rose'][1]['data'][8]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 213.75) {
								$final_param_values_array['wind_rose'][1]['data'][9]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 236.25) {
								$final_param_values_array['wind_rose'][1]['data'][10]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 258.75) {
								$final_param_values_array['wind_rose'][1]['data'][11]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 281.25) {
								$final_param_values_array['wind_rose'][1]['data'][12]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 303.75) {
								$final_param_values_array['wind_rose'][1]['data'][13]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 326.25) {
								$final_param_values_array['wind_rose'][1]['data'][14]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 348.75) {
								$final_param_values_array['wind_rose'][1]['data'][15]++;
							} else {
								$final_param_values_array['wind_rose'][1]['data'][0]++;
							}
						} else if ($wind_data[$stn_id]['wspeed'] <= 10) {
							if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 33.75) {
								$final_param_values_array['wind_rose'][2]['data'][1]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 56.25) {
								$final_param_values_array['wind_rose'][2]['data'][2]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 78.75) {
								$final_param_values_array['wind_rose'][2]['data'][3]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 101.25) {
								$final_param_values_array['wind_rose'][2]['data'][4]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 123.75) {
								$final_param_values_array['wind_rose'][2]['data'][5]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 146.25) {
								$final_param_values_array['wind_rose'][2]['data'][6]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 168.75) {
								$final_param_values_array['wind_rose'][2]['data'][7]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 191.25) {
								$final_param_values_array['wind_rose'][2]['data'][8]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 213.75) {
								$final_param_values_array['wind_rose'][2]['data'][9]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 236.25) {
								$final_param_values_array['wind_rose'][2]['data'][10]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 258.75) {
								$final_param_values_array['wind_rose'][2]['data'][11]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 281.25) {
								$final_param_values_array['wind_rose'][2]['data'][12]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 303.75) {
								$final_param_values_array['wind_rose'][2]['data'][13]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 326.25) {
								$final_param_values_array['wind_rose'][2]['data'][14]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 348.75) {
								$final_param_values_array['wind_rose'][2]['data'][15]++;
							} else {
								$final_param_values_array['wind_rose'][2]['data'][0]++;
							}
						} else if ($wind_data[$stn_id]['wspeed'] <= 15) {
							if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 33.75) {
								$final_param_values_array['wind_rose'][3]['data'][1]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 56.25) {
								$final_param_values_array['wind_rose'][3]['data'][2]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 78.75) {
								$final_param_values_array['wind_rose'][3]['data'][3]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 101.25) {
								$final_param_values_array['wind_rose'][3]['data'][4]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 123.75) {
								$final_param_values_array['wind_rose'][3]['data'][5]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 146.25) {
								$final_param_values_array['wind_rose'][3]['data'][6]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 168.75) {
								$final_param_values_array['wind_rose'][3]['data'][7]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 191.25) {
								$final_param_values_array['wind_rose'][3]['data'][8]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 213.75) {
								$final_param_values_array['wind_rose'][3]['data'][9]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 236.25) {
								$final_param_values_array['wind_rose'][3]['data'][10]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 258.75) {
								$final_param_values_array['wind_rose'][3]['data'][11]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 281.25) {
								$final_param_values_array['wind_rose'][3]['data'][12]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 303.75) {
								$final_param_values_array['wind_rose'][3]['data'][13]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 326.25) {
								$final_param_values_array['wind_rose'][3]['data'][14]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 348.75) {
								$final_param_values_array['wind_rose'][3]['data'][15]++;
							} else {
								$final_param_values_array['wind_rose'][3]['data'][0]++;
							}
						} else if ($wind_data[$stn_id]['wspeed'] <= 20) {
							if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 33.75) {
								$final_param_values_array['wind_rose'][4]['data'][1]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 56.25) {
								$final_param_values_array['wind_rose'][4]['data'][2]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 78.75) {
								$final_param_values_array['wind_rose'][4]['data'][3]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 101.25) {
								$final_param_values_array['wind_rose'][4]['data'][4]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 123.75) {
								$final_param_values_array['wind_rose'][4]['data'][5]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 146.25) {
								$final_param_values_array['wind_rose'][4]['data'][6]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 168.75) {
								$final_param_values_array['wind_rose'][4]['data'][7]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 191.25) {
								$final_param_values_array['wind_rose'][4]['data'][8]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 213.75) {
								$final_param_values_array['wind_rose'][4]['data'][9]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 236.25) {
								$final_param_values_array['wind_rose'][4]['data'][10]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 258.75) {
								$final_param_values_array['wind_rose'][4]['data'][11]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 281.25) {
								$final_param_values_array['wind_rose'][4]['data'][12]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 303.75) {
								$final_param_values_array['wind_rose'][4]['data'][13]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 326.25) {
								$final_param_values_array['wind_rose'][4]['data'][14]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 348.75) {
								$final_param_values_array['wind_rose'][4]['data'][15]++;
							} else {
								$final_param_values_array['wind_rose'][4]['data'][0]++;
							}
						} else {
							if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 33.75) {
								$final_param_values_array['wind_rose'][5]['data'][1]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 56.25) {
								$final_param_values_array['wind_rose'][5]['data'][2]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 78.75) {
								$final_param_values_array['wind_rose'][5]['data'][3]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 101.25) {
								$final_param_values_array['wind_rose'][5]['data'][4]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 123.75) {
								$final_param_values_array['wind_rose'][5]['data'][5]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 146.25) {
								$final_param_values_array['wind_rose'][5]['data'][6]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 168.75) {
								$final_param_values_array['wind_rose'][5]['data'][7]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 191.25) {
								$final_param_values_array['wind_rose'][5]['data'][8]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 213.75) {
								$final_param_values_array['wind_rose'][5]['data'][9]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 236.25) {
								$final_param_values_array['wind_rose'][5]['data'][10]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 258.75) {
								$final_param_values_array['wind_rose'][5]['data'][11]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 281.25) {
								$final_param_values_array['wind_rose'][5]['data'][12]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 303.75) {
								$final_param_values_array['wind_rose'][5]['data'][13]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 326.25) {
								$final_param_values_array['wind_rose'][5]['data'][14]++;
							} else if ($db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_wind_time]['wdir'] <= 348.75) {
								$final_param_values_array['wind_rose'][5]['data'][15]++;
							} else {
								$final_param_values_array['wind_rose'][5]['data'][0]++;
							}
						}
					} else {
						$calm_data_count[$stn_id]++;
					}
				    $current_wind_time += $duration;
				}
			}
		}

		// Pollution Rose Data
		if (in_array('pollution_rose', $required_params)) {
			if (in_array('wdir', $all_params[$stn_id]) && in_array('wspeed', $all_params[$stn_id])) {
				if (in_array('pm2.5', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['pm2.5'] = [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '1-30',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '31-60',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '61-90',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '91-120',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '121-250',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '251-380',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>380',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				if (in_array('pm10', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['pm10'] = [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '1-50',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '51-100',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '101-250',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '251-350',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '351-430',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '431-510',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>510',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				if (in_array('no2', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['no2']= [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '1-40',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '41-80',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '81-180',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '181-280',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '281-400',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '401-500',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>500',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				if (in_array('o3', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['o3']= [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '1-50',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '51-100',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '101-168',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '169-208',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '209-748',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '749-800',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>800',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				if (in_array('co', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['co']= [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '0.1-1.0',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '1.1-2.0',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '2.1-10',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '10.1-17',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '17.1-34',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '34.1-50',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>50',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				if (in_array('so2', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['so2']= [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '1-40',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '41-80',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '81-380',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '381-800',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '801-1600',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '1601-2000',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>2000',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				if (in_array('nh3', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['nh3']= [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '1-200',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '201-400',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '401-800',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '801-1200',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '1201-1800',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '1801-2000',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>2000',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				if (in_array('pb', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['pb']= [
						[
							'name' => '0 (calm)',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ddd'
						],
						[
							'name' => '0.1-0.5',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#01e400'
						],
						[
							'name' => '0.6-1.0',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ffff01'
						],
						[
							'name' => '1.1-2.0',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#ff7d01'
						],
						[
							'name' => '2.1-3.0',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#fe0000'
						],
						[
							'name' => '3.1-3.5',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#9b004c'
						],
						[
							'name' => '3.6-4.0',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#4c0026'
						],
						[
							'name' => '>4.0',
							'data' => [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
							'color' => '#35011b'
						]
					];
				}
				$calm_data_count_1[$stn_id] = 0;
				$calm_data_count_2[$stn_id] = 0;
				$calm_data_count_3[$stn_id] = 0;
				$calm_data_count_4[$stn_id] = 0;
				$calm_data_count_5[$stn_id] = 0;
				$calm_data_count_6[$stn_id] = 0;
				$calm_data_count_7[$stn_id] = 0;
				$calm_data_count_8[$stn_id] = 0;

				while ($current_pollution_time <= $upto_time) {
				    if (in_array('pm2.5', $all_params[$stn_id])) {
				    	$pollution_rose_param_value['pm2.5'] = convert_avg_value('pm2.5', $db_result_set_avg_data[$stn_id][$current_pollution_time]['pm2.5'], $convert_type);
						if (floatval($pollution_rose_param_value['pm2.5']) > 0) {
							if (floatval($pollution_rose_param_value['pm2.5']) <= 30) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm2.5'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm2.5']) <= 60) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm2.5'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm2.5']) <= 90) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm2.5'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm2.5']) <= 120) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm2.5'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm2.5']) <= 250) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm2.5'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm2.5']) <= 380) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm2.5'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm2.5'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_1[$stn_id]++;
						}
					}
					if (in_array('pm10', $all_params[$stn_id])) {
						$pollution_rose_param_value['pm10'] = convert_avg_value('pm10', $db_result_set_avg_data[$stn_id][$current_pollution_time]['pm10'], $convert_type);
						if (floatval($pollution_rose_param_value['pm10']) > 0) {
							if (floatval($pollution_rose_param_value['pm10']) <= 50) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm10'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm10']) <= 100) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm10'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm10']) <= 250) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm10'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm10']) <= 350) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm10'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm10']) <= 300) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm10'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pm10']) <= 510) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm10'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pm10'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_2[$stn_id]++;
						}
					}
					if (in_array('no2', $all_params[$stn_id])) {
						$pollution_rose_param_value['no2'] = convert_avg_value('no2', $db_result_set_avg_data[$stn_id][$current_pollution_time]['no2'], $convert_type);
						if (floatval($pollution_rose_param_value['no2']) > 0) {
							if (floatval($pollution_rose_param_value['no2']) <= 40) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['no2'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['no2']) <= 80) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['no2'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['no2']) <= 180) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['no2'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['no2']) <= 280) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['no2'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['no2']) <= 400) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['no2'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['no2']) <= 500) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['no2'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['no2'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_3[$stn_id]++;
						}
					}
					if (in_array('o3', $all_params[$stn_id])) {
						$pollution_rose_param_value['o3'] = convert_avg_value('o3', $db_result_set_avg_data[$stn_id][$current_pollution_time]['o3'], $convert_type);
						if (floatval($pollution_rose_param_value['o3']) > 0) {
							if (floatval($pollution_rose_param_value['o3']) <= 50) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['o3'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['o3']) <= 100) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['o3'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['o3']) <= 168) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['o3'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['o3']) <= 208) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['o3'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['o3']) <= 748) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['o3'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['o3']) <= 800) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['o3'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['o3'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_4[$stn_id]++;
						}
					}
					if (in_array('co', $all_params[$stn_id])) {
						$pollution_rose_param_value['co'] = convert_avg_value('co', $db_result_set_avg_data[$stn_id][$current_pollution_time]['co'], $convert_type);
						if (floatval($pollution_rose_param_value['co']) > 0) {
							if (floatval($pollution_rose_param_value['co']) <= 1.0) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['co'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['co'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['co']) <= 2.0) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['co'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['co'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['co']) <= 10) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['co'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['co'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['co']) <= 17) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['co'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['co'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['co']) <= 34) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['co'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['co'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['co']) <= 50) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['co'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['co'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['co'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['co'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_5[$stn_id]++;
						}
					}
					if (in_array('so2', $all_params[$stn_id])) {
						$pollution_rose_param_value['so2'] = convert_avg_value('so2', $db_result_set_avg_data[$stn_id][$current_pollution_time]['so2'], $convert_type);
						if (floatval($pollution_rose_param_value['so2']) > 0) {
							if (floatval($pollution_rose_param_value['so2']) <= 40) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['so2'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['so2']) <= 80) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['so2'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['so2']) <= 380) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['so2'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['so2']) <= 800) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['so2'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['so2']) <= 1600) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['so2'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['so2']) <= 2000) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['so2'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['so2'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_6[$stn_id]++;
						}
					}
					if (in_array('nh3', $all_params[$stn_id])) {
						$pollution_rose_param_value['nh3'] = convert_avg_value('nh3', $db_result_set_avg_data[$stn_id][$current_pollution_time]['nh3'], $convert_type);
						if (floatval($pollution_rose_param_value['nh3']) > 0) {
							if (floatval($pollution_rose_param_value['nh3']) <= 200) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['nh3'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['nh3']) <= 400) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['nh3'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['nh3']) <= 800) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['nh3'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['nh3']) <= 1200) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['nh3'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['nh3']) <= 1800) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['nh3'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['nh3']) <= 2000) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['nh3'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['nh3'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_7[$stn_id]++;
						}
					}
					if (in_array('pb', $all_params[$stn_id])) {
						$pollution_rose_param_value['pb'] = convert_avg_value('pb', $db_result_set_avg_data[$stn_id][$current_pollution_time]['pb'], $convert_type);
						if (floatval($pollution_rose_param_value['pb']) > 0) {
							if (floatval($pollution_rose_param_value['pb']) <= 0.5) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pb'][1]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pb']) <= 1.0) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pb'][2]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pb']) <= 2.0) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pb'][3]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pb']) <= 3.0) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pb'][4]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pb']) <= 3.5) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pb'][5]['data'][0]++;
								}
							} else if (floatval($pollution_rose_param_value['pb']) <= 4.0) {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pb'][6]['data'][0]++;
								}
							} else {
								if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 11.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 33.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][1]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 33.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 56.25) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][2]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 56.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 78.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][3]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 78.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 101.25) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][4]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 101.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 123.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][5]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 123.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 146.25) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][6]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 146.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 168.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][7]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 168.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 191.25) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][8]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 191.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 213.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][9]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 213.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 236.25) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][10]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 236.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 258.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][11]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 258.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 281.25) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][12]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 281.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 303.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][13]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 303.75 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 326.25) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][14]++;
								} else if ($db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] > 326.25 && $db_result_set_avg_data[$stn_id][$current_pollution_time]['wdir'] <= 348.75) {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][15]++;
								} else {
									$final_param_values_array['pollution_rose']['pb'][7]['data'][0]++;
								}
							}
						} else {
							$calm_data_count_8[$stn_id]++;
						}
					}
					$current_pollution_time += $duration;
				}
			}

		}

		// Wind Rose Final Data
		if ($final_param_values_array['wind_rose']) {
			for ($i=0; $i <= 15; $i++) { 
				$final_param_values_array['wind_rose'][0]['data'][$i] = $calm_data_count[$stn_id] / 16;
			}

			foreach ($final_param_values_array['wind_rose'] as $index => $value) {
				foreach ($value['data'] as $ind => $data) {
					$final_param_values_array['wind_rose'][$index]['data'][$ind] = floatval(number_format((($data/count($db_result_set_avg_data[$stn_id]))*100), 2, '.', ''));
				}
			}
			$temp_final_data_wind_rose = $final_param_values_array['wind_rose'];
		}
		
		// Pollution Rose Final Data
		if ($final_param_values_array['pollution_rose']) {
			for ($i=0; $i <= 15; $i++) {
				if (in_array('pm2.5', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['pm2.5'][0]['data'][$i] = $calm_data_count_1[$stn_id] / 16;
				} else if (in_array('pm10', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['pm10'][0]['data'][$i] = $calm_data_count_2[$stn_id] / 16;
				} else if (in_array('no2', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['no2'][0]['data'][$i] = $calm_data_count_3[$stn_id] / 16;
				} else if (in_array('o3', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['o3'][0]['data'][$i] = $calm_data_count_4[$stn_id] / 16;
				} else if (in_array('co', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['co'][0]['data'][$i] = $calm_data_count_5[$stn_id] / 16;
				} else if (in_array('so2', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['so2'][0]['data'][$i] = $calm_data_count_6[$stn_id] / 16;
				} else if (in_array('nh3', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['nh3'][0]['data'][$i] = $calm_data_count_7[$stn_id] / 16;
				} else if (in_array('pb', $all_params[$stn_id])) {
					$final_param_values_array['pollution_rose']['pb'][0]['data'][$i] = $calm_data_count_8[$stn_id] / 16;
				}
			}
			foreach ($final_param_values_array['pollution_rose'] as $index => $value) {
				foreach ($value as $ind => $data) {
					foreach ($data['data'] as $key => $val) {
						$final_param_values_array['pollution_rose'][$index][$ind]['data'][$key] = floatval(number_format((($val/count($db_result_set_avg_data[$stn_id]))*100), 2, '.', ''));
					}
				}
			}
			$temp_final_data_pollution_rose = $final_param_values_array['pollution_rose'];
		}

		// Return required parameters values
		foreach ($required_params as $param_name) {
			$result_to_return['station_data'][$stn_id][$param_name] = $final_param_values_array[$param_name];
		}
	}
	
	// Station summary data Calculation
	foreach($station_ids as $stn_key){
		$check[$stn_key] = [];
		$result_to_return['station_summary'][$stn_key] = [];
		foreach ($all_params[$stn_key] as $param_index => $param) {
			$average_value = 0;
			$min_value = 999999999;
			$min_at = 0;
			$max_value = 0;
			$max_at = 0;
			$count = 0;
			$current_time = $from_time + $duration;
			while ($current_time <= $upto_time) {
				if($db_result_set_avg_data[$stn_key][$current_time][$param] != null && $db_result_set_avg_data[$stn_key][$current_time][$param] > 0) {
					$average_value += $db_result_set_avg_data[$stn_key][$current_time][$param];
					$count++;
				}

				if ($param == 'rain') {
					if ($db_result_set_min_max_data[$stn_key][$current_time]['rain'] != null && $db_result_set_min_max_data[$stn_key][$current_time]['rain']['max'] >= 0 && $max_value <= $db_result_set_min_max_data[$stn_key][$current_time]['rain']['max']) {
						$max_value = $db_result_set_min_max_data[$stn_key][$current_time]['rain']['max'];
						$max_at = $db_result_set_min_max_data[$stn_key][$current_time]['rain']['max_at'];
					}

					if ($db_result_set_min_max_data[$stn_key][$current_time]['rain'] != null && $db_result_set_min_max_data[$stn_key][$current_time]['rain']['min'] >= 0 && $min_value >= $db_result_set_min_max_data[$stn_key][$current_time]['rain']['min']) {
						$min_value = $db_result_set_min_max_data[$stn_key][$current_time]['rain']['min'];
						$min_at = $db_result_set_min_max_data[$stn_key][$current_time]['rain']['min_at'];
					}
				} else if ($param == 'wspeed') {
					if ($db_result_set_min_max_data[$stn_key][$current_time]['wspeed'] != null && $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['max'] >= 0 && $max_value <= $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['max']) {
						$max_value = $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['max'];
						$max_at = $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['max_at'];
					}

					if ($db_result_set_min_max_data[$stn_key][$current_time]['wspeed'] != null && $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['min'] >= 0 && $min_value >= $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['min']) {
						$min_value = $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['min'];
						$min_at = $db_result_set_min_max_data[$stn_key][$current_time]['wspeed']['min_at'];
					}
				} else {
					if ($db_result_set_min_max_data[$stn_key][$current_time][$param]['max'] != null && $db_result_set_min_max_data[$stn_key][$current_time][$param]['max'] > 0 && $max_value <= $db_result_set_min_max_data[$stn_key][$current_time][$param]['max']) {
						$max_value = $db_result_set_min_max_data[$stn_key][$current_time][$param]['max'];
						$max_at = $db_result_set_min_max_data[$stn_key][$current_time][$param]['max_at'];
					}

					if ($db_result_set_min_max_data[$stn_key][$current_time][$param]['min'] != null && $db_result_set_min_max_data[$stn_key][$current_time][$param]['min'] >= 0 && $min_value >= $db_result_set_min_max_data[$stn_key][$current_time][$param]['min']) {
						$min_value = $db_result_set_min_max_data[$stn_key][$current_time][$param]['min'];
						$min_at = $db_result_set_min_max_data[$stn_key][$current_time][$param]['min_at'];
					}
				}
				$current_time +=$duration;
			}

			$check[$stn_key][$param] = [
				"avg" => number_format($average_value / $count, 4, '.', ''),
				"min" => number_format($min_value, 4, '.', ''),
				"max" =>  number_format($max_value, 4, '.', ''),
				"min_time" => $min_at,
				"max_time" => $max_at
			];
			if ($param == 'co') {
				$result_to_return['station_summary'][$stn_key]['co'] = [
					"avg" => number_format($average_value / $count, 4, '.', ''),
					"min" => number_format($min_value, 4, '.', ''),
					"max" =>  number_format($max_value, 4, '.', ''),
					"min_time" => $min_at,
					"max_time" => $max_at
				];
			} else if ($param == 'rain') {
				$result_to_return['station_summary'][$stn_key][$param] = [
					"avg" => number_format($average_value / $count, 2, '.', ''),
					"min" => number_format($min_value, 2, '.', ''),
					"max" => number_format($max_value, 2, '.', ''),
					"min_time" => $min_at,
					"max_time" => $max_at
				];
			} else {
				$result_to_return['station_summary'][$stn_key][$param] = [
					"avg" => number_format($average_value / $count, 2, '.', ''),
					"min" => number_format($min_value, 2, '.', ''),
					"max" => number_format($max_value, 2, '.', ''),
					"min_time" => $min_at,
					"max_time" => $max_at
				];
			}
		}
	}
	
	return $result_to_return;
	$api_utilities->log_api_tracking_data(8, $access_time, 'update', $user_id, $log_id);
}

function convert_avg_value($param, $value, $convert_type) {
	// Conversion factors for parameters
	$conv_factors = [
		'so2' => 2.6202863,
		'no2' => 1.88161554,
		'co' => 1.14560327,
		'o3' => 1.96319018
	];

	if ($convert_type) {
		switch ($param) {
			case 'so2':
				return (floatval($value) * $conv_factors['so2']);
				break;
			case 'no2':
				return (floatval($value) * $conv_factors['no2']);
				break;
			case 'co':
				return (floatval($value) * $conv_factors['co']);
				break;
			case 'o3':
				return (floatval($value) * $conv_factors['o3']);
				break;
			default:
				return floatval($value);
				break;
		}
	} else {
		return floatval($value);
	}
}