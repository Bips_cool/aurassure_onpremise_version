<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/save_alert_setting_details.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    city_id: 1,
    alert_threshold: {},
    station_details: [{},{}]
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(239, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$city_id = $received_data['ct_id'];
$alert_threshold = $received_data['alert_threshold'];
$station_details = $received_data['station_details'];

if(isset($city_id) && $city_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$city_id = mysqli_real_escape_string($aurassure_db->connection, $city_id);

	$sql = "UPDATE `cities` SET `ct_threshold_limits`='".json_encode($alert_threshold)."' WHERE `ct_id`=".$city_id;
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to save data!');
	}

	foreach ($station_details as $key => $value) {
		$email_value = $value['alert_status'] ? 1 : 0;
		$sql = "UPDATE `dvc_locations` SET `sad_send_alerts`=".$email_value.", `sad_mobile_nos`='".json_encode($value['mobile_nos'])."', `sad_mail_ids`='".json_encode($value['mail_ids'])."' WHERE dvcloc_id=".$value['id'];
		$result_set = $aurassure_db->query($sql);
	}
	if($result_set) {
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to save data!');
	}
	
	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(239, $access_time, 'update', $user_id, $log_id);