<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/save_city_details.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		city_id: 2,
		name: 'name',
		email: 'email',
		lat: '12.111111111',
		long: '12.111111',
		zoom_lvl: 11,
		is_public: is_public,
		is_premium: is_premium,
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(240, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$city_id = $received_data['city_id'];
$ct_name = $received_data['name'];
$ct_email_ids = $received_data['email'];

if (isset($city_id) && $city_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$city_id = mysqli_real_escape_string($aurassure_db->connection, $city_id);
	$grp_name = mysqli_real_escape_string($aurassure_db->connection, $grp_name);

	$sql = "UPDATE `cities` SET `ct_name`='".$ct_name."', `ct_mail_ids`='".json_encode($ct_email_ids)."' WHERE `ct_id`=".$city_id;

	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to save data!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(240, $access_time, 'update', $user_id, $log_id);