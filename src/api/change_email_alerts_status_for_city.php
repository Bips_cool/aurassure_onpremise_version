<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/change_email_alerts_status_for_city.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    ct_id: 1,
    ct_alerts_status: true/false
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(234, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
$data = json_decode(file_get_contents('php://input'), true);
$ct_id = $data['ct_id'];
$ct_alerts_status = ($data['ct_alerts_status'] ? 1 : 0);

//sanitize received data
$ct_id = mysqli_real_escape_string($aurassure_db->connection, $ct_id);
$ct_alerts_status = mysqli_real_escape_string($aurassure_db->connection, $ct_alerts_status);

// Check Group is added or not
$sql = "UPDATE `cities` SET `ct_send_alerts`='".$ct_alerts_status."' WHERE `ct_id`=".$ct_id;
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$api_utilities->set_success_ststus_in_response();
} else {
	$api_utilities->throw_error('Sorry, unable to save data!');
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(234, $access_time, 'update', $user_id, $log_id);