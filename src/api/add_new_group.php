<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/add_new_group.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    grp_name: 'Demo',
    grp_lat: 19.121212,
    grp_long: 20.131313,
    grp_zoom_lvl: 10,
    grp_has_public: true/false,
    grp_is_premium: true/false
    station_ids: ['1','2','3','4','5','6']
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(233, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
$data = json_decode(file_get_contents('php://input'), true);
$grp_name = $data['grp_name'];
$grp_lat = $data['grp_lat'];
$grp_long = $data['grp_long'];
$grp_zoom_lvl = $data['grp_zoom_lvl'];
$grp_has_public = ($data['grp_has_public'] == true ? 1 : 0);
$grp_is_premium = ($data['grp_is_premium'] == true ? 1 : 0 );
$station_ids = $data['station_ids'];

//sanitize received data
$grp_name = mysqli_real_escape_string($aurassure_db->connection, $grp_name);
$grp_subdomain = $api_utilities->slug($grp_name);
$grp_lat = mysqli_real_escape_string($aurassure_db->connection, $grp_lat);
$grp_long = mysqli_real_escape_string($aurassure_db->connection, $grp_long);
$grp_zoom_lvl = mysqli_real_escape_string($aurassure_db->connection, $grp_zoom_lvl);
$grp_has_public = mysqli_real_escape_string($aurassure_db->connection, $grp_has_public);
$grp_is_premium = mysqli_real_escape_string($aurassure_db->connection, $grp_is_premium);

// Check Group is added or not
$sql = "SELECT grp_name, grp_subdomain FROM groups WHERE grp_name='".$grp_name."' OR grp_subdomain='".$grp_subdomain."'";
$result_set = $aurassure_db->query($sql);
if(mysqli_num_rows($result_set)) {
	// $json_response['sql'] = $sql;
	$api_utilities->throw_error("Group already exists!");
} else {
	// $json_response['sql'] = $sql;
	$add_sql = "INSERT INTO `groups`(`grp_name`, `grp_subdomain`, `grp_lat`, `grp_long`, `grp_zoom_lvl`, `grp_has_public`, `grp_is_premium`) VALUES ('".$grp_name."','".$grp_subdomain."','".$grp_lat."','".$grp_long."','".$grp_zoom_lvl."','".$grp_has_public."','".$grp_is_premium."')";
	$add_result_set = $aurassure_db->query($add_sql);
	if($add_result_set) {
		$inserted_group_id = $aurassure_db->connection->insert_id;
		if (count($station_ids)) {
			$add_station_sql = '';
			for ($i = 0; $i < count($station_ids); $i++) {
				$add_station_sql = "INSERT INTO `stations_under_group`(`grp_id`, `dvcloc_id`) VALUES ('".$inserted_group_id."','".$station_ids[$i]."');";
				$add_station_result_set = $aurassure_db->query($add_station_sql);
			}
			if ($add_station_result_set) {
				$json_response['id'] = $inserted_group_id;
				// $json_response['sql'] = $add_station_sql;
				$api_utilities->set_success_ststus_in_response();
			} else{
				$api_utilities->throw_error('Sorry, unable to Insert Stations!');
			}
		} else {
			$json_response['id'] = $inserted_group_id;
			$api_utilities->set_success_ststus_in_response();
		}
	} else {
		$api_utilities->throw_error('Sorry, unable to Insert Group Data!');
	}
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(233, $access_time, 'update', $user_id, $log_id);