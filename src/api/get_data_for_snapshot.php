<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_data_for_snapshot.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
  body: JSON.stringify({
    station_id: 1,
    time: 1234567890
  })
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");
require_once(getcwd().'/_generate_raw_data_json.php');

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(23, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$station_id = $received_data['station_id'];
$required_data_upto_time = $received_data['time'];

if(isset($station_id) && $station_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$station_id = mysqli_real_escape_string($aurassure_db->connection, $station_id);

	if($api_utilities->session_handler->user_session_data['ci'] == 5) {
		//send all devices for admin portal
		$sql = "SELECT dvcloc_name, dvcloc_city, dvcloc_params, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id;
	} else {
		$sql = "SELECT dvcloc_name, dvcloc_city, dvcloc_params, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, the requested station wasn\'t found.');
	}

	$result = mysqli_fetch_assoc($result_set);
	$temp_station_name = '';
	if($result['dvcloc_name']) {
		$temp_station_name .= $result['dvcloc_name'].', ';
	}
	if($result['dvcloc_city']) {
		$temp_station_name .= $result['dvcloc_city'];
	}
	$temp_station_name = rtrim($temp_station_name, ', ');
	$json_response['name'] = $temp_station_name;
	$json_response['connection_status'] = ($required_data_upto_time - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
	$json_response['last_data_update_time'] = $result['dvcloc_last_data_update_time'];
	$json_response['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
	$json_response['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);
	$json_response['aqi_status'] = EndUserResult::getAQIStatus($result['dvcloc_aqi']);
	$json_response['temp'] = $result['dvcloc_temp'];
	$json_response['humid'] = $result['dvcloc_humid'];
	$json_response['suggestions'] = EndUserResult::getSuggestions($result['dvcloc_aqi'], $result['dvcloc_responsible_param']);
	$station_param_ids = json_decode($result['dvcloc_params'], true);

	//get params and param units of station
	$sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=".$station_id." ORDER BY dvcdat_time DESC LIMIT 1";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		$api_utilities->throw_error('Sorry, no data has been received from the station yet.');
	}
	$result = $result_set->fetch_assoc();
	$station_param_keys = [];
	$station_latest_data = json_decode($result['dvcdat_data'], true);
	foreach ($station_latest_data as $param_key => $param_value) {
		$station_param_keys []= $param_key;
	}

	$sql = "SELECT dvcprm_name, dvcprm_name_noformat, dvcprm_key, dvcprm_unit FROM dvc_params WHERE dvcprm_key IN ('".implode('\',\'', $station_param_keys)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_params = [];
	$json_response['params'] = [];
	$json_response['param_units'] = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$station_params []= $result['dvcprm_key'];
		$json_response['params'] []= [
			'name' => $result['dvcprm_name'],
			'key' => $result['dvcprm_key']
		];
		$json_response['param_units'] []= $result['dvcprm_unit'];
	}

	//get raw data of station for last 24 hours
	$required_data = get_raw_data_json($station_id, $required_data_upto_time-86400, $required_data_upto_time, $station_params, false);
	$json_response['param_values'] = $required_data['param_values'];
	$json_response['time_stamps'] = $required_data['time_stamps'];

	//get hourly param conc.s and indices of station
	$sql = "SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=$station_id AND dhad_upto_time>=".($required_data_upto_time-90000)." AND dhad_upto_time<=".$required_data_upto_time." ORDER BY dhad_upto_time DESC";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$station_hourly_aqis = [];
	$station_param_hourly_concs = [];
	$station_param_hourly_aqis = [];
	foreach ($station_params as $param) {
		$station_param_hourly_concs[$param] = [];
		$station_param_hourly_aqis[$param] = [];
	}
	while($result = mysqli_fetch_assoc($result_set)) {
		$station_hourly_aqis[$result['dhad_upto_time']] = $result['dhad_aqi'];

		$temp_station_param_hourly_concs = json_decode($result['dhad_param_concs'], true);
		$temp_station_param_hourly_aqis = json_decode($result['dhad_param_aqis'], true);
		foreach ($station_params as $param) {
			if($temp_station_param_hourly_concs[$param]) {
				$station_param_hourly_concs[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
			}
			if($temp_station_param_hourly_aqis[$param]) {
				$station_param_hourly_aqis[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
			}
		}
	}

	$json_response['hourly_aqis'] = [];
	$json_response['hourly_param_concs'] = [];
	$json_response['hourly_param_aqis'] = [];
	foreach ($station_params as $param) {
		$json_response['hourly_param_concs'][$param] = [];
		$json_response['hourly_param_aqis'][$param] = [];
	}
	$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $required_data_upto_time));
	for ($i=0; $i<24; $i++) {
		if($station_hourly_aqis[$latest_hour]) {
			$json_response['hourly_aqis'] []= $station_hourly_aqis[$latest_hour];
		} else {
			$json_response['hourly_aqis'] []= null;
		}

		foreach ($station_params as $param) {
			if($station_param_hourly_concs[$param][$latest_hour]) {
				$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$latest_hour];
			} else {
				$json_response['hourly_param_concs'][$param] []= null;
			}
			if($station_param_hourly_aqis[$param][$latest_hour]) {
				$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$latest_hour];
			} else {
				$json_response['hourly_param_aqis'][$param] []= null;
			}
		}

		$latest_hour -= 3600;
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(23, $access_time, 'update', $user_id, $log_id);