<?php
set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\DB\MySQLDB;
//initialize DB connections
use Aurassure\API\Utilities;
$aurassure_db = new MySQLDB();
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$log_id = $api_utilities->log_api_tracking_data(37, $access_time, 'insert', 0);

$key = $_GET['key'];

if(isset($key) && $key == 'Ej14qoqnm9PxZJWX') {

	// Get latest Data
	$sql = "SELECT dvc_locations.dvcloc_aqi, dvc_hourly_avg_data.dhad_param_concs FROM dvc_locations INNER JOIN dvc_hourly_avg_data ON dvc_locations.dvcloc_id = dvc_hourly_avg_data.dvcloc_id WHERE dvc_locations.dvcloc_id= 147 ORDER BY dvc_hourly_avg_data.dhad_upto_time DESC" ;
	$result_set = $aurassure_db->query($sql);
	$current_time = date('H:i:s d/m/Y', $_SERVER['REQUEST_TIME']);
	if($result_set) {
		$station_parameters = [];
		$ordered_parameter_array = ['temperature', 'humidity', 'pm2.5', 'pm10','co2', 'noise', 'no2', 'o3', 'co', 'so2'];
		$result = mysqli_fetch_assoc($result_set);
		$current_station_latest_data = json_decode($result['dhad_param_concs'], true);
		$station_parameters []= intval($result['dvcloc_aqi']);
		foreach ($ordered_parameter_array as $param_key) {
			if($current_station_latest_data[$param_key] >= 1000) {
				$station_parameters []= number_format($current_station_latest_data[$param_key], 0, '.', '');
			} elseif($current_station_latest_data[$param_key] >= 100) {
				$station_parameters []= number_format($current_station_latest_data[$param_key], 1, '.', '');
			} elseif($current_station_latest_data[$param_key] >= 10) {
				$station_parameters []= number_format($current_station_latest_data[$param_key], 2, '.', '');
			} else {
				$station_parameters []= number_format($current_station_latest_data[$param_key], 3, '.', '');
			}
		}
	}

	$sql = "INSERT INTO aurassure_rkl_dmd_api_access_log (ardaal_accessed_at, ardaal_data_packet) VALUES ('".$current_time."','".json_encode($station_parameters)."')";
	$aurassure_db->query($sql);

	$update_sql = "UPDATE `dmd_api_keys` SET `dak_last_accessed_at`='".$_SERVER['REQUEST_TIME']."' WHERE `dak_id`=1";
	$aurassure_db->query($update_sql);

	$insert_sql = "INSERT INTO dmd_api_access_log (dak_id, daal_accessed_at, daal_access_from_ip) VALUES (1,'".$_SERVER['REQUEST_TIME']."', '".$_SERVER['REMOTE_ADDR']."')";
	$aurassure_db->query($insert_sql);


	// echo "*[152,30.20,38.40,75.51,85.27,790.1,52.48,3.190,1.212,0.274,4.162]*";
	echo '*'. str_replace("\"", "",json_encode($station_parameters)) .'*';
}
$api_utilities->log_api_tracking_data(37, $access_time, 'update', 0, $log_id);