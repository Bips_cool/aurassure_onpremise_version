<?php
require_once("/var/www/aurassure/_includes/vendor/autoload.php");

date_default_timezone_set("Asia/Kolkata");

//check for user authentication
use Aurassure\SessionManagement\SessionHandler;
use Aurassure\DataProcessor\AQICalculator;
// $session_handler = new SessionHandler();
// if(!$session_handler->logged_in()) {
// 	die("Please login to access the requested data.");
// }

//initialize DB connections
use Aurassure\DB\MySQLDB;
use Aurassure\API\Utilities;

$aurassure_db = new MySQLDB();
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];

$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(14, $access_time, 'insert', $user_id);

require_once(getcwd().'/_generate_raw_data_json.php');
require_once(getcwd().'/_generate_avg_data_json.php');

//get parameters
$received_data = json_decode($_GET['d'], true);
$city_id = $received_data['city_id'];
$station_ids = $received_data['station_id'];
$parameters = $received_data['parameters'];
$start_time = "00:00:00";
$end_time = date('G:H:s');
$now = date('d-m-Y');
if($received_data['upto_time'] < $now) {
	$end_time = "00:00:00";
} else {
	$end_time = date('G:H:s');
}
$from_time = strtotime($received_data['from_time']." ".$start_time);
$upto_time = strtotime($received_data['upto_time']." ".$end_time);
$data_type = $received_data['data_type'];
$duration = $received_data['duration'];
$conversion_type = $received_data['conversion_type'];
$aqi_duration = $duration;
switch($duration) {
  case 1:
    $duration = 3600;
    break;
  case 8:
    $duration = 8 * 3600;
    break;
  case 24:
    $duration = 24 * 3600;
    break;
}

$merged_city_ids = [11,16,19,20,22];
$all_parameters = ['pm2.5','humidity','temperature','co2','voc','no2','nh3','co','pm10','so2','o3','pm1','noise','rain','uv','lint','o2','no','nox','ch4','lead','wspeed','wdir','press','no3','ph','do','nh4','orp','us_mb','cur','us_hc04'];

if (isset($city_id) && $city_id != null) {
	// get city name
	$sql = "SELECT ct_id, ct_name FROM cities WHERE ct_id=".$city_id;
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// die('Sorry, something went wrong.');
	}
	if(!$result_set->num_rows) {
		// die('Sorry, the requested station wasn\'t found.');
	}
	while ($result = mysqli_fetch_assoc($result_set)) {
		$city_name = $result['ct_name'];
	}

	if ($city_id == 22) {
		$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id IN (".implode(',', $merged_city_ids).")";
	} else {
		$sql = "SELECT dvcloc_id FROM dvc_locations  WHERE ct_id =".$city_id;
	}

	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// $api_utilities->db_error();
	}
	if(!$result_set->num_rows) {
		// $api_utilities->throw_error('Sorry, no Station found in the city.');
	}

	$city_station_ids = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$city_station_ids []= $result['dvcloc_id'];
	}

	$city_station_data = []; $city_param = [];
	$required_data = get_avg_data_json($city_station_ids, $from_time, $upto_time, $duration, $all_parameters, true, $conversion_type);

	foreach ($required_data['station_data'] as $stn_id => $parameters_value) {
		$stn_counter[$stn_id] = 0;
		foreach ($parameters_value as $param_key => $value) {
			$param_counter[$param_key] = 0;
			if ($value != null || $value != []) {
				$avg_val[$param_key] = 0;
				$station_parameters[$stn_id] []= $param_key;
				$city_station_data[$stn_id][$param_key] = $value;
			}
		}
	}

	foreach ($station_parameters as $stn_id => $param) {
		$city_param = array_merge($param);
	}

	$sql = "SELECT dvcprm_name_noformat, dvcprm_key, dvcprm_unit_noformat FROM dvc_params WHERE dvcprm_key IN ('".implode('\',\'', $city_param)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// $db_error();
	}
	$required_params = [];
	if (isset($city_param) && (in_array('pm2.5', $city_param) || in_array('pm10', $city_param) || in_array('so2', $city_param) || in_array('no2', $city_param) || in_array('o3', $city_param) || in_array('co', $city_param))) {
		$required_params['hourly_aqi'] = [
			'name' => 'AQI',
			'unit' => ''
		];
		$city_param []= 'hourly_aqi';
	}
	$check_param_key = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		if(in_array($result['dvcprm_key'], $city_param)) {
			$any_one_required_param_key = $result['dvcprm_key'];
			$check_param_key []= $result['dvcprm_key'];
			if ($conversion_type == 'naqi') {
				if ($result['dvcprm_key'] == 'so2' || $result['dvcprm_key'] == 'no2' || $result['dvcprm_key'] == 'o3') {
					$required_params[$result['dvcprm_key']] = [
						'name' => $result['dvcprm_name_noformat'],
						'unit' => 'μg / m3'
					];
				} else if ($result['dvcprm_key'] == 'co') {
					$required_params[$result['dvcprm_key']] = [
						'name' => $result['dvcprm_name_noformat'],
						'unit' => 'mg / m3'
					];
				} else {
					$required_params[$result['dvcprm_key']] = [
						'name' => $result['dvcprm_name_noformat'],
						'unit' => $result['dvcprm_unit_noformat']
					];
				}
			} else {
				$required_params[$result['dvcprm_key']] = [
					'name' => $result['dvcprm_name_noformat'],
					'unit' => $result['dvcprm_unit_noformat']
				];
			}
		}
	}

	$file_name = str_replace(',', '_', $city_name).'_'.strftime('%d-%b-%Y-%I-%M-%P', $from_time).'_'.strftime('%d-%b-%Y-%I-%M-%P', $upto_time);

	//csv
	header("Content-Type: text/csv");
	header("Content-Disposition: attachment;filename=".$file_name.".csv");
	header("Cache-Control: no-cache, no-store, must-revalidate");
	header("Pragma: no-cache");
	header("Expires: 0");

	$csv_output = fopen("php://output", "w");

	foreach ($city_param as $value) {
		$city_wise_param_data[$value] =	[];
	}

	$city_data = [];
	foreach ($city_param as $param) {
		foreach ($city_station_data as $stn_id => $parameters_key_value) {
			if ($city_station_data[$stn_id][$param]) {
				for ($i = 0; $i < count($city_station_data[$stn_id][$param]); $i++) {
					$city_wise_param_data[$param][$i] []=$city_station_data[$stn_id][$param][$i];
				}
			}
		}
	}

	$last_param;
	foreach ($city_wise_param_data as $param => $param_value) {
		foreach ($param_value as $index => $value) {
			$avg_val[$param] = 0;
			$max_value[$param] = 0;
			$min_value[$param] = 999999;
			$param_counter[$param] = 0;
			for ($i = 0; $i < count($value); $i++) {
				if ($value[$i]['avg'] != null && $value[$i]['avg'] != "0.00") {
					$avg_val[$param] += abs($value[$i]['avg']);
					$param_counter[$param]++;

					if ($value[$i]['max'] != null && $max_value[$param] <= $value[$i]['max']) {
						$max_value[$param] = $value[$i]['max'];
					}

					if ($value[$i]['min'] != null && $min_value[$param] >= $value[$i]['min']) {
						$min_value[$param] = $value[$i]['min'];
					}
				}
			}
			if ($avg_val[$param] != null) {
				if ($param == 'co') {
					$city_data[$param] []= [
						'avg' => number_format(($avg_val[$param]/$param_counter[$param]), 4, '.', ''),
						'min' => $min_value[$param],
						'max' => $max_value[$param]
					];
				} else {
					$city_data[$param] []= [
						'avg' => number_format(($avg_val[$param]/$param_counter[$param]), 2, '.', ''),
						'min' => $min_value[$param],
						'max' => $max_value[$param]
					];
				}
			} else {
				$city_data[$param] []= [
					'avg' => null,
					'min' => null,
					'max' => null
				];
			}
		}
		if (!isset($last_param)) {
			$last_param = $param;
		}
	}

	$param_avg_values = [];
	for ($i = 0; $i < count($city_data[$last_param]); $i++) {
		foreach ($city_data as $param => $param_value) {
			$param_avg_values[$param] = $param_value[$i]['avg'];
		}
		$station_aqi_data = Aurassure\DataProcessor\AQICalculator::getNAQIFromConc($param_avg_values, true);
		$station_aqi = ($station_aqi_data['aqi'] ? $station_aqi_data['aqi'] : 0);
		$station_responsible_parameter = $station_aqi_data['responsible_param'];
		$city_data['hourly_aqi'] []= [
			'avg' => $station_aqi,
			'param' => $station_responsible_parameter,
		];
	}

	
	// creating summary report
	$station_data = $city_data;

	fputcsv($csv_output, ['Aggregated Data of '.$city_name]);
	fputcsv($csv_output, ['From: '.strftime('%d-%b-%Y %I:%M %P', $from_time)]);
	fputcsv($csv_output, ['Upto: '.strftime('%d-%b-%Y %I:%M %P', $upto_time)]);

	$parameters_array = ['Date & Time'];
	$units_array = ['dd-mm-yyyy hh:mm:ss'];
	$min_max_header_array = [''];
	foreach ($required_params as $key => $required_param) {
		$parameters_array []= $required_param['name'];
		if ($key != 'rain' && $key != 'hourly_aqi') {
			$parameters_array []= '';
			$parameters_array []= '';
		} else if ($key == 'hourly_aqi') {
			$parameters_array []= '';
		}
		$units_array []= $required_param['unit'];
		if ($key != 'rain' && $key != 'hourly_aqi') {
			$units_array []= '';
			$units_array []= '';
		} else if ($key == 'hourly_aqi') {
			$units_array []= '';
		}
		if ($key == 'rain') {
			$min_max_header_array []= 'Aggregated';
		} else if ($key == 'hourly_aqi') {
			$min_max_header_array []= 'AQI';
			$min_max_header_array []= 'Major Pollutant';
		} else {
			$min_max_header_array []= 'Avg.';
			$min_max_header_array []= 'Min.';
			$min_max_header_array []= 'Max.';
		}
	}
	fputcsv($csv_output, $parameters_array);
	fputcsv($csv_output, $units_array);
	fputcsv($csv_output, $min_max_header_array);

	if(sizeof($station_data)) {
		$time = $from_time;
		for ($i = 0; $i < count($station_data[$last_param]); $i++) {
			$param_values_array = [strftime("%d-%b-%Y %I:%M %P", $time).' to '.strftime("%d-%b-%Y %I:%M %P", $time+$duration)];
			foreach ($required_params as $key => $required_param) {
				foreach ($station_data as $parameter => $parameter_values) {
					if ($key == $parameter) {
						$round = ($parameter == 'co2') ? 4 : ($parameter == 'hourly_aqi' ? 0 : 2);
						$param_rounded_value = number_format($parameter_values[$i]['avg'], $round, '.', '');
						$param_values_array []= $param_rounded_value;
						if ($parameter == 'hourly_aqi') {
							if ($parameter_values[$i]['param'] == "") {
								$param_values_array []= 'NA';
							} else {
								$param_values_array []= $parameter_values[$i]['param'];
							}
						} else if ($parameter != 'rain' && $parameter != 'hourly_aqi') {
							$param_values_array []= number_format($parameter_values[$i]['min'], $round, '.', '');
							$param_values_array []= number_format($parameter_values[$i]['max'], $round, '.', '');
						}
					}
				}
			}
			fputcsv($csv_output, $param_values_array);
			$time += $duration;
		}
	} else {
		fputcsv($csv_output, ['No data available within the selected period.']);
	}
	
	fputcsv($csv_output, ['Report generated by Aurassure at '.strftime("%d-%b-%Y %I:%M:%S %P", time()).' IST']);
	fclose($csv_output);

} else if(isset($station_ids) && is_array($station_ids) && !empty($station_ids)
	&& isset($parameters) && !empty($parameters)
	&& isset($from_time) && $from_time != ''
	&& isset($upto_time) && $upto_time != '' && $upto_time > $from_time) {

	//get station names
	if ($data_type === 'raw_data') {
		$sql = "SELECT dvcloc_name FROM dvc_locations WHERE dvcloc_id=".$station_ids[0];
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			die('Sorry, something went wrong.');
		}
		if(!$result_set->num_rows) {
			die('Sorry, the requested station wasn\'t found.');
		}
		while ($result = mysqli_fetch_assoc($result_set)) {
			$station_name = $result['dvcloc_name'];
		}
	} else {
		$sql = "SELECT dvcloc_id, dvcloc_name FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $station_ids).") AND dvcloc_data_source=0";
		$result_set = $aurassure_db->query($sql);
		if(!$result_set) {
			// die('Sorry, something went wrong.');
		}
		if(!$result_set->num_rows) {
			// die('Sorry, the requested station wasn\'t found.');
		}
		while ($result = mysqli_fetch_assoc($result_set)) {
			$station_names[$result['dvcloc_id']] = $result['dvcloc_name'];
		}
	}
	
	//get station params and param units
	$sql = "SELECT dvcloc_id, dvcdat_data FROM dvc_raw_data WHERE dvcloc_id IN (".implode(',', $station_ids).") ORDER BY dvcdat_time DESC LIMIT 1";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// $db_error();
	}
	if(!$result_set->num_rows) {
		// $throw_error('Sorry, no data has been received from the station yet.');
	}
	$result = $result_set->fetch_assoc();
	$station_param_keys = [];
	$station_latest_data = json_decode($result['dvcdat_data'], true);
	$station_param_keys []='hourly_aqi';
	foreach ($station_latest_data as $param_key => $param_value) {
		$station_param_keys []=$param_key;
	}
	// echo "<pre>";
	// print_r($station_latest_data);
	// echo "</pre>";
	// echo "<pre>";
	// print_r($station_param_keys);
	// echo "</pre>";

	$sql = "SELECT dvcprm_name_noformat, dvcprm_key, dvcprm_unit_noformat FROM dvc_params WHERE dvcprm_key IN ('".implode('\',\'', $parameters)."')";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		// $db_error();
	}
	$required_params = [];
	$check_param_key = [];
	$any_one_required_param_key = '';
	if (in_array('hourly_aqi', $parameters)) {
		$required_params['hourly_aqi'] = [
			'name' => 'AQI',
			'unit' => ''
		];
		$any_one_required_param_key = 'hourly_aqi';
	}

	while($result = mysqli_fetch_assoc($result_set)) {
		if(in_array($result['dvcprm_key'], $parameters)) {
			$any_one_required_param_key = $result['dvcprm_key'];
			$check_param_key []= $result['dvcprm_key'];
			if ($conversion_type == 'naqi') {
				if ($result['dvcprm_key'] == 'so2' || $result['dvcprm_key'] == 'no2' || $result['dvcprm_key'] == 'o3') {
					$required_params[$result['dvcprm_key']] = [
						'name' => $result['dvcprm_name_noformat'],
						'unit' => 'μg / m3'
					];
				} else if ($result['dvcprm_key'] == 'co') {
					$required_params[$result['dvcprm_key']] = [
						'name' => $result['dvcprm_name_noformat'],
						'unit' => 'mg / m3'
					];
				} else {
					$required_params[$result['dvcprm_key']] = [
						'name' => $result['dvcprm_name_noformat'],
						'unit' => $result['dvcprm_unit_noformat']
					];
				}
			} else {
				$required_params[$result['dvcprm_key']] = [
					'name' => $result['dvcprm_name_noformat'],
					'unit' => $result['dvcprm_unit_noformat']
				];
			}
		}
	}

	// echo "<pre>";
	// print_r($parameters_array);
	// echo "</pre>";

	//fix filename
	if ($data_type == 'raw_data') {
		$file_name = str_replace(',', '_', $station_name).'_'.strftime('%d-%b-%Y-%I-%M-%P', $from_time).'_'.strftime('%d-%b-%Y-%I-%M-%P', $upto_time);
	} else {
		$file_name = str_replace(',', '_', 'Aggregated Data').'_'.strftime('%d-%b-%Y-%I-%M-%P', $from_time).'_'.strftime('%d-%b-%Y-%I-%M-%P', $upto_time);
	}

	//csv
	header("Content-Type: text/csv");
	header("Content-Disposition: attachment;filename=".$file_name.".csv");
	header("Cache-Control: no-cache, no-store, must-revalidate");
	header("Pragma: no-cache");
	header("Expires: 0");

	$csv_output = fopen("php://output", "w");

	if ($data_type === 'raw_data') {
		//get raw data
		$required_data = get_raw_data_json($station_ids[0], $from_time, $upto_time, $parameters, false, $conversion_type);

		fputcsv($csv_output, ['Archive Data of '.$station_name]);
		fputcsv($csv_output, ['From: '.strftime('%H:%M:%S, %d %m %Y', $from_time)]);
		fputcsv($csv_output, ['Upto: '.strftime('%H:%M:%S, %d %m %Y', $upto_time)]);

		$parameters_array = ['Date & Time'];
		$units_array = ['hh:mm:ss dd-mm-yyyy'];
		foreach ($required_params as $required_param) {
			$parameters_array []= $required_param['name'];
			$units_array []= $required_param['unit'];
		}
		fputcsv($csv_output, $parameters_array);
		fputcsv($csv_output, $units_array);

		if(sizeof($required_data['time_stamps'])) {
			foreach ($required_data['time_stamps'] as $data_index => $time_stamp) {
				$param_values_array = [strftime("%H:%M:%S, %d %m %Y", $time_stamp)];
				foreach($required_params as $required_param_key => $required_param) {
					$param_values_array []= $required_data['param_values'][$required_param_key][$data_index];
				}
				fputcsv($csv_output, $param_values_array);
			}
		} else {
			fputcsv($csv_output, ['No data available within the selected period.']);
		}
	} else {
		// get avg data
		$required_data = get_avg_data_json($station_ids, $from_time, $upto_time, $duration, $parameters, false, $conversion_type);
		
		// creating summary report
		$summary_report = $required_data["station_summary"];
		$station_data = $required_data["station_data"];

		fputcsv($csv_output, ['Aggregated Data of Stations']);
		fputcsv($csv_output, ['From: '.strftime('%H:%M:%S, %d-%m-%Y', $from_time)]);
		fputcsv($csv_output, ['Upto: '.strftime('%H:%M:%S, %d-%m-%Y', $upto_time)]);

		$summary_report_head = ['Station','Parameters','Avg.','Min.', 'Min. At','Max.','Max. At'];
		$summary_report_details =[];

		fputcsv($csv_output, []);
		fputcsv($csv_output, ['Summary Report']);
		fputcsv($csv_output, $summary_report_head);			
			
		foreach($summary_report as $station_id => $station_summary) {
			$temp_id = '';
			$station_name = '';
			foreach($station_summary as $param_key => $param_value) {
				if (in_array($param_key, $parameters)) {
					$max_date='';
					$min_date='';
					if ($conversion_type == 'naqi') {
						if ($param_key == 'so2') {
							$param_avg = number_format(($param_value['avg']*2.6202863), 2, '.', '');
							$param_min = number_format(($param_value['min']*2.6202863), 2, '.', '');
							$param_max = number_format(($param_value['max']*2.6202863), 2, '.', '');
						} else if ($param_key == 'no2') {
							$param_avg = number_format(($param_value['avg']*1.88161554), 2, '.', '');
							$param_min = number_format(($param_value['min']*1.88161554), 2, '.', '');
							$param_max = number_format(($param_value['max']*1.88161554), 2, '.', '');
						} else if ($param_key == 'co') {
							$param_avg = number_format(($param_value['avg']*1.14560327), 2, '.', '');
							$param_min = number_format(($param_value['min']*1.14560327), 2, '.', '');
							$param_max = number_format(($param_value['max']*1.14560327), 2, '.', '');
						} else if ($param_key == 'o3') {
							$param_avg = number_format(($param_value['avg']*1.96319018), 2, '.', '');
							$param_min = number_format(($param_value['min']*1.96319018), 2, '.', '');
							$param_max = number_format(($param_value['max']*1.96319018), 2, '.', '');
						} else {
							$param_avg = $param_value['avg'];
							$param_min = $param_value['min'];
							$param_max = $param_value['max'];
						}
					} else {
						$param_avg = $param_value['avg'];
						$param_min = $param_value['min'];
						$param_max = $param_value['max'];
					}

					if ($param_key == 'rain') {
						if (is_array($param_value['min_time'])) {
							$min_date = (isset($param_value['min_time'][0]) && $param_value['min_time'][0] > 0)?strftime("%H:%M", $param_value['min_time'][0]). ' - ' .strftime("%H:%M, %d-%m-%Y", $param_value['min_time'][1]):'NA';
							$max_date = (isset($param_value['max_time'][0]) && $param_value['max_time'][0] > 0)?strftime("%H:%M", $param_value['max_time'][0]). ' - ' .strftime("%H:%M, %d-%m-%Y", $param_value['max_time'][1]):'NA';
						} else {
							$min_date = (isset($param_value['min_time']) && $param_value['min_time'] > 0)?strftime("%H:%M:%S, %d-%m-%Y", $param_value['min_time']):'NA';
							$max_date = (isset($param_value['max_time']) && $param_value['max_time'] > 0 )?strftime("%H:%M:%S, %d-%m-%Y", $param_value['max_time']):'NA';
						}
					} else {
						$min_date = (isset($param_value['min_time']) && $param_value['min_time'] > 0)?strftime("%H:%M:%S, %d-%m-%Y", $param_value['min_time']):'NA';
						$max_date = (isset($param_value['max_time']) && $param_value['max_time'] > 0 )?strftime("%H:%M:%S, %d-%m-%Y", $param_value['max_time']):'NA';
					}

					if ($station_id != $temp_id) {
						$temp_id = $station_id;
						$station_name = $station_names[$station_id];
					}

					$summary_report_details = [
						$station_name,
						$required_params[$param_key]['name'].'('.$required_params[$param_key]['unit'].')',
						( isset($param_value['avg']) ? $param_avg :'NA' ),
						( isset($param_value['min']) && $param_value['min'] != 999999999 ? $param_min : 'NA'),
						( isset($param_value['min_time']) ? $min_date :'NA' ),
						( isset($param_value['max']) ? $param_max :'NA'),
						( isset($param_value['max_time']) ? $max_date :'NA' )
					];
					fputcsv($csv_output, $summary_report_details);
					$summary_report_details = [];
				}
			}
		}

		fputcsv($csv_output, []);
		fputcsv($csv_output, ['Station Data']);

		foreach ($station_data as $station_id => $stn_param_data) {
			$parameters_array = ['Date & Time'];
			$parameters_array []= 'Station';
			$units_array = ['hh:mm dd-mm-yyyy'];
			$units_array []= '';
			$min_max_header_array = [''];
			$min_max_header_array []= '';

			foreach ($required_params as $key => $required_param) {
				foreach ($stn_param_data as $param_key => $param_value) {
					if ($key == $param_key) {
						$parameters_array []= $required_param['name'];
						if ($key != 'rain' && $key != 'hourly_aqi') {
							$parameters_array []= '';
							$parameters_array []= '';
						} else if ($key == 'hourly_aqi') {
							$parameters_array []= '';
						}
						$units_array []= $required_param['unit'];
						if ($key != 'rain' && $key != 'hourly_aqi') {
							$units_array []= '';
							$units_array []= '';
						} else if ($key == 'hourly_aqi') {
							$units_array []= '';
						}
						if ($key == 'rain') {
							$min_max_header_array []= 'Aggregated';
						} else if ($key == 'hourly_aqi') {
							$min_max_header_array []= 'AQI';
							$min_max_header_array []= 'Major Pollutant';
						} else {
							$min_max_header_array []= 'Avg.';
							$min_max_header_array []= 'Min.';
							$min_max_header_array []= 'Max.';
						}
					}
				}
			}
			// fputcsv($csv_output, []);
			// fputcsv($csv_output, ['Aggregate Station Data of '.$station_names[$station_id]]);
			fputcsv($csv_output, $parameters_array);
			fputcsv($csv_output, $units_array);
			fputcsv($csv_output, $min_max_header_array);

			if (count($stn_param_data)) {
				if ($duration == 86400) {
					$time = $from_time - 86400;
				} else {
					$time = $from_time;
				}
				for ($i = 0; $i < count($stn_param_data[$check_param_key[0]]); $i++) {
					$param_values_array = [strftime("%H:%M, %d-%m-%Y", $time).' to '.strftime("%H:%M, %d-%m-%Y", $time + $duration)];
					$param_values_array []= $station_names[$station_id];
					foreach ($required_params as $key => $required_param) {
						foreach ($stn_param_data as $param_key => $param_value) {
							if ($key == $param_key) {
								$round = ($key == 'co') ? 4 : ($key == 'hourly_aqi' ? 0 : 2);
								$param_rounded_value = number_format($stn_param_data[$key][$i]['avg'], $round, '.', '');
								$param_values_array []= $param_rounded_value;
								if ($key == 'hourly_aqi') {
									if ($stn_param_data[$key][$i]['param'] == "") {
										$param_values_array []= 'NA';
									} else {
										$param_values_array []= $stn_param_data[$key][$i]['param'];
									}
								} else if ($key != 'rain' && $key != 'hourly_aqi') {
									$param_values_array []= number_format($stn_param_data[$key][$i]['min'], $round, '.', '');
									$param_values_array []= number_format($stn_param_data[$key][$i]['max'], $round, '.', '');
								}
							}
						}
					}
					fputcsv($csv_output, $param_values_array);
					$time += $duration;
				}
			} else {
				fputcsv($csv_output, ['No data available within the selected period.']);
			}
		}

	}

	fputcsv($csv_output, ['Report generated by Aurassure at '.strftime("%d-%b-%Y %I:%M:%S %P", time()).' IST']);
	fclose($csv_output);
} else {
	echo 'Incomplete Request.';
}
$api_utilities->log_api_tracking_data(14, $access_time, 'update', $user_id, $log_id);