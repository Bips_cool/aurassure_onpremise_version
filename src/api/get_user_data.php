<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_user_data.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		user_city: 2
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(31, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("/var/www/aurassure/_includes/form_data_validation_functions.php");
	// Check the value of required fields are set or not
	$data = json_decode(file_get_contents('php://input'), true);
	if(!has_presence($data['user_city'])) {
		$api_utilities->incomplete_request("Invalid User City!");
	}

	// Sanitize the fields
	$user_city = mysqli_real_escape_string($aurassure_db->connection, $data['user_city']);

	if ($user_city == 5) {
		$sql = "SELECT `usr_id`, `usr_first_name`, `usr_middle_name`, `usr_last_name`, `usr_email`, `usr_mobile`, `usr_role`, `usr_designation`, `usr_for_city`, `usr_for_grp`, `usr_department`, `usr_access`, `usr_under` FROM `usr_login` ORDER BY `usr_first_name` ASC";
	} else {
		$sql = "SELECT `usr_id`, `usr_first_name`, `usr_middle_name`, `usr_last_name`, `usr_email`, `usr_mobile`, `usr_role`, `usr_designation`, `usr_for_city`, `usr_for_grp`, `usr_department`, `usr_access`, `usr_under` FROM `usr_login` WHERE `usr_type`=1 AND `usr_for_city`=".$user_city." ORDER BY `usr_first_name` ASC";
	}
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(mysqli_num_rows($result_set)) {
		$usr_arr = [];
		while ($result = mysqli_fetch_assoc($result_set)) {
			array_push($usr_arr, $result);
		}
		$json_response['users'] = $usr_arr;

		$sql = "SELECT DISTINCT(`usr_id`), MAX(`login_time`) as `time` FROM `usr_login_history` WHERE `usr_id` IN (SELECT `usr_id` FROM `usr_login` WHERE `usr_type`=1 AND `usr_for_city`=".$user_city." ORDER BY `usr_first_name` ASC) GROUP BY `usr_id`";
		$login_result_set = $aurassure_db->query($sql);
		if(!$login_result_set) {
			$api_utilities->db_error();
		}
		if(mysqli_num_rows($login_result_set)) {
			$usr_login_arr = [];
			while ($login_result = mysqli_fetch_assoc($login_result_set)) {
				$user = [];
				$user['id'] = $login_result['usr_id'];
				if (time() < ($login_result['time'] + 86400) && round((time() - $login_result['time']) / 3600)) {
					$user['time'] = round((time() - $login_result['time']) / 3600)." Hours ago";
				} else if(round((time() - $login_result['time']) / 3600) == 0) {
					$user['time'] = round((time() - $login_result['time']) / 60)." Minutes ago";
				} else {
					$user['time'] = $login_result['time'];
				}
				
				$usr_login_arr[$user['id']] = $user['time'];
			}
			$json_response['users_login'] = $usr_login_arr;
		}
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, no user found!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(31, $access_time, 'update', $user_id, $log_id);