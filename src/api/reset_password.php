<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/reset_password.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		user_id: that.state.reset_password,
		user_pass: document.getElementById('reset_password').value
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(34, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("_includes/form_data_validation_functions.php");

	// Check the value of required fields are set or not
	$data = json_decode(file_get_contents('php://input'), true);
	if(!has_presence($data['user_id'])) {
		$api_utilities->incomplete_request("Please select a Valid User!");
	} else if(!has_presence($data['user_pass'])) {
		$api_utilities->incomplete_request("Please give a Password!");
	}

	// Sanitize the fields
	$user_id = mysqli_real_escape_string($aurassure_db->connection, $data['user_id']);
	$user_pass = password_hash($data['user_pass'], PASSWORD_BCRYPT);

	// Update data of user
	$sql = "UPDATE usr_login SET usr_pass='".$user_pass."' WHERE usr_id=".$user_id;
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to submit data!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(34, $access_time, 'update', $user_id, $log_id);