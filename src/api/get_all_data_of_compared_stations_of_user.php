<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_data_of_compared_stations_of_user.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include'
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(18, $access_time, 'insert', $user_id);
use Aurassure\DataProcessor\EndUserResult;

if($_SERVER['REQUEST_METHOD'] === "POST") {
	$api_utilities->set_success_ststus_in_response();
	$sql = "SELECT compared_locations_id FROM usr_compare_locations WHERE usr_id=".$api_utilities->session_handler->user_session_data['ui']." LIMIT 1";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	$loc_ids = [];
	$json_response['stations'] = [];
	if(mysqli_num_rows($result_set)) {
		$result = mysqli_fetch_assoc($result_set);
		$loc_ids = array_filter(json_decode($result['compared_locations_id']));

		if($api_utilities->session_handler->user_session_data['ci'] == 5) {
			//send all devices for admin portal
			$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_params FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $loc_ids).") ORDER BY FIND_IN_SET(dvcloc_id,'".implode(',', $loc_ids)."')";
		} else {
			$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_type, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_params FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $loc_ids).") AND ct_id=".$api_utilities->session_handler->user_session_data['ci']." ORDER BY FIND_IN_SET(dvcloc_id,'".implode(',', $loc_ids)."')";
		}
		// $json_response['sql'] = $sql;
		$compared_locations_result_set = $aurassure_db->query($sql);
		if(mysqli_num_rows($compared_locations_result_set)) {
			//get all parameters
			$sql = "SELECT dvcprm_name, dvcprm_key, dvcprm_unit FROM dvc_params";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				$api_utilities->db_error();
			}
			$all_parameters = [];
			while ($result = $result_set->fetch_assoc()) {
				$all_parameters[$result['dvcprm_key']] = [
					'name' => $result['dvcprm_name'],
					'key' => $result['dvcprm_key'],
					'unit' => $result['dvcprm_unit']
				];
			}

			//get latest parameter details
			$sql = "SELECT `dvcloc_id`, `dvcdvc_last_raw_data`, `dvcdvc_last_data_receive_time` FROM `dvc_devices` WHERE `dvcloc_id` IN (".implode(',', $loc_ids).")";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				$api_utilities->db_error();
			}
			$latest_param = [];
			while ($result = $result_set->fetch_assoc()) {
				$latest_param[$result['dvcloc_id']] = [
					'latest_param_value' => json_decode($result['dvcdvc_last_raw_data'], true),
					'last_data_receive_time' => $result['dvcdvc_last_data_receive_time'],
					'data_receive_connection_status' => ($_SERVER['REQUEST_TIME'] - $result['dvcdvc_last_data_receive_time']) <= 7200 ? 'online' : 'offline'
				];
			}

			//get params and units of all stations
			$sql = "SELECT dvcloc_id, dvcdat_data FROM dvc_raw_data WHERE dvcloc_id IN (".implode(',', $loc_ids).") GROUP BY dvcloc_id ORDER BY dvcdat_time DESC";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				$api_utilities->db_error();
			}
			$station_params = [];
			while ($result = $result_set->fetch_assoc()) {
				$station_params[$result['dvcloc_id']] = [];
				$current_station_latest_data = json_decode($result['dvcdat_data'], true);
				foreach ($current_station_latest_data as $param_key => $param_value) {
					if($all_parameters[$param_key]) {
						$station_params[$result['dvcloc_id']] []= $all_parameters[$param_key];
					}
				}
			}

			//get hourly param indices of station
			$sql = "SELECT dvcloc_id, dhad_aqi, dhad_responsible_param, dhad_param_aqis, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id IN (".implode(',', $loc_ids).") AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." ORDER BY dhad_upto_time DESC";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				$api_utilities->db_error();
			}
			$station_hourly_aqis = [];
			$station_param_hourly_aqis = [];
			foreach ($loc_ids as $station_id) {
				$station_hourly_aqis[$station_id] = [];
				$station_param_hourly_aqis[$station_id] = [];
				foreach ($station_params as $station_id => $param_details) {
					$station_param_hourly_aqis[$station_id][$param_details['key']] = [];
				}
			}
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_hourly_aqis[$result['dvcloc_id']][$result['dhad_upto_time']] = [
					'aqi' => $result['dhad_aqi'],
					'param' => $result['dhad_responsible_param']
				];

				$temp_station_param_hourly_aqis = json_decode($result['dhad_param_aqis'], true);
				foreach ($station_params[$result['dvcloc_id']] as $station_id => $param_details) {
					if($temp_station_param_hourly_aqis[$param_details['key']]) {
						$station_param_hourly_aqis[$result['dvcloc_id']][$param_details['key']][$result['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param_details['key']];
					}
				}
			}

			//set final result set
			while($result = mysqli_fetch_assoc($compared_locations_result_set)) {
				$station = [];
				$station['id'] = $result['dvcloc_id'];
				$temp_station_name = '';
				if($result['dvcloc_name']) {
					$temp_station_name .= $result['dvcloc_name'].', ';
				}
				if($result['dvcloc_city']) {
					$temp_station_name .= $result['dvcloc_city'];
				}
				$temp_station_name = rtrim($temp_station_name, ', ');
				$station['name'] = $temp_station_name;
				$station['type'] = $result['dvcloc_type'];
				$station['latest_param_value'] = $latest_param[$result['dvcloc_id']]['latest_param_value'];
				$station['last_data_receive_time'] = $latest_param[$result['dvcloc_id']]['last_data_receive_time'];
				$station['data_receive_connection_status'] = $latest_param[$result['dvcloc_id']]['data_receive_connection_status'];
				$station['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
				$station['last_data_update_time'] = $result['dvcloc_last_data_update_time'];
				$station['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
				$station['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);
				$station['aqi_status'] = EndUserResult::getAQIStatus($result['dvcloc_aqi']);
				$station['params'] = $station_params[$result['dvcloc_id']];

				$station['hourly_aqis'] = [];
				$station['hourly_param_aqis'] = [];
				foreach ($station_params[$result['dvcloc_id']] as $station_id => $param_details) {
					$station['hourly_param_aqis'][$param_details['key']] = [];
				}
				$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
				for ($i=0; $i<24; $i++) {
					if($station_hourly_aqis[$result['dvcloc_id']][$latest_hour]['aqi']) {
						$station['hourly_aqis'] [] = [
							'aqi' => $station_hourly_aqis[$result['dvcloc_id']][$latest_hour]['aqi'],
							'param' => $station_hourly_aqis[$result['dvcloc_id']][$latest_hour]['param']
						];
					} else {
						$station['hourly_aqis'] [] = [
							'aqi' => null,
							'param' => null
						];
					}

					foreach ($station_params[$result['dvcloc_id']] as $station_id => $param_details) {
						if($station_param_hourly_aqis[$result['dvcloc_id']][$param_details['key']][$latest_hour]) {
							$station['hourly_param_aqis'][$param_details['key']] [] = $station_param_hourly_aqis[$result['dvcloc_id']][$param_details['key']][$latest_hour];
						} else {
							$station['hourly_param_aqis'][$param_details['key']] [] = null;
						}
					}

					$latest_hour -= 3600;
				}

				array_push($json_response['stations'], $station);
			}
		}
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(18, $access_time, 'update', $user_id, $log_id);