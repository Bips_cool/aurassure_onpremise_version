<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/save_group_details.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		group_id: 2,
		name: 'name',
		subdomain: 'subdomain',
		lat: '12.111111111',
		long: '12.111111',
		zoom_lvl: 11,
		is_public: is_public,
		is_premium: is_premium,
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(240, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$group_id = $received_data['group_id'];
$grp_name = $received_data['name'];
$grp_lat = $received_data['lat'];
$grp_long = $received_data['long'];
$grp_zoom_lvl = $received_data['zoom_lvl'];
$grp_has_public = $received_data['is_public'];
$grp_is_premium = $received_data['is_premium'];

if (isset($group_id) && $group_id != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$group_id = mysqli_real_escape_string($aurassure_db->connection, $group_id);
	$grp_name = mysqli_real_escape_string($aurassure_db->connection, $grp_name);
	$grp_subdomain = $api_utilities->slug($grp_name);
	$grp_lat = mysqli_real_escape_string($aurassure_db->connection, $grp_lat);
	$grp_long = mysqli_real_escape_string($aurassure_db->connection, $grp_long);
	$grp_zoom_lvl = mysqli_real_escape_string($aurassure_db->connection, $grp_zoom_lvl);
	$grp_has_public = $grp_has_public ? 1 : 0;
	$grp_is_premium = $grp_is_premium ? 1 : 0;

	$sql = "UPDATE `groups` SET `grp_name`='".$grp_name."', `grp_subdomain`='".$grp_subdomain."', `grp_lat`='".$grp_name."', `grp_long`='".$grp_long."', `grp_zoom_lvl`='".$grp_zoom_lvl."', `grp_has_public`='".$grp_has_public."', `grp_is_premium`='".$grp_is_premium."' WHERE `grp_id`=".$group_id;

	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to save data!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(240, $access_time, 'update', $user_id, $log_id);