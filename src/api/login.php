<?php
set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(32, $access_time, 'insert', 0);
if($_SERVER['REQUEST_METHOD'] == 'POST') {
	require_once("_includes/form_data_validation_functions.php");

	function invalid_login() {
		global $api_utilities;
		$api_utilities->throw_error('Invalid Username or Password.');
	}

	$decoded_posted_data = json_decode($_POST['d'], true);
	$email = $decoded_posted_data['email'];
	$password = $decoded_posted_data['password'];
	$city_id = $decoded_posted_data['city_id'];

	if(!is_valid_email($email)) {
		$api_utilities->incomplete_request("Please give a valid email id.");
	} elseif(!has_presence($password)) {
		$api_utilities->incomplete_request("Please give a valid password.");
	} elseif(!isset($city_id) || !trim($city_id)) {
		$api_utilities->incomplete_request();
	}

	// sanitize the fields
	$email = mysqli_real_escape_string($aurassure_db->connection, $email);
	$city_id = mysqli_real_escape_string($aurassure_db->connection, $city_id);

	// check if the email or mobile no. already exists or not
	$sql = "SELECT usr_id, usr_first_name, usr_pass FROM usr_login WHERE usr_email='".$email."' AND usr_type=1 AND usr_for_city=".$city_id;
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
	}
	if(mysqli_num_rows($result_set)) {
		$result = mysqli_fetch_assoc($result_set);
		if(password_verify($password, $result['usr_pass'])) {
			$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id=$city_id ORDER BY dvcloc_id ASC LIMIT 1";
			$result_set = $aurassure_db->query($sql);
			if($result_set) {
				$result2 = mysqli_fetch_assoc($result_set);
				// $_SESSION['user_default_city'] = $result['dvcloc_id'];
				$json_response['user_default_city'] = $result2['dvcloc_id'];
			}

			$user_session_data = [];
			$user_session_data['un'] = $result['usr_first_name'];
			$user_session_data['ui'] = $result['usr_id'];
			$user_session_data['ci'] = $city_id;
			$user_session_data['user_default_city'] = $result2['dvcloc_id'];
			$json_response['user_default_city'] = $result2['dvcloc_id'];
			if($api_utilities->session_handler->create_session($result['usr_id'], $user_session_data)){
				// insert into login history table
				$sql = "INSERT INTO usr_login_history (usr_id, login_from, login_time, login_ip, login_from_ua) VALUES (".$result['usr_id'].",1,".$_SERVER['REQUEST_TIME'].",'".$_SERVER['REMOTE_ADDR']."','".$_SERVER['HTTP_USER_AGENT']."')";
				$aurassure_db->query($sql);
				// success message
				$api_utilities->set_success_ststus_in_response();
			}else{
				$api_utilities->throw_error('Some thing went wrong');
			}
		} else {
			// invalid password
			$sql = "INSERT INTO usr_valid_user_failed_login_history (usr_id, uvuflh_time, uvuflh_ip, uvuflh_from_ua) VALUES (".$result['usr_id'].", ".$_SERVER['REQUEST_TIME'].", '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."')";
			$aurassure_db->query($sql);
			invalid_login();
		}
	} else {
		// invalid username
		$sql = "INSERT INTO usr_invalid_user_failed_login_history (uiuflh_user_name, uiuflh_time, uiuflh_ip, uiuflh_from_ua) VALUES ('".$email."', ".$_SERVER['REQUEST_TIME'].", '".$_SERVER['REMOTE_ADDR']."', '".$_SERVER['HTTP_USER_AGENT']."')";
		$aurassure_db->query($sql);
		invalid_login();
	}
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(32, $access_time, 'update', 0, $log_id);