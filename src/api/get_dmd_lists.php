<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_dmd_lists.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(237, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
$sql = "SELECT dak_id, dak_key, dak_stn_ids, dak_last_accessed_at FROM dmd_api_keys";
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$dmd_list = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_dmd_array = [];
		$temp_dmd_array['id'] = $result['dak_id'];
		$temp_dmd_array['key'] = $result['dak_key'];
		$temp_dmd_array['station_ids'] = $result['dak_stn_ids'];
		$temp_dmd_array['last_accessed_at'] = $result['dak_last_accessed_at'];

		$dmd_list []= $temp_dmd_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response['dmd_list'] = $dmd_list;
} else {
	$api_utilities->db_error();
	// $json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(237, $access_time, 'update', $user_id, $log_id);