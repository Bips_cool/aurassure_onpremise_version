<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/save_station_alert_configurations.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include',
	body: JSON.stringify({
		station_id: 2,
		alert_users: [{
			id: 1,
			email: true,
			phone: false
		}],
		thresholds: {
			'pm2.5': [80,100],
			'pm10': [80,100]
		}
	})
}).then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

set_include_path("/var/www/aurassure/");
require_once("_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(35, $access_time, 'insert', $user_id);

if($_SERVER['REQUEST_METHOD'] === "POST") {
	require_once("_includes/form_data_validation_functions.php");

	// Check the value of required fields are set or not
	$data = json_decode(file_get_contents('php://input'), true);
	if(!$data['station_id']) {
		$api_utilities->incomplete_request("Please select a valid station!");
	}

	// Sanitize the fields
	$station_id = mysqli_real_escape_string($aurassure_db->connection, $data['station_id']);
	$parameter_thresholds = $data['parameter_thresholds'];
	// $alert_users = json_encode($data['alert_users']);
	// $thresholds = [];
	// foreach ($data['thresholds'] as $param => $threshold) {
	// 	$thresholds[$param] = [floatval($threshold[0]), floatval($threshold[1])];
	// }

	$sql = "UPDATE `dvc_locations` SET `dvcloc_parameter_thresholds`='".json_encode($parameter_thresholds)."' WHERE `dvcloc_id`=".$station_id;
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		$api_utilities->set_success_ststus_in_response();
	} else {
		$api_utilities->throw_error('Sorry, unable to save data!');
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request("Invalid Request!");
}
$api_utilities->log_api_tracking_data(35, $access_time, 'update', $user_id, $log_id);