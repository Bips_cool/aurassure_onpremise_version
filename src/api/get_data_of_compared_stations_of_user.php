<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_data_of_compared_stations_of_user.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(25, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_city, dvcloc_aqi FROM dvc_locations WHERE dvcloc_id IN (SELECT dvcloc_id FROM usr_compare_locations WHERE usr_id=".$api_utilities->session_handler->user_session_data['ui'].")";
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$compared_stations = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['dvcloc_id'];
		$temp_station_name = '';
		if($result['dvcloc_name']) {
			$temp_station_name .= $result['dvcloc_name'].', ';
		}
		if($result['dvcloc_city']) {
			$temp_station_name .= $result['dvcloc_city'];
		}
		$temp_station_name = rtrim($temp_station_name, ', ');
		$temp_location_array['name'] = $temp_station_name;
		$temp_location_array['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
		$temp_location_array['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);

		$compared_stations []= $temp_location_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response["compared_stations"] = $compared_stations;
} else {
	$api_utilities->db_error();
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(25, $access_time, 'update', $user_id, $log_id);