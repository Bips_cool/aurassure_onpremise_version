<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_latest_data_of_station_for_parameter.php', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	credentials: 'include',
	body: JSON.stringify({
		station_id: 1,
		parameters: ['pm2.5'],
		last_data_update_time: 1234567890,
		last_hourly_data_update_time: 1234567890
	})
})
.then(function(Response) {
	return Response.json()
}).then(function(json) {
	console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");
require_once(getcwd().'/_generate_raw_data_json.php');

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(30, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
use Aurassure\DataProcessor\Tools;

$received_data = json_decode(file_get_contents('php://input'), true);
$station_id = $received_data['station_id'];
$parameters = $received_data['parameters'];
$last_data_update_time = $received_data['last_data_update_time'];
$last_hourly_data_update_time = $received_data['last_hourly_data_update_time'];

if(isset($station_id) && $station_id != '' && isset($parameters) && $parameters != '' && isset($last_data_update_time) && $last_data_update_time != '') {
	$api_utilities->set_success_ststus_in_response();

	//sanitize received data
	$station_id = mysqli_real_escape_string($aurassure_db->connection, $station_id);

	$station_temp_param_keys = [];
	if ($station_id == 220) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=220 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				if ($param_key != 'rain') {
					$station_temp_param_keys []= $param_key;
				}
			}
		}
	} else if ($station_id == 221) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=221 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				if ($param_key != 'rain') {
					$station_temp_param_keys []= $param_key;
				}
			}
		}
	} else if ($station_id == 231) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=179 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				$station_temp_param_keys []= $param_key;
			}
		}
	} else if ($station_id == 232) {
		$post_sql = "SELECT dvcdat_data FROM dvc_raw_data WHERE dvcloc_id=153 ORDER BY dvcdat_time DESC LIMIT 1";
		$result_set = $aurassure_db->query($post_sql);
		if($result_set) {
			$result = $result_set->fetch_assoc();
			$station_latest_data = json_decode($result['dvcdat_data'], true);
			foreach ($station_latest_data as $param_key => $param_value) {
				$station_temp_param_keys []= $param_key;
			}
		}
	}

	//get raw data of station for last 24 hours
	$station_params_temp = ['uv', 'lint'];
	$station_params_temp_dust = ['pm1', 'pm2.5', 'pm10'];
	if ($station_id == 220) {
		$required_data_station_one = get_raw_data_json(220, $last_data_update_time, false, $parameters, false);
		$required_data_station_two = get_raw_data_json(168, $last_data_update_time, false, $station_params_temp, false);
		
		$required_data_view_existing_params = [];
		$required_data_view_new_params = [];
		for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
			foreach ($station_temp_param_keys as $param) {
				$required_data_view_existing_params[$i][$param] = $required_data_station_one['param_values'][$param][$i];
			}
		}
		for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
			foreach ($station_params_temp as $param) {
				$required_data_view_new_params[$i][$param] = $required_data_station_two['param_values'][$param][$i];
			}
		}

		$existing_param_station_value_with_timestamp = [];
		foreach ($required_data_station_one['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$existing_param_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_one['time_stamps'][$index],
					'value' => $required_data_view_existing_params[$index]
				];
			}
		}
		
		$new_param_station_value_with_timestamp = [];
		foreach ($required_data_station_two['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$new_param_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_two['time_stamps'][$index],
					'value' => $required_data_view_new_params[$index]
				];
			}
		}

		// receive merged data from function
		$all_station_length_data = Tools::getMergedDataOfStations($existing_param_station_value_with_timestamp, $new_param_station_value_with_timestamp);

		$all_parameter_data_packet = [];
		$all_parameter_timestamp_view = [];
		$all_parameter_data_view = [];
		foreach ($all_station_length_data as $key => $value) {
			$all_parameter_data_packet []= $value;
			$all_parameter_timestamp_view []= $key;
		}
		for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
			foreach ($all_parameter_data_packet[$i] as $key => $value) {
				$all_parameter_data_view[$key] []= $value;
			}
		}

		$required_data['param_values'] = $all_parameter_data_view;
		$required_data['time_stamps'] = $all_parameter_timestamp_view;
		
	} else if ($station_id == 221) {
		$required_data_station_one = get_raw_data_json(221, $last_data_update_time, false, $parameters, false);
		$required_data_station_two = get_raw_data_json(162, $last_data_update_time, false, $station_params_temp, false);
		
		$required_data_view_existing_params = [];
		$required_data_view_new_params = [];
		for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
			foreach ($station_temp_param_keys as $param) {
				$required_data_view_existing_params[$i][$param] = $required_data_station_one['param_values'][$param][$i];
			}
		}
		for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
			foreach ($station_params_temp as $param) {
				$required_data_view_new_params[$i][$param] = $required_data_station_two['param_values'][$param][$i];
			}
		}

		$existing_param_station_value_with_timestamp = [];
		foreach ($required_data_station_one['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$existing_param_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_one['time_stamps'][$index],
					'value' => $required_data_view_existing_params[$index]
				];
			}
		}
		
		$new_param_station_value_with_timestamp = [];
		foreach ($required_data_station_two['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$new_param_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_two['time_stamps'][$index],
					'value' => $required_data_view_new_params[$index]
				];
			}
		}

		// receive merged data from function
		$all_station_length_data = Tools::getMergedDataOfStations($existing_param_station_value_with_timestamp, $new_param_station_value_with_timestamp);

		$all_parameter_data_packet = [];
		$all_parameter_timestamp_view = [];
		$all_parameter_data_view = [];
		foreach ($all_station_length_data as $key => $value) {
			$all_parameter_data_packet []= $value;
			$all_parameter_timestamp_view []= $key;
		}
		for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
			foreach ($all_parameter_data_packet[$i] as $key => $value) {
				$all_parameter_data_view[$key] []= $value;
			}
		}

		$required_data['param_values'] = $all_parameter_data_view;
		$required_data['time_stamps'] = $all_parameter_timestamp_view;

	} else if ($station_id == 231) {
		$required_data_station_one = get_raw_data_json(231, $last_data_update_time, false, $station_params_temp, false);
		$required_data_station_two = get_raw_data_json(179, $last_data_update_time, false, $parameters, false);
		
		$required_data_view_dust = [];
		$required_data_view_other = [];
		for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
			foreach ($station_params_temp as $param) {
				$required_data_view_dust[$i][$param] = $required_data_station_one['param_values'][$param][$i];
			}
		}
		for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
			foreach ($station_temp_param_keys as $param) {
				$required_data_view_other[$i][$param] = $required_data_station_two['param_values'][$param][$i];
			}
		}

		$dust_station_value_with_timestamp = [];
		foreach ($required_data_station_one['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$dust_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_one['time_stamps'][$index],
					'value' => $required_data_view_dust[$index]
				];
			}
		}
		
		$other_station_value_with_timestamp = [];
		foreach ($required_data_station_two['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$other_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_two['time_stamps'][$index],
					'value' => $required_data_view_other[$index]
				];
			}
		}

		// receive merged data from function
		$all_station_length_data = Tools::getMergedDataOfStations($dust_station_value_with_timestamp, $other_station_value_with_timestamp);

		$all_parameter_data_packet = [];
		$all_parameter_timestamp_view = [];
		$all_parameter_data_view = [];
		foreach ($all_station_length_data as $key => $value) {
			$all_parameter_data_packet []= $value;
			$all_parameter_timestamp_view []= $key;
		}
		for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
			foreach ($all_parameter_data_packet[$i] as $key => $value) {
				$all_parameter_data_view[$key] []= $value;
			}
		}

		$required_data['param_values'] = $all_parameter_data_view;
		$required_data['time_stamps'] = $all_parameter_timestamp_view;
		
	} else if ($station_id == 232) {
		$required_data_station_one = get_raw_data_json(232, $last_data_update_time, false, $station_params_temp, false);
		$required_data_station_two = get_raw_data_json(153, $last_data_update_time, false, $parameters, false);
		
		$required_data_view_dust = [];
		$required_data_view_other = [];
		for ($i = 0; $i < count($required_data_station_one['time_stamps']); $i++) {
			foreach ($station_params_temp as $param) {
				$required_data_view_dust[$i][$param] = $required_data_station_one['param_values'][$param][$i];
			}
		}
		for ($i = 0; $i < count($required_data_station_two['time_stamps']); $i++) {
			foreach ($station_temp_param_keys as $param) {
				$required_data_view_other[$i][$param] = $required_data_station_two['param_values'][$param][$i];
			}
		}

		$dust_station_value_with_timestamp = [];
		foreach ($required_data_station_one['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$dust_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_one['time_stamps'][$index],
					'value' => $required_data_view_dust[$index]
				];
			}
		}
		
		$other_station_value_with_timestamp = [];
		foreach ($required_data_station_two['param_values'] as $key => $value) {
			foreach ($value as $index => $val) {
				$other_station_value_with_timestamp[$index]= [
					'time' => $required_data_station_two['time_stamps'][$index],
					'value' => $required_data_view_other[$index]
				];
			}
		}

		// receive merged data from function
		$all_station_length_data = Tools::getMergedDataOfStations($dust_station_value_with_timestamp, $other_station_value_with_timestamp);

		$all_parameter_data_packet = [];
		$all_parameter_timestamp_view = [];
		$all_parameter_data_view = [];
		foreach ($all_station_length_data as $key => $value) {
			$all_parameter_data_packet []= $value;
			$all_parameter_timestamp_view []= $key;
		}
		for ($i = 0; $i < count($all_parameter_data_packet) ; $i++) {
			foreach ($all_parameter_data_packet[$i] as $key => $value) {
				$all_parameter_data_view[$key] []= $value;
			}
		}

		$required_data['param_values'] = $all_parameter_data_view;
		$required_data['time_stamps'] = $all_parameter_timestamp_view;
	} else {
		$required_data = get_raw_data_json($station_id, $last_data_update_time, false, $parameters, false);
	}
	$json_response['param_values'] = $required_data['param_values'];
	$json_response['time_stamps'] = $required_data['time_stamps'];

	//check if hourly avg. data needs to be updated
	
	if ($station_id == 220) {
		$sql = "SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=220 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time." UNION SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=168 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time;
		$result_set = $aurassure_db->query($sql);
		if (mysqli_num_rows($result_set)) {
			$station_data = [];
			$json_response['update_hourly_avg_data'] = true;
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}
			$aqi_val = '';
			$responsible_param = '';
			$connection_status = $station_data['220']['dvcloc_last_data_update_time'];
			$aqi_val = $station_data['220']['aqi'];
			$responsible_param = $station_data['220']['dvcloc_responsible_param'];

			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['220']['temp'];
			$json_response['humid'] = $station_data['220']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);

			//get hourly param conc.s and indices of station
			$sql = "SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=220 AND dhad_upto_time>".$last_hourly_data_update_time."  UNION SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=168 AND dhad_upto_time>".$last_hourly_data_update_time." ORDER BY dhad_upto_time DESC";
			$result_set = $aurassure_db->query($sql);

			if(!$result_set) {
				$api_utilities->db_error();
			}

			$station_hourly_aqis = [];
			$station_param_hourly_concs = [];
			$station_param_hourly_aqis = [];
			foreach ($parameters as $param) {
				$station_param_hourly_concs[$param] = [];
				$station_param_hourly_aqis[$param] = [];
			}
			$multi_stations_hourly_aqis = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$multi_stations_hourly_aqis []= [
					'dhad_upto_time' => $result['dhad_upto_time'],
					'aqi' => $result['dhad_aqi'],
					'param_concs' => json_decode($result['dhad_param_concs'], true),
					'param_aqis' => json_decode($result['dhad_param_aqis'], true)
				];
			}

			for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
				for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
					if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
						if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$i]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
						} else {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$j]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
						}

						$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$i]['param_concs'], $multi_stations_hourly_aqis[$j]['param_concs']);
						$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$i]['param_aqis'], $multi_stations_hourly_aqis[$j]['param_aqis']);
						foreach ($parameters as $param) {
							if($temp_station_param_hourly_concs[$param]) {
								$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
							}
							if($temp_station_param_hourly_aqis[$param]) {
								$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
							}
						}
						
					}
				}
			}

			$json_response['hourly_aqis'] = [];
			$json_response['hourly_param_concs'] = [];
			$json_response['hourly_param_aqis'] = [];
			foreach ($parameters as $param) {
				$json_response['hourly_param_concs'][$param] = [];
				$json_response['hourly_param_aqis'][$param] = [];
			}
			$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
			while ($latest_hour > $last_hourly_data_update_time) {
				if(isset($station_hourly_aqis[$latest_hour])) {
					$json_response['hourly_aqis'] []= $station_hourly_aqis[$latest_hour];
				} else {
					$json_response['hourly_aqis'] []= null;
				}

				foreach ($parameters as $param) {
					if(isset($station_param_hourly_concs[$param][$latest_hour])) {
						$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$latest_hour];
					} else {
						$json_response['hourly_param_concs'][$param] []= null;
					}
					if(isset($station_param_hourly_aqis[$param][$latest_hour])) {
						$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$latest_hour];
					} else {
						$json_response['hourly_param_aqis'][$param] []= null;
					}
				}

				$latest_hour -= 3600;
			}

		} else {
			$json_response['update_hourly_avg_data'] = false;
		}

	} else if ($station_id == 221) {
		$sql = "SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=221 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time." UNION SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=162 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time;
		$result_set = $aurassure_db->query($sql);
		if (mysqli_num_rows($result_set)) {
			$station_data = [];
			$json_response['update_hourly_avg_data'] = true;
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}
			$aqi_val = '';
			$responsible_param = '';
			$connection_status = $station_data['221']['dvcloc_last_data_update_time'];
			$aqi_val = $station_data['221']['aqi'];
			$responsible_param = $station_data['221']['dvcloc_responsible_param'];

			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['221']['temp'];
			$json_response['humid'] = $station_data['221']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);

			//get hourly param conc.s and indices of station
			$sql = "SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=221 AND dhad_upto_time>".$last_hourly_data_update_time."  UNION SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=162 AND dhad_upto_time>".$last_hourly_data_update_time." ORDER BY dhad_upto_time DESC";
			$result_set = $aurassure_db->query($sql);

			if(!$result_set) {
				$api_utilities->db_error();
			}

			$station_hourly_aqis = [];
			$station_param_hourly_concs = [];
			$station_param_hourly_aqis = [];
			foreach ($parameters as $param) {
				$station_param_hourly_concs[$param] = [];
				$station_param_hourly_aqis[$param] = [];
			}
			$multi_stations_hourly_aqis = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$multi_stations_hourly_aqis []= [
					'dhad_upto_time' => $result['dhad_upto_time'],
					'aqi' => $result['dhad_aqi'],
					'param_concs' => json_decode($result['dhad_param_concs'], true),
					'param_aqis' => json_decode($result['dhad_param_aqis'], true)
				];
			}

			for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
				for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
					if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
						if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$i]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
						} else {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$j]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
						}

						$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$i]['param_concs'], $multi_stations_hourly_aqis[$j]['param_concs']);
						$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$i]['param_aqis'], $multi_stations_hourly_aqis[$j]['param_aqis']);
						foreach ($parameters as $param) {
							if($temp_station_param_hourly_concs[$param]) {
								$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
							}
							if($temp_station_param_hourly_aqis[$param]) {
								$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
							}
						}
						
					}
				}
			}

			$json_response['hourly_aqis'] = [];
			$json_response['hourly_param_concs'] = [];
			$json_response['hourly_param_aqis'] = [];
			foreach ($parameters as $param) {
				$json_response['hourly_param_concs'][$param] = [];
				$json_response['hourly_param_aqis'][$param] = [];
			}
			$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
			while ($latest_hour > $last_hourly_data_update_time) {
				if(isset($station_hourly_aqis[$latest_hour])) {
					$json_response['hourly_aqis'] []= $station_hourly_aqis[$latest_hour];
				} else {
					$json_response['hourly_aqis'] []= null;
				}

				foreach ($parameters as $param) {
					if(isset($station_param_hourly_concs[$param][$latest_hour])) {
						$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$latest_hour];
					} else {
						$json_response['hourly_param_concs'][$param] []= null;
					}
					if(isset($station_param_hourly_aqis[$param][$latest_hour])) {
						$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$latest_hour];
					} else {
						$json_response['hourly_param_aqis'][$param] []= null;
					}
				}

				$latest_hour -= 3600;
			}

		} else {
			$json_response['update_hourly_avg_data'] = false;
		}
	} else if ($station_id == 231) {
		$sql = "SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=231 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time." UNION SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=179 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time;
		$result_set = $aurassure_db->query($sql);
		if (mysqli_num_rows($result_set)) {
			$station_data = [];
			$json_response['update_hourly_avg_data'] = true;
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}
			$aqi_val = '';
			$responsible_param = '';
			if (intval($station_data['231']['dvcloc_last_data_update_time']) > intval($station_data['179']['dvcloc_last_data_update_time'])) {
				$connection_status = $station_data['231']['dvcloc_last_data_update_time'];
			} else {
				$connection_status = $station_data['179']['dvcloc_last_data_update_time'];
			}
			if (intval($station_data['231']['aqi']) > intval($station_data['179']['aqi'])) {
				$aqi_val = $station_data['231']['aqi'];
				$responsible_param = $station_data['231']['dvcloc_responsible_param'];
			} else {
				$aqi_val = $station_data['179']['aqi'];
				// $json_response['station_data']=$station_data['179']['dvcloc_responsible_param'];
				$responsible_param = $station_data['179']['dvcloc_responsible_param'];
				// $json_response['station_data1']=$responsible_param;
			}

			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['179']['temp'];
			$json_response['humid'] = $station_data['179']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);

			//get hourly param conc.s and indices of station
			$sql = "SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=231 AND dhad_upto_time>".$last_hourly_data_update_time."  UNION SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=179 AND dhad_upto_time>".$last_hourly_data_update_time." ORDER BY dhad_upto_time DESC";
			$result_set = $aurassure_db->query($sql);

			if(!$result_set) {
				$api_utilities->db_error();
			}

			$station_hourly_aqis = [];
			$station_param_hourly_concs = [];
			$station_param_hourly_aqis = [];
			foreach ($parameters as $param) {
				$station_param_hourly_concs[$param] = [];
				$station_param_hourly_aqis[$param] = [];
			}
			$multi_stations_hourly_aqis = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$multi_stations_hourly_aqis []= [
					'dhad_upto_time' => $result['dhad_upto_time'],
					'aqi' => $result['dhad_aqi'],
					'param_concs' => json_decode($result['dhad_param_concs'], true),
					'param_aqis' => json_decode($result['dhad_param_aqis'], true)
				];
			}

			for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
				for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
					if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
						if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$i]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
						} else {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$j]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
						}

						$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$i]['param_concs'], $multi_stations_hourly_aqis[$j]['param_concs']);
						$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$i]['param_aqis'], $multi_stations_hourly_aqis[$j]['param_aqis']);
						foreach ($parameters as $param) {
							if($temp_station_param_hourly_concs[$param]) {
								$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
							}
							if($temp_station_param_hourly_aqis[$param]) {
								$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
							}
						}
						
					}
				}
			}

			$json_response['hourly_aqis'] = [];
			$json_response['hourly_param_concs'] = [];
			$json_response['hourly_param_aqis'] = [];
			foreach ($parameters as $param) {
				$json_response['hourly_param_concs'][$param] = [];
				$json_response['hourly_param_aqis'][$param] = [];
			}
			$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
			while ($latest_hour > $last_hourly_data_update_time) {
				if(isset($station_hourly_aqis[$latest_hour])) {
					$json_response['hourly_aqis'] []= $station_hourly_aqis[$latest_hour];
				} else {
					$json_response['hourly_aqis'] []= null;
				}

				foreach ($parameters as $param) {
					if(isset($station_param_hourly_concs[$param][$latest_hour])) {
						$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$latest_hour];
					} else {
						$json_response['hourly_param_concs'][$param] []= null;
					}
					if(isset($station_param_hourly_aqis[$param][$latest_hour])) {
						$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$latest_hour];
					} else {
						$json_response['hourly_param_aqis'][$param] []= null;
					}
				}

				$latest_hour -= 3600;
			}

		} else {
			$json_response['update_hourly_avg_data'] = false;
		}

	} else if ($station_id == 232) {
		$sql = "SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=232 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time." UNION SELECT dvcloc_id, dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=153 AND dvcloc_last_data_update_time>".$last_hourly_data_update_time;
		$result_set = $aurassure_db->query($sql);
		if (mysqli_num_rows($result_set)) {
			$station_data = [];
			$json_response['update_hourly_avg_data'] = true;
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_data[$result['dvcloc_id']] = [
					'dvcloc_last_data_update_time' => $result['dvcloc_last_data_update_time'],
					'aqi' => $result['dvcloc_aqi'],
					'temp' => $result['dvcloc_temp'],
					'humid' => $result['dvcloc_humid'],
					'dvcloc_responsible_param' => $result['dvcloc_responsible_param']
				];
			}

			$aqi_val = '';
			$responsible_param = '';
			if (intval($station_data['232']['dvcloc_last_data_update_time']) > intval($station_data['153']['dvcloc_last_data_update_time'])) {
				$connection_status = $station_data['232']['dvcloc_last_data_update_time'];
			} else {
				$connection_status = $station_data['153']['dvcloc_last_data_update_time'];
			}
			if (intval($station_data['232']['aqi']) > intval($station_data['153']['aqi'])) {
				$aqi_val = $station_data['232']['aqi'];
				$responsible_param = $station_data['232']['dvcloc_responsible_param'];
			} else {
				$aqi_val = $station_data['153']['aqi'];
				// $json_response['station_data']=$station_data['153']['dvcloc_responsible_param'];
				$responsible_param = $station_data['153']['dvcloc_responsible_param'];
				// $json_response['station_data1']=$responsible_param;
			}

			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - intval($connection_status)) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $connection_status;
			$json_response['aqi'] = $aqi_val;//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($aqi_val);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($aqi_val);
			$json_response['temp'] = $station_data['153']['temp'];
			$json_response['humid'] = $station_data['153']['humid'];
			$json_response['responsible_param'] = $responsible_param;
			$json_response['suggestions'] = EndUserResult::getSuggestions($aqi_val, $responsible_param);

			//get hourly param conc.s and indices of station
			$sql = "SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=232 AND dhad_upto_time>".$last_hourly_data_update_time."  UNION SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=153 AND dhad_upto_time>".$last_hourly_data_update_time." ORDER BY dhad_upto_time DESC";
			$result_set = $aurassure_db->query($sql);

			if(!$result_set) {
				$api_utilities->db_error();
			}

			$station_hourly_aqis = [];
			$station_param_hourly_concs = [];
			$station_param_hourly_aqis = [];
			foreach ($parameters as $param) {
				$station_param_hourly_concs[$param] = [];
				$station_param_hourly_aqis[$param] = [];
			}
			$multi_stations_hourly_aqis = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$multi_stations_hourly_aqis []= [
					'dhad_upto_time' => $result['dhad_upto_time'],
					'aqi' => $result['dhad_aqi'],
					'param_concs' => json_decode($result['dhad_param_concs'], true),
					'param_aqis' => json_decode($result['dhad_param_aqis'], true)
				];
			}

			for ($i = 0; $i <= sizeof($multi_stations_hourly_aqis); $i++) {
				for ($j = 0; $j <= sizeof($multi_stations_hourly_aqis); $j++) {
					if ($multi_stations_hourly_aqis[$i]['dhad_upto_time'] == $multi_stations_hourly_aqis[$j]['dhad_upto_time']) {
						if ($multi_stations_hourly_aqis[$i]['aqi'] > $multi_stations_hourly_aqis[$j]['aqi']) {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$i]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$i]['param'];
						} else {
							$station_hourly_aqis[$multi_stations_hourly_aqis[$j]['dhad_upto_time']] = [
								'aqi' => $multi_stations_hourly_aqis[$j]['aqi']
							];
							// $json_response['responsible_param'] = $multi_stations_hourly_aqis[$j]['param'];
						}

						$temp_station_param_hourly_concs = array_merge($multi_stations_hourly_aqis[$i]['param_concs'], $multi_stations_hourly_aqis[$j]['param_concs']);
						$temp_station_param_hourly_aqis = array_merge($multi_stations_hourly_aqis[$i]['param_aqis'], $multi_stations_hourly_aqis[$j]['param_aqis']);
						foreach ($parameters as $param) {
							if($temp_station_param_hourly_concs[$param]) {
								$station_param_hourly_concs[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
							}
							if($temp_station_param_hourly_aqis[$param]) {
								$station_param_hourly_aqis[$param][$multi_stations_hourly_aqis[$i]['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
							}
						}
						
					}
				}
			}

			$json_response['hourly_aqis'] = [];
			$json_response['hourly_param_concs'] = [];
			$json_response['hourly_param_aqis'] = [];
			foreach ($parameters as $param) {
				$json_response['hourly_param_concs'][$param] = [];
				$json_response['hourly_param_aqis'][$param] = [];
			}
			$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
			while ($latest_hour > $last_hourly_data_update_time) {
				if(isset($station_hourly_aqis[$latest_hour])) {
					$json_response['hourly_aqis'] []= $station_hourly_aqis[$latest_hour];
				} else {
					$json_response['hourly_aqis'] []= null;
				}

				foreach ($parameters as $param) {
					if(isset($station_param_hourly_concs[$param][$latest_hour])) {
						$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$latest_hour];
					} else {
						$json_response['hourly_param_concs'][$param] []= null;
					}
					if(isset($station_param_hourly_aqis[$param][$latest_hour])) {
						$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$latest_hour];
					} else {
						$json_response['hourly_param_aqis'][$param] []= null;
					}
				}

				$latest_hour -= 3600;
			}

		} else {
			$json_response['update_hourly_avg_data'] = false;
		}

	} else {
		$sql = "SELECT dvcloc_last_data_update_time, dvcloc_aqi, dvcloc_responsible_param, dvcloc_temp, dvcloc_humid FROM dvc_locations WHERE dvcloc_id=".$station_id." AND ct_id=".$api_utilities->session_handler->user_session_data['ci']." AND dvcloc_last_data_update_time>".$last_hourly_data_update_time;
		$result_set = $aurassure_db->query($sql);
		if(mysqli_num_rows($result_set)) {
			$json_response['update_hourly_avg_data'] = true;

			$result = mysqli_fetch_assoc($result_set);
			$json_response['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
			$json_response['last_data_update_time'] = $result['dvcloc_last_data_update_time'];
			$json_response['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
			$json_response['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);
			$json_response['aqi_status'] = EndUserResult::getAQIStatus($result['dvcloc_aqi']);
			$json_response['temp'] = $result['dvcloc_temp'];
			$json_response['humid'] = $result['dvcloc_humid'];
			$json_response['suggestions'] = EndUserResult::getSuggestions($result['dvcloc_aqi'], $result['dvcloc_responsible_param']);

			//get hourly param conc.s and indices of station
			$sql = "SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=$station_id AND dhad_upto_time>".$last_hourly_data_update_time." ORDER BY dhad_upto_time DESC";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				$api_utilities->db_error();
			}
			$station_hourly_aqis = [];
			$station_param_hourly_concs = [];
			$station_param_hourly_aqis = [];
			foreach ($parameters as $param) {
				$station_param_hourly_concs[$param] = [];
				$station_param_hourly_aqis[$param] = [];
			}
			while($result = mysqli_fetch_assoc($result_set)) {
				$station_hourly_aqis[$result['dhad_upto_time']] = $result['dhad_aqi'];

				$temp_station_param_hourly_concs = json_decode($result['dhad_param_concs'], true);
				$temp_station_param_hourly_aqis = json_decode($result['dhad_param_aqis'], true);
				foreach ($parameters as $param) {
					if(isset($temp_station_param_hourly_concs[$param])) {
						$station_param_hourly_concs[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
					}
					if(isset($temp_station_param_hourly_aqis[$param])) {
						$station_param_hourly_aqis[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
					}
				}
			}

			//Vadodara POC
			if($station_id == 139) {
				$sql = "SELECT dhad_aqi, dhad_param_aqis, dhad_param_concs, dhad_upto_time FROM dvc_hourly_avg_data WHERE dvcloc_id=140 AND dhad_upto_time>=".($_SERVER['REQUEST_TIME']-90000)." AND dhad_upto_time<=".$_SERVER['REQUEST_TIME']." ORDER BY dhad_upto_time DESC";
				$weather_result_set = $aurassure_db->query($sql);
				if(!$weather_result_set) {
					$api_utilities->db_error();
				}

				while($result = $weather_result_set->fetch_assoc()) {
					$station_hourly_aqis[$result['dhad_upto_time']] = $result['dhad_aqi'];

					$temp_station_param_hourly_concs = json_decode($result['dhad_param_concs'], true);
					$temp_station_param_hourly_aqis = json_decode($result['dhad_param_aqis'], true);
					foreach ($station_params as $param) {
						if($temp_station_param_hourly_concs[$param]) {
							$station_param_hourly_concs[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_concs[$param];
						}
						if($temp_station_param_hourly_aqis[$param]) {
							$station_param_hourly_aqis[$param][$result['dhad_upto_time']] = $temp_station_param_hourly_aqis[$param];
						}
					}
				}
			}

			$json_response['hourly_aqis'] = [];
			$json_response['hourly_param_concs'] = [];
			$json_response['hourly_param_aqis'] = [];
			foreach ($parameters as $param) {
				$json_response['hourly_param_concs'][$param] = [];
				$json_response['hourly_param_aqis'][$param] = [];
			}
			$latest_hour = strtotime(strftime("%d-%m-%Y %H:00:00", $_SERVER['REQUEST_TIME']));
			while ($latest_hour > $last_hourly_data_update_time) {
				if(isset($station_hourly_aqis[$latest_hour])) {
					$json_response['hourly_aqis'] []= $station_hourly_aqis[$latest_hour];
				} else {
					$json_response['hourly_aqis'] []= null;
				}

				foreach ($parameters as $param) {
					if(isset($station_param_hourly_concs[$param][$latest_hour])) {
						$json_response['hourly_param_concs'][$param] []= $station_param_hourly_concs[$param][$latest_hour];
					} else {
						$json_response['hourly_param_concs'][$param] []= null;
					}
					if(isset($station_param_hourly_aqis[$param][$latest_hour])) {
						$json_response['hourly_param_aqis'][$param] []= $station_param_hourly_aqis[$param][$latest_hour];
					} else {
						$json_response['hourly_param_aqis'][$param] []= null;
					}
				}

				$latest_hour -= 3600;
			}
		} else {
			$json_response['update_hourly_avg_data'] = false;
		}
	}

	echo json_encode($json_response);
} else {
	$api_utilities->incomplete_request();
}
$api_utilities->log_api_tracking_data(30, $access_time, 'update', $user_id, $log_id);