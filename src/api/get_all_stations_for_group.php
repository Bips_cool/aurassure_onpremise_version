<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_stations_for_group.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(241, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$sql = "SELECT dvcloc_id, dvcloc_name, dvcloc_city, ct_id FROM dvc_locations WHERE ";
if($api_utilities->session_handler->user_session_data['ci'] == 5) {
	//send all devices for admin portal
	$sql .= "dvcloc_data_source=0";
} else {
	$sql .= "ct_id=".$api_utilities->session_handler->user_session_data['ci'];
}
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$stations = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['dvcloc_id'];
		$temp_location_array['name'] = $result['dvcloc_name'];
		$temp_location_array['city_name'] = $result['dvcloc_city'];
		$temp_location_array['city_id'] = $result['ct_id'];

		$stations []= $temp_location_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response['stations'] = $stations;
	$json_response['groups'] = "";
	$groups_sql = "SELECT grp_id, grp_name FROM groups";
	$groups_result_set = $aurassure_db->query($groups_sql);
	if($groups_result_set) {
		$groups = [];
		while($groups_result = mysqli_fetch_assoc($groups_result_set)) {
			$temp_groups_array = [];
			$temp_groups_array['id'] = $groups_result['grp_id'];
			$temp_groups_array['name'] = $groups_result['grp_name'];

			$groups []= $temp_groups_array;
		}

		$json_response['groups'] = $groups;
	}
} else {
	$api_utilities->db_error();
	// $json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(241, $access_time, 'update', $user_id, $log_id);