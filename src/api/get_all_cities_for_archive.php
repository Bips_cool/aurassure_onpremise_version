<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_cities_for_archive.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(230, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;
if ($api_utilities->session_handler->user_session_data['ci'] == 11 || $api_utilities->session_handler->user_session_data['ci'] == 16 || $api_utilities->session_handler->user_session_data['ci'] == 19 || $api_utilities->session_handler->user_session_data['ci'] == 20) {
	$sql = "SELECT ct_id, ct_name FROM cities WHERE ct_id=".$api_utilities->session_handler->user_session_data['ci'];
} else if ($api_utilities->session_handler->user_session_data['ci'] == 22) {
	$sql = "SELECT ct_id, ct_name FROM cities WHERE ct_id IN (11,16,19,20)";
}
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$cities = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['ct_id'];
		$temp_location_array['name'] = $result['ct_name'];

		$cities []= $temp_location_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response['cities'] = $cities;
}
echo json_encode($json_response);

$api_utilities->log_api_tracking_data(230, $access_time, 'update', $user_id, $log_id);