<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_stations.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(21, $access_time, 'insert', $user_id);

function json_validator($data=NULL) {
	if (!empty($data)) {
		json_decode($data);
		return (json_last_error() === JSON_ERROR_NONE);
	}
	return false;
}
use Aurassure\DataProcessor\EndUserResult;

$sql = "SELECT `usr_id`, `usr_access` FROM `usr_login` WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui'];
$result_set = $aurassure_db->query($sql);
$result = mysqli_fetch_assoc($result_set);
if ($result['usr_access'] != "*") {
	if (json_validator($result['usr_access'])) {
		$user_access = json_decode($result['usr_access'], true);
	} else {
		$user_access = [];
	}
}

$fragmented_url = explode('/', strtok($_SERVER['HTTP_REFERER'], '?'));

if($api_utilities->session_handler->user_session_data['ci'] == 5) {
	if ($result['usr_access'] == "*") {
		//send all devices for admin portal
		$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE dvcloc_data_source=0";
	} else if (in_array($fragmented_url[4], $user_access)) {
		//send devices allowed for admin portal
		$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $user_access).")";	
	} else {
		$api_utilities->throw_error('Sorry, you have not access to the requested station.');
	}
} else {
	if ($api_utilities->session_handler->user_session_data['ci'] == 22) {
		$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id= 16 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id= 19 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id= 20 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE dvcloc_id=231 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE dvcloc_id=232";
	} else  {
		if ($result['usr_access'] == "*") {
			$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id=".$api_utilities->session_handler->user_session_data['ci'];
		} else if (in_array($fragmented_url[4], $user_access)) {
			$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $user_access).") AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];	
		} else {
			$api_utilities->throw_error('Sorry, you have not access to the requested station.');
		}
	}
}
$result_set = $aurassure_db->query($sql);
if($result_set) {
	$stations = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$temp_location_array = [];
		$temp_location_array['id'] = $result['dvcloc_id'];
		$temp_station_name = '';
		if($result['dvcloc_name']) {
			$temp_station_name .= $result['dvcloc_name'].', ';
		}
		if($result['dvcloc_city']) {
			$temp_station_name .= $result['dvcloc_city'];
		}
		$temp_station_name = rtrim($temp_station_name, ', ');
		$temp_location_array['loc_type'] = $result['dvcloc_type'];
		$temp_location_array['name'] = $temp_station_name;
		$temp_location_array['lat'] = $result['dvcloc_lat'];
		$temp_location_array['long'] = $result['dvcloc_long'];
		$temp_location_array['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dvcloc_last_data_update_time']) <= 7200 ? 'online' : 'offline';
		$temp_location_array['aqi'] = $result['dvcloc_aqi'];//ToDo -> calculate the AQI and send it
		$temp_location_array['aqi_range'] = EndUserResult::getAQIRange($result['dvcloc_aqi']);

		$stations []= $temp_location_array;
	}

	$api_utilities->set_success_ststus_in_response();
	$json_response['stations'] = $stations;
	$json_response['city_map_detail'] = "";
	if ($api_utilities->session_handler->user_session_data['ci'] != 5) {
		$sql = "SELECT `ct_lat`, `ct_long`, `ct_zoom_level` FROM `cities` WHERE `ct_id`=".$api_utilities->session_handler->user_session_data['ci'];
		$result_set = $aurassure_db->query($sql);
		if($result_set) {
			$result = mysqli_fetch_assoc($result_set);
			$json_response['city_map_detail'] = [
				'city_lat' => $result['ct_lat'] != "" ? $result['ct_lat'] : null,
				'city_long' => $result['ct_long'] != "" ? $result['ct_long'] : null,
				'city_zoom_level' => $result['ct_zoom_level']
			];
		}
	}

	if ($api_utilities->session_handler->user_session_data['ci'] == 5) {
		$sql = "SELECT `dak_lat`, `dak_long`, `dak_key`, `dak_last_accessed_at` FROM `dmd_api_keys` WHERE `dak_id`=5";
		$result_set = $aurassure_db->query($sql);
		if($result_set) {
			$dmds = [];
			while($result = mysqli_fetch_assoc($result_set)) {
				$temp_dmd_array = [];
				$temp_dmd_array['dmd_key'] = $result['dak_key'];
				$temp_dmd_array['lat'] = $result['dak_lat'];
				$temp_dmd_array['long'] = $result['dak_long'];
				$temp_dmd_array['last_accessed_at'] = $result['dak_last_accessed_at'];
				$temp_dmd_array['connection_status'] = ($_SERVER['REQUEST_TIME'] - $result['dak_last_accessed_at']) <= 300 ? 'online' : 'offline';
				$dmds []= $temp_dmd_array;
			}
		}
		$json_response['dmds'] = $dmds;
	}
	$json_response['url'] = $fragmented_url;
} else {
	// $api_utilities->db_error();
	$json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(21, $access_time, 'update', $user_id, $log_id);