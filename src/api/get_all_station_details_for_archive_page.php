<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_all_station_details_for_archive_page.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include'
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");
use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(20, $access_time, 'insert', $user_id);

function json_validator($data=NULL) {
	if (!empty($data)) {
		json_decode($data);
		return (json_last_error() === JSON_ERROR_NONE);
	}
	return false;
}
use Aurassure\DataProcessor\EndUserResult;

$sql = "SELECT `usr_id`, `usr_access` FROM `usr_login` WHERE `usr_id`=".$api_utilities->session_handler->user_session_data['ui'];
$result_set = $aurassure_db->query($sql);
$result = mysqli_fetch_assoc($result_set);
if ($result['usr_access'] != "*") {
	if (json_validator($result['usr_access'])) {
		$user_access = json_decode($result['usr_access'], true);
	} else {
		$user_access = [];
	}
}

//get station data of city
//send all devices for admin portal
if($api_utilities->session_handler->user_session_data['ci'] == 5) {
	
	if ($result['usr_access'] == "*") {
		$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_name FROM dvc_locations WHERE dvcloc_data_source=0";
	} else if (is_array($user_access)) {
		$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_name FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $user_access).") AND dvcloc_data_source=0";
	} else {
		$api_utilities->throw_error('Sorry, you have not access to the requested station.');
	}
} else {
	if ($api_utilities->session_handler->user_session_data['ci'] == 22) {
		$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id= 16 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id= 19 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE ct_id= 20 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE dvcloc_id=231 UNION SELECT dvcloc_id, dvcloc_type, dvcloc_lat, dvcloc_long, dvcloc_name, dvcloc_city, dvcloc_last_data_update_time, dvcloc_aqi FROM dvc_locations WHERE dvcloc_id=232";
	} else {
		if ($result['usr_access'] == "*") {
			$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_name FROM dvc_locations WHERE ct_id=".$api_utilities->session_handler->user_session_data['ci'];
		} else if (is_array($user_access)) {
			$sql = "SELECT dvcloc_id, dvcloc_type, dvcloc_name FROM dvc_locations WHERE dvcloc_id IN (".implode(',', $user_access).") AND ct_id=".$api_utilities->session_handler->user_session_data['ci'];
		} else {
			$api_utilities->throw_error('Sorry, you have not access to the requested station.');
		}
	}
}
$city_stations_result_set = $aurassure_db->query($sql);
if(!$city_stations_result_set) {
	$api_utilities->db_error();
}
$station_ids_of_city = [];
while ($result = $city_stations_result_set->fetch_assoc()) {
	$station_ids_of_city []= $result['dvcloc_id'];
}
$city_stations_result_set->data_seek(0);

//get all parameters
$sql = "SELECT dvcprm_name, dvcprm_key, dvcprm_unit FROM dvc_params";
$result_set = $aurassure_db->query($sql);
if(!$result_set) {
	$api_utilities->db_error();
}
$all_parameters = [];
while ($result = $result_set->fetch_assoc()) {
	$all_parameters[$result['dvcprm_key']] = [
		'name' => $result['dvcprm_name'],
		'key' => $result['dvcprm_key'],
		'unit' => $result['dvcprm_unit']
	];
}

//get params and units of all stations
$sql = "SELECT dvcloc_id, dvcdvc_last_raw_data FROM dvc_devices WHERE dvcloc_id IN (".implode(',', $station_ids_of_city).")";
$result_set = $aurassure_db->query($sql);
if(!$result_set) {
	$api_utilities->db_error();
}
$station_parameters = [];
while ($result = $result_set->fetch_assoc()) {
	$station_parameters[$result['dvcloc_id']] = [];
	$current_station_latest_data = json_decode($result['dvcdvc_last_raw_data'], true);
	foreach ($current_station_latest_data as $param_key => $param_value) {
		if($all_parameters[$param_key]) {
			$station_parameters[$result['dvcloc_id']] []= $all_parameters[$param_key];
		}
	}
}

$stations = [];
while($result = mysqli_fetch_assoc($city_stations_result_set)) {
	$temp_location_array = [];
	$temp_location_array['id'] = $result['dvcloc_id'];
	$temp_location_array['name'] = $result['dvcloc_name'];
	$temp_location_array['type'] = $result['dvcloc_type'];
	$temp_location_array['parameters'] = $station_parameters[$result['dvcloc_id']];

	$stations []= $temp_location_array;
}

$api_utilities->set_success_ststus_in_response();
$json_response['stations'] = $stations;

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(20, $access_time, 'update', $user_id, $log_id);