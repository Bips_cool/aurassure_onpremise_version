<?php
/**
 * Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_notifications.php', {
	method: 'POST',
	headers: {'Content-Type': 'application/json'},
	credentials: 'include'
}).then(function(response) {
	return response.json();
}).then(function(json) {
	console.log(json);
	if(json.status === 'success') {
		//Do stuffs here
	} else {
		//Show error message
	}
});*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities([ 'authenticate_user' => false ]);
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(239, $access_time, 'insert', $user_id);

// Check the value of required fields are set or not
$data = json_decode(file_get_contents('php://input'), true);
$notification_type = $data['notification_type'];
if ($notification_type == 'popup') {
	$sql = "SELECT `an_type`, `an_text`, `an_time` FROM `alert_notifications` ORDER BY `an_time` DESC LIMIT 20";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
		// $api_utilities->throw_error('SQL Error: '.$sql);
	}
	if($result_set) {
		$json_response['notifications'] = [];
		while ($result = $result_set->fetch_assoc()) {
			$json_response['notifications'] []= [
				'type' => $result['an_type'],
				'text' => $result['an_text'],
				'time' => intval($result['an_time'])
			];
		}

		$api_utilities->set_success_ststus_in_response();
	}
} else {
	$sql = "SELECT `an_type`, `an_text`, `an_time` FROM `alert_notifications` ORDER BY `an_time` DESC";
	$result_set = $aurassure_db->query($sql);
	if(!$result_set) {
		$api_utilities->db_error();
		// $api_utilities->throw_error('SQL Error: '.$sql);
	}
	if($result_set) {
		$json_response['notifications'] = [];
		while ($result = $result_set->fetch_assoc()) {
			$json_response['notifications'] []= [
				'type' => $result['an_type'],
				'text' => $result['an_text'],
				'time' => intval($result['an_time'])
			];
		}

		$api_utilities->set_success_ststus_in_response();
	}
}

echo json_encode($json_response);

$api_utilities->log_api_tracking_data(239, $access_time, 'update', $user_id, $log_id);