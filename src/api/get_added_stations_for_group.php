<?php
/*
	Testing Code
 */
/*fetch('https://api.aurassure.com/dev/get_stations_added_for_group.php', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  credentials: 'include',
	body: JSON.stringify({
		group_id: 2
	})
})
.then(function(Response) {
  return Response.json()
}).then(function(json) {
  console.log(json)
})*/

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

use Aurassure\API\Utilities;
$api_utilities = new Utilities();
$access_time = $_SERVER["REQUEST_TIME_FLOAT"];
$user_id = $api_utilities->session_handler->user_session_data['ui'];
$log_id = $api_utilities->log_api_tracking_data(241, $access_time, 'insert', $user_id);

use Aurassure\DataProcessor\EndUserResult;

$received_data = json_decode(file_get_contents('php://input'), true);
$group_id = $received_data['group_id'];

if (isset($group_id) && $group_id != '') {

	//sanitize received data
	$group_id = mysqli_real_escape_string($aurassure_db->connection, $group_id);

	$sql = "SELECT `grp_id`, `dvcloc_id` FROM `stations_under_group` WHERE `grp_id` =".$group_id;

	$result_set = $aurassure_db->query($sql);
	// if(!$result_set->num_rows) {
	// 	$api_utilities->throw_error('Sorry, no stations found under this group.');
	// }

	$group_stations = [];
	while($result = mysqli_fetch_assoc($result_set)) {
		$group_stations []= $result['dvcloc_id'];
	}
	$api_utilities->set_success_ststus_in_response();
	$json_response['group_stations'] = $group_stations;

} else {
	$api_utilities->incomplete_request();
	// $json_response['sql'] = $sql;
}

echo json_encode($json_response);
$api_utilities->log_api_tracking_data(241, $access_time, 'update', $user_id, $log_id);