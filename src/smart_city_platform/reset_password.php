<?php
date_default_timezone_set("Asia/Kolkata");
require_once("/var/www/aurassure/_includes/front_end_dependancies.php");

if($session_handler->logged_in()) {
	$session_handler->redirect_to('https://'.$city.'.aurassure.com/');
}

require_once("/var/www/aurassure/_includes/form_data_validation_functions.php");

$error_msg = "";

if($_SERVER['REQUEST_METHOD'] == 'POST') {
	$email = $_POST['email'];
	$token = $_POST['token'];
	$password = $_POST['password'];
	$cpassword = $_POST['cpassword'];

	if(!is_valid_email($email) || !has_presence($token) || !has_presence($password) || !has_presence($cpassword)) {
		$error_msg = "Sorry, something went wrong.";
	} elseif($password != $cpassword) {
		$error_msg = "Sorry, the password doesn't match with the re-typed password.";
	}
} else {
	$email = $_GET['email'];
	$token = $_GET['token'];

	if(!is_valid_email($email) || !has_presence($token)) {
		$error_msg = "Sorry, this link is not valid.";
	}
}

if($error_msg == "") {
	//sanitize fields
	$email = mysqli_real_escape_string($aurassure_db->connection, $email);
	$token = mysqli_real_escape_string($aurassure_db->connection, $token);

	$sql = "SELECT urp_request_time FROM usr_reset_password WHERE urp_email='$email' AND urp_reset_token='$token'";
	$result_set = $aurassure_db->query($sql);
	if($result_set) {
		if(mysqli_num_rows($result_set)) {
			$result = mysqli_fetch_assoc($result_set);
			if(($_SERVER['REQUEST_TIME'] - $result['urp_request_time']) < 86400) {
				if($_SERVER['REQUEST_METHOD'] == 'POST') {
					$sql = "DELETE FROM usr_reset_password WHERE urp_email='$email'";
					if(!$aurassure_db->query($sql)) {
						$error_msg = "Sorry, something went wrong.";
					}
					$new_hashed_password = password_hash($password, PASSWORD_BCRYPT);
					$sql = "UPDATE usr_login SET usr_pass='$new_hashed_password' WHERE usr_email='$email'";
					$result_set = $aurassure_db->query($sql);
					if($result_set && $aurassure_db->connection->affected_rows) {
						$success_msg = "Your password has been reset successfully. Please login using your new password.";
					}
				}
			} else {
				$error_msg = "Sorry this link has been expired.Please request again to reset your password.";
			}
		} else {
			$error_msg = "Sorry, this link is not valid.";
		}
	} else {
		$error_msg = "Sorry, something went wrong.";
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Reset Password - Aurassure</title>
<link rel="shortcut icon" href="<?php echo $FAVICON_LOCATION; ?>">
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<style>
	html {
		height: 100%;
	}
	body {
		font-family: Open Sans;
		font-size: 14px;
		margin: 0;
		padding: 60px;
		display: flex;
		height: calc(100% - 120px);
		background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center/cover no-repeat, -moz-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%, rgba(201,233,232,0.7) 100%) no-repeat; /* FF3.6-15 */
		background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center/cover no-repeat, -webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* Chrome10-25,Safari5.1-6 */
		background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center/cover no-repeat, radial-gradient(ellipse at center, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	}
	@media (max-width: 519px) {
		body {
			padding: 30px 20px;
			height: calc(100% - 60px);
			background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center no-repeat, -moz-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%, rgba(201,233,232,0.7) 100%) no-repeat; /* FF3.6-15 */
			background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center no-repeat, -webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* Chrome10-25,Safari5.1-6 */
			background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center no-repeat, radial-gradient(ellipse at center, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		}
	}
	.wr {
		margin: auto;
		padding: 60px 30px;
		width: 350px;
		max-width: 300px;
		border: 1px solid #DDD;
		background-color: rgba(255,255,255,0.7);
		box-shadow: -1px 2px 10px 4px rgba(0, 0, 0, .1), -1px 2px 10px 4px rgba(0, 0, 0, .2), 0 3px 1px -2px rgba(0, 0, 0, .2);
	}
	@media (max-width: 519px) {
		.wr {
			max-width: 240px;
		}
	}
	.logo-container {
		margin-bottom: 1em;
		text-align: center;
	}
	.logo-container .svg {
		height: 30px;
		margin: auto;
	}
	.form-desc {
		color: #7B7B7B;
		font-size: 16px;
		text-align: center;
		margin-bottom: 1em;
	}
	.frm-if-container {
		margin: 1em 0;
	}
	#form label {
		display: block;
		font-weight: 600;
		margin: 0 0 0.5em 0.8em;
	}
	.frm-if {
		background-color: #FFF;
		border: solid 0.1em #78D2ED;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		-o-border-radius: 0;
		border-radius: 0;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		padding: 0.6em 1.2em;
		width: 100%;
		outline: none;
	}
	.frm-if-err {
		border: solid 0.1em #FF0000;
	}
	.frm-err-msg, .frm-suc-msg, .err-msg, .success-msg {
		color: #FF0000;
		margin: 0.5em 0 0.5em 0.2em;
		font-size: 14px;
		display: none;
	}
	.frm-suc-msg, .success-msg {
		color: #00B100;
		margin-top: 3em;
		text-align: center;
		font-size: 16px;
		font-weight: 600;
	}
	.login-btn {
		background-color: #149DC1;
		border: none;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		-o-border-radius: 0;
		border-radius: 0;
		font-size: 16px;
		color: #fff;
		cursor: pointer;
		padding: 0.45em 1em;
		width: 100%;
	}
	a.login-btn {
		display: block;
		margin-top: 30px;
		text-align: center;
		text-decoration: none;
		width: calc(100% - 2em);
	}
	.err-msg, .success-msg {
		width: 100%;
		display: block;
		margin-top: 50px;
		text-align: center;
	}
	.powered {
		position: fixed;
		bottom: 10px;
		left: 0;
		width: 100%;
		font-size: 12px;
		text-align: center;
	}
	.powered a, .powered a:hover, .powered a:visited, .powered a:active {
		text-decoration: none;
		color: black;
	}
</style>
</head>
<body>
<div class="wr">
<div class="logo-container">
	<img src="https://static.aurassure.com/smart_city_platform/imgs/aurassure_logo.svg" class="svg" title="Aurassure"/>
</div>
<?php if($error_msg != "") { ?>
	<div class="err-msg"><?php echo $error_msg; ?></div>
<?php } elseif(isset($success_msg) && $success_msg != "") { ?>
	<div class='success-msg'><?php echo $success_msg; ?></div>
	<a href="/login" class="login-btn">Login</a>
<?php } else { ?>
<form id="form" action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
	<input type="hidden" name="sff" value="">
	<input type="hidden" name="email" value="<?php echo $email; ?>">
    <input type="hidden" name="token" value="<?php echo $token; ?>">
	<!-- <div class="form-title">Log In</div> -->
	<div id="hide_on_success">
		<div class="form-desc">Reset Password</div>
		<div class="frm-if-container">
			<input type="password" class="frm-if" id="password" name="password" placeholder="Password" required>
			<div class="frm-err-msg">Please give a valid password.</div>
		</div>
		<div class="frm-if-container">
			<input type="password" class="frm-if" id="re_type_password" name="cpassword" placeholder="Re-type Password" required>
			<div class="frm-err-msg">Password doesn't match with the re-typed password.</div>
		</div>
		<input type="submit" class="login-btn" id="form_submit_btn" value="Reset Password" name="submit">
		<div class="frm-err-msg" id="err_msg"></div>
	</div>
	<div class="frm-suc-msg" id="suc_msg"></div>
</form>
<?php } ?>
</div>
<div class="powered">Powered by <a href="https://phoenixrobotix.com" target="_blank">Phoenix Robotix Pvt. Ltd.</a></div>
<?php if($error_msg == "" && !isset($success_msg)) {
	echo $JQUERY_CDN; ?>
<script>
	$(function() {
		//function to validate email address
		var is_valid_email = function(email) {
			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
			return re.test(email);
		};

		//function to invalidate a field
		var invalidate_field = function(field_id) {
			$("#" + field_id).addClass("frm-if-err");
			$("#" + field_id).parent().find(".frm-err-msg").show();
			$("#" + field_id).focus();
		};
		//function to remove all field-invalidations
		var remove_all_field_invalidations = function() {
			$(".frm-if").removeClass("frm-if-err");
			$(".frm-err-msg").hide();
		};
		//function to show a custom error message
		var show_error_message = function(message) {
			$("#err_msg").html(message);
			$("#err_msg").show();
		};
		//function to show a custom success message
		var show_success_message = function(message) {
			$("#hide_on_success").hide();
			$("#suc_msg").html(message);
			$("#suc_msg").show();
		};
		//function to enable form submit button
		var enable_form_submit_button = function() {
			//enable the submit button
			$("#form_submit_btn").prop("disabled", false);
			$("#form_submit_btn").val("Login");
		};

		$("#form").on("submit", function(event) {
			//hide the error div and remove the red border in case it's shown from previous error
			remove_all_field_invalidations();
			//password
			if($("#password").val().trim() == '') {
				invalidate_field("password");
				event.preventDefault();
				return;
			}
			if($("#password").val() != $('#re_type_password').val()) {
				invalidate_field("re_type_password");
				event.preventDefault();
				return;
			}
		});
	});
</script>
<?php } ?>
</body>
</html>
