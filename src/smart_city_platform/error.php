<?php
require_once("/var/www/phoenixrobotix/_includes/front_end_dependancies.php");

if(!isset($error_message)) {
	$error_message = '404 Not Found';
}
?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<title>Aurassure Smart City Platform</title>
<!-- CSS -->
<?php echo $BOOTSTRAP_CSS_CDN; ?>
<!-- Custom CSS -->
<style>
	html, body {
		position: relative;
		background-color: #F2F2F2;
	}
	.data-err {
		margin: auto;
		margin-top: 10em;
		width: 500px;
		padding: 20px;
		font-size: 18px;
		text-align: center;
		color: #5D5D5D;
		background-color: #fff;
		border-radius: 6px;
		-webkit-box-shadow: 2px 2px 4px -2px rgba(0, 0, 0, 0.5);
		-moz-box-shadow: 2px 2px 4px -2px rgba(0, 0, 0, 0.5);
		box-shadow: 2px 2px 4px -2px rgba(0, 0, 0, 0.5);
		z-index: 100000001;
	}
	.data-err-img {
		margin: auto;
		height: 85px;
		width: 85px;
	}
</style>
</head>
<body>
<div class="data-err">
	<div class="data-err-img">
		<?xml version="1.0" encoding="iso-8859-1"?>
		<!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
		<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
		<svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
			 width="66.137px" height="66.137px" viewBox="0 0 66.137 66.137" style="enable-background:new 0 0 66.137 66.137;"
			 xml:space="preserve">
		<g>
			<g>
				<path fill="#5D5D5D" d="M33.067,66.136C14.834,66.136,0,51.302,0,33.068S14.834,0.001,33.067,0.001c18.234,0,33.07,14.834,33.07,33.067
					S51.302,66.136,33.067,66.136z M33.067,4.001C17.039,4.001,4,17.04,4,33.068s13.039,29.067,29.067,29.067
					c16.029,0,29.07-13.039,29.07-29.067S49.096,4.001,33.067,4.001z"/>
				<circle fill="#5D5D5D" cx="24.927" cy="24.825" r="4.334"/>
				<circle fill="#5D5D5D" cx="41.21" cy="24.825" r="4.334"/>
				<path fill="#5D5D5D" d="M20.613,47.246l-2.404-3.197c18.005-13.532,30.864,0.108,30.992,0.247l-2.943,2.709
					C45.831,46.547,35.658,35.938,20.613,47.246z"/>
			</g>
		</g>
		</svg>
	</div>
	<div class="data-err-msg"><?php echo $error_message; ?></div>
</div>
</body>
</html>
