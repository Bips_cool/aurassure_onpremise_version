<?php
date_default_timezone_set("Asia/Kolkata");
require_once("/var/www/aurassure/_includes/front_end_dependancies.php");

if(!isset($session_handler->user_session_data['un']) && $session_handler->user_session_data['ci'] != $city_id) {
	$session_handler->redirect_to('https://'.$city.'.aurassure.com/login');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Change Password - Aurassure</title>
<link rel="shortcut icon" href="https://aurassure.com/favicon.ico">
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<style>
	html {
		height: 100%;
	}
	body {
		font-family: Open Sans;
		font-size: 14px;
		margin: 0;
		padding: 60px;
		display: flex;
		height: calc(100% - 120px);
		background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center/cover no-repeat, -moz-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%, rgba(201,233,232,0.7) 100%) no-repeat; /* FF3.6-15 */
		background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center/cover no-repeat, -webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* Chrome10-25,Safari5.1-6 */
		background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center/cover no-repeat, radial-gradient(ellipse at center, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
	}
	@media (max-width: 519px) {
		body {
			padding: 30px 20px;
			height: calc(100% - 60px);
			background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center no-repeat, -moz-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%, rgba(201,233,232,0.7) 100%) no-repeat; /* FF3.6-15 */
			background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center no-repeat, -webkit-radial-gradient(center, ellipse cover, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* Chrome10-25,Safari5.1-6 */
			background: url("https://static.aurassure.com/smart_city_platform/imgs/login_bg.svg") bottom center no-repeat, radial-gradient(ellipse at center, rgba(255,255,255,0.7) 0%,rgba(201,233,232,0.7) 100%) no-repeat; /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		}
	}
	.wr {
		margin: auto;
		padding: 60px 30px;
		width: 350px;
		max-width: 300px;
		border: 1px solid #DDD;
		background-color: rgba(255,255,255,0.7);
		box-shadow: -1px 2px 10px 4px rgba(0, 0, 0, .1), -1px 2px 10px 4px rgba(0, 0, 0, .2), 0 3px 1px -2px rgba(0, 0, 0, .2);
	}
	@media (max-width: 519px) {
		.wr {
			max-width: 240px;
		}
	}
	.logo-container {
		margin-bottom: 1em;
		text-align: center;
	}
	.logo-container .svg {
		height: 30px;
		margin: auto;
	}
	.form-desc {
		color: #7B7B7B;
		font-size: 16px;
		text-align: center;
		margin-bottom: 1em;
	}
	.frm-if-container {
		margin: 1em 0;
	}
	#form label {
		display: block;
		font-weight: 600;
		margin: 0 0 0.5em 0.8em;
	}
	.frm-if {
		background-color: #FFF;
		border: solid 0.1em #78D2ED;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		-o-border-radius: 0;
		border-radius: 0;
		-webkit-box-sizing: border-box;
		-moz-box-sizing: border-box;
		-o-box-sizing: border-box;
		box-sizing: border-box;
		padding: 0.6em 1.2em;
		width: 100%;
		outline: none;
	}
	.frm-if-err {
		border: solid 0.1em #FF0000;
	}
	.frm-err-msg, .frm-suc-msg {
		color: #FF0000;
		margin: 0.5em 0 0.5em 0.2em;
		font-size: 14px;
		display: none;
	}
	.frm-suc-msg {
		color: #00B100;
		margin-top: 1em;
		font-size: 16px;
		font-weight: 600;
	}
	.login-btn {
		background-color: #149DC1;
		border: none;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		-o-border-radius: 0;
		border-radius: 0;
		font-size: 16px;
		color: #fff;
		cursor: pointer;
		padding: 0.45em 1em;
		width: 100%;
	}
	.forgot-password {
		color: #149DC1;
		text-align: right;
		padding: 1em 0;
	}
	.forgot-password a {
		text-decoration: none;
		color: inherit;
	}
	.forgot-password a:hover {
		color: #1988C9;
	}
	.powered {
		position: fixed;
		bottom: 10px;
		left: 0;
		width: 100%;
		font-size: 12px;
		text-align: center;
	}
	.powered a, .powered a:hover, .powered a:visited, .powered a:active {
		text-decoration: none;
		color: black;
	}
</style>
</head>
<body>
<div class="wr">
<div class="logo-container">
	<img src="https://static.aurassure.com/smart_city_platform/imgs/aurassure_logo.svg" class="svg" title="Aurassure"/>
</div>
<div id="form">
	<div id="hide_on_success">
		<div class="form-desc">Change your Aurassure Account Password</div>
		<div class="frm-if-container">
			<input type="password" class="frm-if" id="old_password" name="old_password" placeholder="Enter the recent password" required autofocus>
			<div class="frm-err-msg">Please give a your recent password.</div>
		</div>
		<div class="frm-if-container">
			<input type="password" class="frm-if" id="new_password" name="new_password" placeholder="Enter a new password" required autofocus>
			<div class="frm-err-msg">Please give a new password.</div>
		</div>
		<div class="frm-if-container">
			<input type="password" class="frm-if" id="re_new_password" name="re_new_password" placeholder="Re-enter the new password" required autofocus>
			<div class="frm-err-msg">Please re-enter the new password.</div>
		</div>
		<div class="frm-err-msg" id="frm_err_msg_match">Passwords do not match.</div>
		<br>
		<button type="button" class="login-btn" id="form_submit_btn">Change Password</button>
		<div class="frm-err-msg" id="err_msg"></div>
	</div>
	<div class="frm-suc-msg" id="suc_msg"></div>
</div>
</div>
<div class="powered">Powered by <a href="https://phoenixrobotix.com" target="_blank">Phoenix Robotix Pvt. Ltd.</a></div>
<?php echo $JQUERY_CDN; ?>
<script>
	$(function() {
		//function to invalidate a field
		var invalidate_field = function(field_id) {
			$("#" + field_id).addClass("frm-if-err");
			$("#" + field_id).parent().find(".frm-err-msg").show();
			$("#" + field_id).focus();
		};
		//function to match password
		var invalidate_match = function(field_id) {
			$("#" + field_id).addClass("frm-if-err");
			$("#frm_err_msg_match").show();
			$("#" + field_id).focus();
		};
		//function to remove all field-invalidations
		var remove_all_field_invalidations = function() {
			$(".frm-if").removeClass("frm-if-err");
			$(".frm-err-msg").hide();
			$("#suc_msg").html("").hide();
		};
		//function to show a custom error message
		var show_error_message = function(message) {
			$("#err_msg").html(message);
			$("#err_msg").show();
		};
		//function to show a custom success message
		var show_success_message = function(message) {
			$("#hide_on_success").hide();
			$("#suc_msg").html(message);
			$("#suc_msg").show();
		};
		//function to enable form submit button
		var enable_form_submit_button = function() {
			//enable the submit button
			$("#form_submit_btn").prop("disabled", false);
			$("#form_submit_btn").val("Change Password");
		};

		$("#form_submit_btn").on("click", function(event) {
			event.preventDefault();
			//hide the error div and remove the red border in case it's shown from previous error
			remove_all_field_invalidations();
			//initialize the final config variable
			var data_to_be_posted = {};
			//recent password
			if($("#old_password").val().trim() == "") {
				invalidate_field("old_password");
				return;
			} else {
				data_to_be_posted['old_password'] = $("#old_password").val().trim();
			}
			//new password
			if($("#new_password").val().trim() == "") {
				invalidate_field("new_password");
				return;
			} else {
				data_to_be_posted['new_password'] = $("#new_password").val().trim();
			}
			//renew password
			if($("#re_new_password").val().trim() == "") {
				invalidate_field("re_new_password");
				return;
			}
			//match password
			if($("#re_new_password").val().trim() != $("#new_password").val().trim()) {
				invalidate_match("re_new_password");
				return;
			}

			$.ajax({
				type: "POST",
				dataType: "json",
				url: "https://api.aurassure.com/smart_city_platform/change_password.php",
				data: {d: JSON.stringify(data_to_be_posted)},
				xhrFields: {
					withCredentials: true
				},
				beforeSend: function() {
					//disable the subm,it button
					$("#form_submit_btn").prop("disabled", true);
					$("#form_submit_btn").val("Please wait...");
				},
				success: function(data) {
					console.log(data);
					if(data.status == "success") {
						$("#suc_msg").html("Password changed successfully").show();
					} else {
						enable_form_submit_button();
						show_error_message(data.message);
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					enable_form_submit_button();
					if(textStatus == "error") {
						//error
						show_error_message("Sorry, couldn't complete your request.");
					} else if(textStatus == "timeout") {
						//timeout
						show_error_message("Sorry, your request was timed out.");
					} else {
						//unknown error
						show_error_message("Sorry, couldn't complete your request. Some error occured.");
					}
				}
			});
			console.log(data_to_be_posted);
		});
	});
</script>
</body>
</html>
