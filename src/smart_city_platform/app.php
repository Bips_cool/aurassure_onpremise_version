<?php
date_default_timezone_set("Asia/Kolkata");

if(!isset($session_handler->user_session_data['un']) || $session_handler->user_session_data['ci'] != $city_id) {
	if($city == 'vadodara' && isset($_GET['sso_token']) && $_GET['sso_token'] == '18reLv0cfylWs7co0ortdBmlHkTZtsRRDeQ4nAiEpW8nSbFRflBROCENfDEODADP') {
		//log the user in
		$session_handler->user_session_data['un'] = 'vadodara';
		$session_handler->user_session_data['ui'] = '151';
		$session_handler->user_session_data['ci'] = '7';
		$session_handler->user_session_data['user_default_city'] = '139';

		// redirect_to(strtok($_SERVER["REQUEST_URI"],'?'));
	} elseif(!in_array($city_id, [11])) {
		$session_handler->redirect_to('https://'.$city.'.aurassure.com/login');
	}
}/* elseif(isset($_GET['sso_token'])) {
	redirect_to(strtok($_SERVER["REQUEST_URI"],'?'));
}*/

//detect user agent
require_once('/var/www/phoenixrobotix/_includes/vendor/mobiledetect/mobiledetectlib/Mobile_Detect.php');
$user_agent_detector = new Mobile_Detect();
$_ROUTER['user_agent'] = 'desktop';
if($user_agent_detector->isMobile()) {
	$_ROUTER['user_agent'] = 'mobile';
} elseif($user_agent_detector->isTablet()) {
	$_ROUTER['user_agent'] = 'tablet';
}

$ROUTER['base_source_directory'] = '/var/www/aurassure/smart_city_platform/';

if($city_id == 11) {
	//Rajkot
	require_once('/var/www/aurassure/smart_city_platform/city_specific/rajkot/desktop.php');
	exit();
} else if ($city_id == 27) {
	require_once('/var/www/phoenixrobotix/src/datoms/flood_monitoring/v0.1.0/index.php');
	exit();
}

if($_ROUTER['user_agent'] == 'desktop') {
	if($city_id == 12 || $city_id == 13) {
		require_once($ROUTER['base_source_directory'].'iram_tech_desktop.php');
	} elseif($city_id == 15) {
		require_once($ROUTER['base_source_directory'].'aqua/desktop.php');
	} else {
		require_once($ROUTER['base_source_directory'].'desktop.php');
	}
} else {
	require_once($ROUTER['base_source_directory'].'mobile.php');
}
