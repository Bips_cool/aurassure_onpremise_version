<?php
date_default_timezone_set("Asia/Kolkata");

require_once("/var/www/aurassure/_includes/vendor/autoload.php");

//initialize DB connections
use Aurassure\DB\MySQLDB;
$aurassure_db = new MySQLDB();

use Aurassure\SessionManagement\SessionHandler;
//initialize session
$session_handler = new SessionHandler();

if(!isset($city)) {
	$city = '';
}

//get the city
$sql = "SELECT ct_id, ct_name, ct_is_premium FROM cities WHERE ct_subdomain='".$city."'";
$result_set = $aurassure_db->query($sql);
if(!$result_set) {
	die('Sorry, something went wrong.');
}
if(!mysqli_num_rows($result_set)) {
	$error_message = "Sorry, Aurassure hasn't come to ".ucfirst($city)." yet.<br>Want Aurassure in ".ucfirst($city)."? <a href='https://aurassure.com/contact'>Let us know.</a>";
	require_once('error.php');
	exit();
}

$result = mysqli_fetch_assoc($result_set);
$city_id = $result['ct_id'];
$city_name = $result['ct_name'];
$city_is_premium = $result['ct_is_premium'];

$fragmented_url = explode('/', strtok($_SERVER['REQUEST_URI'], '?'));

if(isset($fragmented_url[1]) && $fragmented_url[1] == 'login') {
	if($city_id == 12 || $city_id == 13) {
		require_once('iram_tech_login.php');
	} else {
		require_once('login.php');
	}
} elseif(isset($fragmented_url[1]) && $fragmented_url[1] == 'public') {
	if($city_id == 19) {
		if($fragmented_url[2] == '') {
			//redirect user to the default station dashboard of the city
			$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id=".$city_id." LIMIT 1";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				die('Sorry, something went wrong.');
			}
			if($result_set->num_rows) {
				$result = $result_set->fetch_assoc();
				$session_handler->redirect_to('https://'.$city.'.aurassure.com/public/stations/'.$result['dvcloc_id']);
			} else {
				die('Sorry, no station was found in your city');
			}
		} else {
			require_once('city_specific/coimbatore/public/desktop.php');
		}
	} else if ($city_id == 16) {
		if($fragmented_url[2] == '') {
			//redirect user to the default station dashboard of the city
			$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id=".$city_id." LIMIT 1";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				die('Sorry, something went wrong.');
			}
			if($result_set->num_rows) {
				$result = $result_set->fetch_assoc();
				$session_handler->redirect_to('https://'.$city.'.aurassure.com/public/stations/'.$result['dvcloc_id']);
			} else {
				die('Sorry, no station was found in your city');
			}
		} else {
			require_once('city_specific/siliguri/public/desktop.php');
		}
	} else if ($city_id == 20) {
		if($fragmented_url[2] == '') {
			//redirect user to the default station dashboard of the city
			$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id=".$city_id." LIMIT 1";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				die('Sorry, something went wrong.');
			}
			if($result_set->num_rows) {
				$result = $result_set->fetch_assoc();
				$session_handler->redirect_to('https://'.$city.'.aurassure.com/public/stations/'.$result['dvcloc_id']);
			} else {
				die('Sorry, no station was found in your city');
			}
		} else {
			require_once('city_specific/udaipur/public/desktop.php');
		}
	} else if ($city_id == 22) {
		if($fragmented_url[2] == '') {
			//redirect user to the default station dashboard of the city
			$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id=".$city_id." LIMIT 1";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				die('Sorry, something went wrong.');
			}
			if($result_set->num_rows) {
				$result = $result_set->fetch_assoc();
				$session_handler->redirect_to('https://'.$city.'.aurassure.com/public/stations/'.$result['dvcloc_id']);
			} else {
				die('Sorry, no station was found in your city');
			}
		} else {
			require_once('city_specific/iclei/public/desktop.php');
		}
	} else {
		require_once('login.php');
	}
} elseif(isset($fragmented_url[1]) && $fragmented_url[1] == 'logout') {
	require_once('logout.php');
} elseif(isset($fragmented_url[1]) && $fragmented_url[1] == 'forgot-password') {
	require_once('forgot_password.php');
} elseif(isset($fragmented_url[1]) && $fragmented_url[1] == 'reset-password') {
	require_once('reset_password.php');
} elseif(isset($fragmented_url[1]) && $fragmented_url[1] == 'change-password') {
	require_once('change_password.php');
} elseif(isset($fragmented_url[1]) && $fragmented_url[1] == 'get_session') {
	require_once('get_session.php');
} else {
	if($fragmented_url[1] == '') {
		if ($city_id == 27) {
			//redirect user to the default station dashboard of the city
			if ($session_handler->logged_in()) {
				require_once('/var/www/phoenixrobotix/src/datoms/flood_monitoring/v0.1.0/index.php');
			} else {
				require_once('login.php');
			}
		} else {
			//redirect user to the default station dashboard of the city
			$sql = "SELECT dvcloc_id FROM dvc_locations WHERE ct_id=".$city_id." LIMIT 1";
			$result_set = $aurassure_db->query($sql);
			if(!$result_set) {
				die('Sorry, something went wrong.');
			}
			if($result_set->num_rows) {
				$result = $result_set->fetch_assoc();
				$session_handler->redirect_to('https://'.$city.'.aurassure.com/stations/'.$result['dvcloc_id']);
			} else {
				die('Sorry, no station was found in your city');
			}
		}
	} else {
		require_once('app.php');
	}
}
