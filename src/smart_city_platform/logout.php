<?php
if($session_handler->delete_session()) {
	$session_handler->redirect_to('https://'.$city.'.aurassure.com/login');
} else {
	die('Sorry, something went wrong.');
}
