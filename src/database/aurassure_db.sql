-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: phoenixrobotix-main-db.cqmqj3e9zyxp.ap-south-1.rds.amazonaws.com:3306
-- Generation Time: Jul 26, 2018 at 10:15 AM
-- Server version: 5.6.27-log
-- PHP Version: 5.6.36

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aurassure_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `3rd_party_data_push_log`
--

CREATE TABLE `3rd_party_data_push_log` (
  `dvcloc_id` bigint(20) NOT NULL,
  `pdpl_data_received_time` bigint(10) NOT NULL,
  `pdpl_data_pushed_for` text NOT NULL,
  `pdpl_pushed_data` text NOT NULL,
  `pdpl_data_pushed_at` bigint(10) NOT NULL,
  `pdpl_response` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `alerts_log`
--

CREATE TABLE `alerts_log` (
  `del_me` int(10) UNSIGNED NOT NULL,
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `al_sqs_message` text NOT NULL,
  `al_parameter` varchar(50) NOT NULL,
  `al_time` bigint(10) UNSIGNED NOT NULL,
  `al_send_time` bigint(10) UNSIGNED NOT NULL,
  `al_response` text NOT NULL,
  `al_sms_count` tinyint(3) UNSIGNED NOT NULL,
  `al_email_count` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `alert_notifications`
--

CREATE TABLE `alert_notifications` (
  `dvcloc_id` bigint(20) NOT NULL,
  `an_type` varchar(10) NOT NULL,
  `an_text` text NOT NULL,
  `an_time` bigint(10) NOT NULL,
  `an_read_time` bigint(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_access_log`
--

CREATE TABLE `api_access_log` (
  `apat_id` bigint(20) UNSIGNED NOT NULL,
  `apal_access_time` bigint(10) UNSIGNED NOT NULL,
  `apal_access_path` varchar(256) NOT NULL,
  `apal_request_status` smallint(3) UNSIGNED NOT NULL,
  `apal_request_processing_duration` bigint(20) UNSIGNED NOT NULL,
  `apal_returned_data_length` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `api_access_tokens`
--

CREATE TABLE `api_access_tokens` (
  `apat_id` bigint(20) UNSIGNED NOT NULL,
  `apat_token` varchar(64) NOT NULL,
  `apat_last_access_time` bigint(10) UNSIGNED NOT NULL,
  `ct_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aqicn_data`
--

CREATE TABLE `aqicn_data` (
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `aqicndat_time` bigint(10) UNSIGNED NOT NULL,
  `aqicndat_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `aurassure_rkl_dmd_api_access_log`
--

CREATE TABLE `aurassure_rkl_dmd_api_access_log` (
  `ardaal_accessed_at` text NOT NULL,
  `ardaal_data_packet` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `ct_id` smallint(5) UNSIGNED NOT NULL,
  `ct_name` varchar(50) NOT NULL,
  `ct_subdomain` varchar(50) NOT NULL,
  `ct_lat` varchar(250) NOT NULL,
  `ct_long` varchar(250) NOT NULL,
  `ct_zoom_level` int(11) NOT NULL,
  `ct_is_premium` tinyint(1) UNSIGNED NOT NULL,
  `ct_send_alerts` tinyint(1) NOT NULL COMMENT '0 -> False, 1 -> True',
  `ct_mail_ids` text NOT NULL,
  `ct_threshold_limits` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpcb_data_hourly_aqi`
--

CREATE TABLE `cpcb_data_hourly_aqi` (
  `cpcb_data_station_id` bigint(20) UNSIGNED NOT NULL,
  `cdha_aqi` smallint(3) UNSIGNED NOT NULL,
  `cdha_param_aqis` text NOT NULL,
  `cdha_param_concs` text NOT NULL,
  `cdha_upto_time` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpcb_data_pollution_data`
--

CREATE TABLE `cpcb_data_pollution_data` (
  `cpcb_data_station_id` int(10) UNSIGNED NOT NULL,
  `cpcb_data_json` mediumtext NOT NULL,
  `cpcb_data_time` bigint(10) UNSIGNED NOT NULL,
  `cpcb_data_added_on` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cpcb_data_stations`
--

CREATE TABLE `cpcb_data_stations` (
  `cpcb_data_station_id` bigint(20) UNSIGNED NOT NULL,
  `cpcb_data_station_lat` double NOT NULL,
  `cpcb_data_station_long` double NOT NULL,
  `cpcb_data_state_id` int(10) UNSIGNED NOT NULL,
  `cpcb_data_state_name` varchar(100) NOT NULL,
  `cpcb_data_city_id` int(10) UNSIGNED NOT NULL,
  `cpcb_data_city_name` varchar(100) NOT NULL,
  `cpcb_data_station_code` varchar(100) NOT NULL,
  `cpcb_data_station_name` varchar(200) NOT NULL,
  `cpcb_data_added_on` bigint(10) UNSIGNED NOT NULL,
  `cpcb_data_updated_at` bigint(10) UNSIGNED NOT NULL,
  `cpcb_data_aqi` smallint(3) UNSIGNED NOT NULL,
  `cpcb_data_responsible_parameter` varchar(200) NOT NULL,
  `cpcb_data_temp` tinyint(3) UNSIGNED NOT NULL,
  `cpcb_data_humid` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dmd_api_access_log`
--

CREATE TABLE `dmd_api_access_log` (
  `dak_id` bigint(20) NOT NULL,
  `daal_accessed_at` bigint(10) NOT NULL,
  `daal_access_from_ip` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dmd_api_keys`
--

CREATE TABLE `dmd_api_keys` (
  `dak_id` bigint(20) NOT NULL,
  `dak_key` varchar(256) NOT NULL,
  `dak_stn_ids` text NOT NULL,
  `dak_last_accessed_at` bigint(10) NOT NULL,
  `dak_lat` double NOT NULL,
  `dak_long` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_8_hour_avg_data`
--

CREATE TABLE `dvc_8_hour_avg_data` (
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `dhavgd_aqi` smallint(3) UNSIGNED NOT NULL,
  `dhavgd_param_aqis` text NOT NULL,
  `dhavgd_param_concs` text NOT NULL,
  `dhavgd_param_min_max_data` text NOT NULL,
  `dhavgd_responsible_param` text NOT NULL,
  `dhavgd_upto_time` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_daily_avg_data`
--

CREATE TABLE `dvc_daily_avg_data` (
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `ddad_aqi` smallint(3) UNSIGNED NOT NULL,
  `ddad_param_aqis` text NOT NULL,
  `ddad_param_concs` text NOT NULL,
  `ddad_param_min_max_data` text NOT NULL,
  `ddad_responsible_param` text NOT NULL,
  `ddad_upto_time` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_devices`
--

CREATE TABLE `dvc_devices` (
  `dvcdvc_id` bigint(20) NOT NULL,
  `dvcdvc_qr_code` varchar(256) NOT NULL,
  `dvcdvc_imei_no` varchar(250) NOT NULL,
  `dvcdvc_mac_id` varchar(32) NOT NULL,
  `dvcdvc_refresh_token` varchar(64) NOT NULL,
  `dvcdvc_auth_token` varchar(64) NOT NULL,
  `dvcdvc_enc_data` text NOT NULL,
  `dvcdvc_state` tinyint(1) UNSIGNED NOT NULL,
  `dvcdvc_send_data_after_sec` int(10) UNSIGNED NOT NULL,
  `dvcdvc_firmware_version` varchar(15) NOT NULL,
  `dvcdvc_iot_board_qr_code` varchar(8) NOT NULL,
  `dvcdvc_iot_board_version` varchar(10) NOT NULL,
  `dvcdvc_shield_qr_code` varchar(8) NOT NULL,
  `dvcdvc_shield_version` varchar(10) NOT NULL,
  `dvcdvc_sensor_board_qr_code` varchar(8) NOT NULL,
  `dvcdvc_sensor_board_version` varchar(10) NOT NULL,
  `dvcdvc_sensor_no` varchar(10) NOT NULL,
  `dvcdvc_sim_operator` varchar(10) NOT NULL,
  `dvcdvc_sim_sl_no` varchar(32) NOT NULL,
  `dvcdvc_sim_mobile_no` varchar(10) NOT NULL,
  `dvcdvc_sim_type` varchar(10) NOT NULL,
  `dvcdvc_connectivity_options` varchar(50) NOT NULL,
  `dvcdvc_configs` text NOT NULL,
  `dvcdvc_configs_saved` text NOT NULL,
  `dvcdvc_calibration_details` text NOT NULL,
  `dvcdvc_calibration_details_saved` text NOT NULL,
  `dvcdvc_data_sending_configs` text NOT NULL,
  `dvcdvc_last_raw_data` text NOT NULL,
  `dvcdvc_last_data_receive_time` bigint(10) UNSIGNED NOT NULL,
  `dvcdvc_last_online_time` bigint(10) UNSIGNED NOT NULL,
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `dvcdvc_send_offline_alert` tinyint(1) UNSIGNED NOT NULL,
  `dvcdvc_push_data_to_3rd_party_handler` tinyint(1) UNSIGNED NOT NULL,
  `dvcdvc_archive_device` tinyint(1) NOT NULL COMMENT '0 -> Active, 1 -> Archived',
  `dvcdvc_allow_dummy_data` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_hourly_avg_data`
--

CREATE TABLE `dvc_hourly_avg_data` (
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `dhad_aqi` smallint(3) UNSIGNED NOT NULL,
  `dhad_param_aqis` text NOT NULL,
  `dhad_param_concs` text NOT NULL,
  `dhad_param_min_max_data` text NOT NULL,
  `dhad_responsible_param` text NOT NULL,
  `dhad_upto_time` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_locations`
--

CREATE TABLE `dvc_locations` (
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `dvcloc_name` varchar(100) NOT NULL,
  `dvcloc_city` varchar(100) NOT NULL,
  `dvcloc_country` varchar(50) NOT NULL,
  `dvcloc_lat` double NOT NULL,
  `dvcloc_long` double NOT NULL,
  `dvcloc_full_address` text NOT NULL,
  `dvcloc_params` varchar(256) NOT NULL,
  `dvcloc_param_units` varchar(256) NOT NULL,
  `dvcloc_time_zone` varchar(64) NOT NULL,
  `dvcloc_last_data_update_time` bigint(10) UNSIGNED NOT NULL,
  `dvcloc_aqi` smallint(3) UNSIGNED NOT NULL,
  `dvcloc_responsible_param` varchar(15) NOT NULL,
  `dvcloc_temp` tinyint(3) UNSIGNED NOT NULL,
  `dvcloc_humid` tinyint(3) UNSIGNED NOT NULL,
  `ct_id` bigint(20) UNSIGNED NOT NULL,
  `dvcloc_publicly_accessible` tinyint(1) UNSIGNED NOT NULL COMMENT '0 -> No, 1 -> Yes',
  `dvcloc_data_source` tinyint(3) UNSIGNED NOT NULL COMMENT '0 -> Aurassure, 1 -> CPCB',
  `dvcloc_other_source_station_id` bigint(20) UNSIGNED NOT NULL,
  `dvcloc_param_thresholds` text NOT NULL,
  `dvcloc_alert_users` text NOT NULL,
  `dvcloc_type` tinyint(1) UNSIGNED DEFAULT NULL COMMENT AS `1 -> Infra, 2 -> Aqua, 3-> Weather`,
  `dvcloc_parameter_thresholds` text NOT NULL,
  `dvcloc_archive_device` tinyint(1) NOT NULL COMMENT '0 -> Active, 1 -> Archived',
  `dvcloc_send_alerts` tinyint(1) NOT NULL COMMENT '0 -> False, 1 -> True',
  `sad_send_alerts` tinyint(1) NOT NULL,
  `sad_mobile_nos` text NOT NULL,
  `sad_mail_ids` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_messages`
--

CREATE TABLE `dvc_messages` (
  `dvcmsg_id` bigint(20) UNSIGNED NOT NULL,
  `dvcdvc_id` bigint(20) UNSIGNED NOT NULL,
  `dvcmsg_time` bigint(10) UNSIGNED NOT NULL,
  `dvcmsg_by` bigint(20) UNSIGNED NOT NULL,
  `dvcmsg_category` tinyint(1) UNSIGNED NOT NULL COMMENT '0 -> Configuration, 1 -> Calibration, 2-> Data sending toggle',
  `dvcmsg_details` text NOT NULL COMMENT 'This data will be sent in message body',
  `dvcmsg_received` tinyint(1) UNSIGNED NOT NULL COMMENT '0 -> No, 1 -> Yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_params`
--

CREATE TABLE `dvc_params` (
  `dvcprm_id` bigint(20) UNSIGNED NOT NULL,
  `dvcprm_name` varchar(50) NOT NULL,
  `dvcprm_name_noformat` varchar(250) NOT NULL,
  `dvcprm_name_long` varchar(250) NOT NULL,
  `dvcprm_key` varchar(250) NOT NULL,
  `dvcprm_unit` varchar(50) NOT NULL,
  `dvcprm_unit_noformat` varchar(250) NOT NULL,
  `dvcprm_thresholds` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_param_units`
--

CREATE TABLE `dvc_param_units` (
  `dpu_id` bigint(20) UNSIGNED NOT NULL,
  `dpu_unit` varchar(50) NOT NULL,
  `dpu_unit_noformat` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_raw_data`
--

CREATE TABLE `dvc_raw_data` (
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `dvcdat_time` bigint(10) UNSIGNED NOT NULL,
  `dvcdat_data` text NOT NULL,
  `dvcdat_server_time` bigint(10) UNSIGNED NOT NULL,
  `dvcdat_meta_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_raw_received_broken_packets`
--

CREATE TABLE `dvc_raw_received_broken_packets` (
  `dvcdvc_id` bigint(20) UNSIGNED NOT NULL,
  `drrbp_time` bigint(10) UNSIGNED NOT NULL,
  `drrbp_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_raw_received_data`
--

CREATE TABLE `dvc_raw_received_data` (
  `dvcdvc_id` bigint(20) UNSIGNED NOT NULL,
  `drrd_time` bigint(10) UNSIGNED NOT NULL,
  `drrd_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `dvc_temp_auth_token`
--

CREATE TABLE `dvc_temp_auth_token` (
  `dvcdvc_device_id` varchar(32) NOT NULL,
  `dvctat_refresh_token` varchar(64) NOT NULL,
  `dvctat_auth_token` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `grp_id` bigint(20) NOT NULL,
  `grp_name` varchar(255) NOT NULL,
  `grp_subdomain` varchar(255) NOT NULL,
  `grp_lat` double NOT NULL,
  `grp_long` double NOT NULL,
  `grp_zoom_lvl` int(10) NOT NULL,
  `grp_has_public` tinyint(1) NOT NULL,
  `grp_is_premium` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `news_letter_subscription`
--

CREATE TABLE `news_letter_subscription` (
  `nls_id` bigint(20) UNSIGNED NOT NULL,
  `nls_email_id` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stations_under_group`
--

CREATE TABLE `stations_under_group` (
  `grp_id` bigint(20) NOT NULL,
  `dvcloc_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `station_alerts_log`
--

CREATE TABLE `station_alerts_log` (
  `dvcloc_id` bigint(20) UNSIGNED NOT NULL,
  `sal_time` bigint(10) UNSIGNED NOT NULL,
  `sal_sms_count` tinyint(3) UNSIGNED NOT NULL,
  `sal_api_response` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `station_alert_details`
--

CREATE TABLE `station_alert_details` (
  `dvcloc_id` bigint(20) NOT NULL,
  `sad_send_alerts` tinyint(1) NOT NULL,
  `sad_mobile_nos` text NOT NULL,
  `sad_mail_ids` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_app_login`
--

CREATE TABLE `usr_app_login` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `uap_login_token` varchar(64) NOT NULL,
  `uap_login_time` bigint(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_compare_locations`
--

CREATE TABLE `usr_compare_locations` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `compared_locations_id` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_deleted_accounts`
--

CREATE TABLE `usr_deleted_accounts` (
  `usr_id` bigint(20) NOT NULL,
  `usrdel_first_name` varchar(50) NOT NULL,
  `usrdel_middle_name` varchar(50) NOT NULL,
  `usrdel_last_name` varchar(50) NOT NULL,
  `usrdel_email` varchar(254) NOT NULL,
  `usrdel_mobile` varchar(13) NOT NULL,
  `usrdel_dob` bigint(10) NOT NULL,
  `usrdel_gender` tinyint(1) NOT NULL,
  `usrdel_pass` varchar(60) NOT NULL,
  `usrdel_date_created` bigint(10) NOT NULL,
  `usrdel_force_pw_change` tinyint(1) NOT NULL,
  `usrdel_gcm_device_id` varchar(256) NOT NULL,
  `usrdel_logged_in_from_os` tinyint(1) NOT NULL,
  `usrdel_app_settings` text NOT NULL,
  `usrdel_type` tinyint(1) NOT NULL,
  `usrdel_for_city` smallint(5) NOT NULL,
  `usrdel_for_grp` smallint(5) NOT NULL,
  `usrdel_source` tinyint(1) NOT NULL,
  `usrdel_social_id` varchar(256) NOT NULL,
  `usrdel_role` tinyint(1) NOT NULL,
  `usrdel_designation` varchar(100) NOT NULL,
  `usrdel_department` varchar(100) NOT NULL,
  `usrdel_access` text NOT NULL,
  `usrdel_under` bigint(20) NOT NULL,
  `usrdel_deleted_by` varchar(256) NOT NULL,
  `usrdel_deleted_at` int(10) NOT NULL,
  `usrdel_deleted_from_ua` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_favourite_locations`
--

CREATE TABLE `usr_favourite_locations` (
  `ufvl_id` bigint(20) UNSIGNED NOT NULL,
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `ufvl_lat` double NOT NULL,
  `ufvl_long` double NOT NULL,
  `ufvl_name` varchar(256) NOT NULL,
  `ufvl_fav_name` varchar(256) NOT NULL,
  `ufvl_notify` tinyint(1) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_invalid_user_failed_login_history`
--

CREATE TABLE `usr_invalid_user_failed_login_history` (
  `uiuflh_user_name` varchar(254) NOT NULL,
  `uiuflh_time` bigint(10) UNSIGNED NOT NULL,
  `uiuflh_ip` varchar(50) NOT NULL,
  `uiuflh_from_ua` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_login`
--

CREATE TABLE `usr_login` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `usr_first_name` varchar(50) NOT NULL,
  `usr_middle_name` varchar(50) NOT NULL,
  `usr_last_name` varchar(50) NOT NULL,
  `usr_email` varchar(254) NOT NULL,
  `usr_mobile` varchar(13) NOT NULL,
  `usr_dob` bigint(10) UNSIGNED NOT NULL,
  `usr_gender` tinyint(1) UNSIGNED NOT NULL,
  `usr_pass` varchar(60) NOT NULL,
  `usr_date_created` bigint(10) UNSIGNED NOT NULL,
  `usr_force_pw_change` tinyint(1) UNSIGNED NOT NULL,
  `usr_gcm_device_id` varchar(256) NOT NULL,
  `usr_logged_in_from_os` tinyint(1) UNSIGNED NOT NULL COMMENT '1 -> android, 2 -> ios',
  `usr_app_settings` text NOT NULL,
  `usr_type` tinyint(1) UNSIGNED NOT NULL COMMENT '0 -> Normal, 1 -> City-specific',
  `usr_for_city` smallint(5) UNSIGNED NOT NULL,
  `usr_for_grp` smallint(5) NOT NULL,
  `usr_source` tinyint(1) UNSIGNED NOT NULL COMMENT '1 -> Google, 2 -> Facebook',
  `usr_social_id` varchar(256) NOT NULL,
  `usr_role` tinyint(1) UNSIGNED NOT NULL COMMENT '0 -> Normal, 1 -> Manager, 2 -> Admin',
  `usr_designation` varchar(100) NOT NULL,
  `usr_department` varchar(100) NOT NULL,
  `usr_access` text NOT NULL,
  `usr_under` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_login_history`
--

CREATE TABLE `usr_login_history` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `login_from` tinyint(1) UNSIGNED NOT NULL COMMENT '1->website, 2->app',
  `login_time` bigint(10) UNSIGNED NOT NULL,
  `login_ip` varchar(50) NOT NULL,
  `login_from_ua` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_register`
--

CREATE TABLE `usr_register` (
  `del_me` int(10) UNSIGNED NOT NULL,
  `usreg_first_name` varchar(50) NOT NULL,
  `usreg_middle_name` varchar(50) NOT NULL,
  `usreg_last_name` varchar(50) NOT NULL,
  `usreg_email` varchar(254) NOT NULL,
  `usreg_mobile` varchar(13) NOT NULL,
  `usreg_dob` bigint(10) UNSIGNED NOT NULL,
  `usreg_gender` tinyint(1) UNSIGNED NOT NULL,
  `usreg_pass` varchar(60) NOT NULL,
  `usreg_email_verification_token` varchar(256) NOT NULL,
  `usreg_date_created` bigint(10) UNSIGNED NOT NULL,
  `usreg_type` tinyint(1) UNSIGNED NOT NULL COMMENT '0 -> Normal, 1 -> City-specific',
  `usreg_for_city` smallint(5) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_reset_password`
--

CREATE TABLE `usr_reset_password` (
  `del_me` int(10) UNSIGNED NOT NULL,
  `urp_email` varchar(254) NOT NULL,
  `urp_reset_token` varchar(256) NOT NULL,
  `urp_request_time` bigint(10) UNSIGNED NOT NULL,
  `urp_request_from_ua` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_sessions`
--

CREATE TABLE `usr_sessions` (
  `usrs_session_id` varchar(100) NOT NULL,
  `usrs_session_data` text NOT NULL,
  `usrs_login_time` bigint(10) UNSIGNED NOT NULL,
  `usrs_logout_time` bigint(10) UNSIGNED NOT NULL,
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `usrs_login_ip` varchar(50) NOT NULL,
  `usrs_login_from_ua` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `usr_valid_user_failed_login_history`
--

CREATE TABLE `usr_valid_user_failed_login_history` (
  `usr_id` bigint(20) UNSIGNED NOT NULL,
  `uvuflh_time` bigint(10) UNSIGNED NOT NULL,
  `uvuflh_ip` varchar(50) NOT NULL,
  `uvuflh_from_ua` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `3rd_party_data_push_log`
--
ALTER TABLE `3rd_party_data_push_log`
  ADD KEY `dvcloc_id` (`dvcloc_id`),
  ADD KEY `pdpl_data_received_time` (`pdpl_data_received_time`),
  ADD KEY `locid_rectime` (`dvcloc_id`,`pdpl_data_received_time`);

--
-- Indexes for table `alerts_log`
--
ALTER TABLE `alerts_log`
  ADD PRIMARY KEY (`del_me`);

--
-- Indexes for table `alert_notifications`
--
ALTER TABLE `alert_notifications`
  ADD KEY `an_read_time` (`an_read_time`),
  ADD KEY `an_time` (`an_time`),
  ADD KEY `dvcloc_id` (`dvcloc_id`);

--
-- Indexes for table `api_access_log`
--
ALTER TABLE `api_access_log`
  ADD KEY `apat_id` (`apat_id`),
  ADD KEY `apal_access_time` (`apal_access_time`),
  ADD KEY `apat_id_apal_access_time` (`apat_id`,`apal_access_time`);

--
-- Indexes for table `api_access_tokens`
--
ALTER TABLE `api_access_tokens`
  ADD PRIMARY KEY (`apat_id`),
  ADD KEY `apat_last_access_time` (`apat_last_access_time`),
  ADD KEY `ct_id` (`ct_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`ct_id`);

--
-- Indexes for table `cpcb_data_stations`
--
ALTER TABLE `cpcb_data_stations`
  ADD PRIMARY KEY (`cpcb_data_station_id`);

--
-- Indexes for table `dmd_api_keys`
--
ALTER TABLE `dmd_api_keys`
  ADD PRIMARY KEY (`dak_id`);

--
-- Indexes for table `dvc_8_hour_avg_data`
--
ALTER TABLE `dvc_8_hour_avg_data`
  ADD KEY `dvcloc_id` (`dvcloc_id`),
  ADD KEY `dhad_upto_time` (`dhavgd_upto_time`);

--
-- Indexes for table `dvc_daily_avg_data`
--
ALTER TABLE `dvc_daily_avg_data`
  ADD KEY `dvcloc_id` (`dvcloc_id`),
  ADD KEY `dhad_upto_time` (`ddad_upto_time`);

--
-- Indexes for table `dvc_devices`
--
ALTER TABLE `dvc_devices`
  ADD PRIMARY KEY (`dvcdvc_id`);

--
-- Indexes for table `dvc_hourly_avg_data`
--
ALTER TABLE `dvc_hourly_avg_data`
  ADD KEY `dvcloc_id` (`dvcloc_id`),
  ADD KEY `dhad_upto_time` (`dhad_upto_time`),
  ADD KEY `locid_uptotime` (`dvcloc_id`,`dhad_upto_time`);

--
-- Indexes for table `dvc_locations`
--
ALTER TABLE `dvc_locations`
  ADD PRIMARY KEY (`dvcloc_id`);

--
-- Indexes for table `dvc_messages`
--
ALTER TABLE `dvc_messages`
  ADD PRIMARY KEY (`dvcmsg_id`),
  ADD KEY `dvcdvc_id` (`dvcdvc_id`),
  ADD KEY `dvcmsg_time` (`dvcmsg_time`),
  ADD KEY `dvcmsg_received` (`dvcmsg_received`);

--
-- Indexes for table `dvc_params`
--
ALTER TABLE `dvc_params`
  ADD PRIMARY KEY (`dvcprm_id`);

--
-- Indexes for table `dvc_param_units`
--
ALTER TABLE `dvc_param_units`
  ADD PRIMARY KEY (`dpu_id`);

--
-- Indexes for table `dvc_raw_data`
--
ALTER TABLE `dvc_raw_data`
  ADD KEY `dvcloc_id` (`dvcloc_id`),
  ADD KEY `dvcdat_time` (`dvcdat_time`),
  ADD KEY `locid_dattime` (`dvcloc_id`,`dvcdat_time`);

--
-- Indexes for table `dvc_raw_received_data`
--
ALTER TABLE `dvc_raw_received_data`
  ADD KEY `dvcdvc_id` (`dvcdvc_id`),
  ADD KEY `drrd_time` (`drrd_time`),
  ADD KEY `dvcid_datatime` (`dvcdvc_id`,`drrd_time`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`grp_id`);

--
-- Indexes for table `news_letter_subscription`
--
ALTER TABLE `news_letter_subscription`
  ADD PRIMARY KEY (`nls_id`);

--
-- Indexes for table `usr_favourite_locations`
--
ALTER TABLE `usr_favourite_locations`
  ADD PRIMARY KEY (`ufvl_id`);

--
-- Indexes for table `usr_login`
--
ALTER TABLE `usr_login`
  ADD PRIMARY KEY (`usr_id`);

--
-- Indexes for table `usr_register`
--
ALTER TABLE `usr_register`
  ADD PRIMARY KEY (`del_me`);

--
-- Indexes for table `usr_reset_password`
--
ALTER TABLE `usr_reset_password`
  ADD PRIMARY KEY (`del_me`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alerts_log`
--
ALTER TABLE `alerts_log`
  MODIFY `del_me` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `api_access_tokens`
--
ALTER TABLE `api_access_tokens`
  MODIFY `apat_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `ct_id` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `cpcb_data_stations`
--
ALTER TABLE `cpcb_data_stations`
  MODIFY `cpcb_data_station_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `dmd_api_keys`
--
ALTER TABLE `dmd_api_keys`
  MODIFY `dak_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `dvc_devices`
--
ALTER TABLE `dvc_devices`
  MODIFY `dvcdvc_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=186;
--
-- AUTO_INCREMENT for table `dvc_locations`
--
ALTER TABLE `dvc_locations`
  MODIFY `dvcloc_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=257;
--
-- AUTO_INCREMENT for table `dvc_messages`
--
ALTER TABLE `dvc_messages`
  MODIFY `dvcmsg_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10822;
--
-- AUTO_INCREMENT for table `dvc_params`
--
ALTER TABLE `dvc_params`
  MODIFY `dvcprm_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `dvc_param_units`
--
ALTER TABLE `dvc_param_units`
  MODIFY `dpu_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `grp_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `news_letter_subscription`
--
ALTER TABLE `news_letter_subscription`
  MODIFY `nls_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `usr_favourite_locations`
--
ALTER TABLE `usr_favourite_locations`
  MODIFY `ufvl_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=137;
--
-- AUTO_INCREMENT for table `usr_login`
--
ALTER TABLE `usr_login`
  MODIFY `usr_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269;
--
-- AUTO_INCREMENT for table `usr_register`
--
ALTER TABLE `usr_register`
  MODIFY `del_me` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT for table `usr_reset_password`
--
ALTER TABLE `usr_reset_password`
  MODIFY `del_me` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
