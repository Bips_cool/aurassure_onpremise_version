import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import ReactHighcharts from 'react-highcharts';
import { Scrollbars } from 'react-custom-scrollbars';

/**
 * This class is used to render the Station Ananlytics Graph page.
 */
export default class StationAnalyticsGraph extends React.Component {
	/**
	 * This is the Constructor for StationAnalyticsGraph class to set the default task while page load.
	 * @param  {Object} params This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super();
		let parameter = '',
			time = ''
		if (props.location.search && props.location.search.split('?param=')[1] && props.location.search.split('&time=')[1]) {
			parameter = props.location.search.split('?param=')[1].split('&time=')[0];
			time = props.location.search.split('&time=')[1];
		}

		/**
		 * This sets the initial state for this class.
		 * @type {Object}
		 */
		this.state = {
			station_data: null,
			source: 'Aurassure',
			current_param: parameter,
			station_id: props.params.station_id,
			time: parseInt(moment(time,'DD-MM-YYYY,hh:mm:ss').tz('Asia/Kolkata').format('X'))
		};

		/**
		 * This is a global calling function for saving station id for the session.
		 */
		saveStationId(props.params.station_id);

		/**
		 * This is a calling function for fetchStationData.
		 */
		this.fetchStationData(props.params.station_id);
	}

	/**
	 * This function calls API to get data for snapshot & save the values to state.
	 * @param  {Number} id Station Id
	 */
	fetchStationData(id) {
		saveStationId(id);
		var that = this;
		that.setState({station_data: null});
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_data_for_snapshot.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: id,
				time: this.state.time
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			if (json.status === 'success') {
				that.setState({
					station_id: id,
					station_data: (json.param_values) ? json : null
				});
				console.log('Data', that.state.station_data);
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function returns the current parameter name.
	 * @param  {String} parameter Parameter Key
	 * @return {String}           Parameter Name
	 */
	currentParameter(parameter) {
		let param_name = null;
		this.state.station_data.params.map((param) => {
			if (param.key == parameter) {
				param_name = param.name;
			}
		});
		return param_name;
	}

	/**
	 * This method is called right after when a state value is changed.
	 */
	componentDidUpdate(prevProps, prevState) {
		console.log(prevProps, prevState);
		let parameter = '',
			time = '';
		if (this.props.location.search && this.props.location.search.split('?param=')[1] && this.props.location.search.split('&time=')[1]) {
			parameter = this.props.location.search.split('?param=')[1].split('&time=')[0];
			time = parseInt(moment(this.props.location.search.split('&time=')[1],'DD-MM-YYYY,HH:mm:ss').tz('Asia/Kolkata').format('X'));
		}
		if (prevState.current_param != parameter || prevState.time != time) {
			this.setState({
				current_param: parameter,
				time: time
			});
			console.log ('parameter', parameter);
			console.log ('time', time);
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Analytics - Aurassure';
	}

	/**
	 * This renders entire class with navigation bar.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container">
				{(() => {
					if (this.state.station_data === null) {
						return <div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
					} else {
						return <div id="analyticsContainer" className="margin-top">
							{(() => {
								let line_chart_data = [];
								let unit = '';
								let graph_colors = [
									'#43429a',
									'#07adb1',
									'#a44a9c',
									'#f4801f',
									'#c14040',
									'#6fccdd',
									'#61c3ab',
									'#56bc7b',
									'#e2da3e',
									'#41ce00',
									'#aa4728',
									'#b3d23c',
									'#a0632a',
									'#7156a3',
									'#3d577f',
									'#ee3352',
									'#43429a',
									'#07adb1',
									'#a44a9c',
									'#f4801f',
									'#c14040',
									'#6fccdd',
									'#61c3ab',
									'#56bc7b'
								];
								let graph_fill_color = [
									'rgba(67, 66, 154, 0.05)',
									'rgba(7, 173, 177, 0.05)',
									'rgba(164, 74, 156, 0.05)',
									'rgba(244, 128, 31, 0.05)',
									'rgba(193, 64, 64, 0.05)',
									'rgba(111, 204, 221, 0.05)',
									'rgba(97, 195, 171, 0.05)',
									'rgba(86, 188, 123, 0.05)',
									'rgba(226, 218, 62, 0.05)',
									'rgba(65, 206, 0, 0.05)',
									'rgba(170, 71, 40, 0.05)',
									'rgba(179, 210, 60, 0.05)',
									'rgba(160, 99, 42, 0.05)',
									'rgba(113, 86, 163, 0.05)',
									'rgba(61, 87, 127, 0.05)',
									'rgba(238, 51, 82, 0.05)',
									'rgba(67, 66, 154, 0.05)',
									'rgba(7, 173, 177, 0.05)',
									'rgba(164, 74, 156, 0.05)',
									'rgba(244, 128, 31, 0.05)',
									'rgba(193, 64, 64, 0.05)',
									'rgba(111, 204, 221, 0.05)',
									'rgba(97, 195, 171, 0.05)',
									'rgba(86, 188, 123, 0.05)'
								];
								let i;

								if (this.state.station_data && this.state.current_param && this.state.station_data.param_values[this.state.current_param]) {
									this.state.station_data.param_values[this.state.current_param].map((value, index) => {
										line_chart_data.push([
											this.state.station_data.time_stamps[index] * 1000,
											parseFloat(value)
										]);
									});

									this.state.station_data.params.map((parameter, index) => {
										if (this.state.current_param === parameter.key) {
											unit = this.state.station_data.param_units[index];
											/**
											 * Set current parameter unit
											 * @type {String}
											 */
											this.current_param_unit = unit;
											i = index;
										}
									});

									/**
									 * This is the configuration object for the Line Chart of Highcharts.
									 * @type {Object}
									 */
									this.line_chart_config = {
										chart: {
											zoomType: 'x',
											height: 270
										},
										title: {
											text: ''
										},
										subtitle: {
											text: ''
										},
										xAxis: {
											type: 'datetime',
											text: 'Time',
											title: {
												text: 'Time'
											}
										},
										yAxis: {
											title: {
												text: unit
											},floor: 0,
										},
										legend: {
											enabled: false
										},
										tooltip: {
											pointFormat: '<span style="color:{point.color}">' + (this.currentParameter(this.state.current_param)) + ' Conc.: ' + '<b>{point.y}</b> ' + unit + '<br/>',
											useHTML: true
										},
										plotOptions: {
											area: {
												marker: {
													radius: 0
												},
												lineWidth: 1,
												states: {
													hover: {
														lineWidth: 1
													}
												},
												threshold: null
											}
										},

										series: [{
											type: 'area',
											color: graph_colors[i],
											fillColor: graph_fill_color[i],
											data: line_chart_data
										}]
									};
								}

								ReactHighcharts.Highcharts.setOptions({
									global: {
										useUTC: false
									}
								});

								return(
									<span>
										<div id="middleAnalyticsPanel" className="width-full">
											<Scrollbars autoHide>
												{(() => {
													if (this.state.station_data.param_values.length !== 0) {
														let allowed_parameters_in_order = ['pm1', 'pm2.5', 'pm10', 'so2', 'no', 'no2', 'co', 'o3', 'co2', 'o2', 'temperature', 'humidity', 'rain', 'uv', 'light', 'noise', 'voc', 'lint', 'nox', 'ch4', 'lead', 'wspeed', 'press', 'no3', 'ph', 'do', 'nh4', 'orp', 'us_mb', 'cur', 'nh3', 'us_hc04', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'];
														let current_params = {};
														this.state.station_data.params.map((param) => {
															current_params[param.key] = param;
														});
														let parameters = allowed_parameters_in_order.map((param, i) => {
															if(Object.keys(current_params).indexOf(param) >= 0) {
																return(
																	<div className={'parameter-btn hellip' + ((param === this.state.current_param)? ' active': '')} dangerouslySetInnerHTML={{__html: current_params[param].name}} onClick={() => {this.change_param(param);}} key={i} />
																);
															}
														});
														return(
															<div>
																<ReactHighcharts ref="real_time_chart" config={this.line_chart_config} />
																<div className="avg-report-hourly">
																</div>
															</div>
														);
													} else {
														return(<div className="flex no-data-text full-height">
															<center className="table">No data received in last 24 hours.</center>
														</div>);
													}
												})()}
											</Scrollbars>
										</div>
									</span>
								);
							})()}
						</div>;
					}
				})()}
			</div>
		);
	}
}

StationAnalyticsGraph.contextTypes = {
	router: React.PropTypes.object
};
