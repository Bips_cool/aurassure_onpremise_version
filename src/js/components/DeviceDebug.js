import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';
import DebugForm from './imports/DebugForm';

/**
 * This Class Used for Device Debug page.
 */
export default class DeviceDebug extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			device_id: props.params.device_id ? props.params.device_id : 1,
			error_data: null,
			city_id: null,
			time_interval: null,
			status_msg: ''
		};
		/**
		 * This is used for City name.
		 * @type {string}
		 */
		this.city_name = window.location.host.split('.')[0];
		saveStationId(this.state.device_id);
	}

	/**
	 * This function is validate the TO time is grater than FROM time or not.
	 */
	checkTime() {
		if (parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')) < parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))) {
			return true;
		} else {
			this.setState({status_msg: 'Invalid date duration!'});
			return false;
		}
	}

	/**
	 * This function used to fetch device error log.
	 * @param  {number} dev_id This is the selected device id.
	 */
	get_device_error_log(dev_id) {
		if(this.checkTime()) {
			var that = this;
			if (!(dev_id == 0)) {
				console.log('setting the state here', this.state);
				this.setState({
					status_msg: '',
					error_data: null
				});
				console.log('checking the state here', this.state);
				let device_id = dev_id ? dev_id : that.props.params.device_id;
				that.context.router.transitionTo('/debug/'+device_id);
				let time_interval = [(parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))),(parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')))];

				saveStationId(device_id);

				fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_device_error_log.php', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					credentials: 'include',
					body: JSON.stringify({
						device_id: device_id,
						time_interval: dev_id ? time_interval : null
					})
				}).then(function(Response) {
					return Response.json();
				}).then(function(json) {
					console.log('All Error Data', json);
					if (json.status === 'success') {
						let ct_id = that.state.data_sources.map((value) => {
							if (device_id == value.id) {
								return value.ct_id;
							}
						});
						that.setState({
							error_data: json.error_logs,
							device_id: device_id,
							city_id: ct_id.filter(Boolean).join(),
							time_interval: json.time_interval ? json.time_interval : time_interval
						});

						console.log('Time interval',that.state.time_interval);
						document.querySelector('.from_date_time .form-control').value = that.state.time_interval ? moment.unix(that.state.time_interval[0]).tz('Asia/Kolkata').format('DD-MM-YYYY HH:mm') : document.querySelector('.upto_date_time .form-control').value;
						document.querySelector('.upto_date_time .form-control').value = that.state.time_interval ? moment.unix(that.state.time_interval[1]).tz('Asia/Kolkata').format('DD-MM-YYYY HH:mm') : document.querySelector('.upto_date_time .form-control').value;
					} else {
						that.setState({status_msg: json.message});
					}
				}).catch(function(ex) {
					console.log('parsing failed', ex);
					that.setState({status_msg: 'Unable to load data!'});
				});
			} else {
				that.setState({status_msg: 'Please select a valid station!'});
			}
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		if ((this.city_name === 'admin') || (this.city_name === '127')) {
			document.title = 'Debug Logs - Aurassure';

			var that = this;
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_device_list.php', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				credentials: 'include',
				body: JSON.stringify({
					device_id: that.state.device_id
				})
			}).then(function(Response) {
				return Response.json();
			}).then(function(json) {
				console.log('Data Sources',json);
				if (json.status === 'success') {
					let ct_id = json.devices.map((value) => {
						if (that.state.device_id == value.id) {
							return value.ct_id;
						}
					});
					that.setState({
						city_id: ct_id.filter(Boolean).join(),
						data_sources_city: json.cities,
						data_sources: json.devices
					});
					if (that.props.params.device_id) {
						that.get_device_error_log();
					} else if (that.state.city_id) {
						that.get_device_error_log(that.state.city_id);
					} else {
						that.setState({status_msg: 'Please select a station to view data.'});
					}
				} else {
					that.setState({status_msg: json.message});
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.setState({status_msg: 'Unable to load data!'});
			});
		} else {
			document.title = '404 Not Found - Aurassure';
		}
	}

	/**
	 * This renders the Device Debug page.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container device-config-container">
				<NavLink active="debug"/>
				{(() => {
					if ((this.city_name === 'admin') || (this.city_name === '127')) {
						return <Scrollbars autoHide>
							<DebugForm {...this.state} get_device_error_log={(id) => this.get_device_error_log(id)} />
							{(() => {
								if(this.state.status_msg) {
									return <div className="debug-err-msg">{this.state.status_msg}</div>;
								} else if(!this.state.error_data) {
									return <div className="text-center">
										<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
									</div>;
								} else {
									//console.log(this.state.archive_data);
									if(this.state.error_data && (this.state.error_data.length === 0)) {
										return(
											<center className="debug-no-data-text">No data available within the selected period.</center>
										);
									} else {
										let error_report = [];
										if(this.state.error_data) {
											return(
												<div>
													{(() => {
														let dataSourceCities = this.state.data_sources_city.map((value) => {
															if (this.state.city_id && this.state.city_id == value.id) {
																return(<span className="capitalize">{value.name}</span>);
															}
														});
														let dataPlaceNames = this.state.data_sources.map((dname) => {
															if (this.state.device_id == dname.id) {
																return(<span className="capitalize">{dname.name}</span>);
															}
														});
														return(
															<div className="view-debug-data-title">Reports of {dataPlaceNames} ({dataSourceCities}) From {this.state.time_interval ? moment.unix(this.state.time_interval[0]).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss') : document.querySelector('.from_date_time .form-control').value} To {this.state.time_interval ? moment.unix(this.state.time_interval[1]).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss') : document.querySelector('.upto_date_time .form-control').value}</div>
														);
													})()}
													<div className="view-debug-data-title">
														No. of data points: {this.state.error_data.length}
													</div>
													<div className="archive-table-container debug-table-container">
														<table className="table table-stripped table-bordered">
															<thead>
																<tr>
																	<th>Data Record Time</th>
																	<th>Server Time</th>
																	<th>Errors</th>
																</tr>
															</thead>
															{(() => {
																let error_data_rows = [];
																if (this.state.error_data) {
																	this.state.error_data.map((error, j) => {
																		if (error.error) {
																			error_data_rows.push(<tr className={'debug-' + error.color} key={j}>
																				<td>{moment.unix(error.timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</td>
																				<td>{error.server_time !== '0' ? moment.unix(error.server_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss') : 'NA'}</td>
																				<td>{error.error}</td>
																			</tr>);
																		} else {
																			error_data_rows.push(<tr key={j}>
																				<td>{moment.unix(error.timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</td>
																				<td>{error.server_time !== '0' ? moment.unix(error.server_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss') : 'NA'}</td>
																				<td>NA</td>
																			</tr>);
																		}
																	});
																}
																return(
																	<tbody>
																		{error_data_rows}
																	</tbody>
																);
															})()}
														</table>
													</div>
												</div>
											);
										}
										return(
											<div>
												{error_report}
											</div>
										);
									}
								}
							})()}
						</Scrollbars>;
					} else {
						return <h3><center>404 - Not Found</center></h3>;
					}
				})()}
			</div>
		);
	}
}

DeviceDebug.contextTypes = {
	router: React.PropTypes.object
};