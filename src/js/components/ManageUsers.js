import React from 'react';
import NavLink from './NavLink';
import UserCard from './imports/UserCard';
import moment from 'moment-timezone';
import MultiSelect from './imports/MultiSelect';
import { Scrollbars } from 'react-custom-scrollbars';

/**
 * This class is used to render the Manage Users page.
 */
export default class ManageUsers extends React.Component {
	/**
	 * This is the Constructor for ManageUsers class to set the default task while page load.
	 * @param  {Object} params This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);

		/**
		 * This sets the initial state for this class.
		 * @type {Object}
		 */
		this.state = {
			id: (document.getElementById('user_id')) ? document.getElementById('user_id').value : 1,
			city: (document.getElementById('city_id')) ? document.getElementById('city_id').value : 1,
			users: null,
			add_user: null,
			edit_user: null,
			remove_user: null,
			reset_password: null,
			submitting_data: null,
			selected_stations: [],
			master_access: false,
			stations_placeholder: 'Select Stations'
		};

		/**
		 * This is a calling function for getUserData.
		 */
		this.getUserData();
		this.getAllStations();
	}

	/**
	 * This function calls API to get user data during page load.
	 */
	getUserData() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_user_data.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				user_city: that.state.city
			})
		}).then(function(Response) {
			return Response.json()
		}).then(function(json) {
			console.log(json);
			if(json.status === 'success') {
				that.setState({
					users: json.users,
					users_login: json.users_login
				});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	getAllStations() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_stations.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				that.setState({
					all_stations: json.stations
				}, () => {that.setStationList();});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	getStationIds(values) {
		let that = this;
		console.log('values stations', values);
		
		this.setState({
			selected_stations: values
		});
	}

	setStationList(){
		let that = this,
			all_stations_list = {};
		if (that.state.all_stations && Object.keys(that.state.all_stations).length) {
			that.state.all_stations.map((station, index) => {
				// console.log('key', index);
				all_stations_list[station.id] = station.name;
			});
		}
		console.log('station_lists', all_stations_list);
		that.setState({
			all_stations_list: all_stations_list
		}, () => {
			that.resetSelectedStations();
		});
	}

	resetSelectedStations(){
		let that = this,
			selected = [],
			checked_parameters = [];
		if (that.state.selected_stations) {
			that.state.selected_stations.map((id) => {
				that.state.all_stations.map((station, index) => {
					if(station.id == id && selected.indexOf(id) === -1) {
						selected.push(id);
					}
				});
			});
		}

		console.log('selected', selected);

		that.setState({
			selected_stations: selected
		});
	}

	is_valid_json(json_string) {
		try {
			JSON.parse(json_string);
		} catch (e) {
			return false;
		}
		return true;
	}

	/**
	 * This function calls API to add new user & add the values into the user array in state.
	 */
	addUser() {
		var that = this;
		var error = null;
		if (!document.getElementById('fname').value.trim()) {
			error = 'First Name cannot be left blank!';
		} else if (!document.getElementById('email').value.trim()) {
			error = 'Contact Email cannot be left blank!';
		} else if (!document.getElementById('phone').value.trim()) {
			error = 'Contact Phone cannot be left blank!';
		} else if (!document.getElementById('password').value || !document.getElementById('password_confirm').value) {
			error = 'Please Enter & Confirm Password!';
		} else if (document.getElementById('password').value != document.getElementById('password_confirm').value) {
			error = 'Passwords did not match!';
		} else if (document.getElementById('role').value == "") {
			error = 'Please assign a role to the user!';
		}
		if (error) {
			showPopup('danger',error);
		} else {
			console.log('Add User');
			that.setState({submitting_data: 1});
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'add_new_user.php', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				credentials: 'include',
				body: JSON.stringify({
					user_fname: document.getElementById('fname').value,
					user_mname: document.getElementById('mname').value,
					user_lname: document.getElementById('lname').value,
					designation: document.getElementById('designation').value,
					department: document.getElementById('department').value,
					user_email: document.getElementById('email').value,
					user_phone: document.getElementById('phone').value,
					user_pass: document.getElementById('password').value,
					user_role: document.getElementById('role').value,
					user_city: this.state.city,
					selected_stations: this.state.selected_stations,
					master_access: this.state.master_access
				})
			}).then(function(Response) {
				return Response.json()
			}).then(function(json) {
				console.log(json);
				if(json.status === 'success') {
					var users = that.state.users;
					var new_user_data = {
						usr_access: that.state.master_access ? '*' : JSON.stringify(that.state.selected_stations),
						usr_department: document.getElementById('department').value,
						usr_designation: document.getElementById('designation').value,
						usr_email: document.getElementById('email').value,
						usr_first_name: document.getElementById('fname').value,
						usr_id: json.id,
						usr_last_name: document.getElementById('lname').value,
						usr_middle_name: document.getElementById('mname').value,
						usr_mobile: document.getElementById('phone').value,
						usr_role: document.getElementById('role').value,
						usr_under: json.uid
					};
					users.push(new_user_data);
					that.setState({
						users: users,
						add_user:null,
						selected_stations: [],
						master_access: false
					});
					document.getElementById('fname').value = '';
					document.getElementById('mname').value = '';
					document.getElementById('lname').value = '';
					document.getElementById('designation').value = '';
					document.getElementById('department').value = '';
					document.getElementById('email').value = '';
					document.getElementById('phone').value = '';
					document.getElementById('password').value = '';
					document.getElementById('password_confirm').value = '';
					document.getElementById('role').value = '0';
					showPopup('success', 'User added successfully.');
				} else {
					showPopup('danger',json.message);
				}
				that.setState({submitting_data: null});
			}).catch(function(ex) {
				that.setState({submitting_data: null});
				console.log('parsing failed', ex);
				showPopup('danger','Unable to load data!');
			});
		}
	}

	/**
	 * This function calls API to edit user & update the values into the user array in state.
	 */
	editUser() {
		var that = this;
		var error = null;
		if (!document.getElementById('edit_fname').value.trim()) {
			error = 'First Name cannot be left blank!';
		} else if (!document.getElementById('edit_email').value.trim()) {
			error = 'Contact Email cannot be left blank!';
		} else if (!document.getElementById('edit_phone').value.trim()) {
			error = 'Contact Phone cannot be left blank!';
		} else if (document.getElementById('edit_role').value == "") {
			error = 'Please assign a role to the user!';
		}
		if (error) {
			showPopup('danger',error);
		} else {
			console.log('Edit User');
			that.setState({submitting_data: 1});
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'edit_user.php', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				credentials: 'include',
				body: JSON.stringify({
					user_id: that.state.edit_user,
					user_fname: document.getElementById('edit_fname').value,
					user_mname: document.getElementById('edit_mname').value,
					user_lname: document.getElementById('edit_lname').value,
					designation: document.getElementById('edit_designation').value,
					department: document.getElementById('edit_department').value,
					user_email: document.getElementById('edit_email').value,
					user_phone: document.getElementById('edit_phone').value,
					user_role: document.getElementById('edit_role').value,
					selected_stations: that.state.selected_stations,
					master_access: that.state.master_access
				})
			}).then(function(Response) {
				return Response.json()
			}).then(function(json) {
				console.log(json);
				if(json.status === 'success') {
					var users = that.state.users;
					that.state.users.map((user, index) => {
						if (that.state.edit_user == user.usr_id) {
							users[index]['usr_access'] = that.state.master_access ? '*' : JSON.stringify(that.state.selected_stations);
							users[index]['usr_department'] = document.getElementById('edit_department').value;
							users[index]['usr_designation'] = document.getElementById('edit_designation').value;
							users[index]['usr_email'] = document.getElementById('edit_email').value;
							users[index]['usr_first_name'] = document.getElementById('edit_fname').value;
							users[index]['usr_last_name'] = document.getElementById('edit_lname').value;
							users[index]['usr_middle_name'] = document.getElementById('edit_mname').value;
							users[index]['usr_mobile'] = document.getElementById('edit_phone').value;
							users[index]['usr_role'] = document.getElementById('edit_role').value;
						}
					});
					that.setState({
						users: users,
						edit_user:null,
						selected_stations: []
					});
					document.getElementById('edit_fname').value = '';
					document.getElementById('edit_mname').value = '';
					document.getElementById('edit_lname').value = '';
					document.getElementById('edit_designation').value = '';
					document.getElementById('edit_department').value = '';
					document.getElementById('edit_email').value = '';
					document.getElementById('edit_phone').value = '';
					document.getElementById('edit_role').value = '0';
					showPopup('success', 'User updated successfully.');
				} else {
					showPopup('danger',json.message);
				}
				that.setState({submitting_data: null});
			}).catch(function(ex) {
				that.setState({submitting_data: null});
				console.log('parsing failed', ex);
				showPopup('danger','Unable to load data!');
			});
		}
	}

	/**
	 * This function calls API to request to reset password.
	 */
	resetPassword() {
		var that = this;
		var error = null;
		if (!document.getElementById('reset_password').value || !document.getElementById('reset_password_confirm').value) {
			error = 'Please Enter & Confirm Password!';
		} else if (document.getElementById('reset_password').value != document.getElementById('reset_password_confirm').value) {
			error = 'Passwords did not match!';
		}
		if (error) {
			showPopup('danger',error);
		} else {
			console.log('Reset Password');
			that.setState({submitting_data: 1});
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'reset_password.php', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				credentials: 'include',
				body: JSON.stringify({
					user_id: that.state.reset_password,
					user_pass: document.getElementById('reset_password').value
				})
			}).then(function(Response) {
				return Response.json()
			}).then(function(json) {
				console.log(json);
				if(json.status === 'success') {
					that.setState({reset_password: null});
					document.getElementById('reset_password').value = '';
					document.getElementById('reset_password_confirm').value = '';
					showPopup('success', 'Password Reset successfully.');
				} else {
					showPopup('danger',json.message);
				}
				that.setState({submitting_data: null});
			}).catch(function(ex) {
				that.setState({submitting_data: null});
				console.log('parsing failed', ex);
				showPopup('danger','Unable to load data!');
			});
		}
	}

	/**
	 * This function calls API to remove user & update the values into the user array in state.
	 */
	removeUser() {
		var that = this;
		console.log('Remove User');
		that.setState({submitting_data: 1});
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'remove_user.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				user_id: that.state.remove_user
			})
		}).then(function(Response) {
			return Response.json()
		}).then(function(json) {
			console.log(json);
			if(json.status === 'success') {
				var users = [];
				that.state.users.map((user, index) => {
					if (that.state.remove_user != user.usr_id) {
						users.push(user);
					}
				});
				that.setState({
					users: users,
					remove_user:null
				});
				showPopup('success', 'User Removed successfully.');
			} else {
				showPopup('danger',json.message);
			}
			that.setState({submitting_data: null});
		}).catch(function(ex) {
			that.setState({submitting_data: null});
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function is used to fill the fields to edit data of a user.
	 * @param  {Number} id User Id of which the user data will be filled.
	 */
	editUserFillData(id) {
		console.log("Edit:", id);
		var that = this;
		that.state.users.map((user, index) => {
			if (id == user.usr_id) {
				document.getElementById('edit_fname').value = user.usr_first_name;
				document.getElementById('edit_mname').value = user.usr_middle_name;
				document.getElementById('edit_lname').value = user.usr_last_name;
				document.getElementById('edit_designation').value = user.usr_designation;
				document.getElementById('edit_department').value = user.usr_department;
				document.getElementById('edit_email').value = user.usr_email;
				document.getElementById('edit_phone').value = user.usr_mobile;
				document.getElementById('edit_role').value = user.usr_role;
				if (user.usr_access != '*') {
					that.setState({
						selected_stations: JSON.parse(user.usr_access),
						master_access: false
					});
				} else {
					that.setState({
						selected_stations: [],
						master_access: true
					});
				}
				that.setState({edit_user: id, usr_role: user.usr_role});
				console.log('user', user);
			}
		});
	}

	/**
	 * This function saves the data in state to remove the user.
	 * @param  {Number} id User Id of which the user data will be removed.
	 */
	removeUserFetchData(id) {
		console.log("Remove:", id);
		var that = this;
		that.state.users.map((user, index) => {
			if (id == user.usr_id) {
				that.setState({
					remove_user: id,
					remove_user_name: (user.usr_first_name + ' ' + user.usr_middle_name).trim() + ' ' + user.usr_last_name
				});
			}
		});
	}

	/**
	 * This function saves the data in state to reset password of the user.
	 * @param  {Number} id User Id of which the password of the user will be reset.
	 */
	resetPasswordFetchData(id) {
		console.log("Reset Pass:", id);
		var that = this;
		that.state.users.map((user, index) => {
			if (id == user.usr_id) {
				that.setState({
					reset_password: id,
					reset_password_name: (user.usr_first_name + ' ' + user.usr_middle_name).trim() + ' ' + user.usr_last_name
				});
			}
		});
	}

	/**
	 * This function is used to trigger an action to call different functions.
	 * @param  {Number} id     User Id for which the action will be taken.
	 * @param  {String} action Actions which will be compared & call the function inside it.
	 */
	actionTriggered(id, action) {
		if (id) {
			var that = this;
			if (action === 'edit_user') {
				that.editUserFillData(id);
			} else if (action === 'remove_user') {
				that.setState({remove_user:1});
				that.removeUserFetchData(id);
			} else if (action === 'reset_pass') {
				that.setState({reset_password:1});
				that.resetPasswordFetchData(id);
			}
		} else {
			showPopup('danger','Invalid User!');
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Manage Users - Aurassure';
	}

	/**
	 * This renders entire class with navigation bar.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container">
				<NavLink active="users" station_id={(stationId != 1) ? stationId : 1} />
				{(() => {
					if (!this.state.users) {
						return <div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
					} else {
						return <Scrollbars autoHide>
							<div className="add-user">
								<button className="btn btn-primary" onClick={() => this.setState({add_user:1})}>Add User</button>
							</div>
							<div className="master-user-cards-container">
								<UserCard actionTriggered={(id,act) => this.actionTriggered(id,act)} uid={this.state.id} {...this.state} />
							</div>
							<div className="user-cards-container">
								{(() => {
									var users = [];
									this.state.users.map((user, index) => {
										if (this.state.id == user.usr_under) {
											users.push(<UserCard actionTriggered={(id,act) => this.actionTriggered(id,act)} uid={user.usr_id} {...this.state} />);
										}
									});
									if (users.length === 0) {
										users.push(<div className="text-center text-danger">No users found!</div>);
									}
									return users;
								})()}
							</div>
						</Scrollbars>;
					}
				})()}
				
				<div className={'modal' + ((this.state.add_user) ? '' : ' hide')}>
					<div className="modal-bg" onClick={() => this.setState({add_user:null})}></div>
					<div className="modal-content">
						<div className="modal-header">
							<div className="modal-title">Add New User</div>
						</div>
						<div className="modal-body new-user-form">
							<div className="row">
								<div className="label">Name</div>
								<input className="name" id="fname" placeholder="First Name*"/>
								<input className="name" id="mname" placeholder="Middle Name"/>
								<input className="name" id="lname" placeholder="Last Name"/>
							</div>
							<div className="row">
								<div className="label">Job Profile</div>
								<input className="profile" id="designation" placeholder="Designation"/>
								<input className="profile" id="department" placeholder="Department"/>
							</div>
							<div className="row">
								<div className="label">Contact Details</div>
								<input type="email" className="contact" id="email" placeholder="Contact Email*"/>
								<input className="contact" id="phone" placeholder="Contact Phone*"/>
							</div>
							<div className="row">
								<div className="label">Set Password</div>
								<input type="password" className="password" id="password" placeholder="Enter Password*"/>
								<input type="password" className="password" id="password_confirm" placeholder="Confirm Password*"/>
							</div>
							<div className="row">
								<div className="label">Assign User Role</div>
								<select className="role" id="role" defaultValue={0}>
									<option value={0}>User</option>
									<option value={1}>Manager</option>
									<option value={2}>Admin</option>
								</select>
							</div>
							<div className="row">
								<div className="label">Assign Stations</div>
							</div>
							<div className="row">
								<div className="col-lg-6 col-md-6 col-sm-6">
									<div className="flex">
										<input type="checkbox" id="master_access" className="all-check" checked={this.state.master_access} onChange={(e) => this.setState({master_access: (e.target.checked) ? true : false})}/>
										<label htmlFor="master_access">Master Access</label>
									</div>
								</div>
							</div>
							{(() => {
								if (!this.state.master_access && this.state.all_stations_list) {
									return <div className="row">
										<div className="col-lg-8 col-md-8 col-sm-8">
											<MultiSelect
												placeholder={this.state.stations_placeholder}
												options={this.state.all_stations_list}
												selected={this.state.selected_stations}
												callback={(selected) => this.getStationIds(selected)}
												item_type={'Stations'} />
										</div>
									</div>;
								}
							})()}
							{/*<div className="row">
								<div className="label">Select Access</div>
								<div className="flex">
									<input type="checkbox" className="access" id="access1" value={1}/>
									<label htmlFor="access1">Access 1</label>
									<input type="checkbox" className="access" id="access2" value={2}/>
									<label htmlFor="access2">Access 2</label>
								</div>
							</div>*/}
						</div>
						<div className="modal-footer">
							{(() => {
								if (this.state.submitting_data == 1) {
									return <div className="text-center"><svg className="loading-spinner" width="16px" height="16px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
								} else {
									return (<div>
										<button className="btn btn-default" onClick={() => this.setState({add_user:null})}>Cancel</button>
										<button className="btn btn-primary" onClick={() => this.addUser()}>Add User</button>
									</div>);
								}
							})()}
						</div>
					</div>
				</div>
				<div className={'modal' + ((this.state.edit_user) ? '' : ' hide')}>
					<div className="modal-bg" onClick={() => this.setState({edit_user:null})}></div>
					<div className="modal-content">
						<div className="modal-header">
							<div className="modal-title">Edit User</div>
						</div>
						<div className="modal-body new-user-form">
							<div className="row">
								<div className="label">Name</div>
								<input className="name" id="edit_fname" placeholder="First Name*"/>
								<input className="name" id="edit_mname" placeholder="Middle Name"/>
								<input className="name" id="edit_lname" placeholder="Last Name"/>
							</div>
							<div className="row">
								<div className="label">Job Profile</div>
								<input className="profile" id="edit_designation" placeholder="Designation"/>
								<input className="profile" id="edit_department" placeholder="Department"/>
							</div>
							<div className="row">
								<div className="label">Contact Details</div>
								<input type="email" className="contact" id="edit_email" placeholder="Contact Email*"/>
								<input className="contact" id="edit_phone" placeholder="Contact Phone*"/>
							</div>
							<div className="row">
								<div className="label">Assign User Role</div>
								<select className="role" id="edit_role" defaultValue={0}>
									<option value={0}>User</option>
									<option value={1}>Manager</option>
									<option value={2}>Admin</option>
								</select>
							</div>
							<div className="row">
								<div className="col-lg-6 col-md-6 col-sm-6">
									<div className="flex">
										<input type="checkbox" id="master_access" className="all-check" checked={this.state.master_access} onChange={(e) => this.setState({master_access: (e.target.checked) ? true : false})}/>
										<label htmlFor="master_access">Master Access</label>
									</div>
								</div>
							</div>
							{(() => {
								if (!this.state.master_access && this.state.all_stations_list) {
									return <div className="row">
										<div className="col-lg-8 col-md-8 col-sm-8">
											<MultiSelect
												placeholder={this.state.stations_placeholder}
												options={this.state.all_stations_list}
												selected={this.state.selected_stations}
												callback={(selected) => this.getStationIds(selected)}
												item_type={'Stations'} />
										</div>
									</div>;
								}
							})()}
						</div>
						<div className="modal-footer">
							{(() => {
								if (this.state.submitting_data == 1) {
									return <div className="text-center"><svg className="loading-spinner" width="16px" height="16px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
								} else {
									return (<div>
										<button className="btn btn-default" onClick={() => this.setState({edit_user:null})}>Cancel</button>
										<button className="btn btn-primary" onClick={() => this.editUser()}>Save</button>
									</div>);
								}
							})()}
						</div>
					</div>
				</div>
				<div className={'modal' + ((this.state.remove_user) ? '' : ' hide')}>
					<div className="modal-bg" onClick={() => this.setState({remove_user:null})}></div>
					<div className="modal-content">
						<div className="modal-header">
							<div className="modal-title">Remove {this.state.remove_user_name}</div>
						</div>
						<div className="modal-body new-user-form">
							Note: All data associated with this account will be deleted and this action cannot be reverted back.
						</div>
						<div className="modal-footer">
							{(() => {
								if (this.state.submitting_data == 1) {
									return <div className="text-center"><svg className="loading-spinner" width="16px" height="16px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
								} else {
									return (<div>
										<button className="btn btn-default" onClick={() => this.setState({remove_user:null})}>Cancel</button>
										<button className="btn btn-primary" onClick={() => this.removeUser()}>Remove User</button>
									</div>);
								}
							})()}
						</div>
					</div>
				</div>
				<div className={'modal' + ((this.state.reset_password) ? '' : ' hide')}>
					<div className="modal-bg" onClick={() => this.setState({reset_password:null})}></div>
					<div className="modal-content">
						<div className="modal-header">
							<div className="modal-title">Reset {this.state.reset_password_name + '\'s'} Password</div>
						</div>
						<div className="modal-body new-user-form">
							<div className="row">
								<div className="label">Set New Password</div>
								<input type="password" className="password" id="reset_password" placeholder="Enter Password*"/>
								<input type="password" className="password" id="reset_password_confirm" placeholder="Confirm Password*"/>
							</div>
						</div>
						<div className="modal-footer">
							{(() => {
								if (this.state.submitting_data == 1) {
									return <div className="text-center"><svg className="loading-spinner" width="16px" height="16px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
								} else {
									return (<div>
										<button className="btn btn-default" onClick={() => this.setState({reset_password:null})}>Cancel</button>
										<button className="btn btn-primary" onClick={() => this.resetPassword()}>Reset Password</button>
									</div>);
								}
							})()}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
