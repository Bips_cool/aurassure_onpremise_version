import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import ReactHighcharts from 'react-highcharts';
import { Scrollbars } from 'react-custom-scrollbars';
import Calendar from './imports/Calendar';
import CompareCheck from './imports/CompareCheck';
import StationSearch from './imports/StationSearch';
import AquaDeviceDetails from './imports/AquaDeviceDetails';
import OutlierLimit from './imports/OutlierLimit';

/**
 * This Class Used for Station detailed analytics.
 */
export default class AquaStationDetailedAnalytics extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {Object} params
	 */
	constructor(props) {
		super(props);
		

		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			station_data: props.station_data,
			source: 'Aurassure',
			pathname: props.pathname,
			pattern: props.pattern,
			current_param: '',
			station_id: props.station_id,
			graph_type: 'real-time',
			station_type: props.station_type,
			calendar_time: props.calendar_time
		};
		console.log('props', props);
	}

	/**
	 * This function is used to push the outlier data to main dataset.
	 * @param  {String} current_param This is the current parameter.
	 * @param  {Number} outlimit      This is the user input for outlier limit.
	 */
	outlierDataPush(current_param, outlimit) {
		let station_data = this.state.station_data;
		let params = station_data.params.map((param, index) => {
			if (param.key == current_param) {
				station_data.params = {
					key: current_param,
					name: param.name,
					outlimit: outlimit
				}
			} else {
				return param;
			}
		});
		this.setState({
			station_data: station_data
		});
		console.log('station_data', station_data);
	}

	/**
	 * This function is to fetch detailed analytics for the station.
	 * @param  {Number} id This is the selected station id.
	 */
	fetchStationData(id) {
		this.props.fetchStationDataType(id);
	}

	/**
	 * This function is used for update station data for the parameter.
	 */
	updateStationData() {
		var that = this;
		var parameters = [];
		if (that.state.station_data.params && that.state.station_data.params.length) {
			that.state.station_data.params.map((param) => {
				parameters.push(param.key);
			});
		}
		let upto_time = that.state.calendar_time != null ? that.state.calendar_time[1] : 0;
		let current_time = moment().tz('Asia/Kolkata').format('DD-MM-YYYY');
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_updated_data_of_station_for_parameter.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: this.props.station_id,
				parameters: parameters,
				last_data_update_time: this.state.station_data.time_stamps[this.state.station_data.time_stamps.length - 1],
				last_hourly_data_update_time: this.state.station_data.last_data_update_time
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Refresh Data:',json);
			if (json.status === 'success') {
				if (that.state.station_data) {
					var data = that.state.station_data;
					Object.keys(json.param_values).map((key) => {
						// console.log('key', key);
						let real_time_chart = (that.state.graph_type == 'real-time') ? that.refs['real_time_chart_' + key].getChart() : '' ;
						json.time_stamps.map((time_stamp, data_point_index) => {
							data.time_stamps.push(time_stamp);
							for (let param in json.param_values) {
								data.param_values[param].push(json.param_values[param][data_point_index]);
								if (time_stamp - data.time_stamps[0] > 24*3600) {
									data.param_values[param].shift();
								}
								if (param === key) {
									if (upto_time == current_time) {
										if (that.state.graph_type == 'real-time') {
											if (time_stamp - data.time_stamps[0] > 24*3600) {
												real_time_chart.series[0].removePoint(0);
											}
											console.log('addPoint '+ param, [
												time_stamp * 1000,
												parseFloat(json.param_values[param][data_point_index])
											]);
											real_time_chart.series[0].addPoint([
												time_stamp * 1000,
												parseFloat(json.param_values[param][data_point_index])
											], true);
										}
									}
									that.state.station_data.time_stamps.push(time_stamp);
									if (document.getElementById('last_data_time')) {
										document.getElementById('last_data_time').innerHTML = (moment.unix(that.state.station_data.time_stamps[(that.state.station_data.time_stamps.length - 1)]).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm'));
										document.getElementById("last_data_time").previousSibling.className = 'icon clock' + (((moment().tz('Asia/Kolkata').format('X') - that.state.station_data.time_stamps[(that.state.station_data.time_stamps.length - 1)]) > 7200 || !that.state.station_data.time_stamps.length) ? ' offline' : '');
									}
									document.getElementById('parameter_value_' + param).innerHTML = (parseFloat(json.param_values[param][data_point_index]).toFixed(2));
								}
							}
							if (time_stamp - data.time_stamps[0] > 24*3600) {
								let removed_time_stamp = data.time_stamps.shift();
							}
						});
					});
				}

				if (json.update_hourly_avg_data === true) {
					console.log('station_data', that.state.station_data);
					if (that.state.station_data) {
						data.aqi = json.aqi;
						data.aqi_range = json.aqi_range;
						data.aqi_status = json.aqi_status;
						data.connection_status = json.connection_status;
						var i;

						data.hourly_aqis = data.hourly_aqis.slice(0, 24 - json.hourly_aqis.length);
						for (i = json.hourly_aqis.length - 1; i >= 0; i--) {
							data.hourly_aqis.unshift(json.hourly_aqis[i]);
						}

						for (let param in json.hourly_param_aqis) {
							data.hourly_param_aqis[param] = data.hourly_param_aqis[param].slice(0, 24 - json.hourly_param_aqis[param].length);
							for (i = json.hourly_param_aqis[param].length - 1; i >= 0 ; i--) {
								data.hourly_param_aqis[param].unshift(json.hourly_param_aqis[param][i]);
							}
						}

						for (let param in json.hourly_param_concs) {
							data.hourly_param_concs[param] = data.hourly_param_concs[param].slice(0, 24 - json.hourly_param_concs[param].length);
							for (i = json.hourly_param_concs[param].length - 1; i >= 0; i--) {
								data.hourly_param_concs[param].unshift(json.hourly_param_concs[param][i]);
							}
						}

						data.suggestions = json.suggestions;
						data.humid = json.humid;
						data.last_data_update_time = json.last_data_update_time;
						data.temp = json.temp;
					}

					that.setState({
						station_data: data
					});
					console.log('station_data', that.state.station_data);
				}			
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	change_graph(type) {
		this.setState({graph_type: type});
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		if(this.data_update_interval_handle) {
			clearInterval(this.data_update_interval_handle);
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Analytics - Aurassure';
	}

	/**
	 * This renders the Station Detailed Analytics.
	 * @return {ReactElement} markup
	 */
	render() {
		return <div id="aquaAnalyticsContainer">
			{(() => {
				let line_chart_data = [];
				let unit = '';
				let graph_colors = [
					'#43429a',
					'#07adb1',
					'#a44a9c',
					'#f4801f',
					'#c14040',
					'#6fccdd',
					'#61c3ab',
					'#56bc7b',
					'#e2da3e',
					'#41ce00',
					'#aa4728',
					'#b3d23c',
					'#a0632a',
					'#7156a3',
					'#3d577f',
					'#ee3352',
					'#43429a',
					'#07adb1',
					'#a44a9c',
					'#f4801f',
					'#c14040',
					'#6fccdd',
					'#61c3ab',
					'#56bc7b'
				];
				let graph_fill_color = [
					'rgba(67, 66, 154, 0.05)',
					'rgba(7, 173, 177, 0.05)',
					'rgba(164, 74, 156, 0.05)',
					'rgba(244, 128, 31, 0.05)',
					'rgba(193, 64, 64, 0.05)',
					'rgba(111, 204, 221, 0.05)',
					'rgba(97, 195, 171, 0.05)',
					'rgba(86, 188, 123, 0.05)',
					'rgba(226, 218, 62, 0.05)',
					'rgba(65, 206, 0, 0.05)',
					'rgba(170, 71, 40, 0.05)',
					'rgba(179, 210, 60, 0.05)',
					'rgba(160, 99, 42, 0.05)',
					'rgba(113, 86, 163, 0.05)',
					'rgba(61, 87, 127, 0.05)',
					'rgba(238, 51, 82, 0.05)',
					'rgba(67, 66, 154, 0.05)',
					'rgba(7, 173, 177, 0.05)',
					'rgba(164, 74, 156, 0.05)',
					'rgba(244, 128, 31, 0.05)',
					'rgba(193, 64, 64, 0.05)',
					'rgba(111, 204, 221, 0.05)',
					'rgba(97, 195, 171, 0.05)',
					'rgba(86, 188, 123, 0.05)'
				];
				let i;

				ReactHighcharts.Highcharts.setOptions({
					global: {
						useUTC: false
					}
				});
				let current_time = moment().unix();
				let previous_time = current_time - 3600;
				let categories = [];
				/**
				 * This sets the graph config to blank array.
				 * @type {Array}
				 */
				this.individual_param_aqi_graph_config = [];
				let should_plot_individual_param_aqi = [];
				if (this.state.station_data.hourly_param_aqis) {
					Object.keys(this.state.station_data.hourly_param_aqis).map(function(key, index) {
						let bar_chart = [];
						bar_chart[index] = [];
						should_plot_individual_param_aqi[index] = false;
						this.state.station_data.hourly_param_aqis[key].map((value) => {
							let color = window.color_code.color_1;
							if(value >= 401) {
								color = window.color_code.color_6;
							} else if(value >= 301) {
								color = window.color_code.color_5;
							} else if(value >= 201) {
								color = window.color_code.color_4;
							} else if(value >= 101) {
								color = window.color_code.color_3;
							} else if(value >= 51) {
								color = window.color_code.color_2;
							}
							bar_chart[index].push({
								y: parseInt(value),
								color: color
							});
							if(value) {
								should_plot_individual_param_aqi[index] = true;
							}
						});
						bar_chart.map((data, i) => {
							bar_chart[i].reverse();
						});

						/**
						 * This is a Object to plot Individual parameter aqi graph data.
						 * @type {Object}
						 * @property {Object} chart This sets chart properties.
						 * @property {Object} plotOptions This customize the graph plot settings/options.
						 * @property {Object} title This sets the title for the graph.
						 * @property {Object} subtitle This sets the subtitle for the graph.
						 * @property {Object} xAxis This defines what to show in xAxis.
						 * @property {Object} yAxis This defines what to show in yAxis.
						 * @property {Object} legend This hides the printing options from graph.
						 * @property {Object} tooltip This sets what to show in tooltip.
						 * @property {Array} series This contains data for ploting graph.
						 */
						this.individual_param_aqi_graph_config[index] = {
							chart: {
								type: 'column',
								height: 65,
								width: 180
							},
							plotOptions: {
								series: {
									pointPadding: 0,
									groupPadding: 0
								}				
							},
							title: {
								text: ''
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								title: {
									enabled: true,
									text: '',
									style: {
										fontWeight: 'normal'
									}
								},
								type: '',
								lineWidth: 0,
								minorGridLineWidth: 0,
								lineColor: 'transparent',      
								labels: {
									enabled: false
								},
								minorTickLength: 0,
								tickLength: 0,
								categories: categories
							},
							yAxis: {
								title: {
									text: ''
								},
								lineWidth: 0,
								minorGridLineWidth: 0,
								lineColor: 'transparent',      
								labels: {
									enabled: false
								},
								minorTickLength: 0,
								tickLength: 0,
								gridLineColor: 'transparent'

							},
							legend: {
								enabled: false
							},
							tooltip: {
								pointFormat: '<span style="color:{point.color}">Index <b>{point.y}</b>'
							},
							series: [{
								data: bar_chart[index]
							}]
						};
					}, this);
				}

				let graph_data = [];
				if (this.state.station_data) {
					this.state.station_data.hourly_aqis.map((hourly_aqi) => {
						let color = window.color_code.color_1;
						if(hourly_aqi >= 401) {
							color = window.color_code.color_6;
						} else if(hourly_aqi >= 301) {
							color = window.color_code.color_5;
						} else if(hourly_aqi >= 201) {
							color = window.color_code.color_4;
						} else if(hourly_aqi >= 101) {
							color = window.color_code.color_3;
						} else if(hourly_aqi >= 51) {
							color = window.color_code.color_2;
						}
						graph_data.push({
							y: parseInt(hourly_aqi),
							color: color
						});
					});
				}
				graph_data.reverse();

				/**
				 * This is a Object to plot Hourly aqi graph data.
				 * @type {Object}
				 * @property {Object} chart This sets chart properties.
				 * @property {Object} plotOptions This customize the graph plot settings/options.
				 * @property {Object} title This sets the title for the graph.
				 * @property {Object} subtitle This sets the subtitle for the graph.
				 * @property {Object} xAxis This defines what to show in xAxis.
				 * @property {Object} yAxis This defines what to show in yAxis.
				 * @property {Object} legend This hides the printing options from graph.
				 * @property {Object} tooltip This sets what to show in tooltip.
				 * @property {Array} series This contains data for ploting graph.
				 */
				this.hourly_aqi_graph_config = {
					chart: {
						type: 'column',
						height: 80,
						width: 230
					},
					plotOptions: {
						series: {
							pointPadding: 0,
							groupPadding: 0
						}				
					},
					title: {
						text: ''
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						title: {
							enabled: true,
							text: '',
							style: {
								fontWeight: 'normal'
							}
						},
						type: '',
						lineWidth: 0,
						minorGridLineWidth: 0,
						lineColor: 'transparent',      
						labels: {
							enabled: false
						},
						minorTickLength: 0,
						tickLength: 0,
						categories: categories
					},
					yAxis: {
						title: {
							text: 'AQI'
						},
						lineWidth: 0,
						minorGridLineWidth: 0,
						lineColor: 'transparent',      
						labels: {
							enabled: false
						},
						minorTickLength: 0,
						tickLength: 0,
						gridLineColor: 'transparent'

					},
					legend: {
						enabled: false
					},
					tooltip: {
						pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b><br/>'
					},
					series: [{
						data: graph_data
					}]
				};

				let line_chart_data_graph = [], individual_graph_bar_data  = [], param_name = '', curr_time = moment().unix(), prev_time = curr_time - 3600, category = [], avg_min_max = [];
				if (this.state.station_data.param_values) {
					Object.keys(this.state.station_data.param_values).map((key) =>{
						let line_charting_data = [], individual_graph_data_bar_chart = [], total = 0, avg = '', min = '', max = '';

						this.state.station_data.param_values[key].map((value, index) => {
							line_charting_data.push([
								this.state.station_data.time_stamps[index] * 1000,
								parseFloat(value)
							]);
						});

						this.state.station_data.params.map((parameter, index) => {
							if (key === parameter.key) {
								unit = this.state.station_data.param_units[index];
								this.current_param_unit = unit;
								i = index;
							}
						});

						this.state.station_data.params.map((parameter, index) => {
							if (key == parameter.key) {
								param_name = this.state.station_data.params[index].name;
								this.current_param_name = param_name;
								i = index;
							}
						});

						this.line_charting_config = {
							chart: {
								zoomType: 'x',
								height: 200,
								marginTop: 30
							},
							title: {
								text: ''
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								type: 'datetime',
								text: 'Time'
							},
							yAxis: {
								title: {
									text: unit
								},
								floor: 0,
							},
							legend: {
								enabled: false
							},
							tooltip: {
								pointFormat: '<span style="color:{point.color}">' + (key === 'temperature' || key === 'humidity' || key === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>'
							},
							plotOptions: {
								area: {
									marker: {
										radius: 0
									},
									lineWidth: 1,
									states: {
										hover: {
											lineWidth: 1
										}
									},
									threshold: null
								}
							},

							series: [{
								type: 'area',
								color: graph_colors[i],
								fillColor: graph_fill_color[i],
								data: line_charting_data
							}]
						};
						line_chart_data_graph.push(this.line_charting_config);

						var breakpoint_table = {
							'pm2.5': [ 
								[0, 30],
								[31, 60],
								[61, 90],
								[91, 120],
								[121, 250],
								[251, 380]
							],
							'pm10': [
								[0, 50],
								[51, 100],
								[101, 250],
								[251, 350],
								[351, 430],
								[431, 510]
							],
							'no2': [
								[0, 40],
								[41, 80],
								[81, 180],
								[181, 280],
								[281, 400],
								[401, 500]
							],
							'o3': [
								[0, 50],
								[51, 100],
								[101, 168],
								[169, 208],
								[209, 748],
								[749, 800]
							],
							'co': [
								[0, 1.0],
								[1.1, 2.0],
								[2.1, 10],
								[10.1, 17],
								[17.1, 34],
								[34.1, 50]
							],
							'so2': [
								[0, 40],
								[41, 80],
								[81, 380],
								[381, 800],
								[801, 1600],
								[1601, 2000]
							],
							'nh3': [
								[0, 200],
								[201, 400],
								[401, 800],
								[801, 1200],
								[1201, 1800],
								[1801, 2000]
							],
							'pb': [
								[0, 0.5],
								[0.6, 1.0],
								[1.1, 2.0],
								[2.1, 3.0],
								[3.1, 3.5],
								[3.6, 4.0]
							]
						};

						if (this.state.station_data) {
							this.state.station_data.hourly_param_concs[key].map((hourly_conc, i) => {
								if (hourly_conc === null) {
									hourly_conc = 0;
								}

								let color = window.color_code.color_1;
								if (breakpoint_table[key] !== undefined) {
									let factor_usepa_to_naaqi;
									let decimal_upto;
									switch(key) {
										case 'no2':
											factor_usepa_to_naaqi = 46.0055/24.45;
											break;
										case 'o3':
											factor_usepa_to_naaqi = 48/24.45;
											break;
										case 'co':
											factor_usepa_to_naaqi = 28.01/24.45;
											break;
										case 'so2':
											factor_usepa_to_naaqi = 64.066/24.45;
											break;
										default:
											factor_usepa_to_naaqi = 1;
											break;
									}

									if (key == 'co' || key == 'pb') {
										decimal_upto = 1;
									} else {
										decimal_upto = 0
									}

									if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[key][5][0]) {
										color = window.color_code.color_6;
									} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[key][4][0]) {
										color = window.color_code.color_5;
									} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[key][3][0]) {
										color = window.color_code.color_4;
									} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[key][2][0]) {
										color = window.color_code.color_3;
									} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[key][1][0]) {
										color = window.color_code.color_2;
									} else if ((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[key][0][0]) {
										color = window.color_code.color_1;
									}
								}

								individual_graph_data_bar_chart.push({
									y: parseFloat(hourly_conc.toFixed(2)),
									color: color
								});

								category.push(moment.unix(prev_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(curr_time - 3600 * i).tz('Asia/Kolkata').format('HH'));		
							});
							category.reverse();
						}
						individual_graph_data_bar_chart.reverse();

						this.individual_avg_graph_config = {
							chart: {
								type: 'column',
								height: 200,
								marginTop: 30
							},
							plotOptions: {
								series: {
									pointPadding: 0,
									groupPadding: 0
								}
								
							},
							title: {
								text: ''
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								title: {
									enabled: true,
									text: 'Time',
									style: {
										fontWeight: 'normal'
									}
								},
								type: '',
								lineWidth: 0,
								minorGridLineWidth: 0,
								lineColor: 'transparent',      
								labels: {
									enabled: false
								},
								minorTickLength: 0,
								tickLength: 0,
								categories: category
							},
							yAxis: {
								title: {
									text: 'Conc. '
								}
							},
							legend: {
								enabled: false
							},
							tooltip: {
								pointFormat: '<span style="color:{point.color}">' + (key === 'temperature' || key === 'humidity' || key === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>'
							},
							series: [{
								data: individual_graph_data_bar_chart
							}]
						};
						individual_graph_bar_data.push(this.individual_avg_graph_config);

						for(var i = 0; i < this.state.station_data.param_values[key].length; i++) {
							if (this.state.station_data.param_values[key][i] !== null) {
								total += parseFloat(this.state.station_data.param_values[key][i]);
							}
						}
						avg = Math.round(total / this.state.station_data.param_values[key].length * 100) / 100;
						// console.log('avg', avg);

						min = Math.min.apply(null, this.state.station_data.param_values[key]);
						// console.log('min', min);

						max = Math.max.apply(null, this.state.station_data.param_values[key]);
						// console.log('max', max);
						let min_max_avg = {};
						min_max_avg['avg'] = parseFloat(avg.toFixed(2));
						min_max_avg['min'] = parseFloat(min.toFixed(2));
						min_max_avg['max'] = parseFloat(max.toFixed(2));
						avg_min_max.push(min_max_avg);
					});
						console.log('all_array', avg_min_max);
					// console.log('line_chart_data_graph', line_chart_data_graph);
					// console.log('individual_graph_bar_data', individual_graph_bar_data);
				}

				var suggestions = [];
				if(this.state.station_data.suggestions) {
					this.state.station_data.suggestions.map(function(suggestion, i) {
						suggestions.push(
							<div className="panel-item" key={i}>
								<div className={'icon suggestion-' + (i+1)}></div>
								<div className="panel-text">{suggestion}</div>
							</div>
						);
					}, this);
				} else {
					suggestions.push(<li className="hourly-aqi-na-error">NA</li>);
				}

				return(
					<div className="full-container">
						<div className="clearfix" />
						<div id="aquaLeftAnalyticsPanel" className="aquaLeftAnalyticsPanel-height">
							<AquaDeviceDetails {...this.state} fetchStationData={(id) => this.fetchStationData(id)} ref={(child) => {this._device_child = child;}} />
						</div>
						<div className="parameter-btn-group">
							<div className={'parameter-btn hellip' + ((this.state.graph_type === 'real-time') ? ' active': '')} onClick={() => {this.change_graph('real-time');}} >Real Time</div>
							<div className={'parameter-btn hellip' + ((this.state.graph_type === 'average') ? ' active': '')} onClick={() => {this.change_graph('average');}} >Hourly</div>
						</div>
						<div className="calender-picker pull-right">
							<Calendar ref={(child) => { this._child = child; }} {...this.state} changeDate={(interval) => this.props.changeDate(interval)} />
						</div>
						<div id="aquaMiddleAnalyticsPanel" className="aqua-middle-pannel">
							<Scrollbars autoHide>
								{(() => {
									if (this.state.station_data.param_values && this.state.station_data.param_values.length !== 0) {
										let allowed_parameters_in_order = ['pm1', 'pm2.5', 'pm10', 'so2', 'no', 'no2', 'co', 'o3', 'co2', 'o2', 'temperature', 'humidity', 'rain', 'uv', 'light', 'noise', 'voc', 'lint', 'nox', 'ch4', 'lead', 'wspeed', 'press', 'no3', 'ph', 'do', 'nh4', 'orp', 'us_mb', 'cur', 'nh3', 'us_hc04', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'];
										let current_params = {};
										this.state.station_data.params.map((param) => {
											current_params[param.key] = param;
										});
										let parameters = allowed_parameters_in_order.map((param, i) => {
											if(Object.keys(current_params).indexOf(param) >= 0) {
												return(
													<div className={'parameter-btn hellip' + ((param === this.state.current_param)? ' active': '')} dangerouslySetInnerHTML={{__html: current_params[param].name}} onClick={() => {this.change_param(param);}} key={i} />
												);
											}
										});
										if (this.props.station_type == '2') {
											return(
												<div className="line-graph-container">
													{(() => {
														if (this.state.graph_type === 'real-time') {
															return <div className="real-time-charts">
																{(() => {
																	let linechart = line_chart_data_graph.map((graph_indivisual, index) => {
																		// console.log('graph_indivisual', graph_indivisual);
																		return <div className="linegraph-container">
																			<div className="linegraph-title" dangerouslySetInnerHTML={{__html: this.state.station_data.params[index].name}} />
																			<ReactHighcharts ref={'real_time_chart_' + this.state.station_data.params[index].key} config={graph_indivisual} key={index} />
																		</div>;
																	});
																	return linechart
																})()}
															</div>;
														} else {
															return <div className="avg-report-hourly">
																{/*<OutlierLimit {...this.state} ref={(child) => {this._child = child;}} outlierDataPush={(current_param, outlimit) => this.outlierDataPush(current_param, outlimit)} />*/}
																{(() => {
																	let bargraph = individual_graph_bar_data.map((graph_indivisual, index) => {
																		return <div className="bargraph-container">
																			<div className="bargraph-title" dangerouslySetInnerHTML={{__html: this.state.station_data.params[index].name}} />
																			<ReactHighcharts ref={'avg_chart_' + this.state.station_data.params[index].key} config={graph_indivisual} />
																		</div>;
																	});
																	return bargraph
																})()}

																{/*Commented For Avg hourly graph*/}
																{/*<ReactHighcharts ref="avg_chart" config={this.individual_graph_config} />*/}
															</div>;
														}
													})()}
													{(() => {
														let avg_side = avg_min_max.map((value, index) => {
															// console.log(value, index);
															return <div className="avg-min-max-container">
																<div className="avg-container">
																	<div className="avg-value">
																		<span className="avg-val">{value.avg}</span>
																		<small dangerouslySetInnerHTML={{__html: this.state.station_data.param_units[index]}} />
																	</div>
																	<div className="avg-label">Average</div>
																</div>
																<div className="min-max-container">
																	<div className="min-max-value">
																		<span className="min-max-val">{value.min + ' / ' + value.max}</span>
																		<small dangerouslySetInnerHTML={{__html: this.state.station_data.param_units[index]}} />
																	</div>
																	<div className="min-max-label">Min / Max</div>
																</div>
															</div>;
														});
														return avg_side;
													})()}
													{/*Commented Parameter Button group*/}
													{/*<div className="parameter-btn-group">
														{parameters}
													</div>*/}

													{/*Commented single linechart view*/}
													{/*<ReactHighcharts ref="real_time_chart" config={this.line_chart_config} />*/}

													{(() => {
														/*if (breakpoint_table[this.state.current_param] !== undefined) {
															return (
																<div className="graph-index">
																	<div className="index">
																		<div className="index-0"></div>
																		<div className="index-text">0</div>
																	</div>
																	<div className="index">
																		<div className="index-1"></div>
																		<div className="index-text">
																			{breakpoint_table[this.state.current_param][0][1]}
																		</div>
																	</div>
																	<div className="index">
																		<div className="index-2"></div>
																		<div className="index-text">
																			{breakpoint_table[this.state.current_param][1][1]}
																		</div>
																	</div>
																	<div className="index">
																		<div className="index-3"></div>
																		<div className="index-text">
																			{breakpoint_table[this.state.current_param][2][1]}
																		</div>
																	</div>
																	<div className="index">
																		<div className="index-4"></div>
																		<div className="index-text">
																			{breakpoint_table[this.state.current_param][3][1]}
																		</div>
																	</div>
																	<div className="index">
																		<div className="index-5"></div>
																		<div className="index-text">
																			{breakpoint_table[this.state.current_param][4][1]}
																		</div>
																	</div>
																	<div className="index">
																		<div className="index-6"></div>
																		<div className="index-text">
																			{breakpoint_table[this.state.current_param][5][1]}
																		</div>
																	</div>
																	<div className="unit" dangerouslySetInnerHTML={{__html: unit}}></div>
																</div>
															);
														}*/
													})()}
												</div>
											);
										}
									} else {
										return(<div className="flex no-data-text full-height">
											<center className="table">No data received in last 24 hours.</center>
										</div>);
									}
								})()}
							</Scrollbars>
						</div>
					</div>
				);
			})()}
		</div>;
	}
}

AquaStationDetailedAnalytics.contextTypes = {
	router: React.PropTypes.object
};
