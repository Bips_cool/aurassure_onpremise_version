import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import AquaDeviceDetails from './imports/AquaDeviceDetails';
import DeviceDetails from './imports/DeviceDetails';
import CompareLocations from './imports/CompareLocations';

/**
 * This Class Used for Dashboard page.
 */
export default class Dashboard extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			all_stations: null,
			station_data: null,
			source: 'Aurassure',
			station_id: props.params.station_id,
			station_type: null,
			calling: false
		};

		saveStationId(props.params.station_id);
		/**
		 * This is used for bind the all stations data.
		 * @type {object}
		 */
		this.retrive_data = this.retrive_data.bind(this);
		this.retrive_data();
		this.map_view = false;
		this.myStyles = [
			{
				"featureType": "administrative",
				"elementType": "geometry",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "labels",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "transit",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			}
		];
		
	}

	/**
	 * This function used for fetch stations data.
	 * @param  {number} station_id This is the selected station's id.
	 */
	fetchStationData(station_id) {
		if (station_id) {
			console.log('Station Id:',station_id);
			saveStationId(station_id);
			var that = this;
			if (!that.state.calling) {
				that.setState({station_data: null});
			}
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_data_of_station.php', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				credentials: 'include',
				body: JSON.stringify({
					station_id: station_id
				})
			}).then(function(Response) {
				return Response.json();
			}).then(function(json) {
				console.log('Station Data:',json);
				if (json.status === 'success') {
					that.setState({
						station_id: station_id,
						station_data: json,
						station_type: json.loc_type,
						calling: true
					});
					if (that.state.station_data) {
						var data = that.state.station_data;
						if (json.latest_param_value && Object.keys(json.latest_param_value).length) {
							Object.keys(json.latest_param_value).map((key) => {
								for (let param in json.latest_param_value) {
									if (param === key) {
										if (document.getElementById('last_data_time')) {
											document.getElementById('last_data_time').innerHTML = (moment.unix(that.state.station_data.last_data_receive_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm'));
											document.getElementById("last_data_time").previousSibling.className = 'icon clock' + (((moment().tz('Asia/Kolkata').format('X') - that.state.station_data.last_data_receive_time) > 7200 || (that.state.station_data.last_data_receive_time == null)) ? ' offline' : '');
										}
										if (document.getElementById('parameter_value_' + key)) {
											document.getElementById('parameter_value_' + key).innerHTML = (parseFloat(json.latest_param_value[key]).toFixed(2));
										}
									}
								}
							});
							if (document.getElementById('wind_speed_data')) {
								document.getElementById('wind_speed_data').innerHTML = Math.round(parseFloat(that.state.station_data.latest_param_value.wspeed)) + ' m / s';
							}
							if (document.getElementById('wind_direction_data')) {
								document.getElementById('wind_direction_data').innerHTML = Math.round(parseFloat(that.state.station_data.latest_param_value.wdir)) + ' °';
							}
							if (document.getElementById('pressure_data_dashboard')) {
								document.getElementById('pressure_data_dashboard').innerHTML = Math.round(parseFloat(that.state.station_data.latest_param_value.press)) + ' mBar';
							}
						}
					}
					if (that._device_child) {
						that._device_child.fetch_compared_station_ids();
					}
				} else {
					showPopup('danger',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				showPopup('danger','Unable to load data!');
			});
		}
	}

	/**
	 * This function is used to fetch all stations data.
	 * @param  {number} i 
	 */
	retrive_data(i) {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_stations.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				that.setState({
					all_stations: json.stations,
					dmd_details: json.dmds,
					city_map_details: json.city_map_detail
				}, () => {that.DMDMarkerIcon();});
				if (!that.map_view) {
					that.mapView();
				}
				if (i && that._child) {
					console.log('Calling Child');
					that._child.fetch_stations();
				}
				if(that.data_update_interval_handle) {
					clearInterval(that.data_update_interval_handle);
				}
				that.data_update_interval_handle = setInterval(() => {
					that.fetchStationData(that.state.station_id);
					that.setState({calling: true});
				}, 15000);
				// if (that.state.station_data && that.state.station_data.latest_param_value && Object.keys(that.state.station_data.latest_param_value).length !== 0) {
				// }
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This is used for generating google map.
	 */
	mapView() {
		let that = this;
		that.map_view = true;
		let city_lat = 23.678543, city_long = 78.234321, zoom_lvl=5;
		if (that.state.city_map_details && (that.state.city_map_details != null) && (that.state.city_map_details.city_lat != null)) {
			city_lat = that.state.city_map_details.city_lat;
		}
		if (that.state.city_map_details && ((that.state.city_map_details != null) && (that.state.city_map_details.city_long != null))) {
			city_long = that.state.city_map_details.city_long;
		}
		if (that.state.city_map_details && (that.state.city_map_details != null) && (that.state.city_map_details.city_zoom_level != 0)) {
			zoom_lvl = that.state.city_map_details.city_zoom_level;
		}
		// console.log('lat', city_lat);
		// console.log('long', city_long);
		// console.log('zoom_lvl', zoom_lvl);
		var mapProp= {
			center:new google.maps.LatLng(parseFloat(city_lat), parseFloat(city_long)),
			zoom:parseInt(zoom_lvl),
			styles: that.myStyles
		};
		/**
		 * This is used for generating google map.
		 */
		that.map=new google.maps.Map(document.getElementById('mapView'),mapProp);
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		/**
		 * This is to refresh data in every 60 seconds.
		 */
		this.retrive_data();
		if(this.refresh_interval_id) {
			clearInterval(this.refresh_interval_id);
		}
		this.refresh_interval_id = setInterval(() => {
			this.retrive_data();
		}, 30000);

		document.title = ((!this.state.compare) ? 'Dashboard' : 'Compare') + ' - Aurassure';
	}

	/**
	 * This is invoked immediately before mounting occurs. It is called before render.
	 */
	componentWillMount() {
		if (this.props.params.station_id) {
			this.fetchStationData(this.props.params.station_id);
		}
		clearInterval(this.refresh_interval_id);
	}

	/**
	 * This is invoked before rendering when new props or state are being received. This method is not called for the initial render.
	 * @param  {object} nextProps This is to compare this.props with nextProps for that upadte can be skipped.
	 */
	shouldComponentUpdate(nextProps) {
		if(this.props.params.station_id && this.props.params.station_id !== nextProps.params.station_id) {
			this.fetchStationData(nextProps.params.station_id);
		}
		return true;
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		clearInterval(this.refresh_interval_id);
		clearInterval(this.data_update_interval_handle);
		// if(this.data_update_interval_handle && this.state.station_type == '2') {
		// }
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 */
	componentDidUpdate() {
		console.log('Component Updated.');
		console.log('All Stations: ', this.state.all_stations);
		if(this.state.all_stations) {
			this.state.all_stations.map((point) => {
				var marker_lat_long = new google.maps.LatLng(point.lat, point.long);
				// console.log(point.lat, point.long);
				var marker_icon = '';
				if (point.connection_status === 'online') {
					if(point.aqi_range == 1) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-1.png';
					} else if(point.aqi_range == 2) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-2.png';
					} else if(point.aqi_range == 3) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-3.png';
					} else if(point.aqi_range == 4) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-4.png';
					} else if(point.aqi_range == 5) {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-5.png';
					} else {
						marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-6.png';
					}
				} else {
					marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-0.png';
				}

				var marker = new google.maps.Marker({
					position: marker_lat_long,
					map: this.map,
					title: point.name,
					icon: marker_icon
				});
				// console.log('marker_icon', marker);

				var that = this;
				google.maps.event.addListener(marker,'click',function() {
					that.fetchStationData(point.id);
					that.context.router.transitionTo('/stations/'+point.id);
				});
			});
		}
		this.DMDMarkerIcon();
	}

	DMDMarkerIcon() {
		console.log('DMD Details: ', this.state.dmd_details);
		if(this.state.dmd_details) {
			this.state.dmd_details.map((point) => {
				var marker_lat_long = new google.maps.LatLng(point.lat, point.long);
				// console.log(point.lat, point.long);
				var marker_icon = '';
				if (point.connection_status === 'online') {
					marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-1.png';
				} else {
					marker_icon = 'https://static.aurassure.com/imgs/dashboard/location-status/new-marker/location-status-0.png';
				}

				var marker = new google.maps.Marker({
					position: marker_lat_long,
					map: this.map,
					title: point.dmd_key,
					icon: marker_icon
				});
				// console.log('marker_icon', marker);

				let content = '', circle_class = '';
				let dashboard_map_pointer = new google.maps.InfoWindow();
				google.maps.event.addListener(marker, 'mouseover',function() {
					if (point.connection_status =='online') {
						circle_class = 'online';
					} else {
						circle_class = 'offline';
					}
					
					content = '<div class="pointer-details">';
					content += '<div class="flex">';
					content += '<div class="circle center '+ circle_class +'"></div>';
					content += '<div class="qr-code">'+point.dmd_key+'</div>';
					content += '</div>';
					content += '<div class="status-change-time">Last Accessed at : '+moment.unix(point.last_accessed_at).format("DD-MM-YYYY HH:mm")+'</div>';
					content += '</div>';
					dashboard_map_pointer.setContent(content);
					dashboard_map_pointer.open(this.map, marker);
				})
			});
		}
	}

	/**
	 * This renders the Dashboard page.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container">
				<NavLink active={(this.state.compare) ? 'compare' : 'dashboard'} station_id={1} />
				<div className="full-height" id="mapView"></div>
				{(() => {
					if (!this.state.all_stations) {
						return <div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
					} else {
						if(this.state.all_stations.length) {
							switch (this.state.station_type) {
								case '1' :
									return <DeviceDetails {...this.state} fetchStationData={(i) => this.fetchStationData(i)} ref={(child) => {this._device_child = child;}} />;
									break;

								case '2' :
									return <div className="full-height" id="panelView">
										<AquaDeviceDetails {...this.state} fetchStationData={(i) => this.fetchStationData(i)} ref={(child) => {this._device_child = child;}} />
									</div>;
									break;

								default :
									return <DeviceDetails {...this.state} fetchStationData={(i) => this.fetchStationData(i)} ref={(child) => {this._device_child = child;}} />;
									break;
							}
						} else {
							return(
								<div className="full-height" id="panelView">
									<div className="hourly-aqi-na-error">Sorry, No station was found in your city!</div>
								</div>
							);
						}
					}
				})()}
			</div>
		);
	}
}

Dashboard.contextTypes = {
	router: React.PropTypes.object
};
