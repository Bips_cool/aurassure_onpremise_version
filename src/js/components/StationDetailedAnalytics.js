import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';
import HighchartsSolidGauge from 'highcharts-solid-gauge';
import { Scrollbars } from 'react-custom-scrollbars';
import CompareCheck from './imports/CompareCheck';
import StationSearch from './imports/StationSearch';
import OutlierLimit from './imports/OutlierLimit';

HighchartsMore(ReactHighcharts.Highcharts);
HighchartsSolidGauge(ReactHighcharts.Highcharts);
/**
 * This Class Used for Station detailed analytics.
 */
export default class StationDetailedAnalytics extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {Object} params
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */

		this.state = {
			station_data: props.station_data,
			source: 'Aurassure',
			current_param: props.current_param,
			station_id: props.station_id
		};
		this.chart = true;
	}

	/**
	 * This function is used to push the outlier data to main dataset.
	 * @param  {String} current_param This is the current parameter.
	 * @param  {Number} outlimit      This is the user input for outlier limit.
	 */
	/*outlierDataPush(current_param, outlimit) {
		let station_data = this.state.station_data;
		let params = station_data.params.map((param, index) => {
			if (param.key == current_param) {
				station_data.params = {
					key: current_param,
					name: param.name,
					outlimit: outlimit
				}
			} else {
				return param;
			}
		});
		this.setState({
			station_data: station_data
		});
		console.log('station_data', station_data);
	}*/

	/**
	 * This function is to fetch detailed analytics for the station.
	 * @param  {Number} id This is the selected station id.
	 */
	fetchStationData(id) {
		this.props.fetchStationDataType(id);
	}

	/**
	 * This function is used for update station data for the parameter.
	 */
	update_station_data() {
		var that = this;
		var parameters = [];
		if (that.state.station_data.params && that.state.station_data.params.length) {
			that.state.station_data.params.map((param) => {
				parameters.push(param.key);
			});
		}

		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_updated_data_of_station_for_parameter.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: this.props.station_id,
				parameters: parameters,
				last_data_update_time: this.state.station_data.time_stamps[this.state.station_data.time_stamps.length - 1],
				last_hourly_data_update_time: this.state.station_data.last_data_update_time
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Refresh Data:',json);
			if (json.status === 'success') {
				if (that.state.station_data && json.param_values != null) {
					var data = that.state.station_data;
					// console.log('magneto_chart',that.refs.magneto_chart.getChart());
					if (json.param_values.wspeed) {
						let speed_chart = that.refs.speed_chart.getChart();
						var point = speed_chart.series[0].points[0];
						let newVal;
						if (json.param_values.wspeed && json.param_values.wspeed.length) {
							newVal = parseFloat(json.param_values.wspeed[json.param_values.wspeed.length - 1]);
						}
						point.update(newVal);
					}
					if (json.param_values.wdir) {
						let magneto_chart = that.refs.magneto_chart.getChart();
						let point = magneto_chart.series[0].points[0];
						let newVal;
						if (json.param_values.wdir && json.param_values.wdir.length) {
							newVal = parseFloat(json.param_values.wdir[json.param_values.wdir.length - 1]);
						}
						console.log(newVal);
						point.update(newVal);
						let point2 = magneto_chart.series[1].points[0];
						point2.update(newVal);
					}
					if (json.param_values.press) {
						let pressure_chart = that.refs.pressure_chart.getChart();
						var point = pressure_chart.series[0].points[0];
						let newVal;
						if (json.param_values.press && json.param_values.press.length) {
							newVal = (parseFloat(json.param_values.press[json.param_values.press.length - 1]) * 0.750062);
						}
						point.update(newVal);
					}
					let real_time_chart = that.refs.real_time_chart.getChart();
					json.time_stamps.map((time_stamp, data_point_index) => {
						data.time_stamps.push(time_stamp);

						for (let param in json.param_values) {
							data.param_values[param].push(json.param_values[param][data_point_index]);
							if (time_stamp - data.time_stamps[0] > 24*3600) {
								data.param_values[param].shift();
							}
							if(param === that.state.current_param) {
								if (time_stamp - data.time_stamps[0] > 24*3600) {
									real_time_chart.series[0].removePoint(0);
								}
								console.log('addPoint', [
									time_stamp * 1000,
									parseFloat(json.param_values[param][data_point_index])
								]);
								if (that.state.current_param == 'rain') {
									real_time_chart.setTitle({
										text: 'Current value: ' + that.state.station_data.param_values[that.state.current_param][that.state.station_data.param_values[that.state.current_param].length - 1] + ' ' + that.current_param_unit + ' / ' + (that.state.station_data.param_values[that.state.current_param][that.state.station_data.param_values[that.state.current_param].length - 1] * 25.4) + ' mm'
									});
								} else {
									real_time_chart.setTitle({
										text: 'Current value: ' + that.state.station_data.param_values[that.state.current_param][that.state.station_data.param_values[that.state.current_param].length - 1] + ' ' + that.current_param_unit
									});
								}
								real_time_chart.series[0].addPoint([
									time_stamp * 1000,
									parseFloat(json.param_values[param][data_point_index])
								], true);
								that.state.station_data.time_stamps.push(time_stamp);
								document.getElementById('last_data_time').innerHTML = (moment.unix(that.state.station_data.time_stamps[(that.state.station_data.time_stamps.length - 1)]).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm'));
								document.getElementById("last_data_time").previousSibling.className = 'icon clock' + (((moment().tz('Asia/Kolkata').format('X') - that.state.station_data.time_stamps[(that.state.station_data.time_stamps.length - 1)]) > 7200 || !that.state.station_data.time_stamps.length) ? ' offline' : '');
								if (that.state.station_data.param_values.temperature) {
									document.getElementById('current_temp').innerHTML = Math.round(parseFloat(that.state.station_data.param_values.temperature[(that.state.station_data.param_values.temperature.length - 1)])) + ' °C';
								}
								if (that.state.station_data.param_values.humidity) {
									document.getElementById('current_humid').innerHTML = Math.round(parseFloat(that.state.station_data.param_values.humidity[(that.state.station_data.param_values.humidity.length - 1)])) + ' %';
								}
								if (that.state.station_data.param_values.wspeed) {
									document.getElementById('wind_speed_data').innerHTML = Math.round(parseFloat(that.state.station_data.param_values.wspeed[(that.state.station_data.param_values.wspeed.length - 1)])) + ' m / s';
								}
								if (that.state.station_data.param_values.wdir) {
									document.getElementById('wind_direction_data').innerHTML = Math.round(parseFloat(that.state.station_data.param_values.wdir[(that.state.station_data.param_values.wdir.length - 1)])) + ' °';
								}
								if (that.state.station_data.param_values.press) {
									document.getElementById('pressure_data').innerHTML = Math.round(parseFloat(that.state.station_data.param_values.press[(that.state.station_data.param_values.press.length - 1)])) + ' mBar';
								}
							}
						}
						if (time_stamp - data.time_stamps[0] > 24*3600) {
							let removed_time_stamp = data.time_stamps.shift();
						}
					});
				}

				if (json.update_hourly_avg_data === true) {
					if (that.state.station_data) {
						data.aqi = json.aqi;
						data.aqi_range = json.aqi_range;
						data.aqi_status = json.aqi_status;
						data.connection_status = json.connection_status;
						var i;

						data.hourly_aqis = data.hourly_aqis.slice(0, 24 - json.hourly_aqis.length);
						for (i = json.hourly_aqis.length - 1; i >= 0; i--) {
							data.hourly_aqis.unshift(json.hourly_aqis[i]);
						}

						for (let param in json.hourly_param_aqis) {
							data.hourly_param_aqis[param] = data.hourly_param_aqis[param].slice(0, 24 - json.hourly_param_aqis[param].length);
							for (i = json.hourly_param_aqis[param].length - 1; i >= 0 ; i--) {
								data.hourly_param_aqis[param].unshift(json.hourly_param_aqis[param][i]);
							}
						}

						for (let param in json.hourly_param_concs) {
							data.hourly_param_concs[param] = data.hourly_param_concs[param].slice(0, 24 - json.hourly_param_concs[param].length);
							for (i = json.hourly_param_concs[param].length - 1; i >= 0; i--) {
								data.hourly_param_concs[param].unshift(json.hourly_param_concs[param][i]);
							}
						}

						data.suggestions = json.suggestions;
						data.humid = json.humid;
						data.last_data_update_time = json.last_data_update_time;
						data.temp = json.temp;
					}

					that.setState({
						station_data: data
					});	
				}			
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function used for change parameter.
	 * @param  {String} param This is the selected parameter.
	 */
	change_param(param) {
		console.log('Changing Param to: ', param);
		this.setState({current_param: param});
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		if(this.data_update_interval_handle) {
			clearInterval(this.data_update_interval_handle);
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Analytics - Aurassure';
		this.data_update_interval_handle = setInterval(() => this.update_station_data(), 15000);
	}

	/**
	 * This renders the Station Detailed Analytics.
	 * @return {ReactElement} markup
	 */
	render() {
		return <div id="analyticsContainer">
			{(() => {
				let line_chart_data = [];
				let unit = '';
				let speed_index;
				let wind_index;
				let pressure_index;
				let graph_colors = [
					'#43429a',
					'#07adb1',
					'#a44a9c',
					'#f4801f',
					'#c14040',
					'#6fccdd',
					'#61c3ab',
					'#56bc7b',
					'#e2da3e',
					'#41ce00',
					'#aa4728',
					'#b3d23c',
					'#a0632a',
					'#7156a3',
					'#3d577f',
					'#ee3352',
					'#43429a',
					'#07adb1',
					'#a44a9c',
					'#f4801f',
					'#c14040',
					'#6fccdd',
					'#61c3ab',
					'#56bc7b'
				];
				let graph_fill_color = [
					'rgba(67, 66, 154, 0.05)',
					'rgba(7, 173, 177, 0.05)',
					'rgba(164, 74, 156, 0.05)',
					'rgba(244, 128, 31, 0.05)',
					'rgba(193, 64, 64, 0.05)',
					'rgba(111, 204, 221, 0.05)',
					'rgba(97, 195, 171, 0.05)',
					'rgba(86, 188, 123, 0.05)',
					'rgba(226, 218, 62, 0.05)',
					'rgba(65, 206, 0, 0.05)',
					'rgba(170, 71, 40, 0.05)',
					'rgba(179, 210, 60, 0.05)',
					'rgba(160, 99, 42, 0.05)',
					'rgba(113, 86, 163, 0.05)',
					'rgba(61, 87, 127, 0.05)',
					'rgba(238, 51, 82, 0.05)',
					'rgba(67, 66, 154, 0.05)',
					'rgba(7, 173, 177, 0.05)',
					'rgba(164, 74, 156, 0.05)',
					'rgba(244, 128, 31, 0.05)',
					'rgba(193, 64, 64, 0.05)',
					'rgba(111, 204, 221, 0.05)',
					'rgba(97, 195, 171, 0.05)',
					'rgba(86, 188, 123, 0.05)'
				];
				let i;

				if (this.state.station_data && this.state.current_param && this.state.station_data.param_values != null && this.state.station_data.param_values[this.state.current_param]) {
					this.state.station_data.param_values[this.state.current_param].map((value, index) => {
						line_chart_data.push([
							this.state.station_data.time_stamps[index] * 1000,
							parseFloat(value)
						]);
					});

					this.state.station_data.params.map((parameter, index) => {
						if (this.state.current_param === parameter.key) {
							unit = this.state.station_data.param_units[index];
							//set current param unit
							/**
							 * This is the current parameter's unit.
							 * @type {String}
							 */
							this.current_param_unit = unit;
							i = index;
						}
					});

					/**
					 * This is a Object to plot Line chart graph data.
					 * @type {Object}
					 * @property {Object} chart This sets chart properties.
					 * @property {Object} title This sets the title for the graph.
					 * @property {Object} subtitle This sets the subtitle for the graph.
					 * @property {Object} xAxis This defines what to show in xAxis.
					 * @property {Object} yAxis This defines what to show in yAxis.
					 * @property {Object} legend This hides the printing options from graph.
					 * @property {Object} tooltip This sets what to show in tooltip.
					 * @property {Object} plotOptions This customize the graph plot settings/options.
					 * @property {Array} series This contains data for ploting graph.
					 */
					
					this.line_chart_config = {
						chart: {
							zoomType: 'x',
							height: (window.innerHeight - 400)
						},
						title: {
							text: 'Current value: ' + (this.state.current_param == 'rain' ? this.state.station_data.param_values[this.state.current_param][this.state.station_data.param_values[this.state.current_param].length - 1] + ' ' + unit + ' / ' + ((this.state.station_data.param_values[this.state.current_param][this.state.station_data.param_values[this.state.current_param].length - 1] * 25.4) + ' mm') : this.state.station_data.param_values[this.state.current_param][this.state.station_data.param_values[this.state.current_param].length - 1] + ' ' + unit)
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							type: 'datetime',
							text: 'Time'
						},
						yAxis: {
							title: {
								text: unit
							}
						},
						legend: {
							enabled: false
						},
						tooltip: {
							pointFormat: (this.state.current_param == 'rain' ? '<span style="color:{point.color}">' + ('Conc. ') + '<b>{point.y}</b> ' + unit + ' / ' + '<b>{point.y}</b> ' + 'mm' + '<br/>' : '<span style="color:{point.color}">' + (this.state.current_param === 'temperature' || this.state.current_param === 'humidity' || this.state.current_param === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>')
						},
						plotOptions: {
							area: {
								marker: {
									radius: 0
								},
								lineWidth: 1,
								states: {
									hover: {
										lineWidth: 1
									}
								},
								threshold: null
							}
						},

						series: [{
							type: 'area',
							color: graph_colors[i],
							fillColor: graph_fill_color[i],
							data: line_chart_data
						}]
					};
				}

				ReactHighcharts.Highcharts.setOptions({
					global: {
						useUTC: false
					}
				});

				let individual_graph_data = [];
				let current_time = moment().unix();
				let previous_time = current_time - 3600;
				let categories = [];
				var breakpoint_table = {
					'pm2.5': [ 
						[0, 30],
						[31, 60],
						[61, 90],
						[91, 120],
						[121, 250],
						[251, 380]
					],
					'pm10': [
						[0, 50],
						[51, 100],
						[101, 250],
						[251, 350],
						[351, 430],
						[431, 510]
					],
					'no2': [
						[0, 40],
						[41, 80],
						[81, 180],
						[181, 280],
						[281, 400],
						[401, 500]
					],
					'o3': [
						[0, 50],
						[51, 100],
						[101, 168],
						[169, 208],
						[209, 748],
						[749, 800]
					],
					'co': [
						[0, 1.0],
						[1.1, 2.0],
						[2.1, 10],
						[10.1, 17],
						[17.1, 34],
						[34.1, 50]
					],
					'so2': [
						[0, 40],
						[41, 80],
						[81, 380],
						[381, 800],
						[801, 1600],
						[1601, 2000]
					],
					'nh3': [
						[0, 200],
						[201, 400],
						[401, 800],
						[801, 1200],
						[1201, 1800],
						[1801, 2000]
					],
					'pb': [
						[0, 0.5],
						[0.6, 1.0],
						[1.1, 2.0],
						[2.1, 3.0],
						[3.1, 3.5],
						[3.6, 4.0]
					]
				};

				if (this.state.station_data && this.state.station_data.param_values != null) {
					this.state.station_data.hourly_param_concs[this.state.current_param].map((hourly_conc, i) => {
						if (hourly_conc === null) {
							hourly_conc = 0;
						}

						let color = window.color_code.color_1;
						if (breakpoint_table[this.state.current_param] !== undefined) {
							let factor_usepa_to_naaqi;
							let decimal_upto;
							switch(this.state.current_param) {
								case 'no2':
									factor_usepa_to_naaqi = 46.0055/24.45;
									break;
								case 'o3':
									factor_usepa_to_naaqi = 48/24.45;
									break;
								case 'co':
									factor_usepa_to_naaqi = 28.01/24.45;
									break;
								case 'so2':
									factor_usepa_to_naaqi = 64.066/24.45;
									break;
								default:
									factor_usepa_to_naaqi = 1;
									break;
							}

							if (this.state.current_param == 'co' || this.state.current_param == 'pb') {
								decimal_upto = 1;
							} else {
								decimal_upto = 0
							}

							if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][5][0]) {
								color = window.color_code.color_6;
							} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][4][0]) {
								color = window.color_code.color_5;
							} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][3][0]) {
								color = window.color_code.color_4;
							} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][2][0]) {
								color = window.color_code.color_3;
							} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][1][0]) {
								color = window.color_code.color_2;
							} else if ((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][0][0]) {
								color = window.color_code.color_1;
							}
						}

						individual_graph_data.push({
							y: parseFloat(hourly_conc.toFixed(2)),
							mm: parseFloat((hourly_conc*25.4).toFixed(2)),
							color: color
						});

						categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));		
					});
					categories.reverse();
				}
				individual_graph_data.reverse();

				/**
				 * This is a Object to plot Individual graph data.
				 * @type {Object}
				 * @property {Object} chart This sets chart properties.
				 * @property {Object} plotOptions This customize the graph plot settings/options.
				 * @property {Object} title This sets the title for the graph.
				 * @property {Object} subtitle This sets the subtitle for the graph.
				 * @property {Object} xAxis This defines what to show in xAxis.
				 * @property {Object} yAxis This defines what to show in yAxis.
				 * @property {Object} legend This hides the printing options from graph.
				 * @property {Object} tooltip This sets what to show in tooltip.
				 * @property {Array} series This contains data for ploting graph.
				 */
				this.individual_graph_config = {
					chart: {
						type: 'column',
						height: (window.innerHeight >= 800) ? 200 : 175
					},
					plotOptions: {
						series: {
							pointPadding: 0,
							groupPadding: 0
						}
						
					},
					title: {
						text: '1 Hour Average Report',
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						title: {
							enabled: true,
							text: 'Time',
							style: {
								fontWeight: 'normal'
							}
						},
						type: '',
						lineWidth: 0,
						minorGridLineWidth: 0,
						lineColor: 'transparent',      
						labels: {
							enabled: false
						},
						minorTickLength: 0,
						tickLength: 0,
						categories: categories
					},
					yAxis: {
						title: {
							text: this.state.current_param === 'temperature' || this.state.current_param === 'humidity' || this.state.current_param === 'noise' ? 'Value ' : 'Conc. '
						},
						lineWidth: 0,
						minorGridLineWidth: 0,
						lineColor: 'transparent',      
						labels: {
							enabled: false
						},
						minorTickLength: 0,
						tickLength: 0,
						gridLineColor: 'transparent'

					},
					legend: {
						enabled: false
					},
					tooltip: {
						pointFormat: ((this.state.current_param == 'rain') ? '<span style="color:{point.color}">' + 'Conc. ' + '<b>{point.y}</b> ' + unit + ' / ' + '<b>{point.mm}</b> ' + 'mm' + '<br/>' : '<span style="color:{point.color}">' + (this.state.current_param === 'temperature' || this.state.current_param === 'humidity' || this.state.current_param === 'noise' ? 'Value ' : 'Conc. ') + '<b>{point.y}</b> ' + unit + '<br/>')
					},
					series: [{
						data: individual_graph_data
					}]
				};

				/**
				 * This sets the graph config to blank array.
				 * @type {Array}
				 */
				this.individual_param_aqi_graph_config = [];
				let should_plot_individual_param_aqi = [];
				if (this.state.station_data.hourly_param_aqis) {
					Object.keys(this.state.station_data.hourly_param_aqis).map(function(key, index) {
						let bar_chart = [];
						bar_chart[index] = [];
						should_plot_individual_param_aqi[index] = false;
						this.state.station_data.hourly_param_aqis[key].map((value) => {
							let color = window.color_code.color_1;
							if(value >= 401) {
								color = window.color_code.color_6;
							} else if(value >= 301) {
								color = window.color_code.color_5;
							} else if(value >= 201) {
								color = window.color_code.color_4;
							} else if(value >= 101) {
								color = window.color_code.color_3;
							} else if(value >= 51) {
								color = window.color_code.color_2;
							}
							bar_chart[index].push({
								y: parseInt(value),
								color: color
							});
							if(value) {
								should_plot_individual_param_aqi[index] = true;
							}
						});
						bar_chart.map((data, i) => {
							bar_chart[i].reverse();
						});

						/**
						 * This is a Object to plot Individual parameter aqi graph data.
						 * @type {Object}
						 * @property {Object} chart This sets chart properties.
						 * @property {Object} plotOptions This customize the graph plot settings/options.
						 * @property {Object} title This sets the title for the graph.
						 * @property {Object} subtitle This sets the subtitle for the graph.
						 * @property {Object} xAxis This defines what to show in xAxis.
						 * @property {Object} yAxis This defines what to show in yAxis.
						 * @property {Object} legend This hides the printing options from graph.
						 * @property {Object} tooltip This sets what to show in tooltip.
						 * @property {Array} series This contains data for ploting graph.
						 */
						this.individual_param_aqi_graph_config[index] = {
							chart: {
								type: 'column',
								height: 65,
								width: 180
							},
							plotOptions: {
								series: {
									pointPadding: 0,
									groupPadding: 0
								}				
							},
							title: {
								text: ''
							},
							subtitle: {
								text: ''
							},
							xAxis: {
								title: {
									enabled: true,
									text: '',
									style: {
										fontWeight: 'normal'
									}
								},
								type: '',
								lineWidth: 0,
								minorGridLineWidth: 0,
								lineColor: 'transparent',      
								labels: {
									enabled: false
								},
								minorTickLength: 0,
								tickLength: 0,
								categories: categories
							},
							yAxis: {
								title: {
									text: ''
								},
								lineWidth: 0,
								minorGridLineWidth: 0,
								lineColor: 'transparent',      
								labels: {
									enabled: false
								},
								minorTickLength: 0,
								tickLength: 0,
								gridLineColor: 'transparent'

							},
							legend: {
								enabled: false
							},
							tooltip: {
								pointFormat: '<span style="color:{point.color}">Index <b>{point.y}</b>'
							},
							series: [{
								data: bar_chart[index]
							}]
						};
					}, this);
				}

				let graph_data = [];
				if (this.state.station_data) {
					this.state.station_data.hourly_aqis.map((hourly_aqi) => {
						let color = window.color_code.color_1;
						if(hourly_aqi.aqi >= 401) {
							color = window.color_code.color_6;
						} else if(hourly_aqi.aqi >= 301) {
							color = window.color_code.color_5;
						} else if(hourly_aqi.aqi >= 201) {
							color = window.color_code.color_4;
						} else if(hourly_aqi.aqi >= 101) {
							color = window.color_code.color_3;
						} else if(hourly_aqi.aqi >= 51) {
							color = window.color_code.color_2;
						}
						graph_data.push({
							y: parseInt(hourly_aqi.aqi),
							param: ((hourly_aqi.param == "" || hourly_aqi.param == null) ? '-' : hourly_aqi.param.toUpperCase()),
							color: color
						});
					});
				}
				graph_data.reverse();

				/**
				 * This is a Object to plot Hourly aqi graph data.
				 * @type {Object}
				 * @property {Object} chart This sets chart properties.
				 * @property {Object} plotOptions This customize the graph plot settings/options.
				 * @property {Object} title This sets the title for the graph.
				 * @property {Object} subtitle This sets the subtitle for the graph.
				 * @property {Object} xAxis This defines what to show in xAxis.
				 * @property {Object} yAxis This defines what to show in yAxis.
				 * @property {Object} legend This hides the printing options from graph.
				 * @property {Object} tooltip This sets what to show in tooltip.
				 * @property {Array} series This contains data for ploting graph.
				 */
				this.hourly_aqi_graph_config = {
					chart: {
						type: 'column',
						height: 80,
						width: 230
					},
					plotOptions: {
						series: {
							pointPadding: 0,
							groupPadding: 0
						}				
					},
					title: {
						text: ''
					},
					subtitle: {
						text: ''
					},
					xAxis: {
						title: {
							enabled: true,
							text: '',
							style: {
								fontWeight: 'normal'
							}
						},
						type: '',
						lineWidth: 0,
						minorGridLineWidth: 0,
						lineColor: 'transparent',      
						labels: {
							enabled: false
						},
						minorTickLength: 0,
						tickLength: 0,
						categories: categories
					},
					yAxis: {
						title: {
							text: 'AQI'
						},
						lineWidth: 0,
						minorGridLineWidth: 0,
						lineColor: 'transparent',      
						labels: {
							enabled: false
						},
						minorTickLength: 0,
						tickLength: 0,
						gridLineColor: 'transparent'

					},
					legend: {
						enabled: false
					},
					tooltip: {
						pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
					},
					series: [{
						data: graph_data
					}]
				};

				if (this.state.station_data && this.state.station_data.latest_param_value && this.state.station_data.latest_param_value.wdir) {
					wind_index = this.state.station_data.params.findIndex(x => x.key=="wdir");
					this.chart = false;
					this.magneto_config = {
						chart: {
							type: 'gauge',
							height: 145,
							width: 145
						},
						exporting: {
							enabled: false
						},
						credits: {
							enabled: false
						},

						title: {
							text: ''
						},
						tooltip: {
							enabled: false,
							backgroundColor: '#FCFFC5',
							borderColor: '#fff',
							pointFormat: '{series.name}: <b>{point.y}</b><br/>',
							valueSuffix: ' °'
						},
						 plotOptions: {
							series: {
								dataLabels: {
									enabled: false
								}
							}
						},

						pane: {
							startAngle: 0,
							endAngle: 360,
							background: [{
								borderWidth: 1,
								outerRadius: '50%'
							}, {
								backgroundColor: {
									linearGradient: {
										x1: 0,
										y1: 0
									},
									stops: [
										[0, '#FFF'],
										[1, '#333']
									]
								},
								borderWidth: 4,
								outerRadius: '10%'
							}, {
								backgroundColor: {
									linearGradient: {
										x1: 0,
										y1: 0,
									},
									stops: [
										[0, '#333'],
										[1, '#FFF']
									]
								},
								borderWidth: 4,
								outerRadius: '10%'
							}, {
								// default background
							}, {
								backgroundColor: '#DDD',
								borderWidth: 4,
								outerRadius: '10%',
								innerRadius: '8%'
							}]
						},
						// the value axis
						yAxis: [{
							title: {
								text: '',
							},
							min: 0,
							max: 360,
							lineColor: '#333',
							offset: -10,
							tickInterval: 0,
							tickWidth: 2,
							tickPosition: 'outside',
							tickLength: 10,
							tickColor: '#333',
							minorTickInterval: 5,
							minorTickWidth: 1,
							minorTickLength: 10,
							minorTickPosition: 'outside',
							minorTickColor: '#666',
							labels: {
								step: 1,
								distance: -12,
								rotation: 'auto'
							},
						}, {
							title: {
								text: '',
							},
							type: 'category',
							categories: ['N', '' , 'NE', '', 'E', '', 'SE', '', 'S', '', 'SW', '' , 'W', '', 'NW', '', 'N'],
							min: 0,
							max: 16,
							lineColor: '#ddd',
							offset: -20,
							tickInterval: 1,
							tickWidth: 1,
							tickPosition: 'outside',
							tickLength: 20, // =50-10
							tickColor: '#ddd',
							minorTickInterval: 1,
							minorTickWidth: 0,
							minorTickLength: 50,
							minorTickPosition: 'inside',
							minorTickColor: '#0f0',
							labels: {
								step: 1,
								distance: 10,
								rotation: 'auto'
							},
							endOnTick: true,
						}, {
							type: 'number',
							title: {
								text: '',
							},
							labels: {
								enabled: false,
							},
							min: 0,
							max: 12,
							lineColor: '#ddd',
							offset: -50,
							tickInterval: 10,
							tickWidth: 0,
							tickPosition: 'inside',
							tickLength: 45,
							tickColor: '#ddd',
							minorTickWidth: 0,
							endOnTick: false,
						}],

						series: [{
							name: 'East',
							data: [parseFloat(this.state.station_data.latest_param_value.wdir)],
							dial: {
								radius: '60%',
								baseWidth: 10,
								baseLength: '0%',
								rearLength: 0,
								borderWidth: 1,
								borderColor: '#9A0000',
								backgroundColor: '#CC0000',
							}
						}, {
								name: 'West',
							data: [parseFloat(this.state.station_data.latest_param_value.wdir)],
							dial: {
								radius: '-60%',
								baseWidth: 10,
								baseLength: '0%',
								rearLength: 0,
								borderWidth: 1,
								borderColor: '#1B4684',
								backgroundColor: '#3465A4',
							}
						}]
					}
				}

				if (this.state.station_data && this.state.station_data.latest_param_value && this.state.station_data.latest_param_value.wspeed) {
					this.chart = false;
					speed_index = this.state.station_data.params.findIndex(x => x.key=="wspeed");
					this.speedo_meter = {
						chart: {
							type: 'gauge',
							height: 145,
							width: 145
						},

						title: {
							text: ''
						},

						pane: {
							startAngle: -150,
							endAngle: 150,
							background: [{
								backgroundColor: {
									linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
									stops: [
										[0, '#FFF'],
										[1, '#333']
									]
								},
								borderWidth: 0,
								outerRadius: '10%'
							}, {
								backgroundColor: {
									linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
									stops: [
										[0, '#333'],
										[1, '#FFF']
									]
								},
								borderWidth: 1,
								outerRadius: '10%'
							}, {
								// default background
							}, {
								backgroundColor: '#DDD',
								borderWidth: 0,
								outerRadius: '10%',
								innerRadius: '10%'
							}]
						},

						// the value axis
						yAxis: {
							min: 0,
							max: 60,

							minorTickInterval: 'auto',
							minorTickWidth: 1,
							minorTickLength: 10,
							minorTickPosition: 'inside',
							minorTickColor: '#666',

							tickPixelInterval: 30,
							tickWidth: 2,
							tickPosition: 'inside',
							tickLength: 10,
							tickColor: '#666',
							labels: {
								step: 2,
								rotation: 'auto'
							},
							title: {
								text: ''
							},
							plotBands: [{
								from: 0,
								to: 20,
								color: '#55BF3B' // green
							}, {
								from: 20,
								to: 40,
								color: '#DDDF0D' // yellow
							}, {
								from: 40,
								to: 60,
								color: '#DF5353' // red
							}]
						},
						plotOptions: {
							series: {
								dataLabels: {
									enabled: false
								}
							}
						},

						exporting: {
							enabled: false
						},
						credits: {
							enabled: false
						},
						tooltip: {
							enabled: false
						},
						series: [{
							data: [parseFloat(this.state.station_data.latest_param_value.wspeed)]
						}]
					}
				}

				if (this.state.station_data && this.state.station_data.latest_param_value && this.state.station_data.latest_param_value.press) {
					pressure_index = this.state.station_data.params.findIndex(x => x.key=="press");
					this.chart = false;
					let that = this;
					this.pressure_config = {
						chart: {
							type: 'gauge',
							alignTicks: false,
							plotBackgroundColor: null,
							plotBackgroundImage: null,
							plotBorderWidth: 0,
							plotShadow: false,
							height: 175,
							width: 175
						},
						title: {
							text: ''
						},
						pane: {
							startAngle: -150,
							endAngle: 150
						},
						yAxis: [{
							min: 720,
							max: 870,
							lineColor: '#339',
							tickColor: '#339',
							minorTickColor: '#339',
							offset: -25,
							lineWidth: 2,
							labels: {
								distance: -20,
								rotation: 'auto'
							},
							tickLength: 5,
							minorTickLength: 5,
							endOnTick: false
						}, {
							min: 960,
							max: 1160,
							tickPosition: 'outside',
							lineColor: '#933',
							lineWidth: 2,
							minorTickPosition: 'outside',
							tickColor: '#933',
							minorTickColor: '#933',
							tickLength: 5,
							minorTickLength: 5,
							labels: {
								distance: 12,
								rotation: 'auto'
							},
							offset: -20,
							endOnTick: false
						}],
						exporting: {
							enabled: false
						},
						credits: {
							enabled: false
						},
						tooltip: {
							enabled: false
						},
						series: [{
							data: [(parseFloat(this.state.station_data.latest_param_value.press) * 0.750062)],
							dataLabels: {
								formatter: function () {
									let mmHg = this.y.toFixed(2),
										mBar = Math.round(mmHg / 0.750062).toFixed(2);
									// return '<span style="color:#339">' + mBar + ' ' + that.state.station_data.param_units[pressure_index]+ '</span><br/>' +'<span style="color:#933">' + mmHg + ' mmHg</span>';
								},
								backgroundColor: {
									linearGradient: {
										x1: 0,
										y1: 0,
										x2: 0,
										y2: 1
									},
									stops: [
										[0, '#DDD'],
										[1, '#FFF']
									]
								}
							}
						}]
					}
				}

				var suggestions = [];
				if(this.state.station_data.suggestions) {
					this.state.station_data.suggestions.map(function(suggestion, i) {
						suggestions.push(
							<div className="panel-item" key={i}>
								<div className={'icon suggestion-' + (i+1)}></div>
								<div className="panel-text">{suggestion}</div>
							</div>
						);
					}, this);
				} else {
					suggestions.push(<li className="hourly-aqi-na-error">NA</li>);
				}

				return(
					<span>
						<div id="leftAnalyticsPanel">
							<Scrollbars autoHide>
								<div className="location-time-container">
									<StationSearch {...this.state} fetchStationData={(id) => this.fetchStationData(id)} page={'analytics'} />
									<div className="data-source">Data Source: {this.state.source}</div>
									<div className="time-container">
										<div className={'icon clock' + (((moment().tz('Asia/Kolkata').format('X') - this.state.station_data.time_stamps[(this.state.station_data.time_stamps.length - 1)]) > 7200 || !this.state.station_data.time_stamps.length) ? ' offline' : '')}></div>
										<div className="time" id="last_data_time">
										{(() => {
											if(this.state.station_data.time_stamps.length) {
												return moment.unix(this.state.station_data.time_stamps[(this.state.station_data.time_stamps.length - 1)]).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm');
											} else {
												return 'NA';
											}
										})()}
										</div>
										<CompareCheck {...this.state} ref={(child) => {this._child = child;}} />
									</div>
								</div>
								<div className="properties-container">
									<div className="aqi-container">
										<div className={'icon aqi-' + this.state.station_data.aqi_range}></div>
										<div className= "param-value">
											<span className="aqi-value">
												{(() => {
													if(this.state.station_data.aqi != 0) {
														return this.state.station_data.aqi;
													} else {
														return 'NA';
													}
												})()}
											</span>
											<span className="aqi-text">AQI</span>
										</div>
										<div className="aqi-status">
											{(() => {
												if(this.state.station_data.aqi != 0) {
													return(
														<div className={'status aqi-' + this.state.station_data.aqi_range}>
															{this.state.station_data.aqi_status}
														</div>
													);
												}
											})()}
											{(() => {
												if(this.state.station_data.aqi != 0 && this.state.station_data.params && Object.keys(this.state.station_data.params).length) {
													let aqi_name = this.state.station_data.params.map((param) => {
														if (param.key == this.state.station_data.hourly_aqis[0].param) {
															return <div className = "responsible-param-name" dangerouslySetInnerHTML={{__html: "Major Pollutant : " + param.name}} />;
														}
													});
													return aqi_name;
												}
											})()}
										</div>
									</div>
								</div>
								<div className="graph-container">
									{(() => {
										let hourly_aqis = [];
										this.state.station_data.hourly_aqis.map((hourly_aqi) => {
											if(hourly_aqi.aqi) {
												hourly_aqis.push(parseInt(hourly_aqi.aqi));
											}
										});
										if (hourly_aqis.length) {
											return (<div>
													<ReactHighcharts ref="chart" config={this.hourly_aqi_graph_config} />
													<div className="graph-right-item">
														<div className="text">
															{Math.min.apply(null, hourly_aqis)}<div className="label">Min</div>
														</div>
														<div className="text">
															{Math.max.apply(null, hourly_aqis)}<div className="label">Max</div>
														</div>
													</div>
												</div>
											);
										} else {
											return(<div className="flex no-data-text">
												<center className="text-danger table">No data to show</center>
											</div>);
										}
										
									})()}
								</div>
								<div className="aqi-scale-title">AQI Scale</div>
								<div className="colored-bar">
									<div className="color-bar color-one"></div>
									<div className="color-bar color-two"></div>
									<div className="color-bar color-three"></div>
									<div className="color-bar color-four"></div>
									<div className="color-bar color-five"></div>
									<div className="color-bar color-six"></div>
								</div>
								<div className="colored-index">
									<div className="color-index first-index">0</div>
									<div className="color-index second-index">50</div>
									<div className="color-index third-index">100</div>
									<div className="color-index fourth-index">200</div>
									<div className="color-index fifth-index">300</div>
									<div className="color-index sixth-index">400</div>
									<div className="color-index seventh-index">500</div>
								</div>
								<div className="colored-index-text">
									<div className="first-index-text">Good</div>
									<div className="last-index-text">Severe</div>
								</div>
								<div className="panel-container">
									<div className="panel-title">Last 24 Hrs Data</div>
									{(() => {
										let hourly_aqis = [];
										this.state.station_data.hourly_aqis.map((hourly_aqi) => {
											if(hourly_aqi) {
												hourly_aqis.push(parseInt(hourly_aqi));
											}
										});
										if (hourly_aqis.length) {
											let graphs = Object.keys(this.state.station_data.hourly_param_aqis).map(function(key, index) {
												if(should_plot_individual_param_aqi[index]) {
													let current_param_index;
													for(let i=0; i<24; i++) {
														if(this.state.station_data.hourly_param_aqis[key][i]) {
															current_param_index = this.state.station_data.hourly_param_aqis[key][i];
															break;
														}
													}
													
													return (
														<div className="graph-item" key={index}>
															<div className="graph-left-item">
																<div className="text">{key}</div>
																<div className="text">
																	{current_param_index}<sub>AQI</sub>
																</div>
															</div>
															<ReactHighcharts ref="chart" config={this.individual_param_aqi_graph_config[index]} />
															<div className="graph-right-item">
																<div className="text">
																	{Math.min.apply(null, this.state.station_data.hourly_param_aqis[key].filter((value) => value ? true : false))}<div className="label">Min</div>
																</div>
																<div className="text">
																	{Math.max.apply(null, this.state.station_data.hourly_param_aqis[key].filter((value) => value ? true : false))}<div className="label">Max</div>
																</div>
															</div>
														</div>
													);
												}
											}, this);

											return (<div className="graph-history-container">
												{graphs}
											</div>);
										} else {
											return(<div className="flex no-data-text">
												<center className="text-danger table">No data to show</center>
											</div>);
										}
									})()}
								</div>
							</Scrollbars>
						</div>
						<div id="middleAnalyticsPanel">
							<Scrollbars autoHide>
								<div className="title-text">Trends & Analysis</div>
								{(() => {
									if (this.state.station_data.param_values != null && this.state.station_data.param_values && this.state.station_data.param_values.length !== 0) {
										let allowed_parameters_in_order = ['pm1', 'pm2.5', 'pm10', 'so2', 'no', 'no2', 'co', 'o3', 'co2', 'o2', 'temperature', 'humidity', 'rain', 'uv', 'light', 'noise', 'voc', 'lint', 'nox', 'ch4', 'lead', 'wspeed', 'press', 'no3', 'ph', 'do', 'nh4', 'orp', 'us_mb', 'cur', 'nh3', 'us_hc04', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'];
										let current_params = {};
										this.state.station_data.params.map((param) => {
											current_params[param.key] = param;
										});
										let parameters = allowed_parameters_in_order.map((param, i) => {
											if(Object.keys(current_params).indexOf(param) >= 0) {
												return(
													<div className={'parameter-btn hellip' + ((param === this.state.current_param)? ' active': '')} dangerouslySetInnerHTML={{__html: current_params[param].name}} onClick={() => {this.change_param(param);}} key={i} />
												);
											}
										});
										return(
											<div>
												<div className="parameter-btn-group">
													{parameters}
												</div>
												{(() => {
													if (this.line_chart_config) {
														return <ReactHighcharts ref="real_time_chart" config={this.line_chart_config} />;
													}
												})()}
												{(() => {
													if (this.individual_graph_config) {
														return <div className="avg-report-hourly">
															{/*<OutlierLimit {...this.state} ref={(child) => {this._child = child;}} outlierDataPush={(current_param, outlimit) => this.outlierDataPush(current_param, outlimit)} />*/}
															<ReactHighcharts ref="avg_chart" config={this.individual_graph_config} />
														</div>;
													}
												})()}
												{(() => {
													/*if (breakpoint_table[this.state.current_param] !== undefined) {
														return (
															<div className="graph-index">
																<div className="index">
																	<div className="index-0"></div>
																	<div className="index-text">0</div>
																</div>
																<div className="index">
																	<div className="index-1"></div>
																	<div className="index-text">
																		{breakpoint_table[this.state.current_param][0][1]}
																	</div>
																</div>
																<div className="index">
																	<div className="index-2"></div>
																	<div className="index-text">
																		{breakpoint_table[this.state.current_param][1][1]}
																	</div>
																</div>
																<div className="index">
																	<div className="index-3"></div>
																	<div className="index-text">
																		{breakpoint_table[this.state.current_param][2][1]}
																	</div>
																</div>
																<div className="index">
																	<div className="index-4"></div>
																	<div className="index-text">
																		{breakpoint_table[this.state.current_param][3][1]}
																	</div>
																</div>
																<div className="index">
																	<div className="index-5"></div>
																	<div className="index-text">
																		{breakpoint_table[this.state.current_param][4][1]}
																	</div>
																</div>
																<div className="index">
																	<div className="index-6"></div>
																	<div className="index-text">
																		{breakpoint_table[this.state.current_param][5][1]}
																	</div>
																</div>
																<div className="unit" dangerouslySetInnerHTML={{__html: unit}}></div>
															</div>
														);
													}*/
												})()}
											</div>
										);
									} else {
										return(<div className="flex no-data-text full-height">
											<center className="table">No data received in last 24 hours.</center>
										</div>);
									}
								})()}
							</Scrollbars>
						</div>
						<div id="rightAnalyticsPanel">
							<Scrollbars autoHide>
								<div className="panel-container">
									<div className="panel-title">Weather Report</div>
									{(() => {
										if (this.state.station_data.param_values != null && this.state.station_data.param_values && this.state.station_data.param_values.length !== 0) {
											return (
											<div>
												<div className="temperature-container">
													<div className="icon temperature"></div>
													<div className="property-text" id="current_temp">
														{(this.state.station_data.param_values.temperature) ? Math.round(parseFloat(this.state.station_data.param_values.temperature[(this.state.station_data.param_values.temperature.length - 1)])) + '°C' : 'NA'}
													</div>
													<div className="clearfix"></div>
													{(() => {
														if (this.state.station_data.param_values.temperature) {
															return (
																<div className="avg-min-max">
																	<div className="avg">
																		{(() => {
																			var total = 0;
																			for(var i = 0; i < this.state.station_data.param_values.temperature.length; i++) {
																				if (this.state.station_data.param_values.temperature[i] !== null) {
																					total += parseFloat(this.state.station_data.param_values.temperature[i]);
																				}
																			}
																			var avg = Math.round(total / this.state.station_data.param_values.temperature.length * 100) / 100;

																			return (
																				<div className="value">{avg}</div>
																			);
																		})()}
																		<div className="label">AVG</div>
																	</div>
																	<div className="min">
																		<div className="value">
																			{Math.min.apply(null, this.state.station_data.param_values.temperature)}
																		</div>
																		<div className="label">MIN</div>
																	</div>
																	<div className="max">
																		<div className="value">
																			{Math.max.apply(null, this.state.station_data.param_values.temperature)}
																		</div>
																		<div className="label">MAX</div>
																	</div>
																</div>
															);
														}
													})()}
												</div>
												<div className="humidity-container">
													<div className="icon humidity"></div>
													<div className="property-text">
														<span id="current_humid">
															{(this.state.station_data.param_values.humidity) ? Math.round(parseFloat(this.state.station_data.param_values.humidity[(this.state.station_data.param_values.humidity.length - 1)])) + '%' : 'NA'}
														</span>
														<small>RH</small>
													</div>
													<div className="clearfix"></div>
													{(() => {
														if (this.state.station_data.param_values.humidity) {
															return (
																<div className="avg-min-max">
																	<div className="avg">
																		{(() => {
																			var total = 0;
																			for(var i = 0; i < this.state.station_data.param_values.humidity.length; i++) {
																				if (this.state.station_data.param_values.humidity[i] !== null) {
																					total += parseFloat(this.state.station_data.param_values.humidity[i]);
																				}
																			}
																			var avg = Math.round(total / this.state.station_data.param_values.humidity.length * 100) / 100;

																			return (
																				<div className="value">{avg}</div>
																			);
																		})()}
																		<div className="label">AVG</div>
																	</div>
																	<div className="min">
																		<div className="value">
																			{Math.min.apply(null, this.state.station_data.param_values.humidity)}
																		</div>
																		<div className="label">MIN</div>
																	</div>
																	<div className="max">
																		<div className="value">
																			{Math.max.apply(null, this.state.station_data.param_values.humidity)}
																		</div>
																		<div className="label">MAX</div>
																	</div>
																</div>
															);
														}
													})()}
												</div>
												{(() => {
													if (this.state.station_data.hourly_param_concs.rain && this.state.station_data.hourly_param_concs.rain.length) {
														let rainsum = 0, rain_unit = '';
														// console.log('value_rain', this.state.station_data.hourly_param_concs.rain);
														this.state.station_data.hourly_param_concs.rain.map((value) => {
															// console.log('value', value);
															rainsum += (value ? parseFloat(value) : 0);
														})
														this.state.station_data.params.map((parameter, index) => {
															if (parameter.key === 'rain') {
																rain_unit = this.state.station_data.param_units[index];
															}
														});
														return <div className="rainfall-container">
															<div className="rainfall-icon-title">
																<div className="icon-rainfall rainfall"></div>
																<div className="property-text">
																	<small className="rainfall-title">Rainfall</small>
																	<span id="current_humid">
																		{rainsum.toFixed(2) + ' ' + rain_unit}
																	</span>
																	<span id="current_humid">
																		{' / ' + (rainsum*25.4).toFixed(2) + ' mm'}
																	</span>
																</div>
															</div>
															<div className="clearfix"></div>
															{(() => {
																if (this.state.station_data.hourly_param_concs.rain) {
																	let bar_chart_rain = [];
																	this.state.station_data.hourly_param_concs.rain.map((value,index) => {
																		let color = '#a44a9c';
																		// console.log('value', value);
																		bar_chart_rain.push({
																			y: (value ? parseFloat(value.toFixed(2)) : 0),
																			mm: (value ? parseFloat(value*25.4.toFixed(2)) : 0),
																			color: color
																		});
																		// console.log('value1', value);
																	});
																	// console.log('value', reversed_rain);
																	this.individual_rain_graph_config = {
																		chart: {
																			type: 'column',
																			height: 65,
																			width: 180
																		},
																		plotOptions: {
																			series: {
																				pointPadding: 0,
																				groupPadding: 0
																			}				
																		},
																		title: {
																			text: ''
																		},
																		subtitle: {
																			text: ''
																		},
																		xAxis: {
																			title: {
																				enabled: true,
																				text: '',
																				style: {
																					fontWeight: 'normal'
																				}
																			},
																			type: '',
																			lineWidth: 0,
																			minorGridLineWidth: 0,
																			lineColor: 'transparent',      
																			labels: {
																				enabled: false
																			},
																			minorTickLength: 0,
																			tickLength: 0,
																			categories: categories
																		},
																		yAxis: {
																			title: {
																				text: ''
																			},
																			lineWidth: 0,
																			minorGridLineWidth: 0,
																			lineColor: 'transparent',      
																			labels: {
																				enabled: false
																			},
																			minorTickLength: 0,
																			tickLength: 0,
																			gridLineColor: 'transparent'

																		},
																		legend: {
																			enabled: false
																		},
																		tooltip: {
																			pointFormat: '<span style="color:{point.color}">Rainfall <b>{point.y}</b>' + ' ' +rain_unit + ' / ' + '<b>{point.mm}</b> ' + 'mm' + '</span>'
																		},
																		series: [{
																			data: bar_chart_rain.reverse()
																		}]
																	};
																	// console.log('individual_param_aqi_graph_config', this.individual_rain_graph_config);
																	// console.log('rainu', this.state.station_data.hourly_param_concs.rain[this.state.station_data.hourly_param_concs.rain.length - 1]);
																	if(this.state.station_data.hourly_param_concs.rain) {
																		return <div className="graph-rain-history-container">
																			<div className="graph-item">
																				<div className="graph-left-item">
																					<div className="text">Hourly Rain</div>
																				</div>
																				<ReactHighcharts ref="chart" config={this.individual_rain_graph_config} />
																				<div className="graph-right-item">
																					<div className="text">
																						{((Math.min.apply(null, this.state.station_data.hourly_param_concs.rain.filter((value) => !isNaN(value) ? true : false)))*25.4).toFixed(2)}
																						<br />
																						{(Math.min.apply(null, this.state.station_data.hourly_param_concs.rain.filter((value) => !isNaN(value) ? true : false))).toFixed(2)}
																						<div className="label">Min</div>
																					</div>
																					<div className="text">
																						{((Math.max.apply(null, this.state.station_data.hourly_param_concs.rain.filter((value) => !isNaN(value) ? true : false)))*25.4).toFixed(2)}
																						<br />
																						{(Math.max.apply(null, this.state.station_data.hourly_param_concs.rain.filter((value) => !isNaN(value) ? true : false))).toFixed(2)}
																						<div className="label">Max</div>
																					</div>
																				</div>
																			</div>
																		</div>;
																	}
																} else {
																	return(<div className="flex no-data-text">
																		<center className="text-danger table">No data to show</center>
																	</div>);
																}
															})()}
														</div>;
													}
												})()}
											</div>);
										} else {
											return(<div className="flex no-data-text">
												<center className="text-danger table">No data received in last 24 hours.</center>
											</div>);
										}
									})()}
								</div>
								{(() => {
									if (this.state.station_data && this.state.station_data.latest_param_value) {
										return <div className="speed-magneto">
											<div className="speed-magneto-chart">
											{(() => {
												if (this.state.station_data.latest_param_value.wspeed) {
													return <ReactHighcharts ref="speed_chart" config={this.speedo_meter} />;
												}
											})()}
											{(() => {
												if (this.state.station_data.latest_param_value.wdir) {
													return <ReactHighcharts ref="magneto_chart" config={this.magneto_config} />;
												}
											})()}
											</div>
											<div className="main-wind-label">
												{(() => {
													if (this.state.station_data.latest_param_value.wspeed) {
														return <div className="wind-label">
															<div className="wind-speed-data" id="wind_speed_data">{this.state.station_data.latest_param_value.wspeed + ' ' + this.state.station_data.param_units[speed_index]}</div>
															<div className="wind-speed-label">Wind Speed</div>
														</div>;
													}
												})()}
												{(() => {
													if (this.state.station_data.latest_param_value.wdir) {
														return <div className="wind-label">
															<div className="wind-direction-data" id="wind_direction_data">{parseInt(this.state.station_data.latest_param_value.wdir) + ' °'}</div>
															<div className="wind-direction-label">Wind Direction</div>
														</div>;
													}
												})()}
											</div>
										</div>;
									}
								})()}
								{(() => {
									if (this.state.station_data && this.state.station_data.latest_param_value && this.state.station_data.latest_param_value.press) {
										return <div className="pressure-chart">
											<ReactHighcharts ref="pressure_chart" config={this.pressure_config} />
											<div className="pressure-title">
												<div className="pressure-data" id="pressure_data">{this.state.station_data.latest_param_value.press + ' ' + this.state.station_data.param_units[pressure_index]}</div>
												<div className="pressure-label">Pressure</div>
											</div>
										</div>;
									}
								})()}
								{(() => {
									if (this.chart) {
										return <div className="panel-container">
											<div className="panel-title">Suggestions</div>
											<div className="panel-list">
												{suggestions}
											</div>
										</div>;
									}
								})()}
							</Scrollbars>
						</div>
					</span>
				);
			})()}
		</div>;
	}
}

StationDetailedAnalytics.contextTypes = {
	router: React.PropTypes.object
};
