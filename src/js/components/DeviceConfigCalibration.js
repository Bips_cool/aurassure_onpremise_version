import React from 'react';
import { findDOMNode } from 'react-dom';
import NavLink from './NavLink';
import ReactTooltip from 'react-tooltip';
import { Scrollbars } from 'react-custom-scrollbars';
import io from 'socket.io-client';
import { Link } from 'react-router';
import _ from 'lodash';

/**
 * This Class Used for Calibrataion devices.
 */
export default class DeviceConfigCalibration extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			is_admin: document.getElementById('city_id') ? (document.getElementById('city_id').value == 5 ? true : false) : false,
			device_id: props.params.device_id ? props.params.device_id : 1,
			allow_calibrations_success_msg: false,
			calibrations: null,
			data_sending_configs: null,
			device_qr_code: null,
			status: null,
			params: null,
			param_calibs: null,
			enable_save: true,
			param_data_sending_configs: null,
			sen_id: {
				'temperature': '1',
				'humidity': '2',
				'so2': '3',
				'no2': '4',
				'o3': '5',
				'no': '6',
				'co': '7',
				'co2': '8',
				'o2': '9',
				'noise': '10',
				'uv': '11',
				'press': '12',
				'pm1': '13',
				'pm2.5': '14',
				'pm10': '15',
				'wspeed': '16',
				'wdir': '17',
				'rain': '18',
				'us_mb': '19',
				'cur': '20',
				'nh3': '21',
				'us_hc04': '22'
			},
			parameters: {
				'pm1': 'PM1',
				'pm2.5': 'PM2.5',
				'pm10': 'PM10',
				'no': 'NO',
				'no2': 'NO2',
				'nox': 'NOx',
				'so2': 'SO2',
				'o2': 'O2 ( Winsen Ze-03 O2 )',
				'o3': 'O3',
				'ch4': 'CH4',
				'lead': 'Lead',
				'co': 'CO',
				'co2': 'CO2',
				'voc': 'VOC',
				'nh3': 'NH3',
				'temperature': 'Temperature',
				'humidity': 'Humidity',
				'noise': 'Noise',
				'rain': 'Rainfall',
				'uv': 'UV ( ML8511 )',
				'lint': 'Light',
				'wspeed': 'Wind Speed',
				'wdir': 'Wind Direction',
				'press': 'Atmospheric Pressure',
				'no3': 'Nitrate',
				'ph': 'pH',
				'do': 'Dissolved Oxygen',
				'nh4': 'Amonium',
				'us_mb': 'Ultrasonic ( Maxbotix )',
				'cur': 'Current Sensor',
				'nh3': 'Ammonia',
				'us_hc04': 'Ultrasonic ( HC04 )'
			},
			templates: {
				template1: ['no2','so2','co','o3','no'], //All 5 parameters - Alpha Sense
				template2: ['pm1','pm2.5','pm10','nox','co2','noise','lint','rain','wspeed','wdir','temperature','humidity','voc','lead','ch4','press','no3','ph','do','nh4','orp','cur'], //2 parameters
				template3: ['o2'], //3 parameters
				template4: ['uv'], //3 parameters
				template5: ['nh3'],
				template6: ['us_mb','us_hc04']
			}
		};

		/**
		 * This is used for City name.
		 * @type {string}
		 */
		this.city_name = window.location.host.split('.')[0];
	}

	/**
	 * This function is used to check the parameter type and how many fields it'll have.
	 * @param  {string} param This is the selected parameter.
	 * @param  {number} index This is the index of selected parameters array.
	 */
	changeCalibrationType(param, index) {
		console.log(param,index);
		let params = this.state.params;
		params[index] = param;

		let param_calibs = this.state.param_calibs;
		if (this.state.templates.template1.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[params[index]] ? this.state.sen_id[params[index]] : '-1',
				offset: param_calibs[index].offset,
				multiplier: param_calibs[index].multiplier,
				we_zero: param_calibs[index].we_zero,
				aux_zero: param_calibs[index].aux_zero,
				sensitivity: param_calibs[index].sensitivity
			};
		} else if (this.state.templates.template6.indexOf(param) > -1) {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[params[index]] ? this.state.sen_id[params[index]] : '-1',
				offset: param_calibs[index].offset,
				multiplier: param_calibs[index].multiplier,
				zeroPoint: '0'
			};
		} else {
			param_calibs[index] = {
				sen_id: this.state.sen_id && this.state.sen_id[params[index]] ? this.state.sen_id[params[index]] : '-1',
				offset: param_calibs[index].offset,
				multiplier: param_calibs[index].multiplier
			};
		}

		this.setState({
			params: params,
			param_calibs: param_calibs,

		});
	}

	/**
	 * This function called after changing the values of the parameter and set that to state.
	 * @param  {number} index  This is the index of selected parameter.
	 * @param  {string} param  This is the key of the parameter which holds the value.
	 * @param  {number} target This is the value of the parameter.
	 */
	changeCalibarationValues(index, param, target) {
		let param_calibs = this.state.param_calibs;
		param_calibs[index][param] = target.value;
		this.setState({param_calibs: param_calibs});

		let value_check = document.getElementsByClassName('form-check'),
			check_fields = [];
		for (var i = 0; i < value_check.length; i++) {
			let valueCheck = value_check[i].value;
			// console.log(valueCheck.split('.')[1]);
			if (
				!isNaN(valueCheck.trim()) && 
				(valueCheck.trim() != '') && 
				!(
					(valueCheck.indexOf('.') > -1) && ((valueCheck.split('.')[0] === '') || (valueCheck.split('.')[1] === ''))
				)
			) {
				check_fields[i] = true;
				value_check[i].parentElement.className = 'form-config';
			} else {
				check_fields[i] = false;
				value_check[i].parentElement.className = 'form-config error';
			}
		}
		this.setState({enable_save: (check_fields.indexOf(false) > -1) ? false : true});
	}

	/**
	 * This is a toggle that used to send data or not.
	 * @param  {number} index This is the index of selected parameter.
	 * @param {boolean} value This sats the toggle on or off.
	 */
	changeCalibarationDataSending(index, value) {
		let param_data_sending_configs = this.state.param_data_sending_configs;
		param_data_sending_configs[index] = value;
		this.setState({param_data_sending_configs: param_data_sending_configs});
	}

	/**
	 * This function is used to add a new row of parameter.
	 */
	addNewCalibarationRow() {
		let parameters = this.state.parameters;

		if (!this.state.is_admin) {
			let delete_param = this.state.templates.template2.map((del_param, id) => {
				delete parameters[del_param];
			});
		}
		let param = Object.keys(this.state.parameters)[0];

		let params = this.state.params ? this.state.params : [];
		params.push(param);

		let param_calibs = this.state.param_calibs ? this.state.param_calibs : [];
		// param_calibs.push({sen_id: this.state.sen_id[param]});
		if (this.state.templates.template1.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				offset: '0',
				multiplier: '1',
				we_zero: '0',
				aux_zero: '0',
				sensitivity: '0'
			});
		} else if (this.state.templates.template2.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				offset: '0',
				multiplier: '1'
			});
		} else if (this.state.templates.template3.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				offset: '0',
				multiplier: '1.0',
				o2_v1: '1.0'
			});
		} else if (this.state.templates.template4.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				offset: '0',
				multiplier: '1.0',
				uv_adcmin: '0'
			});
		} else if (this.state.templates.template5.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				offset: '0',
				multiplier: '1.0',
				nh3_v1: '1.0'
			});
		} else if (this.state.templates.template6.indexOf(param) > -1) {
			param_calibs.push({
				sen_id: this.state.sen_id && this.state.sen_id[param] ? this.state.sen_id[param] : '-1',
				offset: '0',
				multiplier: '1.0',
				zeroPoint: '0'
			});
		}

		let param_data_sending_configs = this.state.param_data_sending_configs ? this.state.param_data_sending_configs : [];
		param_data_sending_configs.push('on');

		this.setState({
			params: params,
			param_calibs: param_calibs,
			param_data_sending_configs: param_data_sending_configs
		});
	}

	/**
	 * This function is used to remove a parameter row.
	 * @param  {number} index This is the parameter index.
	 */
	removeCalibarationRow(index) {
		let params = this.state.params;
		params.splice(index, 1);

		let param_calibs = this.state.param_calibs;
		param_calibs.splice(index, 1);

		let param_data_sending_configs = this.state.param_data_sending_configs;
		param_data_sending_configs.splice(index, 1);

		this.setState({
			params: params,
			param_calibs: param_calibs,
			param_data_sending_configs: param_data_sending_configs
		});
		console.log('params', params);
		console.log('params calib', param_calibs);
		console.log('params data sending', param_data_sending_configs);
	}

	/**
	 * This function is to save Caliberations for device.
	 */
	saveDeviceCalibration() {
		let calibrations = {};
		calibrations['device_id'] = this.state.device_id;
		calibrations['calibrations'] = {};
		calibrations['data_sending_configs'] = {};
		this.state.params.map((param, index) => {
			calibrations['calibrations'][param] = this.state.param_calibs[index];
			calibrations['data_sending_configs'][param] = (this.state.param_data_sending_configs[index] == true) ? 'on' : 'off';
			console.log('Saving Calibrations', calibrations);
		});
		console.log('Saved Calibrations', JSON.stringify(calibrations));
		this.socket.emit('save_device_calibrations', JSON.stringify(calibrations));
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Device Configuration - Aurassure';

		/**
		 * Socket is used to sync the data between client and server in real time.
		 */
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
			query: {
				s_token: this.readCookie('PHPSESSID')
			}
		});
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket');
			console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			this.socket.emit('get_device_calibrations', JSON.stringify({
				device_id: this.state.device_id
			}));
		});

		this.socket.on('update_device_calibrations', (payload) => {
			console.log('update_device_calibrations', JSON.parse(payload));
			if (this.state.device_id == JSON.parse(payload).device_id) {
				let calib = JSON.parse(payload),
					saved_calib = JSON.parse(payload),
					params = [],
					param_calibs = [],
					param_data_sending_configs = [],
					saved_params = [],
					saved_param_calibs = [],
					saved_param_data_sending_configs = [];
				if (calib.calibrations) {
					Object.keys(calib.calibrations).map((param, index) => {
						params.push(param);
						param_calibs.push(calib.calibrations[param]);
						param_data_sending_configs.push((calib.data_sending_configs[param] == 'on') ? true : false);
						saved_params.push(param);
						saved_param_calibs.push(saved_calib.calibrations[param]);
						saved_param_data_sending_configs.push((saved_calib.data_sending_configs[param] == 'on') ? true : false);
					});
				}
				let saved_calibrations = {
					params: saved_params,
					param_calibs: saved_param_calibs,
					param_data_sending_configs: saved_param_data_sending_configs
				};
				this.setState({
					params: params,
					param_calibs: param_calibs,
					param_data_sending_configs: param_data_sending_configs,
					status: calib.status,
					device_qr_code: calib.device_qr_code,
					saved_calibrations: saved_calibrations
				});
				console.log('call to previous state of caliberation',saved_calibrations);
				if (this.state.allow_calibrations_success_msg) {
					showPopup('success', 'Configurations saved successfully!');
				} else {
					this.setState({
						allow_calibrations_success_msg: true
					});
				}
			}
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			console.log('update_location_details_in_dashboard', JSON.parse(payload));
			if (JSON.parse(payload).device_id == this.state.device_id) {
				this.setState(JSON.parse(payload));
			}
		});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {

		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 * @param  {object} prevProps This is the Previous saved props.
	 * @param  {object} prevState This is the Previous saved state.
	 */
	componentDidUpdate(prevProps, prevState) {
		if (this.state.saved_calibrations) {
			/*console.log('param calib', this.state.saved_calibrations.param_calibs);
			console.log('data sending', this.state.saved_calibrations.param_data_sending_configs);*/
			if (
				(_.isEqual(this.state.saved_calibrations.param_calibs, this.state.param_calibs) && 
				_.isEqual(this.state.saved_calibrations.param_data_sending_configs, this.state.param_data_sending_configs) && 
				_.isEqual(this.state.saved_calibrations.params, this.state.params)) || (this.state.enable_save == false)
			) {
				findDOMNode(this.refs.saveCalib).setAttribute("disabled", true);
			} else {
				findDOMNode(this.refs.saveCalib).removeAttribute("disabled");
			}
		}
		ReactTooltip.rebuild();
	}

	/**
	 * This renders the Device Configuration page.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container device-config-container">
				<NavLink active="configure" />
				<Scrollbars autoHide>
					<div className="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1 device-details-list">
						<h4 className="title-text">
							<Link to={'/device-config'} className="back-arrow pull-left">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 491.1 491.1" width="24" height="24"><path fill="#CCC" d="M111.85,208.25l192.8-192.8c20.6-20.6,54-20.6,74.6,0s20.6,54,0,74.6l-155.5,155.5l155.5,155.5c20.6,20.6,20.6,54,0,74.6 s-54,20.6-74.6,0l-192.8-192.8C91.25,262.25,91.25,228.85,111.85,208.25z"/></svg>
							</Link>
							<span className={'device-name' + ((this.state.status == "data_off") ? ' data-off' : ((this.state.status == "online") ? ' active' : ''))}>Device: {this.state.device_qr_code}</span>
						</h4>
						<hr className="separator" />
						<div className="tab-container">
							<Link to={'/device-config/' + this.state.device_id + '/details'} className="tab-item">Details</Link>
							<Link to={'/device-config/' + this.state.device_id + '/configuration'} className="tab-item">Configuration</Link>
							<Link to={'/device-config/' + this.state.device_id + '/calibration'} className="tab-item active">Calibration</Link>
						</div>
						<div className="panel panel-default">
							<div className="panel-body details-list">
								<h4 className="title-text">Calibration</h4>
								<div className="parameter-container">
									{(() => {
										if (this.state.params && this.state.params.length) {
											let parameters = this.state.parameters;

											if (!this.state.is_admin) {
												let delete_param = this.state.templates.template2.map((del_param, id) => {
													delete parameters[del_param];
												});
											}
											let calibrations = this.state.params.map((param, index) => {
												// console.log('Parameter '+index, param);
												return <div className={'config-item-border' + (!this.state.is_admin ? (this.state.templates.template2.includes(param) ? ' hide' : '') : '')} key={index}>
													<div className="form-config">
														<label>Calibration Type</label>
														<select className="form-control" value={param ? param : 0} onChange={(e) => this.changeCalibrationType(e.target.value, index)}>
															<option value={0} disabled>Calibration Type</option>
															{(() => {
																let params = Object.keys(parameters).map((key, ind) => {
																	return <option value={key} key={ind}>{this.state.parameters[key]}</option>;
																});
																return params;
															})()}
														</select>
													</div>
													{(() => {
														let calibration = [];
														if (this.state.templates.template1.indexOf(param) > -1) {
															calibration.push(<div className="form-config">
																<label>WEzero</label>
																<input type="text" className="form-control form-check" value={this.state.param_calibs[index].we_zero} placeholder="WEzero" onChange={(e) => this.changeCalibarationValues(index, 'we_zero', e.target)}/>
															</div>);
															calibration.push(<div className="form-config">
																<label>AUXzero</label>
																<input type="text" className="form-control form-check" value={this.state.param_calibs[index].aux_zero} placeholder="AUXzero" onChange={(e) => this.changeCalibarationValues(index, 'aux_zero', e.target)}/>
															</div>);
															calibration.push(<div className="form-config">
																<label>Sensitivity</label>
																<input type="text" className="form-control form-check" value={this.state.param_calibs[index].sensitivity} placeholder="Sensitivity" onChange={(e) => this.changeCalibarationValues(index, 'sensitivity', e.target)}/>
															</div>);
															{(() => {
																if (this.state.is_admin) {
																	calibration.push(<div className="form-config">
																		<label>Offset</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].offset} placeholder="Offset" onChange={(e) => this.changeCalibarationValues(index, 'offset', e.target)}/>
																	</div>);
																	calibration.push(<div className="form-config">
																		<label>Multiplier</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].multiplier} placeholder="Multiplier" onChange={(e) => this.changeCalibarationValues(index, 'multiplier', e.target)}/>
																	</div>);
																}
															})()}
															return calibration;
														} else if (this.state.templates.template2.indexOf(param) > -1) {
															{(() => {
																if (this.state.is_admin) {
																	calibration.push(<div className="form-config">
																		<label>Offset</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].offset} placeholder="Offset" onChange={(e) => this.changeCalibarationValues(index, 'offset', e.target)}/>
																	</div>);
																	calibration.push(<div className="form-config">
																		<label>Multiplier</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].multiplier} placeholder="Multiplier" onChange={(e) => this.changeCalibarationValues(index, 'multiplier', e.target)}/>
																	</div>);
																}
															})()}
															return calibration;
														} else if (this.state.templates.template3.indexOf(param) > -1) {
															{(() => {
																if (this.state.is_admin) {
																	calibration.push(<div className="form-config">
																		<label>Offset</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].offset} placeholder="Offset" onChange={(e) => this.changeCalibarationValues(index, 'offset', e.target)}/>
																	</div>);
																	calibration.push(<div className="form-config">
																		<label>Multiplier</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].multiplier} placeholder="Multiplier" onChange={(e) => this.changeCalibarationValues(index, 'multiplier', e.target)}/>
																	</div>);
																}
															})()}
															calibration.push(<div className="form-config">
																<label>O2 V1</label>
																<input type="text" className="form-control form-check" value={this.state.param_calibs[index].o2_v1} placeholder="O2 V1" onChange={(e) => this.changeCalibarationValues(index, 'o2_v1', e.target)}/>
															</div>);
															return calibration;
														} else if (this.state.templates.template4.indexOf(param) > -1) {
															{(() => {
																if (this.state.is_admin) {
																	calibration.push(<div className="form-config">
																		<label>Offset</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].offset} placeholder="Offset" onChange={(e) => this.changeCalibarationValues(index, 'offset', e.target)}/>
																	</div>);
																	calibration.push(<div className="form-config">
																		<label>Multiplier</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].multiplier} placeholder="Multiplier" onChange={(e) => this.changeCalibarationValues(index, 'multiplier', e.target)}/>
																	</div>);
																}
															})()}
															calibration.push(<div className="form-config">
																<label>UV Adcmin</label>
																<input type="text" className="form-control form-check" value={this.state.param_calibs[index].uv_adcmin} placeholder="UV Adcmin" onChange={(e) => this.changeCalibarationValues(index, 'uv_adcmin', e.target)}/>
															</div>);
															return calibration;
														} else if (this.state.templates.template5.indexOf(param) > -1) {
															{(() => {
																if (this.state.is_admin) {
																	calibration.push(<div className="form-config">
																		<label>Offset</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].offset} placeholder="Offset" onChange={(e) => this.changeCalibarationValues(index, 'offset', e.target)}/>
																	</div>);
																	calibration.push(<div className="form-config">
																		<label>Multiplier</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].multiplier} placeholder="Multiplier" onChange={(e) => this.changeCalibarationValues(index, 'multiplier', e.target)}/>
																	</div>);
																}
															})()}
															calibration.push(<div className="form-config">
																<label>NH3 V1</label>
																<input type="text" className="form-control form-check" value={this.state.param_calibs[index].nh3_v1} placeholder="NH3 V1" onChange={(e) => this.changeCalibarationValues(index, 'nh3_v1', e.target)}/>
															</div>);
															return calibration;
														} else if (this.state.templates.template6.indexOf(param) > -1) {
															{(() => {
																if (this.state.is_admin) {
																	calibration.push(<div className="form-config">
																		<label>Offset</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].offset} placeholder="Offset" onChange={(e) => this.changeCalibarationValues(index, 'offset', e.target)}/>
																	</div>);
																	calibration.push(<div className="form-config">
																		<label>Multiplier</label>
																		<input type="text" className="form-control form-check" value={this.state.param_calibs[index].multiplier} placeholder="Multiplier" onChange={(e) => this.changeCalibarationValues(index, 'multiplier', e.target)}/>
																	</div>);
																}
															})()}
															calibration.push(<div className="form-config">
																<label>zeroPoint</label>
																<select className="form-control form-check" value={parseInt(this.state.param_calibs[index].zeroPoint)} onChange={(e) => this.changeCalibarationValues(index, 'zeroPoint', e.target)}>
																	<option value={''} disabled>zeroPoint</option>
																	<option value={'0'}>0</option>
																	<option value={'1'}>1</option>
																</select>
															</div>);
															return calibration;
														}
														})()}
													{/*<div className="form-config">
														<label>Send Data</label>
														<div className="onoffswitch">
															<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id={'send_data_ ' + param + '_' + index} defaultChecked={this.state.param_data_sending_configs[index]} onClick={(e) => this.changeCalibarationDataSending(index, !this.state.param_data_sending_configs[index])}/>
															<label className="onoffswitch-label" htmlFor={'send_data_ ' + param + '_' + index}>
																<span className="onoffswitch-inner"></span>
																<span className="onoffswitch-switch"></span>
															</label>
														</div>
													</div>*/}
													<button className="btnn btn-sm btn-right red-fill-btn remove-btn" onClick={() => this.removeCalibarationRow(index)}>✖</button>
												</div>;
											});
											return calibrations;
										} else if (this.state.params && (this.state.params.length == 0)) {
											return <center className="panel-body details-list text-center">No Calibration settings found for device.</center>;
										} else {
											return <div className="panel-body details-list text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
										}
									})()}
									<button className="btnn btn-left btn-sm btn-xs green-fill-btn add-new" onClick={() => this.addNewCalibarationRow()}>Add New</button>
								</div>
								{(() => {
									if (!this.state.data_loading) {
										return <button className="btn-right green-fill-btn btn-width" onClick={() => this.saveDeviceCalibration()} ref="saveCalib">Save</button>;
									} else {
										return <button className="btn-right green-fill-btn btn-width invisible">Save</button>;
									}
								})()}
							</div>
						</div>
					</div>
					<ReactTooltip effect="solid" />
				</Scrollbars>
				{/*(() => {
					if ((this.city_name === 'admin') || (this.city_name === '127')) {
					} else {
						return <h3><center>404 - Not Found</center></h3>;
					}
				})()*/}
			</div>
		);
	}
}

DeviceConfigCalibration.contextTypes = {
	router: React.PropTypes.object
};