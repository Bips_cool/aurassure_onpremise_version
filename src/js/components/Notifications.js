import React from 'react';
import NavLink from './NavLink';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment-timezone';

export default class Notifications extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			mob_notifications: false,
			data_loading: 1,
			notification_details: null,
			active_station: {}
		};
		this.getNotificationDetails();
	}

	getNotificationDetails(){
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_notifications_new', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('Notifications:', json);
			if(json.status === 'success') {
				that.setState({
					notification_details : json.notifications,
					data_loading: null
				});
			} else {
				showPopup('danger',json.message);
				that.setState({data_loading: null});
			}
		});
	}

	componentDidMount() {
		document.title = 'Notifications -  DATOMS® Pollution Monitoring';
	}

	render() {
		return <div className="full-page-container">
			<NavLink activeLink={'notifications'} />
			<div id="notifications">
				<div className="notifications-headers">
					<div className="blank-space">
					</div>
					<div className="notifications-head">
						<div className="notifications-title"> Notifications </div>
					</div>
				</div>
				<div className="notifications-body">
					<div className="notifications-details">
						<Scrollbars autoHide>
							<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 alert-details-table">
								{(() => {
									if (this.state.notification_details == null) {
										return <div className="loading">
											<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
										</div>;											
									} else if (this.state.notification_details && !this.state.notification_details.length) {
										return <div className="loading">
											<center>No Notifications Found</center>
										</div>;
									} else {
										let notification_lists = this.state.notification_details.map((elem, ind) => {
											//console.log('elem',elem);
											return <div className="row notifications-body">
												<div className={'col-lg-12 col-md-12 col-sm-12 col-xs-12'}>
													<div className="flex">
														<div className="station-details" >
															<div className={'icon-circle ' + (elem.type == 'very poor' ? 'very-poor' : elem.type)} />
															<span className="notification-text" dangerouslySetInnerHTML={{__html: elem.text}}/>
															{/*<span className={'open-mark' + (this.state.mob_notifications ? ' open' : '')} onClick={() => this.setState({mob_notifications: !this.state.mob_notifications})} />*/}
														</div>
													</div>
													<small className="padding-left">{moment.unix(elem.time).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')}</small>
												</div>
												{/*(() => {
													if (elem.type != 'offline') {
														return <div className={'col-lg-8 col-md-8 col-sm-8 col-xs-8 display-type' + (this.state.mob_notifications ? ' activate' : '')}>
															<div className="flex padding-left-flex">
																<div className="notification-details width-devide">
																	<div className="parameter-full">{elem.details ? elem.details.param_long_name : ''}</div>
																	<small><span dangerouslySetInnerHTML={{__html: elem.details ? elem.details.param_name : ''}}/></small>
																</div>
																<div className="notification-threshold width-devide">
																	<div className="parameter-unit"><span dangerouslySetInnerHTML={{__html: elem.details ? elem.details.param_value : ''}}/></div>
																	<small>Against Pres. Stand <span dangerouslySetInnerHTML={{__html: elem.details ? elem.details.param_threshold : ''}}/></small>
																</div>
															</div>
														</div>;
													}
												})()*/}
											</div>;
										});
										return notification_lists;
									}
								})()}
							</div>
						</Scrollbars>
					</div>
				</div>
			</div>
		</div>;
	}
}

Notifications.contextTypes = {
	router: React.PropTypes.object
};
