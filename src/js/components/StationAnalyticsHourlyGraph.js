import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import ReactHighcharts from 'react-highcharts';
import { Scrollbars } from 'react-custom-scrollbars';

/**
 * This Class Used for Station analytics hourly graph.
 */
export default class StationAnalyticsHourlyGraph extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {Object} props
	 */
	constructor(props) {
		super();
		let parameter = '',
			time = ''
		if (props.location.search && props.location.search.split('?param=')[1] && props.location.search.split('&time=')[1]) {
			parameter = props.location.search.split('?param=')[1].split('&time=')[0];
			time = props.location.search.split('&time=')[1];
		}
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			station_data: null,
			source: 'Aurassure',
			current_param: parameter,
			station_id: props.params.station_id,
			time: parseInt(moment(time,'DD-MM-YYYY,hh:mm:ss').tz('Asia/Kolkata').format('X'))
		};

		saveStationId(props.params.station_id);
		this.fetchStationData(props.params.station_id);
	}

	/**
	 * This function is used to fetch data for snapshot.
	 * @param  {Number} id This is the current station id.
	 */
	fetchStationData(id) {
		saveStationId(id);
		var that = this;
		that.setState({station_data: null});
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_data_for_snapshot.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: id,
				time: this.state.time
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			if (json.status === 'success') {
				that.setState({
					station_id: id,
					station_data: (json.param_values) ? json : null
				});
				console.log('Data', that.state.station_data);
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function is used for current parameter name.
	 * @param  {String} parameter This is the current parameter.
	 * @return {String}           Parameter Name.
	 */
	currentParameter(parameter) {
		let param_name = null;
		this.state.station_data.params.map((param) => {
			if (param.key == parameter) {
				param_name = param.name;
			}
		});
		return param_name;
	}

	/**
	 * This function is used to change current parameter.
	 * @param  {String} param This is the parameter.
	 */
	change_param(param) {
		console.log('Changing Param to: ', param);
		this.setState({current_param: param});
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 * @param  {object} prevProps This is the Previous saved props.
	 * @param  {object} prevState This is the Previous saved state.
	 */
	componentDidUpdate(prevProps, prevState) {
		console.log(prevProps, prevState);
		let parameter = '',
			time = '';
		if (this.props.location.search && this.props.location.search.split('?param=')[1] && this.props.location.search.split('&time=')[1]) {
			parameter = this.props.location.search.split('?param=')[1].split('&time=')[0];
			time = parseInt(moment(this.props.location.search.split('&time=')[1],'DD-MM-YYYY,HH:mm:ss').tz('Asia/Kolkata').format('X'));
		}
		if (prevState.current_param != parameter || prevState.time != time) {
			this.setState({
				current_param: parameter,
				time: time
			});
			console.log ('parameter', parameter);
			console.log ('time', time);
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Analytics - Aurassure';
	}

	/**
	 * This renders the Hourly Station Detailed Analytics.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container">
				{(() => {
					if (this.state.station_data === null) {
						return <div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
					} else {
						return <div id="analyticsContainer" className="margin-top">
							{(() => {
								let line_chart_data = [];
								let unit = '';
								let graph_colors = [
									'#43429a',
									'#07adb1',
									'#a44a9c',
									'#f4801f',
									'#c14040',
									'#6fccdd',
									'#61c3ab',
									'#56bc7b',
									'#e2da3e',
									'#41ce00',
									'#aa4728',
									'#b3d23c',
									'#a0632a',
									'#7156a3',
									'#3d577f',
									'#ee3352',
									'#43429a',
									'#07adb1',
									'#a44a9c',
									'#f4801f',
									'#c14040',
									'#6fccdd',
									'#61c3ab',
									'#56bc7b'
								];
								let graph_fill_color = [
									'rgba(67, 66, 154, 0.05)',
									'rgba(7, 173, 177, 0.05)',
									'rgba(164, 74, 156, 0.05)',
									'rgba(244, 128, 31, 0.05)',
									'rgba(193, 64, 64, 0.05)',
									'rgba(111, 204, 221, 0.05)',
									'rgba(97, 195, 171, 0.05)',
									'rgba(86, 188, 123, 0.05)',
									'rgba(226, 218, 62, 0.05)',
									'rgba(65, 206, 0, 0.05)',
									'rgba(170, 71, 40, 0.05)',
									'rgba(179, 210, 60, 0.05)',
									'rgba(160, 99, 42, 0.05)',
									'rgba(113, 86, 163, 0.05)',
									'rgba(61, 87, 127, 0.05)',
									'rgba(238, 51, 82, 0.05)',
									'rgba(67, 66, 154, 0.05)',
									'rgba(7, 173, 177, 0.05)',
									'rgba(164, 74, 156, 0.05)',
									'rgba(244, 128, 31, 0.05)',
									'rgba(193, 64, 64, 0.05)',
									'rgba(111, 204, 221, 0.05)',
									'rgba(97, 195, 171, 0.05)',
									'rgba(86, 188, 123, 0.05)'
								];
								let i;

								if (this.state.station_data && this.state.current_param && this.state.station_data.param_values[this.state.current_param]) {
									this.state.station_data.param_values[this.state.current_param].map((value, index) => {
										line_chart_data.push([
											this.state.station_data.time_stamps[index] * 1000,
											parseFloat(value)
										]);
									});

									this.state.station_data.params.map((parameter, index) => {
										if (this.state.current_param === parameter.key) {
											unit = this.state.station_data.param_units[index];
											//set current param unit
											/**
											 * This is the current parameter's unit.
											 * @type {String}
											 */
											this.current_param_unit = unit;
											i = index;
										}
									});
								}

								ReactHighcharts.Highcharts.setOptions({
									global: {
										useUTC: false
									}
								});

								let individual_graph_data = [];
								let current_time = moment().unix();
								let previous_time = current_time - 3600;
								let categories = [];
								var breakpoint_table = {
									'pm2.5': [ 
										[0, 30],
										[31, 60],
										[61, 90],
										[91, 120],
										[121, 250],
										[251, 380]
									],
									'pm10': [
										[0, 50],
										[51, 100],
										[101, 250],
										[251, 350],
										[351, 430],
										[431, 510]
									],
									'no2': [
										[0, 40],
										[41, 80],
										[81, 180],
										[181, 280],
										[281, 400],
										[401, 500]
									],
									'o3': [
										[0, 50],
										[51, 100],
										[101, 168],
										[169, 208],
										[209, 748],
										[749, 800]
									],
									'co': [
										[0, 1.0],
										[1.1, 2.0],
										[2.1, 10],
										[10.1, 17],
										[17.1, 34],
										[34.1, 50]
									],
									'so2': [
										[0, 40],
										[41, 80],
										[81, 380],
										[381, 800],
										[801, 1600],
										[1601, 2000]
									],
									'nh3': [
										[0, 200],
										[201, 400],
										[401, 800],
										[801, 1200],
										[1201, 1800],
										[1801, 2000]
									],
									'pb': [
										[0, 0.5],
										[0.6, 1.0],
										[1.1, 2.0],
										[2.1, 3.0],
										[3.1, 3.5],
										[3.6, 4.0]
									]
								};

								if (this.state.station_data) {
									this.state.station_data.hourly_param_concs[this.state.current_param].map((hourly_conc, i) => {
										if (hourly_conc === null) {
											hourly_conc = 0;
										}

										let color = window.color_code.color_1;
										if (breakpoint_table[this.state.current_param] !== undefined) {
											let factor_usepa_to_naaqi;
											let decimal_upto;
											switch(this.state.current_param) {
												case 'no2':
													factor_usepa_to_naaqi = 46.0055/24.45;
													break;
												case 'o3':
													factor_usepa_to_naaqi = 48/24.45;
													break;
												case 'co':
													factor_usepa_to_naaqi = 28.01/24.45;
													break;
												case 'so2':
													factor_usepa_to_naaqi = 64.066/24.45;
													break;
												default:
													factor_usepa_to_naaqi = 1;
													break;
											}

											if (this.state.current_param == 'co' || this.state.current_param == 'pb') {
												decimal_upto = 1;
											} else {
												decimal_upto = 0
											}

											if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][5][0]) {
												color = window.color_code.color_6;
											} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][4][0]) {
												color = window.color_code.color_5;
											} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][3][0]) {
												color = window.color_code.color_4;
											} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][2][0]) {
												color = window.color_code.color_3;
											} else if((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][1][0]) {
												color = window.color_code.color_2;
											} else if ((hourly_conc * factor_usepa_to_naaqi).toFixed(decimal_upto) >= breakpoint_table[this.state.current_param][0][0]) {
												color = window.color_code.color_1;
											}
										}

										individual_graph_data.push({
											y: parseFloat(hourly_conc.toFixed(2)),
											color: color
										});

										categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));		
									});
									categories.reverse();
								}
								individual_graph_data.reverse();

								/**
								 * This is a Object to plot Individual graph data.
								 * @type {Object}
								 * @property {Object} chart This sets chart properties.
								 * @property {Object} plotOptions This customize the graph plot settings/options.
								 * @property {Object} title This sets the title for the graph.
								 * @property {Object} subtitle This sets the subtitle for the graph.
								 * @property {Object} xAxis This defines what to show in xAxis.
								 * @property {Object} yAxis This defines what to show in yAxis.
								 * @property {Object} legend This hides the printing options from graph.
								 * @property {Object} tooltip This sets what to show in tooltip.
								 * @property {Array} series This contains data for ploting graph.
								 */
								this.individual_graph_config = {
									chart: {
										type: 'column',
										height: 270
									},
									plotOptions: {
										series: {
											pointPadding: 0,
											groupPadding: 0
										}
										
									},
									title: {
										text: '',
									},
									subtitle: {
										text: ''
									},
									xAxis: {
										title: {
											enabled: true,
											text: 'Time',
											style: {
												fontWeight: 'normal'
											}
										},floor: 0,
										type: '',
										lineWidth: 1,
										minorGridLineWidth: 0,
										lineColor: '#ddd',
										labels: {
											enabled: false
										},
										minorTickLength: 0,
										tickLength: 0,
										categories: categories
									},
									yAxis: {
										title: {
											text: this.state.current_param === 'temperature' || this.state.current_param === 'humidity' || this.state.current_param === 'noise' ? 'Value ' : 'Conc. '
										},
										lineWidth: 1,
										minorGridLineWidth: 0,
										lineColor: '#ddd',      
										labels: {
											enabled: false
										},
										minorTickLength: 0,
										tickLength: 0,
										gridLineColor: 'transparent'

									},
									legend: {
										enabled: false
									},
									tooltip: {
										pointFormat: '<span style="color:{point.color}">' + (this.currentParameter(this.state.current_param)) + ' Conc.: ' + '<b>{point.y}</b> ' + unit + '<br/>',
										useHTML: true
									},
									series: [{
										data: individual_graph_data
									}]
								};

								/**
								 * This sets the graph config to blank array.
								 * @type {Array}
								 */
								this.individual_param_aqi_graph_config = [];
								let should_plot_individual_param_aqi = [];
								Object.keys(this.state.station_data.hourly_param_aqis).map(function(key, index) {
									let bar_chart = [];
									bar_chart[index] = [];
									should_plot_individual_param_aqi[index] = false;
									this.state.station_data.hourly_param_aqis[key].map((value) => {
										let color = window.color_code.color_1;
										if(value >= 401) {
											color = window.color_code.color_6;
										} else if(value >= 301) {
											color = window.color_code.color_5;
										} else if(value >= 201) {
											color = window.color_code.color_4;
										} else if(value >= 101) {
											color = window.color_code.color_3;
										} else if(value >= 51) {
											color = window.color_code.color_2;
										}
										bar_chart[index].push({
											y: parseInt(value),
											color: color
										});
										if(value) {
											should_plot_individual_param_aqi[index] = true;
										}
									});
									bar_chart.map((data, i) => {
										bar_chart[i].reverse();
									});

									/**
									 * This is a Object to plot Individual parameter aqi graph data.
									 * @type {Object}
									 * @property {Object} chart This sets chart properties.
									 * @property {Object} plotOptions This customize the graph plot settings/options.
									 * @property {Object} title This sets the title for the graph.
									 * @property {Object} subtitle This sets the subtitle for the graph.
									 * @property {Object} xAxis This defines what to show in xAxis.
									 * @property {Object} yAxis This defines what to show in yAxis.
									 * @property {Object} legend This hides the printing options from graph.
									 * @property {Object} tooltip This sets what to show in tooltip.
									 * @property {Array} series This contains data for ploting graph.
									 */
									this.individual_param_aqi_graph_config[index] = {
										chart: {
											type: 'column',
											height: 65,
											width: 180
										},
										plotOptions: {
											series: {
												pointPadding: 0,
												groupPadding: 0
											}				
										},
										title: {
											text: ''
										},
										subtitle: {
											text: ''
										},
										xAxis: {
											title: {
												enabled: true,
												text: '',
												style: {
													fontWeight: 'normal'
												}
											},
											type: '',
											lineWidth: 0,
											minorGridLineWidth: 0,
											lineColor: 'transparent',      
											labels: {
												enabled: false
											},
											minorTickLength: 0,
											tickLength: 0,
											categories: categories
										},
										yAxis: {
											title: {
												text: ''
											},
											lineWidth: 0,
											minorGridLineWidth: 0,
											lineColor: 'transparent',      
											labels: {
												enabled: false
											},
											minorTickLength: 0,
											tickLength: 0,
											gridLineColor: 'transparent'

										},
										legend: {
											enabled: false
										},
										tooltip: {
											pointFormat: '<span style="color:{point.color}">Index <b>{point.y}</b>'
										},
										series: [{
											data: bar_chart[index]
										}]
									};
								}, this);

								let graph_data = [];
								if (this.state.station_data) {
									this.state.station_data.hourly_aqis.map((hourly_aqi) => {
										let color = window.color_code.color_1;
										if(hourly_aqi >= 401) {
											color = window.color_code.color_6;
										} else if(hourly_aqi >= 301) {
											color = window.color_code.color_5;
										} else if(hourly_aqi >= 201) {
											color = window.color_code.color_4;
										} else if(hourly_aqi >= 101) {
											color = window.color_code.color_3;
										} else if(hourly_aqi >= 51) {
											color = window.color_code.color_2;
										}
										graph_data.push({
											y: parseInt(hourly_aqi),
											color: color
										});
									});
								}
								graph_data.reverse();

								var suggestions = [];
								if(this.state.station_data.suggestions) {
									this.state.station_data.suggestions.map(function(suggestion, i) {
										suggestions.push(
											<div className="panel-item" key={i}>
												<div className={'icon suggestion-' + (i+1)}></div>
												<div className="panel-text">{suggestion}</div>
											</div>
										);
									}, this);
								} else {
									suggestions.push(<li className="hourly-aqi-na-error">NA</li>);
								}

								return(
									<span>
										<div id="middleAnalyticsPanel" className="width-full">
											<Scrollbars autoHide>
												{(() => {
													if (this.state.station_data.param_values.length !== 0) {
														let allowed_parameters_in_order = ['pm1', 'pm2.5', 'pm10', 'so2', 'no', 'no2', 'co', 'o3', 'co2', 'o2', 'temperature', 'humidity', 'rain', 'uv', 'light', 'noise', 'voc', 'lint', 'nox', 'ch4', 'lead', 'wspeed', 'press', 'no3', 'ph', 'do', 'nh4', 'orp', 'us_mb', 'cur', 'nh3', 'us_hc04', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'];
														let current_params = {};
														this.state.station_data.params.map((param) => {
															current_params[param.key] = param;
														});
														let parameters = allowed_parameters_in_order.map((param, i) => {
															if(Object.keys(current_params).indexOf(param) >= 0) {
																return(
																	<div className={'parameter-btn hellip' + ((param === this.state.current_param)? ' active': '')} dangerouslySetInnerHTML={{__html: current_params[param].name}} onClick={() => {this.change_param(param);}} key={i} />
																);
															}
														});
														return(
															<div>
																<div className="avg-report-hourly">
																	<ReactHighcharts ref="avg_chart" config={this.individual_graph_config} />
																</div>
															</div>
														);
													} else {
														return(<div className="flex no-data-text full-height">
															<center className="table">No data received in last 24 hours.</center>
														</div>);
													}
												})()}
											</Scrollbars>
										</div>
									</span>
								);
							})()}
						</div>;
					}
				})()}
			</div>
		);
	}
}

StationAnalyticsHourlyGraph.contextTypes = {
	router: React.PropTypes.object
};
