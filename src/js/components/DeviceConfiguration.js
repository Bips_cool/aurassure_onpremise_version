import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import ReactTooltip from 'react-tooltip';
import { arrayMove } from 'react-sortable-hoc';
import { Scrollbars } from 'react-custom-scrollbars';
import Link from 'react-router/Link';
import io from 'socket.io-client';
import ConfigFilter from './imports/ConfigFilter';
import _ from 'lodash';

/**
 * This Class Used for showing Device configuration status of devices.
 */
export default class DeviceConfiguration extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} params
	 */
	constructor(params) {
		super();
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			device_details: null,
			status: null,
			id: null,
			adding_status: null,
			city_id: 'all',
			city_id_exclude: ['all', 'unconfigured', 'no_data_received'],
			search_value: '',
			modal_view: false,
			modal_sync_view: false,
			sync_index: null,
			sorted: false,
			sort_keys: null,
			date_time_online: false,
			filter_keys: {
				city_id: '0',
				connection: '0',
				sync: '0',
				health: '0',
				data_sending: '0',
				unconfigured: '0',//false
				device_archive_status: 'active',
				no_data_received: '0',//false
				publicly_accessible: '0'
			},
			health_status: {
				0: 'Any',
				1: 'OK',
				2: 'Power Failure',
				3: 'Code Restart',
				4: 'Modem Restart',
				5: 'Other Error',
				6: 'Battery Voltage Down',
				7: 'Network Signal Low',
				8: 'Debug Data Not Sent'
			},
			modem_types: {
				1: 'GPRS',
				2: 'WIFI',
				3: 'Ethernet'
			}
		};

		/**
		 * This is used for City name.
		 * @type {string}
		 */
		this.city_name = window.location.host.split('.')[0];
		this.device_name = '';
	}

	/**
	 * This function used to update filter options
	 * @param  {object} options This is the filterd options.
	 */
	updateFilter(options) {
		if (options) {
			this.setState({filter_keys: options}, () => this.getFilteredStations(this.state.search_value));
		} else {
			this.getFilteredStations(this.state.search_value);
		}
		// console.log('options', options);
	}

	/**
	 * This function is to get get filterd stations after search.
	 * @param  {string} search_value This is the search input value.
	 */
	getFilteredStations(search_value) {
		// console.log('getFilteredStations');
		if (this.state.device_details && this.state.device_details.length) {
			let device_list = [], current_time = moment().unix(), filtered_stations_list = [];
			this.state.device_details.map((config, index) => {
				device_list.push(config);
			});
			search_value = search_value.toLowerCase();
			// console.log('Device List', device_list);
			// console.log(this.state);
			this.state.device_details.map((config, index) => {
				// City ID
				if (
					!(
						(this.state.filter_keys.city_id === '0') ||
						(config.city_id.toString() === this.state.filter_keys.city_id)
					)
				) {
					// device_list.push(config);
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Connection Status
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.connection === '0') ||
						(
							(config.status === this.state.filter_keys.connection) ||
							((config.status === 'data_off') && (this.state.filter_keys.connection === 'online'))
						)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Sync Status
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.sync === '0') ||
						(config.device_sync_status === this.state.filter_keys.sync)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Health Status
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.health === '0') ||
						(config.device_error_status.toString() === this.state.filter_keys.health)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Data Sending Status
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.data_sending === '0') ||
						(config.data_sending_status === 'wait') ||
						(config.data_sending_status === this.state.filter_keys.data_sending)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Publicly Accessible Status
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.publicly_accessible === '0') ||
						(config.publicly_accessible === this.state.filter_keys.publicly_accessible)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Archived Devices
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.device_archive_status === '0') ||
						(config.device_archive_status === this.state.filter_keys.device_archive_status)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Unconfigured
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.unconfigured === '0') ||
						(config.device_configured_properly === this.state.filter_keys.unconfigured)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// No Data Received
				if (
					_.some(device_list, config) &&
					!(
						(this.state.filter_keys.no_data_received === '0') ||
						(config.data_received_from_device === this.state.filter_keys.no_data_received)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
				// Only online devices
				if (this.state.date_time_online) {
					let less_fifteen_offline = current_time -  config.device_last_online_timestamp,
					fifteen_minute = 900;
					if (_.some(device_list, config) && (less_fifteen_offline >= fifteen_minute)) {
						device_list.splice(device_list.indexOf(config), 1);
					}
					console.log('date_time_online_device_list', device_list);
				}
				// Text Search
				let data_string = config.station_id + ' ' +
								config.name + ' ' +
								this.state.device_cities[config.city_id] + ' ' +
								config.device_firmware_version;
				// console.log('Search', data_string.toLowerCase());
				if (
					_.some(device_list, config) &&
					!(
						(search_value == '') ||
						(data_string.toLowerCase().search(search_value) > -1)
					)
				) {
					device_list.splice(device_list.indexOf(config), 1);
				}
			});
			
			//Apply Sorting
			device_list = device_list.filter(Boolean);
			if (this.state.sort_keys) {
				if (this.state.sorted && this.state.sort_keys === 'id') {
					if (this.state.sort_keys === 'id') {
						device_list.sort(function(a, b) {
							return a.station_id - b.station_id;
						});
						// console.log('process started1', device_list);
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				} else {
					if (this.state.sort_keys === 'id') {
						device_list.sort(function(a, b) {
							return b.station_id - a.station_id;
						});
						// console.log('process started1', device_list);
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				}
				if (this.state.sorted && this.state.sort_keys === 'name') {
					if (this.state.sort_keys === 'name') {
						device_list.sort(function(a, b) {
							let nameA = a.name.toUpperCase();
							let nameB = b.name.toUpperCase();
							if (nameA < nameB) {
								return -1;
							}
							if (nameA > nameB) {
								return 1;
							}
							return 0;
						});
						// console.log('process started1', device_list);
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});	
					}
				} else {
					if (this.state.sort_keys === 'name') {
						device_list.sort(function(a, b) {
							let nameA = a.name.toUpperCase();
							let nameB = b.name.toUpperCase();
							if (nameA > nameB) {
								return -1;
							}
							if (nameA < nameB) {
								return 1;
							}
							return 0;
						});
						// console.log('process started1', device_list);
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});	
					}
				}
				if (this.state.sorted && this.state.sort_keys === 'city') {
					if (this.state.sort_keys === 'city') {
						let that = this;
						device_list.sort(function(a, b) {
							let nameA = that.state.device_cities[a.city_id].toUpperCase();
							let nameB = that.state.device_cities[b.city_id].toUpperCase();
							if (nameA < nameB) {
								return -1;
							}
							if (nameA > nameB) {
								return 1;
							}
							return 0;
						});
						// console.log('process started1', device_list);	
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				} else {
					if (this.state.sort_keys === 'city') {
						let that = this;
						device_list.sort(function(a, b) {
							let nameA = that.state.device_cities[a.city_id].toUpperCase();
							let nameB = that.state.device_cities[b.city_id].toUpperCase();
							if (nameA > nameB) {
								return -1;
							}
							if (nameA < nameB) {
								return 1;
							}
							return 0;
						});
						// console.log('process started1', device_list);	
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				}
				if (this.state.sorted && this.state.sort_keys === 'firmware') {
					if (this.state.sort_keys === 'firmware') {
						device_list.sort(function(a, b) {
							let nameA = a.device_firmware_version;
							let nameB = b.device_firmware_version;
							if (nameA < nameB) {
								return -1;
							}
							if (nameA > nameB) {
								return 1;
							}
							return 0;
						});
						// console.log('process started1', device_list);	
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				} else {
					if (this.state.sort_keys === 'firmware') {
						device_list.sort(function(a, b) {
							let nameA = a.device_firmware_version;
							let nameB = b.device_firmware_version;
							if (nameA > nameB) {
								return -1;
							}
							if (nameA < nameB) {
								return 1;
							}
							return 0;
						});
						// console.log('process started1', device_list);	
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				}
				if (this.state.sorted && this.state.sort_keys === 'active') {
					if (this.state.sort_keys === 'active') {
						device_list.sort(function(a, b) {
							return a.device_last_data_receive_timestamp - b.device_last_data_receive_timestamp;
						});
						// console.log('process started1', device_list);	
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				} else {
					if (this.state.sort_keys === 'active') {
						device_list.sort(function(a, b) {
							return b.device_last_data_receive_timestamp - a.device_last_data_receive_timestamp;
						});
						// console.log('process started1', device_list);	
						device_list.map((config) => {
							filtered_stations_list.push(config);
							// console.log('process started2', config);
						});
					}
				}
			} else {
				filtered_stations_list = device_list;
			}
			this.setState({
				filtered_stations: filtered_stations_list,
				search_value: search_value
			});
			// console.log('Filtered Devices:', device_list.filter(Boolean));
			// return device_list.filter(Boolean);
		}
	}

	/**
	 * This function is to seach the input value.
	 * @param  {string} value This is the input value of search field.
	 */
	searchBoxFilter(value) {
		this.getFilteredStations(value.trim());
	}

	/**
	 * This Changes the data toggle for sending data status.
	 * @param  {number} id                  This is Device id
	 * @param  {boolean} data_sending_status This sets the value to true or false.
	 */
	changeDeviceStatus(id,data_sending_status) {
		// console.log('Device ID', id);
		// console.log('Device Switch Status', data_sending_status);
		if (this.state.device_details) {
			let devices_updated_status = this.state.device_details;
			devices_updated_status.map((value,key) => {
				if (value && id == value.id) {
					devices_updated_status[key]['data_sending_status'] = data_sending_status;
				}
			});
			this.setState({device_details: devices_updated_status});
		}
		this.socket.emit('change_device_data_sending_status', 
			JSON.stringify({
				device_id: id,
				data_sending_status: data_sending_status
			})
		);
	}

	/**
	 * This function is to add a new device.
	 */
	addNewDevice() {
		this.setState({modal_view: false});
		this.socket.emit('add_new_station', JSON.stringify({
			city_id: this.state.city_id
		}));
		// console.log(this.state.city_id, this.state.device_cities[this.state.city_id]);
	}

	/**
	 * This function used to sync or not to sync device settings.
	 */
	deviceSyncingStatus() {
		this.setState({modal_sync_view: false});
		if (
			_.find(this.state.device_details, { 'id': this.state.sync_device_id}).device_sync_status === 'in_sync' || _.find(this.state.device_details, { 'id': this.state.sync_device_id}).device_sync_status === 'not_in_sync'
			) {
			this.socket.emit('sync_settings_to_device', JSON.stringify({
				device_id: this.state.sync_device_id
			}));
			this.setState({sync_index: null});
		} else {
			this.socket.emit('stop_syncing_settings_to_device', JSON.stringify({
				device_id: this.state.sync_device_id
			}));
			this.setState({sync_index: null});
		}
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Device Configuration - Aurassure';

		/**
		 * Socket is used to sync the data between client and server in real time.
		 */
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
			query: {
				s_token: this.readCookie('PHPSESSID')
			}
		});
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket');
			// console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			this.socket.emit('get_list_of_devices', JSON.stringify({
				city_id: document.getElementById('city_id') ? document.getElementById('city_id').value : 5
			}));
		});

		this.socket.on('update_list_of_devices', (payload) => {
			let data = JSON.parse(payload);
			this.setState({
				device_details: data.devices,
				device_cities: data.cities
			}, () => this.getFilteredStations(this.state.search_value));
			// console.log('update_list_of_devices', this.state.device_details);
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			let data_status = JSON.parse(payload);
			if (this.state.device_details) {
				let status_change = this.state.device_details;
				status_change.map((stat,index) => {
					if (stat && data_status.device_id == stat.id) {
						Object.keys(data_status).map((key) => {
							if (key != 'device_id') {
								status_change[index][key] = data_status[key];
							}
						});
					}
				});
				this.setState({device_details: status_change}, () => this.updateFilter());
			}
			// console.log('Dashboard successfully connected to socket.');
			// console.log('payload', data_status);
		});

		this.socket.on('new_station_added_successfully', (payload) => {
			let add_new_device = JSON.parse(payload);
			let device_details = this.state.device_details;
			device_details.push(add_new_device);
			this.setState({
				device_details: device_details
			}, () => this.getFilteredStations(this.state.search_value));
			// console.log('new_station_added_successfully', add_new_device);
			showPopup('success', 'New device added successfully!');
		});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {
		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	sortTableName(devices_table) {
		this.setState({sorted: !this.state.sorted}, () => this.updateFilter());
	}

	toggleShowOnlineDevices() {
		this.setState({date_time_online: !this.state.date_time_online}, () => this.updateFilter());
	}
	/**
	 * This function is used to render device rows.
	 * @param  {object} config In this object the device details stored.
	 */
	getDeviceRow(config) {
		// console.log('config.name', config.name);
		// if (this.state.devices_list_name) {
		// 	this.state.devices_list_name.map((config,index) => {
		// 		return config.name[name];
		// 	});
		// }
		let current_time = moment().unix(),
			less_fifteen_online = current_time - config.device_last_data_receive_timestamp,
			less_fifteen_offline = current_time - config.device_last_online_timestamp,
			fifteen_minute = 900,
			online_icon_class = '';
		if(less_fifteen_offline <= fifteen_minute) {
			if(less_fifteen_online <= fifteen_minute) {
				online_icon_class = ' active';
			} else {
				online_icon_class = ' data-off';
			}
		}
		let device_last_online_timestamp = (config.device_last_online_timestamp ? moment.unix(config.device_last_online_timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss') : 'Never'),
			device_last_data_receive_timestamp = (config.device_last_data_receive_timestamp ? moment.unix(config.device_last_data_receive_timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss') : 'Never');
		return <tr>
			<td>{config.station_id}</td>
			<td className="hellip">{config.name ? config.name : '-'}</td>
			<td className="hellip">{this.state.device_cities[config.city_id]}</td>
			<td className="text-center">{config.device_firmware_version ? config.device_firmware_version : '-'}</td>
			<td>
				<div className={'center device-status' + online_icon_class} data-tip={(device_last_online_timestamp == 'Never' ? 'Device never connected' : (device_last_data_receive_timestamp == 'Never' ? 'No data received till date' : 'Last data received at ' + device_last_data_receive_timestamp) + ' , Last active at ' + device_last_online_timestamp)}>
					<div className="circle center" />
					{device_last_data_receive_timestamp}
				</div>
			</td>
			<td className="text-center open-popup" onClick={(e) => this.setState({modal_sync_view: true, sync_device_id: config.id})}>
				{(() => {
					switch (config.device_sync_status) {
						case 'in_sync':
							return <div className="sync-status" data-tip="In Sync">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="green" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31-132.6 78-7.8-3.8-16.4-5.7-25.5-5.7-32.8 0-59.8 24.4-64 56C47 152.3 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="green" d="M374.7 144.5L353.2 123 231 245.2l-38.5-38.5-21 21 50.6 51 .8-.2 9.2 9.3"/></svg>
							</div>;
							break;
						case 'not_in_sync':
							return <div className="sync-status" data-tip="Device not in sync">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="red" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="red" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm83.6 52.2h21v-21h-21zM363.8 124H301v62.7l23-23c11.4 10.4 18.7 26 18.7 43.8 0 27.2-17.8 50-41.8 59.5v22c35.2-9.4 62.3-41.8 62.3-80.4 0-23-9.4-44-25-58.5zM259 218h21v-62.7h-21z"/></svg>
							</div>;
							break;
						case 'syncing':
							return <div className="sync-status" data-tip="Syncing...">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="#FF830D" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="#FF830D" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm188-83.5h-62.7v62.7l23-23c11.5 10.4 18.8 26 18.8 43.8 0 27.2-17.8 50-41.8 59.5v22c35.5-9.4 62.6-41.8 62.6-80.4 0-23-9.4-44-25-58.5z"/></svg>
							</div>;
							break;
						case 'waiting_for_device':
							return <div className="sync-status" data-tip="Waiting for device to connect...">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="#800080" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="#800080" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm188-83.5h-62.7v62.7l23-23c11.5 10.4 18.8 26 18.8 43.8 0 27.2-17.8 50-41.8 59.5v22c35.5-9.4 62.6-41.8 62.6-80.4 0-23-9.4-44-25-58.5z"/></svg>
							</div>;
							break;
					}
				})()}
			</td>
			<td className="text-center">
				{(() => {
					switch (config.device_modem_type) {
						case '1':
							return <div className={'device-modem-type' + ((config.device_modem_type == config.device_modem_type_priority) ? ' match' : ' no-match')} data-tip={(config.device_modem_type == config.device_modem_type_priority) ? 'Connected via GPRS' : 'Connected via ' + this.state.modem_types[config.device_modem_type] + ', but priority set to ' + this.state.modem_types[config.device_modem_type_priority]}>
								<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="170 0 170 500" height="20" width="20"><path d="M64 402H9c-3 0-5 1-6 3-2 1-3 3-3 6v55c0 3 1 5 3 7l6 2h55l7-2 2-7v-55l-2-6c-2-2-4-3-7-3zM283 292h-55l-6 3-3 6v165c0 3 1 5 3 7l6 2h55l7-2 2-7V301l-2-6c-2-2-4-3-7-3zM174 365h-55c-3 0-5 1-7 3s-2 4-2 7v91c0 3 0 5 2 7l7 2h55l6-2c2-2 3-4 3-7v-91c0-3-1-5-3-7l-6-3zM393 183h-55l-7 2-2 7v274l2 7 7 2h55l6-2c2-2 3-4 3-7V192c0-3-1-5-3-7l-6-2zM509 39c-2-2-4-2-7-2h-54c-3 0-5 0-7 2s-2 4-2 7v420c0 3 0 5 2 7l7 2h54l7-2c2-2 3-4 3-7V46c0-3-1-5-3-7z"/></svg>
							</div>;
							break;
						case '2':
							return <div className={'device-modem-type' + ((config.device_modem_type == config.device_modem_type_priority) ? ' match' : ' no-match')} data-tip={(config.device_modem_type == config.device_modem_type_priority) ? 'Connected via WIFI' : 'Connected via ' + this.state.modem_types[config.device_modem_type] + ', but priority set to ' + this.state.modem_types[config.device_modem_type_priority]}>
								<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="135 0 170 500" height="20" width="20"><path d="M455 161l-1-1a318 318 0 0 0-454 3l1 4 34 33a5 5 0 0 0 7 0 262 262 0 0 1 372 0 5 5 0 0 0 7 0l33-33a5 5 0 0 0 1-6z"/><path d="M228 154c-62 0-121 24-165 68v7l33 33a5 5 0 0 0 8 0 175 175 0 0 1 248 0 5 5 0 0 0 7 0l33-33a5 5 0 0 0 0-7c-44-44-102-68-164-68z"/><path d="M228 241c-39 0-75 15-103 43a5 5 0 0 0 0 7l33 33a5 5 0 0 0 7 0 88 88 0 0 1 125 0 5 5 0 0 0 7 0l33-33c2-2 2-5 0-7-27-28-63-43-102-43zM228 334c-14 0-26 5-36 15-2 2-2 5 0 7l32 32a5 5 0 0 0 7 0l33-32c2-2 2-5 0-7a50 50 0 0 0-36-15z"/></svg>
							</div>;
							break;
						case '3':
							return <div className={'device-modem-type' + ((config.device_modem_type == config.device_modem_type_priority) ? ' match' : ' no-match')} data-tip={(config.device_modem_type == config.device_modem_type_priority) ? 'Connected via Ethernet' : 'Connected via ' + this.state.modem_types[config.device_modem_type] + ', but priority set to ' + this.state.modem_types[config.device_modem_type_priority]}>
								<svg version="1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="20" width="20"><path d="M501 245H267v-53h64c6 0 10-5 10-11V53c0-6-4-10-10-10H181c-6 0-10 4-10 10v128c0 6 4 11 10 11h64v53H11a11 11 0 1 0 0 22h106v53H53c-6 0-10 5-10 11v128c0 6 4 10 10 10h150c6 0 10-4 10-10V331c0-6-4-11-10-11h-64v-53h234v53h-64c-6 0-10 5-10 11v128c0 6 4 10 10 10h150c6 0 10-4 10-10V331c0-6-4-11-10-11h-64v-53h106a11 11 0 1 0 0-22z"/></svg>
							</div>;
							break;
						default:
        					return <div className={'device-modem-type' + ((config.device_modem_type == config.device_modem_type_priority) ? ' match' : ' no-match')} data-tip={'Connection type not set'}>
        						<svg xmlns="http://www.w3.org/2000/svg" width="20px" height="20px" viewBox="30 -45 450 600"><path d="M256 0C115.39 0 0 115.39 0 256s115.39 256 256 256 256-115.39 256-256S396.61 0 256 0zM97.108 368.954C73.48 335.805 61 296.737 61 256c0-107.52 87.48-195 195-195 40.737 0 79.805 12.48 112.954 36.108a15.132 15.132 0 0 1 1.886 1.625L98.733 370.841a15.215 15.215 0 0 1-1.625-1.887zM256 451c-40.737 0-79.805-12.48-112.954-36.108a15.132 15.132 0 0 1-1.886-1.625l272.105-272.105a14.92 14.92 0 0 1 1.626 1.885C438.52 176.195 451 215.263 451 256c0 107.52-87.48 195-195 195z" /></svg>
        					</div>;
					}
				})()}
			</td>
			{(() => {
				if(less_fifteen_online <= fifteen_minute) {
					if (config.device_error_status == 1) {
						return <td className="text-center" data-tip={this.state.health_status[config.device_error_status] + ' - View Debug Log'}>
							<Link to={'/debug/' + config.station_id} className="device-error-log">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448.8 448.8" width="16"><path fill="#18BC9C" d="M142.8 324l-107-107.2L0 252.4l142.8 143 306-306-35.7-36"/></svg>
							</Link>
						</td>;
					} else {
						return <td className="text-center" data-tip={this.state.health_status[config.device_error_status] + ' - View Debug Log'}>
							<Link to={'/debug/' + config.station_id} className={'device-error-log debug-' + config.device_error_status}>
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 286.1 286.1" width="16"><path fill="#F25A59" d="M143 0C64 0 0 64 0 143s64 143 143 143 143-64 143-143S222 0 143 0zm0 259.2c-64.2 0-116.2-52-116.2-116.2S78.8 26.8 143 26.8s116.2 52 116.2 116.2-52 116.2-116.2 116.2zm0-196.5c-10.2 0-18 5.4-18 14V156c0 8.6 7.8 14 18 14 10 0 18-5.7 18-14V76.6c0-8.4-8-14-18-14zm0 125c-9.8 0-17.8 8-17.8 18 0 9.7 8 17.7 17.8 17.7s18-8 18-17.8c0-10-8-18-18-18z"/></svg>
							</Link>
						</td>;
					}
				} else {
					return <td className="text-center" data-tip={'Connectivity Failure - View Debug Log'}>
						<Link to={'/debug/' + config.station_id} className={'device-error-log debug-' + 9}>
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 286.1 286.1" width="16"><path fill="#F25A59" d="M143 0C64 0 0 64 0 143s64 143 143 143 143-64 143-143S222 0 143 0zm0 259.2c-64.2 0-116.2-52-116.2-116.2S78.8 26.8 143 26.8s116.2 52 116.2 116.2-52 116.2-116.2 116.2zm0-196.5c-10.2 0-18 5.4-18 14V156c0 8.6 7.8 14 18 14 10 0 18-5.7 18-14V76.6c0-8.4-8-14-18-14zm0 125c-9.8 0-17.8 8-17.8 18 0 9.7 8 17.7 17.8 17.7s18-8 18-17.8c0-10-8-18-18-18z"/></svg>
						</Link>
					</td>;
				}
			})()}
			<td className="device-analytics">
				<Link to={'/stations/' + config.station_id + '/analytics'} className="proceed" data-tip={'View Analytics'}>
					<svg xmlns="http://www.w3.org/2000/svg" height="20" width="20" viewBox="0 0 486.742 486.742">
						<path d="M33 362.37v78.9c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-138.8l-44.3 44.3c-9.4 9.3-21.4 14.7-34.3 15.6zM142 301.47v139.8c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-82.3c-13.9-.3-26.9-5.8-36.7-15.6l-41.9-41.9zM251 350.27v91c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-167.9l-69.9 69.9c-2.7 2.7-5.6 5-8.7 7zM432.7 170.17l-72.7 72.7v198.4c0 4.8 3.9 8.8 8.8 8.8h61c4.8 0 8.8-3.9 8.8-8.8v-265.6c-2-1.7-3.5-3.2-4.6-4.2l-1.3-1.3z"/><path d="M482.6 41.37c-2.9-3.1-7.3-4.7-12.9-4.7h-1.6c-28.4 1.3-56.7 2.7-85.1 4-3.8.2-9 .4-13.1 4.5-1.3 1.3-2.3 2.8-3.1 4.6-4.2 9.1 1.7 15 4.5 17.8l7.1 7.2c4.9 5 9.9 10 14.9 14.9l-171.6 171.7-77.1-77.1c-4.6-4.6-10.8-7.2-17.4-7.2-6.6 0-12.7 2.6-17.3 7.2L7.2 286.87c-9.6 9.6-9.6 25.1 0 34.7l4.6 4.6c4.6 4.6 10.8 7.2 17.4 7.2s12.7-2.6 17.3-7.2l80.7-80.7 77.1 77.1c4.6 4.6 10.8 7.2 17.4 7.2 6.6 0 12.7-2.6 17.4-7.2l193.6-193.6 21.9 21.8c2.6 2.6 6.2 6.2 11.7 6.2 2.3 0 4.6-.6 7-1.9 1.6-.9 3-1.9 4.2-3.1 4.3-4.3 5.1-9.8 5.3-14.1.8-18.4 1.7-36.8 2.6-55.3l1.3-27.7c.3-5.8-1-10.3-4.1-13.5z"/>
					</svg>
				</Link>
			</td>
			<td className="hide">
				{(() => {
					if (config.data_sending_status == 'wait') {
						return <div className="onoffswitch text-center center">
							<div className="inline-loading" data-tip={'Waiting for Response from Device'}>
								<svg className="loading-spinner" width="20px" height="20px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
							</div>
						</div>;
					} else {
						return <div className="onoffswitch center" data-tip={'Toggle device data sending'}>
							<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id={'myonoffswitch ' + (config.id)} defaultChecked={(config.data_sending_status == 'on') ? true : false} onChange={() => this.changeDeviceStatus(config.id,(config.data_sending_status == 'on') ? 'off' : 'on')}/>
							<label className="onoffswitch-label" htmlFor={'myonoffswitch ' + (config.id)}>
								<span className="onoffswitch-inner"></span>
								<span className="onoffswitch-switch"></span>
							</label>
						</div>;
					}
				})()}
			</td>
			<td className="device-config">
				<Link to={'/device-config/'+(config.id)} className="proceed" data-tip={'Configure Device'}>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 451.8 451.8" width="22" height="20"><path fill="none" stroke="#DDD" strokeWidth="56.5" d="M127.6 30.6L323 224 129.5 423" strokeLinecap="round" strokeLinejoin="round"/></svg>
				</Link>
			</td>
		</tr>;
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 */
	componentDidUpdate() {
		ReactTooltip.rebuild();
	}

	/**
	 * This renders the Device List page.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container device-config-container">
				<NavLink active="configure" />
				<Scrollbars autoHide>
					{(() => {
						if (this.state.device_details && this.state.device_details.length) {
							return <div className="industry-details-list">
								<div className="industry-details-container flex">
									<div className="no-of-device">
										<h4 className="title-text">
										{(() => {
											if (this.state.filtered_stations && this.state.device_details) {
												if (this.state.filtered_stations.length == this.state.device_details.length) {
													return <span>Number of Devices: {this.state.filtered_stations.length}</span>;
												} else {
													return <span>Devices: {this.state.filtered_stations.length} of {this.state.device_details.length}</span>;
												}
											}
										})()}
										</h4>
									</div>
									<span className="span-add-new-device">
										<button className="btn-add-new-device btn-left green-fill-btn" onClick={(e) => this.setState({modal_view: true, city_id: null})} data-tip={'Add Device'}>+</button>
									</span>
									<ConfigFilter {...this.state} updateFilter={(options) => this.updateFilter(options)} searchBoxFilter={(value) => this.searchBoxFilter(value)} />
								</div>
								<div className="view-online-container">
									<div className="view-online">
										<div className="online-device">
											<span className="text-online-device">Show Online Devices Only</span>
											<div className="onoffswitch" data-tip={(this.state.date_time_online == true) ? 'Toggle to view all devices' : 'Toggle to view latest online devices'}>
												<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id="myonoffswitch" defaultChecked={(this.state.date_time_online == true) ? true : false} onChange={() => this.toggleShowOnlineDevices()} />
												<label className="onoffswitch-label" htmlFor="myonoffswitch">
													<span className="onoffswitch-inner"></span>
													<span className={'onoffswitch-switch' + (this.state.date_time_online ? ' blue' : '')}></span>
												</label>
											</div>
										</div>
									</div>
								</div>
								<div className="panel panel-default">
									{(() => {
										let devices_table = [];
										if (this.state.device_details && this.state.filtered_stations) {
											// console.log(this.state.device_details);
											let device_list = this.state.filtered_stations.map((config,index) => {
												devices_table.push(config);
												return this.getDeviceRow(config);
											});
										return <table className="table details-list">
											<thead>
												<tr>
													<th className={'activebles' + ((this.state.sort_keys === 'id' ) ? ' active-item' : '')} onClick={(e) => this.setState({sort_keys: 'id'}, () => this.sortTableName(devices_table))} >ID
														<span className={'sort-arrow-down' + ((this.state.sorted && this.state.sort_keys === 'id' ) ? ' up' : '')} />
													</th>
													<th className={'activebles' + ((this.state.sort_keys === 'name' ) ? ' active-item' : '')} onClick={(e) => this.setState({sort_keys: 'name'}, () => this.sortTableName(devices_table))}>Station Name
														<span className={'sort-arrow-down' + ((this.state.sorted && this.state.sort_keys === 'name') ? ' up' : '')} />
													</th>
													<th className={'activebles' + ((this.state.sort_keys === 'city' ) ? ' active-item' : '')} onClick={(e) => this.setState({sort_keys: 'city'}, () => this.sortTableName(devices_table))} >City
														<span className={'sort-arrow-down' + ((this.state.sorted && this.state.sort_keys === 'city' ) ? ' up' : '')} />
													</th>
													<th className={'activebles' + ((this.state.sort_keys === 'firmware' ) ? ' active-item' : '')} onClick={(e) => this.setState({sort_keys: 'firmware'}, () => this.sortTableName(devices_table))} >Firmware
														<span className={'sort-arrow-down' + ((this.state.sorted && this.state.sort_keys === 'firmware' ) ? ' up' : '')} />
													</th>
													<th className={'activebles' + ((this.state.sort_keys === 'active' ) ? ' active-item' : '')} onClick={(e) => this.setState({sort_keys: 'active'}, () => this.sortTableName(devices_table))} >Active
														<span className={'sort-arrow-down' + ((this.state.sorted && this.state.sort_keys === 'active' ) ? ' up' : '')} />
													</th>
													<th>Sync</th>
													<th>Connectivity</th>
													<th>Health</th>
													<th>Analytics</th>
													<th className="hide">Data Sending</th>
													<th>Configure</th>
												</tr>
											</thead>
											{(() => {
												if (this.state.filtered_stations.length) {
													return <tbody>{device_list}</tbody>;
												} else {
													return <tbody>
														<tr className="no-match">
															<td colSpan="9" className="table title-text">No Matching Devices Found</td>
														</tr>
													</tbody>;
												}
											})()}
										</table>;
										}
									})()}
								</div>
								<ReactTooltip effect="solid" />
							</div>;
						} else if (this.state.device_details && this.state.device_details.length == 0) {
							return <div className="loading">
								<h3 className="table title-text">
									<center>No devices to show</center>
								</h3>
							</div>;
						} else {
							return <div className="loading">
								<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
							</div>;
						}
						// if ((this.city_name === 'admin') || (this.city_name === '127')) {
						// } else {
						// 	return <h3><center>404 - Not Found</center></h3>;
						// }
					})()}
					{(() => {
						if (this.state.modal_view) {
							return <div className="modal">
								<div className="modal-dialog">
									<div className="modal-content">
									<div className="modal-header modal-header-add">
										<button type="button" className="close" onClick={() => this.setState({modal_view: false})}>×</button>
										<h4 className="modal-title-add">Add Device</h4>
									</div>
									<div className="modal-body add-modal-body">
										<label className="col-lg-4 modal-select-label">City:</label>
										<div className="col-lg-5 modal-selectbox">
											<select className="form-control select-style" defaultValue={0} onChange={(e) => this.setState({city_id: e.target.value})}>
												<option value={0} disabled>Select City</option>
												{(() => {
													let device_cities = Object.keys(this.state.device_cities).map((key) => {
														return <option value={key}>{this.state.device_cities[key]}</option>;
													});
													return device_cities;
												})()}
											</select>
										</div>
									</div>
									<div className="modal-footer">
										<button type="button" className="btn btn-default" onClick={(e) => this.setState({modal_view: null}) } >Close</button>
										<button type="button" className="btn btn-primary" disabled={(this.state.city_id === null ? true : false)} onClick={(e) => this.addNewDevice()}>Add Device</button>
									</div>
									</div>
								</div>
							</div>;
						}
					})()}
					{(() => {
						if (this.state.modal_sync_view) {
							return <div className="modal">
								<div className="modal-dialog">
									<div className="modal-content">
									<div className="modal-header modal-header-add">
										<button type="button" className="close" onClick={() => this.setState({modal_sync_view: false})}>×</button>
										<h4 className="modal-title-add">Sync Settings</h4>
									</div>
									<div className="modal-body add-modal-body">
										{(() => {
											if (
												_.find(this.state.device_details, { 'id': this.state.sync_device_id}).device_sync_status === 'in_sync' || _.find(this.state.device_details, { 'id': this.state.sync_device_id}).device_sync_status === 'not_in_sync'
												) {
												return <label className="col-lg-12 sync-body">Sync settings to device ?</label>;
											} else {
												return <label className="col-lg-12 sync-body">Stop sync settings to device ?</label>;
											}
										})()}
									</div>
									<div className="modal-footer border-zero">
										<button type="button" className="btn btn-default" onClick={(e) => this.setState({modal_sync_view: false})} >Cancel</button>
										{(() => {
											if (
												_.find(this.state.device_details, { 'id': this.state.sync_device_id}).device_sync_status === 'in_sync' || _.find(this.state.device_details, { 'id': this.state.sync_device_id}).device_sync_status === 'not_in_sync'
												) {
												return <button type="button" className="btn btn-primary" onClick={(e) => this.deviceSyncingStatus()}>Sync</button>;
											} else {
												return <button type="button" className="btn btn-primary" onClick={(e) => this.deviceSyncingStatus()}>Stop Syncing</button>;
											}
										})()}
									</div>
									</div>
								</div>
							</div>;
						}
					})()}
				</Scrollbars>
			</div>
		);
	}
}