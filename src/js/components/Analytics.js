import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import StationDetailedAnalytics from './StationDetailedAnalytics';
import AquaStationDetailedAnalytics from './AquaStationDetailedAnalytics';

/**
 * This Class Used for Analytics page.
 */
export default class Analytics extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This setting the interval to null at initial stage.
		 */

		this.interval = null;

		if (props.location.search && props.location.search.split('?')[1].split('&')[0].split('=')[1] && props.location.search.split('?')[1].split('&')[1].split('=')[1]) {
			this.interval = [props.location.search.split('?')[1].split('&')[0].split('=')[1], props.location.search.split('?')[1].split('&')[1].split('=')[1]];
			console.log('interval', this.interval);
		}
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			station_data: null,
			source: 'Aurassure',
			calendar_time: this.interval,
			station_id: props.params.station_id,
			location: props.location,
			pathname: props.pathname,
			pattern: props.pattern,
			current_param: ''
		};
		/**
		 * This is used for bind the all stations data.
		 * @type {object}
		 */
		saveStationId(props.params.station_id);
		this.fetchStationDataType(props.params.station_id);
	}

	changeDate(interval) {
		// console.log('changeDateinterval', interval);
		let that = this;
		let query_time = '?from=' + interval[0] + '&upto=' + interval[1];
		that.context.router.transitionTo('/stations/' + this.state.station_id + '/analytics/' + query_time);
		that.setState({
			calendar_time: interval,
			data_loading_no_calender: 1
		}, () => {
			that.fetchStationData(that.state.station_id);
		});
	}

	/**
	 * This function used for fetch stations data.
	 * @param  {number} station_id This is the selected station's id.
	 */
	fetchStationDataType(id) {
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_data_of_station.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: id
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Station Data:',json);
			if (json.status === 'success') {
				that.setState({
					station_id: that.props.params.station_id,
					station_detail_data: json,
					station_type: json.loc_type,
					station_data: null
				}, () => that.fetchStationData(that.props.params.station_id));
				console.log('success_id', id);
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	fetchStationData(id) {
		saveStationId(id);
		var that = this;
		that.setState({station_data: null});
		let from_time, upto_time;
		if (that.state.calendar_time === null) {
			from_time = 0;
			upto_time = 0;
		} else {
			from_time = parseInt(moment(that.state.calendar_time[0] + ' 00:00', 'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'));
			upto_time = parseInt(moment(that.state.calendar_time[1] + ' 23:59', 'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'));
		}
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_detailed_analytics_of_station.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_id: id,
				time_interval: (that.state.station_type == '2' ? [from_time, upto_time] : [0, 0])
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			if (json.status === 'success') {
				that.setState({
					station_id: id,
					station_data: json,
					current_param: json.params[0].key
				});
				// that.outlierDataPush();
				// If calendar state null, set the child calendar time as the received time interval.
				if (that.state.calendar_time === null) {
					let from_time = json.time_stamps[0] ? moment.unix(json.time_stamps[0]).tz('Asia/Kolkata').format('DD-MM-YYYY') : moment.tz('Asia/Kolkata').format('DD-MM-YYYY'),
						upto_time = json.time_stamps[0] ? moment.unix(json.time_stamps[(json.time_stamps.length - 1)]).tz('Asia/Kolkata').format('DD-MM-YYYY') : moment.tz('Asia/Kolkata').format('DD-MM-YYYY');
					console.log('Calendar Time:', [from_time, upto_time]);
					that.setState({calendar_time: [from_time, upto_time] });
					if (that._child) {
						that._child.setCalendarDates(null, { fromTime: from_time, uptoTime: upto_time });
					}
				}
				if (that._analytics_child_aqua && that._analytics_child_aqua.data_update_interval_handle) {
					clearInterval(that._analytics_child_aqua.data_update_interval_handle);
				} else if (that._analytics_child && that._analytics_child.data_update_interval_handle) {
					clearInterval(that._analytics_child.data_update_interval_handle);
				}
				if (that.state.station_data && that.state.station_data.param_values && Object.keys(that.state.station_data.param_values).length) {
					if (that._analytics_child_aqua) {
						that._analytics_child_aqua.data_update_interval_handle = setInterval(() => {
							that._analytics_child_aqua.updateStationData();
						}, 15000);
					} else if (that._analytics_child) {
						that._analytics_child.data_update_interval_handle = setInterval(() => {
							that._analytics_child.update_station_data();
						}, 15000);
					}
				}
				console.log('Data', that.state.station_data);
				console.log('Aqua_child_data', that._analytics_child_aqua ? that._analytics_child_aqua.state.station_data : 'no');
				console.log('infra_child_data', that._analytics_child ? that._analytics_child.state.station_data : 'no');
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 */
	componentDidUpdate() {
	}

	componentWillUnmount() {
		if (this._analytics_child_aqua && this._analytics_child_aqua.data_update_interval_handle) {
			clearInterval(this._analytics_child_aqua.data_update_interval_handle);
		} else if (this._analytics_child && this._analytics_child.data_update_interval_handle) {
			clearInterval(this._analytics_child.data_update_interval_handle);
		}
	}

	/**
	 * This renders the Dashboard page.
	 * @return {ReactElement} markup
	 */
	render() {
		return (
			<div className="full-page-container" id="analytics_container_demo_check">
				<NavLink active={'analytics'} />
				{(() => {
					if (this.state.station_data === null) {
						return <div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
					} else {
						if (this.state.station_type == '1') {
							return <StationDetailedAnalytics {...this.state} fetchStationDataType={(id) => this.fetchStationDataType(id)} ref={(child) => {this._analytics_child = child;}} />;
						} else if (this.state.station_type == '2') {
							return <AquaStationDetailedAnalytics {...this.state} fetchStationDataType={(id) => this.fetchStationDataType(id)} changeDate={(interval) => this.changeDate(interval)} ref={(child) => {this._analytics_child_aqua = child;}} />;
						} else if (this.state.station_type == null || this.state.station_type == '0' || this.state.station_type == undefined) {
							return <StationDetailedAnalytics {...this.state} fetchStationDataType={(id) => this.fetchStationDataType(id)} ref={(child) => {this._analytics_child = child;}} />;
						}
					}
				})()}
			</div>
		);
	}
}

Analytics.contextTypes = {
	router: React.PropTypes.object
};