import React from 'react';
import NavLink from './NavLink';
import Select from 'react-select';
import { Scrollbars } from 'react-custom-scrollbars';

/**
 * This class is used to render the Device Alerts.
 */
export default class DeviceAlerts extends React.Component {
	/**
	 * This is the Constructor for DeviceAlerts class to set the default task while page load.
	 * @param  {Object} params This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);

		/**
		 * This sets the initial state for this class.
		 * @type {Object}
		 */
		this.state = {
			city_id: props.city_id,
			device_id: props.params.device_id ? props.params.device_id : 1,
			alert_data_view: 0,
			data_loading: 1
		};
		console.log('Device ID:',this.state.device_id);
		/**
		 * This variable stores the name of city.
		 * @type {String}
		 */
		this.city_name = window.location.host.split('.')[0];
	}

	/**
	 * This function sets or change state value of the city id & resets the state values of device id & alert data view.
	 * @param  {Number} id City Id
	 */
	changeCity(id) {
		this.setState({
			city_id: id,
			device_id: 0,
			alert_data_view: 0
		});
	}

	/**
	 * This function sets or change the state value of device id.
	 * @param  {Number} dev_id Device Id
	 */
	changeUrl(dev_id) {
		this.setState({device_id: dev_id});
	}

	/**
	 * This function calls API to get alert data of station & updates the state values.
	 */
	getAlertData() {
		var that = this;
		that.setState({param_data_loading: 1});
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_alert_data_of_station.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			body: JSON.stringify({
				station_id: that.state.device_id
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Data All Alert Details',json);
			if (json.status === 'success') {
				that.setState({
					params: json.params,
					param_units: json.param_units,
					station_data: json.station_data,
					parameter_thresholds: json.station_data.parameter_thresholds != null ? json.station_data.parameter_thresholds : [],
					operator: {
						'<': '<',
						'>': '>'
					},
					alert_data_view: 1,
					param_data_loading: null
				});
			} else {
				that.setState({status_msg: json.message});
				showPopup('danger', json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			that.setState({status_msg: 'Unable to load data!'});
		});
	}

	updateValues(key, value, index, array, array_index) {
		let station_data = this.state.station_data;
		// console.log(key + '->' + index, value, array_index);
		Object.keys(this.state.station_data).map((data_key) => {
			if ((key == data_key) && (index === undefined)) {
				station_data[key] = value;
				console.log('[key]', value);
			} else if ((key == data_key) && (index !== undefined) && (array === undefined)) {
				if (station_data[key].constructor !== Array) {
					station_data[key] = [];
				}
				station_data[key][parseInt(index)] = value;
				// console.log('[key][index]', value);
			} else if ((key == data_key) && (index !== undefined) && (array !== undefined)) {
				let val = null;
				if (value && value.constructor === Array) {
					val = value.map((v) => {
						return v.value;
					}).filter(Boolean);
				} else {
					val = value;
				}
				if (!station_data[key] || station_data[key].constructor !== Array) {
					station_data[key] = [];
				}
				if (!station_data[key][parseInt(index)] || station_data[key][parseInt(index)].constructor !== Array && station_data[key][parseInt(index)].constructor !== Object) {
					station_data[key][parseInt(index)] = {};
				}
				if ((key == 'param_details') && (array == 'ranges') && (array_index !== undefined)) {
					station_data[key][parseInt(index)][array][array_index] = val;
					console.log('[key][index][array][array_index]', val);
				} else {
					station_data[key][parseInt(index)][array] = val;
					console.log('[key][index][array]', val);
				}
			}
		});
		this.setState({station_data: station_data});
		console.log('State Updated', this.state.station_data);
	}

	addParameterRow() {
		let parameter_thresholds = this.state.parameter_thresholds;
		if (parameter_thresholds == null || parameter_thresholds == undefined || parameter_thresholds == '') {
			parameter_thresholds = [];
			parameter_thresholds.push({
				param_name: '',
				expression: '',
				threshold: '',
				param_unit_id: ''
			});
		} else {
			parameter_thresholds.push({
				param_name: '',
				expression: '',
				threshold: '',
				param_unit_id: ''
			});
		}
		this.setState({parameter_thresholds: parameter_thresholds});
		console.log('Parameter Added', this.state.parameter_thresholds);
	}

	removeParameterRow(index) {
		let parameter_thresholds = this.state.parameter_thresholds;
		parameter_thresholds.splice(index, 1);
		this.setState({parameter_thresholds: parameter_thresholds});
		console.log('Parameter Removed', this.state.parameter_thresholds);
	}

	/**
	 * This function appends a user row.
	 * @param {Number} user_id User Id of the selected User
	 */
	// addUserRow(user_id) {
	// 	let alert_users = this.state.alert_users;
	// 	if (!alert_users) {
	// 		alert_users = [];
	// 	}
	// 	alert_users.push({
	// 		id: parseInt(user_id),
	// 		phone: true,
	// 		email: true
			
	// 	});
	// 	this.setState({alert_users: alert_users});
	// 	console.log('User Added', this.state.alert_users);
	// }

	/**
	 * This function updates the data of a user.
	 * @param  {Number} index   Index of the array of users
	 * @param  {Number} user_id User Id of the user for which the data is going to be update
	 * @param  {String} phone   Phone no. of the User
	 * @param  {String} email   Email of the User
	 */
	// updateUserRow(index, user_id, phone, email) {
	// 	let alert_users = this.state.alert_users;
	// 	alert_users[index] = {
	// 		id: parseInt(user_id),
	// 		phone: phone,
	// 		email: email
	// 	};
	// 	this.setState({alert_users: alert_users});
	// 	console.log('User Updated', this.state.alert_users);
	// }

	/**
	 * This function removes a user from the array of users.
	 * @param  {Number} index Index of the User in the array which is going to be removed
	 */
	// removeUserRow(index) {
	// 	let alert_users = this.state.alert_users;
	// 	this.state.alert_users.map((id, user_index) => {
	// 		if (user_index == index) {
	// 			alert_users.splice(index, 1);
	// 		}
	// 	});
	// 	this.setState({alert_users: alert_users});
	// 	console.log('User Removed', this.state.alert_users);
	// }

	/**
	 * This function calls API to save the station alert configurations & update the state values.
	 */
	saveAlertData() {
		var that = this;
		that.setState({data_saving: 1});
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'save_station_alert_configurations.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			body: JSON.stringify({
				station_id: that.state.device_id,
				parameter_thresholds: that.state.station_data.parameter_thresholds
			})
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Data Saved',json);
			if (json.status === 'success') {
				that.setState({
					station_data: that.state.station_data,
					alert_data_view: 1,
					data_loading: null,
					data_saving: null
				});
				showPopup('success', 'Data saved successfully!');
			} else {
				that.setState({status_msg: json.message});
				showPopup('danger', json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			that.setState({status_msg: 'Unable to load data!'});
		});
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		if ((this.city_name === 'admin') || (this.city_name === '127')) {
			document.title = 'Alert Settings - Aurassure';

			var that = this;
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_device_list.php', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				credentials: 'include',
				body: JSON.stringify({
					device_id: that.state.device_id
				})
			}).then(function(Response) {
				return Response.json();
			}).then(function(json) {
				console.log('Data Sources List',json);
				if (json.status === 'success') {
					let ct_id = json.devices.map((value) => {
						if (that.state.device_id == value.id) {
							return value.ct_id;
						}
					});
					that.setState({
						city_id: ct_id.filter(Boolean).join(),
						data_city: json.cities,
						data_stations: json.devices,
						data_loading: null
					});
				} else {
					that.setState({status_msg: json.message});
					showPopup('danger', json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.setState({status_msg: 'Unable to load data!'});
			});
		} else {
			document.title = '404 Not Found - Aurassure';
		}
	}

	/**
	 * This renders entire class with navigation bar.
	 * @return {ReactElement} markup
	 */
	render() {
		return (
			<div className="full-page-container device-config-container">
				<NavLink active="alert-settings"/>
				{(() => {
					if ((this.city_name === 'admin') || (this.city_name === '127')) {
						if (!this.state.data_loading) {
							return <Scrollbars autoHide>
								<div className="archive-form alert-settings">
									<h3 className="archive-title">Alerts</h3>
									<div className="archive-options">
										<div className="option-row">
											<div className="option-label">Data Source</div>
											<div className="option-content">
												<select className="form-control" id="cityID" title="Select City" value={this.state.city_id} onChange={(e) => this.changeCity(e.target.value)}>
													<option value={0} disabled>Select City</option>
													{(() => {
														if (this.state.data_city) {
															let dataSourceCities = this.state.data_city.map((value) => {
																return(<option value={value.id}>{value.name}</option>);
															});
															return dataSourceCities;
														}
													})()}
												</select>
												<div className="hyphen table"></div>
												<select className="form-control" id="dataID" title="Select Device" value={this.state.device_id} onChange={(e) => this.changeUrl(e.target.value)}>
													<option value={0} disabled>Select Device</option>
													{(() => {
														if (this.state.data_stations && this.state.city_id) {
															let dataSources = this.state.data_stations.map((value) => {
																if (this.state.city_id == value.ct_id) {
																	return(<option value={value.id}>{value.name}</option>);
																}
															});
															return dataSources;
														}
													})()}
												</select>
												<button type="button" className="btn btn-primary btn-view-alert" disabled={this.state.device_id === 0 ? true : false} onClick={() => this.getAlertData()}>View</button>
											</div>
										</div>
										{(() => {
											if (this.state.param_data_loading) {
												return <div className="parameter-limit">
													<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
												</div>;
											} else if (this.state.alert_data_view) {
												return <div>
													<div className="parameter-limit">
														<div className="stack-items">
															<div className="alerts-pre-warning">
																<div className="alerts-warning-head">
																	<div className="warn-box">Parameters Name</div>
																	<div className="warn-box">Operator</div>
																	<div className="warn-box">Set Threshold limit</div>
																</div>
																{(() => {
																	if (this.state.parameter_thresholds && this.state.parameter_thresholds.length) {
																		let alert_parameters = this.state.parameter_thresholds.map((param, index) => {
																			return <div className="alerts-warning-body">
																				<div className="param-width-big flex">
																					{(() => {
																						let param_names_options = this.state.params.map((parameter) => {
																							return {value: parameter.name_noformat_lower, label: parameter.name_noformat};
																						});
																						// console.log(index, param.param_name);
																						return <div className="param-width">
																							<Select options={param_names_options} onChange={(e) => this.updateValues('parameter_thresholds', (e.value).toLowerCase(), index,'param_name')} value={param.param_name} placeholder="Parameter Name" clearable={false} />
																						</div>
																					})()}
																					{(() => {
																						let param_operator_options = Object.keys(this.state.operator).map((key) => {
																							return {value: key, label: this.state.operator[key]};
																						});
																						// console.log(index, param.param_name);
																						return <div className="param-width">
																							<Select options={param_operator_options} onChange={(e) => this.updateValues('parameter_thresholds', e.value, index,'expression')} value={param.expression} placeholder="Select Operator" clearable={false} />
																						</div>
																					})()}
																				</div>
																				{/*<div className="warn-box line-height" dangerouslySetInnerHTML={{__html: param_name.name}}/>*/}
																				<div className="param-width-small flex">
																					<input type="text" className="parameter-limit-threshold" placeholder="Set a Threshold limit" value={param.threshold == null ? 0 : param.threshold } onChange={(e) => this.updateValues('parameter_thresholds', e.target.value, index,'threshold')}/>
																					<div className="param-unit-width">
																						{(() => {
																							let param_units_options = this.state.param_units.map((parameter) => {
																								return {value: parameter.id, label: parameter.unit_noformat};
																							});
																							// console.log(index, param.param_name);
																							return <Select options={param_units_options} onChange={(e) => this.updateValues('parameter_thresholds', e.value, index,'param_unit_id')} value={param.param_unit_id} placeholder="Parameter Unit" clearable={false} />
																						})()}
																					</div>
																				</div>
																				<span className="remove-param" onClick={() => this.removeParameterRow(index)}>✖</span>
																				{/*<span className="param-unit" dangerouslySetInnerHTML={{__html: this.state.param.unit}} />*/}
																			</div>;
																		});
																		return alert_parameters;
																	} else {
																		return <div className="add-parameter">Add Parameter For Alerts</div>
																	}
																})()}
															</div>
														</div>
														<button className="btn btn-left btn-sm green-border-btn" onClick={() => this.addParameterRow()}>＋ Add Parameter</button>
														{/*<div className="parameter-row">
															<div className="parameter-label">Add Users</div>
															<div className="parameter-content">
																{(() => {
																	let users = Object.keys(this.state.users).map((key) => {
																		return {value: parseInt(key), label: this.state.users[key]};
																	});
																	return <Select options={users} onChange={(e) => this.addUserRow(e.value)} value={0} clearable={false} />;
																})()}
																{(() => {
																	if (this.state.alert_users) {
																		let user_phone_email = this.state.alert_users.map((user, index) => {
																			return <div className="user-contact">
																				{this.state.users[user.id]}
																				<div className="flex">
																					<span className="close-btn" onClick={() => this.removeUserRow(index)}>✖</span>
																				</div>
																			</div>;
																		});
																		return user_phone_email;
																	}
																})()}
															</div>
														</div>*/}
													</div>
													{(() => {
														if (this.state.data_saving) {
															return <div className="archive-btns">
																<svg className="loading-spinner" width="20px" height="20px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
																<div className="save-btn-loading">Saving...</div>
															</div>;
														} else {
															return <div className="archive-btns">
																<button type="button" className="btn btn-primary" onClick={() => this.saveAlertData()}>Save</button>
															</div>;
														}
													})()}
													
												</div>;
											}
										})()}
									</div>
								</div>
							</Scrollbars>;
						} else {
							return <div className="loading">
								<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
							</div>;
						}
					} else {
						return <h3><center>404 - Not Found</center></h3>;
					}
				})()}
			</div>
		);
	}
}

DeviceAlerts.contextTypes = {
	router: React.PropTypes.object
};