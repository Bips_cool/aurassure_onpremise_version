import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import _ from 'lodash';
import { arrayMove } from 'react-sortable-hoc';
import { Scrollbars } from 'react-custom-scrollbars';
import { SortableList } from './imports/CompareItem';
import { AquaSortableList } from './imports/AquaCompareItem';
/**
 * This Class Used for Compare page.
 */
export default class Compare extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} params
	 */
	constructor(params) {
		super();
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			show_search_list: null,
			search_term: '',
			station_id: 1,
			stations: null,
			all_stations: null,
			index: null,
			items: []
		};

		this.fetch_compare_station_data();
	}

	/**
	 * This function used to fetch compared station details to user.
	 */
	fetch_compare_station_data() {
		var that = this;
		that.setState({stations: null});
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_data_of_compared_stations_of_user.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('Stations', json);
			if (json.status === 'success') {
				let stations = [];
				json.stations.map((station, i) => {
					stations.push(station);
				});
				that.setState({stations: stations});
				that.creatingGraphConfig();
				that.retrive_data();
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function is used to configure graph for compared stations.
	 */
	creatingGraphConfig() {
		var that = this;
		if (that.state.stations) {
			let current_time = moment().unix();
			let previous_time = current_time - 3600;
			let graph_data = [];
			let categories = [];
			let config = [];
			let items = [];
			let plot_graph = [];
			let param_data = [];

			that.state.stations.map((station, ind) => {
				graph_data = [];
				categories = [];
				plot_graph[ind] = false;
				station.hourly_aqis.map((hourly_aqi, i) => {
					let color = window.color_code.color_1;
					if(hourly_aqi.aqi >= 401) {
						color = window.color_code.color_6;
					} else if(hourly_aqi.aqi >= 301) {
						color = window.color_code.color_5;
					} else if(hourly_aqi.aqi >= 201) {
						color = window.color_code.color_4;
					} else if(hourly_aqi.aqi >= 101) {
						color = window.color_code.color_3;
					} else if(hourly_aqi.aqi >= 51) {
						color = window.color_code.color_2;
					}
					graph_data.push({
						y: parseInt(hourly_aqi.aqi),
						param: ((hourly_aqi.param == "" || hourly_aqi.param == null) ? '-' : (hourly_aqi.param.toUpperCase())),
						color: color
					});

					if(hourly_aqi.aqi) {plot_graph[ind] = true;}

					categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));
				});
				categories.reverse();
				graph_data.reverse();

				let param_plot_graph = [];
				let param_graph_data = [];
				let param_categories = [];
				let param_aqis_config = [];
				Object.keys(station.hourly_param_aqis).map((param, i) => {
					param_plot_graph[param] = false;
					param_categories = [];
					param_graph_data = [];
					station.hourly_param_aqis[param].map((aqi, a) => {
						let color = window.color_code.color_1;
						if(aqi >= 401) {
							color = window.color_code.color_6;
						} else if(aqi >= 301) {
							color = window.color_code.color_5;
						} else if(aqi >= 201) {
							color = window.color_code.color_4;
						} else if(aqi >= 101) {
							color = window.color_code.color_3;
						} else if(aqi >= 51) {
							color = window.color_code.color_2;
						}
						param_graph_data.push({
							y: parseInt(aqi),
							color: color
						});

						if(aqi) {param_plot_graph[param] = true;}

						param_categories.push(moment.unix(previous_time - 3600 * a).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * a).tz('Asia/Kolkata').format('HH'));
					});
					param_categories.reverse();
					param_graph_data.reverse();
					param_aqis_config[param] = {
						chart: {
							type: 'column',
							height: 70,
							width: 120
						},
						plotOptions: {
							series: {
								pointPadding: 0,
								groupPadding: 0
							}
						},
						title: {
							text: ''
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							title: {
								enabled: false,
								text: '',
								style: {
									fontWeight: 'normal'
								}
							},
							type: '',
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							categories: param_categories

						},
						yAxis: {
							title: {
								text: ''
							},
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							gridLineColor: 'transparent'

						},
						legend: {
							enabled: false
						},
						tooltip: {
							pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b><br/>'
						},
						series: [{
							data: param_graph_data
						}]
					};
				});

				items.push({
					id: station.id,
					name: station.name,
					time: station.last_data_update_time,
					aqi: station.aqi,
					aqi_range: station.aqi_range,
					aqi_status: station.aqi_status,
					connection_status: station.connection_status,
					responsible_param: station.hourly_aqis[0].param,
					source: 'aurassure',
					plot_graph: plot_graph[ind],
					type: station.type,
					latest_param_value: station.latest_param_value,
					last_data_receive_time: station.last_data_receive_time,
					data_receive_connection_status: station.data_receive_connection_status,
					params: station.params,
					parameters_value: {
						'so2': 1.2,
						'no': 11111,
						'PM': 2.1,
						'pm<sub>2.5</sub>': 1.1,
						'spm': 1.2
					},
					parameters_unit: {
						'so2': 'Mg/nm<sub>3</sub>',
						'no': 'ppm',
						'PM': 'Mg/nm<sub>3</sub>',
						'pm<sub>2.5</sub>': 'm<sup>3</sup>',
						'spm': 'Mg/nm<sub>3</sub>'
					},
					hourly_aqi_config: {
						chart: {
							type: 'column',
							height: 80,
							width: 168
						},
						plotOptions: {
							series: {
								pointPadding: 0,
								groupPadding: 0
							}
						},
						title: {
							text: ''
						},
						subtitle: {
							text: ''
						},
						xAxis: {
							title: {
								enabled: true,
								text: 'Past 24 hr Trend',
								style: {
									fontWeight: 'normal'
								}
							},
							type: '',
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							categories: categories

						},
						yAxis: {
							title: {
								text: 'AQI'
							},
							lineWidth: 0,
							minorGridLineWidth: 0,
							lineColor: 'transparent',      
							labels: {
								enabled: false
							},
							minorTickLength: 0,
							tickLength: 0,
							gridLineColor: 'transparent'

						},
						legend: {
							enabled: false
						},
						tooltip: {
							pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
						},
						series: [{
							data: graph_data
						}]
					},
					param_aqis_config: param_aqis_config,
					param_plot_graph: param_plot_graph,
					hourly_param_aqis: station.hourly_param_aqis,
					remove_station: function() {that.remove_station(station.id);}
				});
			});
			that.setState({items: items});
			let index = [];
			that.state.items.map((item, i) => {
				index.push(item.id);
			});
			that.setState({index: index});
		}
	}

	/**
	 * This function is used to fetch all stations data.
	 */
	retrive_data() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_stations.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				var compared_stations = json.stations;
				if (that.state.stations && Object.keys(that.state.stations).length) {
					that.state.stations.map(function(c_station) {
						compared_stations.map(function(station, j) {
							if (c_station.id === station.id) {
								console.info('Removed ['+j+']:',station.id);
								// compared_stations.splice(compared_stations[j], 1);
								delete compared_stations[j];
							}
						}, that);
					}, that);
				}
				that.setState({
					all_stations: json.stations,
					compared_stations: compared_stations
				});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function is used for serch box to compare stations.
	 * @param  {object} e This the user input value in search box.
	 */
	search_list(e) {
		this.setState({search_term: e.trim().toLowerCase()});
	}

	/**
	 * This function sets state to open search list.
	 */
	open_search_list() {
		this.setState({show_search_list: 1});
	}

	/**
	 * This function sets state for closing search list and clear user input value in searchbox.
	 */
	close_search_list() {
		this.setState({
			show_search_list: null,
			search_term: ''
		});
		document.getElementById('search').value = '';
	}

	/**
	 * This function used to compare updated station list.
	 * @param {object} object
	 * @param  {number} options.oldIndex This is the old index of the compared station.
	 * @param  {number} options.newIndex This is the new index of the compared station.
	 */
	onSortEnd({oldIndex, newIndex}) {
		this.setState({items: arrayMove(this.state.items, oldIndex, newIndex)});
		let index = [];
		this.state.items.map((item, i) => {
			index.push(parseInt(item.id));
		});
		if (index.toString() != this.state.index.toString()) {
			this.setState({index: index});
			console.log('Sorted Station:',index);
			if (index.length) {
				fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'update_station_compare_list_of_user.php', {
					credentials: 'include',
					method: 'POST',
					headers: {'Content-Type': 'application/json'},
					body: JSON.stringify({
						station_array: index
					})
				}).then(function(response) {
					return response.json();
				}).then(function(json) {
					console.log('Sort Response:',json);
					if (json.status === 'success') {
					} else {
						showPopup('danger',json.message);
					}
				}).catch(function(ex) {
					console.log('parsing failed', ex);
					showPopup('danger','Unable to load data!');
				});
			} else {
				showPopup('danger','No station found to compare!');
			}
		}
	}

	/**
	 * This function used for add station to compared list.
	 * @param {number} id This is the station id of selected station for compare.
	 */
	add_station(id) {
		console.log('Add Station:',id);
		if (id) {
			var that = this;
			var station_array = that.state.index;
			if (station_array.indexOf(id) === -1) {
				// station_array.splice(station_array.indexOf(id), 1);
				station_array.push(parseInt(id));
			}
			console.log('Added Station:',station_array);
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'add_station_to_compare_list_of_user.php', {
				credentials: 'include',
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					station_array: station_array.map(Number)
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('Add Response:',json);
				if (json.status === 'success') {
					document.getElementById('search').value = '';
					that.setState({show_search_list: null});
					that.fetch_compare_station_data();
				} else {
					showPopup('danger',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				showPopup('danger','Unable to load data!');
			});
		} else {
			showPopup('danger','Invalid Station!');
		}
	}

	/**
	 * This function used for remove station from compare list.
	 * @param  {number} id This is the station id of selected station.
	 */
	remove_station(id) {
		console.log('Remove Station:',id);
		if (id) {
			var that = this;
			var station_array = that.state.index;
			if (station_array.indexOf(id) > -1) {
				station_array.splice(station_array.indexOf(id), 1);
			}
			console.log('Removed Station:',station_array);
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'delete_station_from_compare_list_of_user.php', {
				credentials: 'include',
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				body: JSON.stringify({
					station_array: station_array.map(Number)
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('Remove Response:',json);
				if (json.status === 'success') {
					that.fetch_compare_station_data();
				} else {
					showPopup('danger',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				showPopup('danger','Unable to load data!');
			});
		} else {
			showPopup('danger','Invalid Station!');
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Compare Stations - Aurassure';
	}

	/**
	 * This renders the Compare page.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container">
				<NavLink active="compare" station_id={(stationId != 1) ? stationId : 1} />
				{(() => {
					if (!this.state.stations || !this.state.all_stations) {
						return <div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
					} else if(!this.state.all_stations.length) {
						return(
							<div className="hourly-aqi-na-error compare-search">Sorry, No station was found in your city!</div>
						);
					} else {
						var stations = [];
						this.state.compared_stations.map(function(station, i) {
							if ((this.state.search_term == '') || (station.name).toLowerCase().search(this.state.search_term) >= 0) {
								stations.push(<li className="hellip" key={i} onClick={() => this.add_station(station.id)}>
									{station.name}
								</li>);
							}
						}, this);
						if (!stations.length && (this.state.search_term != '')) {
							stations.push(<li className="hellip">No results found by term {'"'+this.state.search_term+'"'}.</li>);
						} else if (!stations.length && (this.state.search_term == '')) {
							stations.push(<li className="hellip" onClick={() => this.close_search_list()}>No stations available.</li>);
						}
						return(
							<span>
								<div className="compare-search">
									<input className="form-control" id="search" placeholder="Search Stations to compare" onChange={(e) => this.search_list(e.target.value)} onFocus={() => this.open_search_list()} />
									<span className={'close' + ((this.state.show_search_list) ? '' : ' hide')} onClick={() => this.close_search_list()}>✖</span>
									<ul id="searchResults" className={(this.state.show_search_list) ? '' : 'hide'}>
										{stations}
									</ul>
								</div>
								{(() => {
									if(this.state.stations && this.state.stations.length) {
										return <Scrollbars autoHide>
											<SortableList items={this.state.items} onSortEnd={this.onSortEnd.bind(this)} axis={'x'} helperClass="sorting-item" />
										</Scrollbars>;
									} else {
										return <center>Search for a location to add it to your compare list.</center>;
									}
								})()}
							</span>
						);
					}
				})()}
			</div>
		);
	}
}
