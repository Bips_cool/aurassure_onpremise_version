import React from 'react';
import {Link} from 'react-router';
import ReactDOM from 'react-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment-timezone';

/**
 * This class is used to render Navigation Menu.
 */
export default class NavLink extends React.Component {
	/**
	 * This is the Constructor for ManageUsers class to set the default task while page load.
	 * @param  {Object} params This will import the attributes passed from its Parent Class.
	 */
	constructor() {
		super();
		/**
		 * This sets the initial state for this class.
		 * @type {Object}
		 */
		this.state = {
			notification_popup: false,
			menu_opened: false,
			about_modal: false
		};
		this.getNotificationCount();
		this.count_notification_interval = null;
	}

	toggleMenu() {
		this.setState({
			menu_opened: !this.state.menu_opened
		});
		// console.log('toggling user menu');
	}

	toggleNotificationMenu() {
		this.setState({
			notification_popup: !this.state.notification_popup
		}, () => this.getNotification());
		// console.log('toggling user menu');
	}

	closeDropdowns(event) {
		const notificationdropdownarea = ReactDOM.findDOMNode(this.refs.notificationdropdownarea);
		if (notificationdropdownarea && !notificationdropdownarea.contains(event.target)) {
			// console.log('closing user menu');
			this.setState({
				notification_popup: false
			});
		}
	}

	getNotification(){
		let that = this;
		if(that.state.notification_popup){
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_notifications_new', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				credentials: 'include',
				body: JSON.stringify({
					notification_type: 'popup'
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('Notifications Nav:', json);
				if(json.status === 'success') {
					that.setState({
						notifications : json.notifications,
						data_loading: 0
					}, () => that.getReadNotification());
				} else {
					showPopup('danger',json.message);
				}
			});
		}
	}

	getNotificationCount(){
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_notification_counts_new', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('Notifications Count:',json);
			if(json.status === 'success') {
				that.setState({
					notifications_count : json.notifications_count,
					data_loading: 0
				});
			} else {
				showPopup('danger',json.message);
			}
		});
	}

	getReadNotification(){
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##mark_read_notifications_new', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('read_notifications:',json);
			if(json.status === 'success') {
				that.setState({
					notifications_count: 0,
					data_loading: 0
				});
			} else {
				showPopup('danger',json.message);
			}
		});
	}

	/**
	 * This function is used to toggle the state value of menu_opened.
	 */
	toggleMenu() {
		var that = this;
		that.setState({
			menu_opened: !that.state.menu_opened
		});
	}

	componentDidMount() {
		window.react_container.addEventListener('click', (event) => this.closeDropdowns(event));

		this.count_notification_interval = setInterval(() => this.getNotificationCount(), 15000);
	}

	componentWillUnmount() {
		window.react_container.removeEventListener('click', (event) => this.closeDropdowns(event));

		if(this.count_notification_interval) {
			clearInterval(this.count_notification_interval);
			this.count_notification_interval = null;
		}
	}

	/**
	 * This renders the navigation bar.
	 * @return {ReactElement} markup
	 */
	render() {
		let user_name = document.getElementById('user_name') ? document.getElementById('user_name').value : 'User',
			city_name = window.location.host.split('.')[0];
		
		return (
			<div className="navbar-with-modal">
				<nav className="navbar navbar-inverse">
					<div className="container-fluid">
						<div className="navbar-header">
							<svg className="logo" xmlns="http://www.w3.org/2000/svg" height="30" width="30" viewBox="0 0 1000 1000" enableBackground="new 0 0 1000 1000"><path opacity="1" fill="#FFFFFF" d="M786,226c-14.6-44.9-36.1-83.2-64.5-114.9S657.9,54.7,616.2,37c-41.8-17.7-89.5-26.6-143.3-26.6 c-74.7,0-142.7,12.8-204,38.3c-61.4,25.5-118.6,64.1-171.8,115.7l30.4,53.3c5,8.7,11.9,16.1,20.4,22c8.5,5.9,18.5,8.9,29.9,8.9 c14.5,0,29.2-5.2,44.1-15.7c14.9-10.4,32.5-22.1,53.1-35.1c20.6-13,44.9-24.7,73.1-35.1c28.1-10.4,62.8-15.7,103.9-15.7 c62.6,0,109.9,19.3,141.9,57.8c31.4,37.9,47.4,93.5,47.9,166.9c20.6-4.5,41.2-8.9,61.7-13.5c2.9-0.6,5.8-1.2,8.7-1.9 c29-6.4,58-12.9,86.9-19.8c2.5-0.6,5-1.2,7.4-1.9C803.7,295.9,796.9,259.7,786,226z M779.5,767.4c-2.5,2.5-5.1,5-7.8,7.3 c-33.8,31.8-71.1,61.1-111.8,88.2c-0.2,0.3-0.7,0.4-1,0.7c-1.2,0.8-2.4,1.6-3.6,2.3c0.1,0.4,0.2,0.7,0.3,1.1l17.5,82.5 c3.1,16.5,9.8,27.3,19.9,32.4c10.1,5.1,23.4,7.6,39.9,7.6h75V738.8C798.7,748.5,789.2,758.1,779.5,767.4z"/><path fill="#FFFFFF" d="M942.7,373.9c-15.7,122.8-61.6,229-134.8,318.5c-16.8,20.5-34.9,40-54.5,58.7c-2.2,2.2-4.5,4.4-6.9,6.4 c-29.7,27.9-62.6,53.7-98.3,77.6c-0.2,0.3-0.7,0.3-0.9,0.6c-0.6,0.4-1.1,0.8-1.7,1.1c-1.6,1.1-3.2,2.1-4.8,3.1 c-78.6,50.8-163.2,79.6-252.2,87.5c-1.3,0.1-2.6,0.3-3.9,0.3c-62.9,5.2-128.1,0.1-194.7-15c-2.6-0.5-5.1-1.3-7.6-1.8 c-6.9-1.7-12.9-3.5-18.1-5.4c-21.9-7.6-30.5-16.7-27.4-31.5c0-0.2,0-0.3,0.1-0.5c0,0,0-0.1,0.1-0.1c1.9-10.4,2.8-22.6,15.9-40.4 c12.6-16.6,27-28.6,42.5-39.8c0.2-0.1,0.3-0.3,0.5-0.4c11.7-11.1,24.1-21.2,37.1-30.6c38.8-28.3,82.4-50.4,126.6-71.9 c6.8-3.5,14.1-6.5,21.1-10c1.5-0.6,2.9-1.3,4.2-2.1c1.5-0.8,2.7-2,3.9-3.2c1.5-1.6,1.8-4,0.8-6c0,0,0-0.1,0-0.1 c-1.3-2.4-4.2-3.5-6.7-2.4c-5.7,2.4-11.4,4.8-17.1,7.2c-44.8,18.9-89.7,37.5-134.1,57.5c-2.8,1.1-5.3,2.4-7.8,4 c-0.1,0.1-0.2,0.1-0.3,0.2c-17,8.4-32.9,19.4-49,29.2c-30.9,19.6-51.8,43.2-65.8,69.3c-0.2,0.4-0.3,0.6-0.5,0.9c0,0-0.1,0-0.1,0.1 c-4.3,8.2-7.9,16.6-11,25.3c-5,14.3-8.4,29.2-10.7,44.6c-0.6,4-5,5.6-8,3.7c-0.6-0.4-1.2-0.9-1.6-1.6c-1.7-2.6-3.3-5.2-4.7-7.8 c-12-20.9-15.6-40.1-16.4-53.1c-0.6-9,0.2-15,0.4-16.6c0.2-0.3,0.2-0.3,0.2-0.3c5.3-22.6,11.8-44.6,20-66.2 c1.8-4.8,3.6-9.5,5.6-14.2c16.2-39.7,37.4-77.7,64.6-113.9c20.5-27.4,44.3-53.7,71.7-78.8c68.2-62.1,154.5-94.2,245-115.4 c23.8-5.6,47.6-11,71.5-16.2c35.7-7.9,71.5-15.5,107.2-23.3c14.9-3.2,29.8-6.5,44.7-9.8c2.5-0.5,5.1-1.1,7.6-1.7 c25.5-5.6,51-11.3,76.4-17.4c12.9-3.1,25.3-6.8,37.1-11.3c39-14.7,71.5-37.1,94.8-69.6c9.9-13.8,18.2-29.5,24.5-47.2 c0.8-2.3,3.1-3.7,5.5-3.5c0.4,0,0.8,0.1,1.1,0.1c2.6,0.2,4.6,2.4,4.8,5c0.2,3.9,0.4,7.7,0.7,11.6 C941.9,296.4,947.5,335.9,942.7,373.9z"/></svg>
							<Link className="brand" activeClassName="active" to={'/stations/'+stationId}>
								<span>Aur</span>
								<svg version="1.1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" height="14px" width="14px" viewBox="0 0 1000 1000" enableBackground="new 0 0 1000 1000"><path opacity="1" fill="#FFFFFF" d="M786,226c-14.6-44.9-36.1-83.2-64.5-114.9S657.9,54.7,616.2,37c-41.8-17.7-89.5-26.6-143.3-26.6 c-74.7,0-142.7,12.8-204,38.3c-61.4,25.5-118.6,64.1-171.8,115.7l30.4,53.3c5,8.7,11.9,16.1,20.4,22c8.5,5.9,18.5,8.9,29.9,8.9 c14.5,0,29.2-5.2,44.1-15.7c14.9-10.4,32.5-22.1,53.1-35.1c20.6-13,44.9-24.7,73.1-35.1c28.1-10.4,62.8-15.7,103.9-15.7 c62.6,0,109.9,19.3,141.9,57.8c31.4,37.9,47.4,93.5,47.9,166.9c20.6-4.5,41.2-8.9,61.7-13.5c2.9-0.6,5.8-1.2,8.7-1.9 c29-6.4,58-12.9,86.9-19.8c2.5-0.6,5-1.2,7.4-1.9C803.7,295.9,796.9,259.7,786,226z M779.5,767.4c-2.5,2.5-5.1,5-7.8,7.3 c-33.8,31.8-71.1,61.1-111.8,88.2c-0.2,0.3-0.7,0.4-1,0.7c-1.2,0.8-2.4,1.6-3.6,2.3c0.1,0.4,0.2,0.7,0.3,1.1l17.5,82.5 c3.1,16.5,9.8,27.3,19.9,32.4c10.1,5.1,23.4,7.6,39.9,7.6h75V738.8C798.7,748.5,789.2,758.1,779.5,767.4z"/><path fill="#FFFFFF" d="M942.7,373.9c-15.7,122.8-61.6,229-134.8,318.5c-16.8,20.5-34.9,40-54.5,58.7c-2.2,2.2-4.5,4.4-6.9,6.4 c-29.7,27.9-62.6,53.7-98.3,77.6c-0.2,0.3-0.7,0.3-0.9,0.6c-0.6,0.4-1.1,0.8-1.7,1.1c-1.6,1.1-3.2,2.1-4.8,3.1 c-78.6,50.8-163.2,79.6-252.2,87.5c-1.3,0.1-2.6,0.3-3.9,0.3c-62.9,5.2-128.1,0.1-194.7-15c-2.6-0.5-5.1-1.3-7.6-1.8 c-6.9-1.7-12.9-3.5-18.1-5.4c-21.9-7.6-30.5-16.7-27.4-31.5c0-0.2,0-0.3,0.1-0.5c0,0,0-0.1,0.1-0.1c1.9-10.4,2.8-22.6,15.9-40.4 c12.6-16.6,27-28.6,42.5-39.8c0.2-0.1,0.3-0.3,0.5-0.4c11.7-11.1,24.1-21.2,37.1-30.6c38.8-28.3,82.4-50.4,126.6-71.9 c6.8-3.5,14.1-6.5,21.1-10c1.5-0.6,2.9-1.3,4.2-2.1c1.5-0.8,2.7-2,3.9-3.2c1.5-1.6,1.8-4,0.8-6c0,0,0-0.1,0-0.1 c-1.3-2.4-4.2-3.5-6.7-2.4c-5.7,2.4-11.4,4.8-17.1,7.2c-44.8,18.9-89.7,37.5-134.1,57.5c-2.8,1.1-5.3,2.4-7.8,4 c-0.1,0.1-0.2,0.1-0.3,0.2c-17,8.4-32.9,19.4-49,29.2c-30.9,19.6-51.8,43.2-65.8,69.3c-0.2,0.4-0.3,0.6-0.5,0.9c0,0-0.1,0-0.1,0.1 c-4.3,8.2-7.9,16.6-11,25.3c-5,14.3-8.4,29.2-10.7,44.6c-0.6,4-5,5.6-8,3.7c-0.6-0.4-1.2-0.9-1.6-1.6c-1.7-2.6-3.3-5.2-4.7-7.8 c-12-20.9-15.6-40.1-16.4-53.1c-0.6-9,0.2-15,0.4-16.6c0.2-0.3,0.2-0.3,0.2-0.3c5.3-22.6,11.8-44.6,20-66.2 c1.8-4.8,3.6-9.5,5.6-14.2c16.2-39.7,37.4-77.7,64.6-113.9c20.5-27.4,44.3-53.7,71.7-78.8c68.2-62.1,154.5-94.2,245-115.4 c23.8-5.6,47.6-11,71.5-16.2c35.7-7.9,71.5-15.5,107.2-23.3c14.9-3.2,29.8-6.5,44.7-9.8c2.5-0.5,5.1-1.1,7.6-1.7 c25.5-5.6,51-11.3,76.4-17.4c12.9-3.1,25.3-6.8,37.1-11.3c39-14.7,71.5-37.1,94.8-69.6c9.9-13.8,18.2-29.5,24.5-47.2 c0.8-2.3,3.1-3.7,5.5-3.5c0.4,0,0.8,0.1,1.1,0.1c2.6,0.2,4.6,2.4,4.8,5c0.2,3.9,0.4,7.7,0.7,11.6 C941.9,296.4,947.5,335.9,942.7,373.9z"/></svg>
								<span>ssure<sup>®</sup></span>
							</Link>
							<div className="navigation-links">
								<Link to={'/stations/'+stationId} className={'link' + ((this.props.active === 'dashboard') ? ' active': '')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329 329" height="24" width="24">
										<path fill="#fff" d="M329 240V21.5H0V240h139.3v31.2h-50L75 307.4h179l-14.5-36.2h-50V240zM29.5 52.3h269.7V209H29.6V52.4z"/>
										<rect className="fill" fill="none" width="249.5" height="135.2" x="40.4" y="62.7"/>
									</svg>
									<label>Dashboard</label>
								</Link>
								<Link to={'/stations/'+stationId+'/analytics'} className={'link' + ((this.props.active === 'analytics') ? ' active': '')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 425 503" height="24" width="24">
										<g transform="translate(-33.568 -534.928)">
											<rect className="fill" width="425" height="503" x="32.6" y="534.5" fill="none" ry="20" rx="20"/>
											<g className="unfill" fill="#fff" stroke="#fff" strokeWidth="20" strokeLinecap="round">
												<path className="inactive" d="M451 666.3v-.5l-.2-.4c0-.2 0-.3-.2-.5v-1l-.3-1s0-1-.2-1v-2l-97-115v-1l-.2-.6h-.3l-.7-.6-1-.3h-.3l-1-.7h-1.7c-.2-.7-.4-.7-.7-.7H51c-2.5 0-5 1-7 3s-3 4.8-3 7v468c0 6 4.6 10 10 10h390c5.6 0 10-4 10-10V666zm-96.8-85.8l65.3 77.6h-65.3zm77 429.8H61v-448l273 .8v105c0 6 4.4 10 10 10l87 1z"/>
												<path d="M290.8 759.4H147v-15l29.2-34 30 30c1.5 1.5 3.6 2 5.5 1.6 2-.5 3.6-2 4-4l14.3-47 22 26c1 1.6 3 2.4 5 2.2 2-.3 3.6-1.4 4.4-3.2l28.8-57.4c1.4-3 .2-6.3-2.6-7.7-2.8-2-6.3-1-7.7 2l-25 50-23-28c-2-2-4-3-6-2-2 0-4 2-5 4l-14 48-28-28c-2-2-3-2-5-2s-3 1-4 2l-24 29V610c0-3-2.6-5.7-5.8-5.7-3 0-5.8 2.6-5.8 5.7v149.4h-11c-3 0-5.6 2.6-5.6 5.8 0 3 3 5.7 6 5.7h12v5c0 3 3 5 6 5s6-3 6-6v-5h143c3 0 6-3 6-6s-2.4-6-5.5-6z"/>
												<path strokeWidth="50" d="M136.2 852.4h219.2zm.8 88.3h219z" strokeLinejoin="round"/>
											</g>
										</g>
									</svg>
									<label>Analytics</label>
								</Link>
								<Link to={'/compare'} className={'link' + ((this.props.active === 'compare') ? ' active': '')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 120 130" height="24" width="24">
										<g stroke="#fff">
											<path className="fill" fill="none" strokeWidth="13.2" d="M6.6 6.3h63.7v74H6.6z"/>
											<path className="unfill" fill="#fff" strokeWidth="2" d="M19 23.6v7.2h24.5v6L57.2 27l-13.7-9.6v6.2z"/>
											<path className="fill" fill="none" strokeWidth="13.2" d="M113.4 49H49.7v74h63.7z"/>
											<path className="unfill" fill="#fff" strokeWidth="2" d="M101 98.5v7.2H76.2v6L62.6 102l13.7-9.7v6.2z"/>
										</g>
									</svg>
									<label>Compare</label>
								</Link>
								<Link to={'/archive'} className={'link' + ((this.props.active === 'archive') ? ' active': '')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" height="24" width="24">
										<g stroke="#fff" strokeLinejoin="round">
											<path className="fill" fill="none" strokeWidth="2.9" d="M46.4 32.8l.3-18.5L34.2 2.6 3 2.4v54h29.7V32.7z" strokeLinecap="round"/>
											<path className="unfill" fill="none" strokeWidth="3.5" d="M11 35.6h15zm0 11.6h15zM11 24h29zm0-11.5h20.7z"/>
											<rect className="fill" fill="none" width="20.6" height="20.6" x="38.1" y="38.1" strokeWidth="1.2" rx=".6" ry=".6" strokeLinecap="round"/>
											<path className="unfill" fill="#fff" strokeWidth="1.2" d="M38 38v20.7h20.7V38zm19 19H39.8V39.8h7.7v10.8L44 47l-1.4 1 5.7 5.7L54 48l-1-1-3.7 3.6V39.8H57z" strokeLinecap="round"/>
										</g>
									</svg>
									<label>Archive</label>
								</Link>
								<Link to={'/device-config'} className={'link' + ((this.props.active === 'configure') ? ' active': '')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 956 956" height="24" width="24">
										<path className="fill" fill="none" stroke="#FFF" strokeWidth="60" d="M893.2 412.8L804.5 398c-6.5-26.4-16-51.6-28-75l56.7-70.6c12-15 11.5-36.3-1.2-50.7L796.7 162c-12.8-14.4-34-17.4-50.2-7.2l-76 47.7c-33.3-23.3-71-40.5-112-50.5l-14.8-89.2c-3-19-19.4-32.8-38.6-32.8h-53c-19 0-35 14-38 32.8L399 152c-33.7 8.3-65.3 21.6-94.2 39.2l-72-51.6c-15.6-11-37-9.3-50.6 4.2l-38 37.6c-13.5 13.6-15.2 35-4 50.5l51.5 72c-17.4 28-30.5 60-38.8 93l-90 15c-19 3-33 19-33 38v54c0 19 14 35.3 32.2 38.4l89.8 15c6.8 27.4 16.6 54 29.7 78l-56.6 70c-12 15-11.3 36.4 1.5 50.8l35 39.7c13 15 35 18 51 8l77-48.6c32.3 22.8 69.3 39 109 48.8l15 89.3c3 19 19 32.6 38.3 32.6h53c19.6 0 36-14 39-33l15-89c33.4-8.2 64.6-21.3 93-38.5l75 54c15 11.2 37 9.4 50-4.2l37.6-37.5c13.4-13.7 15.3-35 4-50.7L765 652c17.5-29 30.8-60.8 39-94l88.8-15c19-3 32.6-19.6 32.6-38.8V451c0-19-14-35.4-33-38.5zM479.4 646c-92.7 0-168-75.3-168-168 0-92.8 75.3-168 168-168 92.8 0 168 75.2 168 168s-75.2 168-168 168z" strokeLinejoin="round"/>
									</svg>
									<label>Configure</label>
								</Link>
								{(() => {
									if ((city_name === 'admin') || (city_name === 'dev') || (city_name === '127')) {
										let link_access = [];
										link_access.push(<Link to={'/debug/'+stationId} className={'link' + ((this.props.active === 'debug') ? ' active': '')}>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 286.1 286.1" height="24" width="24">
												<circle className="fill" cx="143.1" cy="143.1" r="130.6" fill="none" stroke="#fff" strokeWidth="20" strokeLinecap="round" strokeLinejoin="round"/>
												<path className="unfill" fill="#fff" stroke="#fff" strokeWidth="10" d="M153.5 200.7h-20V219h20zm-1-136.3h-17.7V169h17.6z" strokeLinejoin="round" strokeLinecap="round"/>
											</svg>
											<label>Debug</label>
										</Link>);
										link_access.push(<Link to={'/settings'} className={'link' + ((this.props.active === 'settings') ? ' active' : '')}>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 956 956" height="24" width="24">
												<path className="fill" fill="none" stroke="#FFF" strokeWidth="60" d="M893.2 412.8L804.5 398c-6.5-26.4-16-51.6-28-75l56.7-70.6c12-15 11.5-36.3-1.2-50.7L796.7 162c-12.8-14.4-34-17.4-50.2-7.2l-76 47.7c-33.3-23.3-71-40.5-112-50.5l-14.8-89.2c-3-19-19.4-32.8-38.6-32.8h-53c-19 0-35 14-38 32.8L399 152c-33.7 8.3-65.3 21.6-94.2 39.2l-72-51.6c-15.6-11-37-9.3-50.6 4.2l-38 37.6c-13.5 13.6-15.2 35-4 50.5l51.5 72c-17.4 28-30.5 60-38.8 93l-90 15c-19 3-33 19-33 38v54c0 19 14 35.3 32.2 38.4l89.8 15c6.8 27.4 16.6 54 29.7 78l-56.6 70c-12 15-11.3 36.4 1.5 50.8l35 39.7c13 15 35 18 51 8l77-48.6c32.3 22.8 69.3 39 109 48.8l15 89.3c3 19 19 32.6 38.3 32.6h53c19.6 0 36-14 39-33l15-89c33.4-8.2 64.6-21.3 93-38.5l75 54c15 11.2 37 9.4 50-4.2l37.6-37.5c13.4-13.7 15.3-35 4-50.7L765 652c17.5-29 30.8-60.8 39-94l88.8-15c19-3 32.6-19.6 32.6-38.8V451c0-19-14-35.4-33-38.5zM479.4 646c-92.7 0-168-75.3-168-168 0-92.8 75.3-168 168-168 92.8 0 168 75.2 168 168s-75.2 168-168 168z" strokeLinejoin="round"/>
											</svg>
											<label>Settings</label>
										</Link>);
										{/*link_access.push(<Link to={'/alert-settings'} className={'link' + ((this.props.active === 'alert-settings') ? ' active' : '')}>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 459.3 459.3" height="24" width="24">
												<path className="fill" stroke="#fff" strokeWidth="40" fill="none" d="M175.216 404.514c-.001.12-.009.239-.009.359 0 30.078 24.383 54.461 54.461 54.461s54.461-24.383 54.461-54.461c0-.12-.008-.239-.009-.359H175.216zM403.549 336.438l-49.015-72.002c0-22.041 0-75.898 0-89.83 0-60.581-43.144-111.079-100.381-122.459V24.485C254.152 10.963 243.19 0 229.667 0s-24.485 10.963-24.485 24.485v27.663c-57.237 11.381-100.381 61.879-100.381 122.459 0 23.716 0 76.084 0 89.83l-49.015 72.002c-5.163 7.584-5.709 17.401-1.419 25.511 4.29 8.11 12.712 13.182 21.887 13.182H383.08c9.175 0 17.597-5.073 21.887-13.182C409.258 353.839 408.711 344.022 403.549 336.438z"/>
											</svg>
											<label>Alerts</label>
										</Link>);*/}
										link_access.push(<Link to={'/groups'} className={'link grp-ct' + ((this.props.active === 'groups') ? ' active' : '')}>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.246 62.246" height="24" width="24" fill="#fff">
												<path className="fill" stroke="#fff" d="M57.548 45.107H19.965a4.7 4.7 0 0 0 0 9.4h37.583a4.7 4.7 0 0 0 0-9.4zM57.548 26.402H19.965a4.7 4.7 0 0 0 0 9.399h37.583a4.699 4.699 0 0 0 0-9.399zM19.965 17.096h37.583a4.7 4.7 0 0 0 0-9.399H19.965a4.7 4.7 0 0 0 0 9.399z"/><circle cx="4.77" cy="12.439" r="4.77"/><circle cx="4.77" cy="31.102" r="4.769"/><circle cx="4.77" cy="49.807" r="4.77"/>
											</svg>
											<label>Groups & Cities</label>
										</Link>);
										link_access.push(<Link to={'/dmd-list'} className={'link' + ((this.props.active === 'dmd') ? ' active' : '')}>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.246 62.246" height="24" width="24" fill="#fff">
												<path className="fill" stroke="#fff" d="M57.548 45.107H19.965a4.7 4.7 0 0 0 0 9.4h37.583a4.7 4.7 0 0 0 0-9.4zM57.548 26.402H19.965a4.7 4.7 0 0 0 0 9.399h37.583a4.699 4.699 0 0 0 0-9.399zM19.965 17.096h37.583a4.7 4.7 0 0 0 0-9.399H19.965a4.7 4.7 0 0 0 0 9.399z"/><circle cx="4.77" cy="12.439" r="4.77"/><circle cx="4.77" cy="31.102" r="4.769"/><circle cx="4.77" cy="49.807" r="4.77"/>
											</svg>
											<label>DMD</label>
										</Link>);
										link_access.push(<div ref="notificationdropdownarea" className={'link notification' + ((this.props.activeLink === 'notifications') ? ' active' : (this.state.notification_popup) ? ' active' : '')} onClick={() => this.toggleNotificationMenu()}>
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 520" height="24" width="24">
												<path className="fill" fill="none" stroke="#FFF" strokeWidth="40" d="M255 510c28 0 51-23 51-51H204c0 28 23 51 51 51zm165.8-153V216.7c0-79-53.6-142.8-127.6-160.6V38.5C293.2 18 275.4 0 255 0c-20.4 0-38.3 18-38.3 38.3V56c-74 18-127.4 81.8-127.4 161v140l-51 51v25.5h433.4V408l-51-51z"/>
											</svg>
											{(() => {
												if(this.state.notifications_count){
													return <span className="badge-count">
														{this.state.notifications_count}
													</span>;
												}
											})()}
											<div className="dropdown-label-notification">Notifications</div>
											{(() => {
												if (this.state.notification_popup) {
													return <div className="notification-list">
														{(() => {
															if (this.state.notifications && this.state.notifications.length) {
																let notifications = this.state.notifications.map((elem, ind) => {
																	//console.log('elem',elem);
																	return <div className="notification-item">
																		<div className={'station-name icon-circle hellip ' + (elem.type === 'very poor' ? 'very-poor' : (elem.type))} dangerouslySetInnerHTML={{__html: elem.text}} />
																		<small className="padding-left">{moment.unix(elem.time).tz('Asia/Kolkata').format('DD-MM-YYYY, HH:mm')}</small>
																	</div>;
																});
																return <div className="notifications">
																	{(() => {
																		if (this.state.data_loading) {
																			return <div className="inline-loading notification-loading">
																				<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
																			</div>;
																		}else{
																			return <Scrollbars autoHide>
																				{notifications}
																			</Scrollbars>;
																		}
																	})()}
																</div>;
															}else if(this.state.notifications && !this.state.notifications.length){
																return <div className="inline-loading notification-loading">
																		<div className="no-notifications-text">No Notifications Found</div>
																</div>;
															}else{
																return <div className="inline-loading notification-loading">
																	<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
																</div>;
															}
														})()}
														<Link to={'/notifications'} className="view-all">View all</Link>
													</div>;
												}
											})()}
										</div>);
										return link_access;
									}
								})()}
								<Link to={'/users'} className={'link' + ((this.props.active === 'users') ? ' active': '')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="24" width="24">
										<path className="fill" fill="none" stroke="#FFF" strokeWidth="40" d="M52 347l37-52v-82.4L52 165l22.2-38.4 66.8 8 65-37 25.6-62H276l23.4 60 69.4 34.6 64.6-3.6 22.2 38.5-39 52 2 84.8 37 45.3-22.2 38.5-66.8-10-67.2 36.8-23.4 64h-44.4l-27.7-64-63-37-67 10.2z" strokeLinejoin="round"/>
										<ellipse className="unfill" cx="253" cy="181.2" fill="#FFF" stroke="#FFF" strokeWidth="32.7" rx="33.7" ry="33.7" strokeLinejoin="round"/>
										<rect className="unfill" width="152.2" height="89.5" x="175.7" y="260.3" fill="#FFF" stroke="#FFF" strokeWidth="40" rx="76.1" ry="44.8" strokeLinecap="round" strokeLinejoin="round"/>
									</svg>
									<label>Manage</label>
								</Link>
								<div className="link aaa" onClick={(e) => this.setState({about_modal: true})}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 92 92" height="24" width="24"><path fill="#FFFFFF" d="M45.386,0.004C19.983,0.344-0.333,21.215,0.005,46.619c0.34,25.393,21.209,45.715,46.611,45.377 c25.398-0.342,45.718-21.213,45.38-46.615C91.656,19.986,70.786-0.335,45.386,0.004z M45.25,74l-0.254-0.004 c-3.912-0.116-6.67-2.998-6.559-6.852c0.109-3.788,2.934-6.538,6.717-6.538l0.227,0.004c4.021,0.119,6.748,2.972,6.635,6.937 C51.904,71.346,49.123,74,45.25,74z M61.705,41.341c-0.92,1.307-2.943,2.93-5.492,4.916l-2.807,1.938 c-1.541,1.198-2.471,2.325-2.82,3.434c-0.275,0.873-0.41,1.104-0.434,2.88l-0.004,0.451H39.43l0.031-0.907 c0.131-3.728,0.223-5.921,1.768-7.733c2.424-2.846,7.771-6.289,7.998-6.435c0.766-0.577,1.412-1.234,1.893-1.936 c1.125-1.551,1.623-2.772,1.623-3.972c0-1.665-0.494-3.205-1.471-4.576c-0.939-1.323-2.723-1.993-5.303-1.993 c-2.559,0-4.311,0.812-5.359,2.478c-1.078,1.713-1.623,3.512-1.623,5.35v0.457H27.936l0.02-0.477 c0.285-6.769,2.701-11.643,7.178-14.487C37.947,18.918,41.447,18,45.531,18c5.346,0,9.859,1.299,13.412,3.861 c3.6,2.596,5.426,6.484,5.426,11.556C64.369,36.254,63.473,38.919,61.705,41.341z"/></svg>
									<label>Help</label>
								</div>
							</div>
						</div>
						<div className={'navbar-menu' + ((this.state.menu_opened) ? ' active' : '')} onClick={() => this.toggleMenu()} >
							<span className="dropdown-toggle">{user_name.charAt(0).toUpperCase()}</span>
							<div className="clearfix"></div>
							<div className="dropdown-label">Me▾</div>
							<div className="dropdown-menu" onClick={(e) => {e.stopPropagation();}}>
								<div className="username">
									<div>{user_name}</div>
								</div>
								<a href="##PR_STRING_REPLACE_ACCOUNT_BASE_PATH##/change-password" target="_blank">Change Password</a>
								<a href="##PR_STRING_REPLACE_ACCOUNT_BASE_PATH##/logout">Logout</a>

							</div>
						</div>
					</div>
				</nav>
				{(() => {
					if (this.state.about_modal) {
						return <div className="modal">
							<div className="modal-dialog">
								<div className="modal-content">
									<div className="modal-header modal-header-about">
										<button type="button" className="close" onClick={() => this.setState({about_modal: false})}>×</button>
										<h4 className="modal-title-add">About Aurassure</h4>
									</div>
									<div className="modal-body about-modal-body">
										<div className="modal-body-about-logo">
											<svg className="logo" height="80" width="80" xmlns="http://www.w3.org/2000/svg" data-name="Layer 1" viewBox="0 0 94 102">
												<defs>
												<linearGradient id="a" x1="1.36" x2="92.74" y1="63.11" y2="63.11" gradientUnits="userSpaceOnUse">
													<stop offset="0%" stopColor="#2598ac"/>
													<stop offset="12%" stopColor="#2c9ea8"/>
													<stop offset="68%" stopColor="#49b896"/>
													<stop offset="100%" stopColor="#54c290"/>
												</linearGradient>
												</defs>
												<path fill="url(#a)" d="M93 29a73 73 0 0 1-11 34l-6 8-3 3a73 73 0 0 1-53 23H6a45 45 0 0 1 41-34h-3A45 45 0 0 0 1 92a45 45 0 0 1 45-46 73 73 0 0 0 47-17z"/>
												<path fill="#d7dee0" d="M62 87l2 10 2 3h12V73a74 74 0 0 1-16 14zm16-51a48 48 0 0 0-2-13 32 32 0 0 0-7-12 30 30 0 0 0-11-7 37 37 0 0 0-15-3 54 54 0 0 0-21 4A56 56 0 0 0 5 17l3 6 2 2h3l5-2 5-4 8-4 11-2q10 0 15 6t5 17v4z"/>
											</svg>
										</div>
										<div className="modal-body-content">
											<div className="modal-body-content-title">Aurassure</div>
											<div className="modal-body-content-version">Version 2.0.0</div>
										</div>
										<div className="modal-user-manual">
											<a href="https://aurassure.com/help/" target="_blank" className="user-manual">User Manual
											</a>
										</div>
									</div>
									<div className="modal-footer about-modal-footer">
										<div className="about-modal-copyright">© 2018 <a href="https://phoenixrobotix.com" target="_blank">Phoenix Robotix Pvt. Ltd.</a></div>
									</div>
								</div>
							</div>
						</div>;
					}
				})()}
			</div>
		);
	}
}

NavLink.contextTypes = {
	router: React.PropTypes.object
};
