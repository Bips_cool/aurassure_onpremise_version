import React from 'react';
import { findDOMNode } from 'react-dom';
import NavLink from './NavLink';
import { Scrollbars } from 'react-custom-scrollbars';
import io from 'socket.io-client';
import ReactTooltip from 'react-tooltip';
import { Link } from 'react-router';
import MultiSelect from './imports/MultiSelect';
import ClipboardButton from 'react-clipboard.js';

/**
 * This Class Used for Details of devices.
 */
export default class DeviceConfigDetails extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			is_admin: document.getElementById('city_id') ? (document.getElementById('city_id').value == 5 ? true : false) : false,
			device_id: props.params.device_id ? props.params.device_id : 1,
			device_auth_token: null,
			location_name: null,
			location_type: null,
			all_cities: null,
			city_id: null,
			lat: null,
			long: null,
			device_mac_id: null,
			station_full_address: null,
			device_firmware_versions: ['1.0.0'],
			device_qr_code: null,
			device_firmware_version: null,
			iot_board_versions: ['1.0.0'],
			iot_board_qr_code: null,
			iot_board_version: null,
			shield_versions: ['1.0.0'],
			shield_qr_code: null,
			shield_version: null,
			sensor_board_versions: ['1.0.0'],
			sensor_board_qr_code: null,
			sensor_board_version: null,
			sensor_no: null,
			sim_mobile_no: null,
			sim_sl_no: null,
			device_archive_status: false,
			connectivity_gprs: false,
			connectivity_wifi: false,
			connectivity_ethernet: false,
			publicly_accessible: null,
			enable_data_push: null,
			data_loading: 1,
			saving_status: null,
			status: null,
			all_locations: {
				1 : 'Infra',
				2 : 'Aqua',
				3 : 'Weather'
			},
			group_placeholder: 'Please select a group',
		};
		/**
		 * This binding the function after success.
		 */
		this.onSuccess = this.onSuccess.bind(this);
		/**
		 * This binding the function to get text.
		 */
		this.getText = this.getText.bind(this);
		/**
		 * This is used for City name.
		 * @type {string}
		 */
		this.city_name = window.location.host.split('.')[0];
		this.accept_scanned_qr_code= false;
		this.insert_qr_code_into= null
	}

	/**
	 * This function calls on success of the clipboard button and generate the success tooltip.
	 */
	onSuccess() {
		console.info('successfully coppied');
		let that = this,
			tooltip = findDOMNode(that.refs.copy);
		tooltip.setAttribute("data-tip", 'Copied!');
		// tooltip.setAttribute("data-tip-disable", false);
		ReactTooltip.hide(tooltip);
		ReactTooltip.rebuild();
		ReactTooltip.show(tooltip);
	}

	/**
	 * This function calls onMouseLeave and used for tooltip regeneration.
	 */
	removeTooltip() {
		findDOMNode(this.refs.copy).setAttribute("data-tip", 'Click to copy to clipboard.');
		// findDOMNode(this.refs.copy).setAttribute("data-tip-disable", true);
		ReactTooltip.rebuild();
	}

	/**
	 * This function uses for getting text for clipboard.
	 */
	getText() {
		return this.state.device_auth_token;
	}

	/**
	 * This enables the QR code fields accept key names.
	 * @param  {string} key This is the key of the qr input field.
	 */
	enableQrCodeAcception(key) {
		this.accept_scanned_qr_code = true;
		this.insert_qr_code_into = key;
		console.log('Focus In', key);
	}

	/**
	 * This disables the QR code fields and sets qr_code to null.
	 */
	disableQrCodeAcception() {
		this.accept_scanned_qr_code = false;
		this.insert_qr_code_into = null;
		console.log('Focus Out');
	}

	/*getQrCode() {
		this.socket.emit('receive_qr_code_from_scanner', JSON.stringify({
			qr_code: 'a1'
		}));
	}*/

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 */
	componentDidUpdate() {
		ReactTooltip.rebuild();
	}

	getGroupIds(values) {
		let that = this;
		console.log('values stations', values);
		
		this.setState({
			groups_selected: values
		});
		console.log('groups_select', that.state.groups_select);
		console.log('groups_selected', that.state.groups_selected);
	}

	setGroupList(){
		let that = this,
			groups_selected = that.state.groups_select,
			all_group_options = this.state.all_groups;
		console.log('station_lists', all_group_options);
		that.setState({
			all_group_options: all_group_options,
			groups_selected: groups_selected
		}, () => {
			that.resetSelectedStations();
		});
	}

	resetSelectedStations() {
		let that = this,
			selected = [];
		if (that.state.groups_selected) {
			that.state.groups_selected.map((id) => {
				Object.keys(that.state.all_groups).map((group_id) => {
					if(group_id == id && selected.indexOf(id) === -1) {
						selected.push(id);
					}
				});
			});
		}
		console.log('selected', selected);
		that.setState({
			groups_selected: selected
		});
	}

	/**
	 * This function used for saving details of the device.
	 */
	saveDeviceDetails() {
		console.log('save_device_details', this.state);
		this.socket.emit('save_device_details', JSON.stringify({
			device_id: this.state.device_id,
			device_auth_token: this.state.device_auth_token,
			location_id: this.state.location_id,
			location_name: this.state.location_name,
			location_type: this.state.location_type,
			selected_groups: this.state.groups_selected,
			city_id: this.state.city_id,
			lat: this.state.lat,
			long: this.state.long,
			device_mac_id: this.state.device_mac_id,
			station_full_address: this.state.station_full_address,
			device_qr_code: this.state.device_qr_code,
			device_firmware_version: this.state.device_firmware_version,
			iot_board_qr_code: this.state.iot_board_qr_code,
			iot_board_version: this.state.iot_board_version,
			shield_qr_code: this.state.shield_qr_code,
			shield_version: this.state.shield_version,
			sensor_board_qr_code: this.state.sensor_board_qr_code,
			sensor_board_version: this.state.sensor_board_version,
			sensor_no: this.state.sensor_no,
			sim_operator: this.state.sim_operator,
			sim_sl_no: this.state.sim_sl_no,
			sim_mobile_no: this.state.sim_mobile_no,
			sim_type: this.state.sim_type,
			device_archive_status: this.state.device_archive_status,
			connectivity_gprs: this.state.connectivity_gprs,
			connectivity_wifi: this.state.connectivity_wifi,
			connectivity_ethernet: this.state.connectivity_ethernet,
			publicly_accessible: this.state.publicly_accessible,
			enable_data_push: this.state.enable_data_push
		}));
		this.setState({saving_status: 1});
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		document.title = 'Device Configuration - Aurassure';

		/**
		 * Socket is used to sync the data between client and server in real time.
		 */
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
			query: {
				s_token: this.readCookie('PHPSESSID')
			}
		});
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket');
			console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			this.socket.emit('get_device_details', JSON.stringify({
				device_id: this.state.device_id
			}));
		});

		this.socket.on('update_device_details', (payload) => {
			let device_details = JSON.parse(payload);
			console.log('update_device_details', device_details);
			if (device_details.device_id == this.state.device_id) {
				this.setState({
					device_id: device_details.device_id,
					device_auth_token: device_details.device_auth_token,
					location_id: device_details.location_id,
					location_name: device_details.location_name,
					location_type: device_details.location_type,
					all_cities: device_details.all_cities,
					all_groups: device_details.all_groups,
					groups_select: device_details.selected_groups,
					city_id: device_details.city_id,
					lat: device_details.lat,
					long: device_details.long,
					device_mac_id: device_details.device_mac_id,
					station_full_address: device_details.station_full_address,
					device_qr_code: device_details.device_qr_code,
					device_firmware_version: device_details.device_firmware_version === '' ? this.state.device_firmware_versions[0] : device_details.device_firmware_version,
					iot_board_qr_code: device_details.iot_board_qr_code,
					iot_board_version: device_details.iot_board_version === '' ? this.state.iot_board_versions[0] : device_details.iot_board_version,
					shield_qr_code: device_details.shield_qr_code,
					shield_version: device_details.shield_version === '' ? this.state.shield_versions[0] : device_details.shield_version,
					sensor_board_qr_code: device_details.sensor_board_qr_code,
					sensor_board_version: device_details.sensor_board_version === '' ? this.state.sensor_board_versions[0] : device_details.sensor_board_version,
					sensor_no: device_details.sensor_no,
					sim_operators: ['Airtel','BSNL','Vodafone','Custom'],
					sim_operator: device_details.sim_operator,
					sim_types: ['Prepaid','Postpaid'],
					sim_type: device_details.sim_type,
					sim_mobile_no: device_details.sim_mobile_no,
					sim_sl_no: device_details.sim_sl_no,
					device_archive_status: device_details.device_archive_status,
					connectivity_gprs: device_details.connectivity_gprs,
					connectivity_wifi: device_details.connectivity_wifi,
					connectivity_ethernet: device_details.connectivity_ethernet,
					publicly_accessible: device_details.publicly_accessible,
					enable_data_push: device_details.enable_data_push,
					data_loading: null,
					status: device_details.status
				}, () => {this.setGroupList()});
			}
		});

		this.socket.on('update_qr_code', (payload) => {
			console.log('update_qr_code', JSON.parse(payload));
			let qr_code = JSON.parse(payload).qr_code;
			if (this.accept_scanned_qr_code) {
				let state_object = {};
				state_object[this.insert_qr_code_into] = qr_code;
				this.setState(state_object);
			}
		});

		this.socket.on('device_details_saved', (payload) => {
			console.log('device_details_saved', JSON.parse(payload));
			if (JSON.parse(payload).device_id == this.state.device_id) {
				this.setState({saving_status: null});
				showPopup('success', 'Details saved successfully!');
			}
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			console.log('update_location_details_in_dashboard', JSON.parse(payload));
			if (JSON.parse(payload).device_id == this.state.device_id) {
				this.setState(JSON.parse(payload));
			}
		});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {
		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * This renders the Device Details Page.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container device-config-container">
				<NavLink active="configure" />
				<Scrollbars autoHide>
					<div className="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1 device-details-list">
						<h4 className="title-text">
							<Link to={'/device-config'} className="back-arrow pull-left">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 491.1 491.1" width="24" height="24"><path fill="#CCC" d="M111.85,208.25l192.8-192.8c20.6-20.6,54-20.6,74.6,0s20.6,54,0,74.6l-155.5,155.5l155.5,155.5c20.6,20.6,20.6,54,0,74.6 s-54,20.6-74.6,0l-192.8-192.8C91.25,262.25,91.25,228.85,111.85,208.25z"/></svg>
							</Link>
							<span className={'device-name' + ((this.state.status == "data_off") ? ' data-off' : ((this.state.status == "online") ? ' active' : ''))}>Device: {this.state.device_qr_code}</span>
						</h4>
						<hr className="separator" />
						<div className="tab-container">
							<Link to={'/device-config/' + this.state.device_id + '/details'} className="tab-item active">Details</Link>
							<Link to={'/device-config/' + this.state.device_id + '/configuration'} className="tab-item">Configuration</Link>
							<Link to={'/device-config/' + this.state.device_id + '/calibration'} className="tab-item">Calibration</Link>
						</div>
						<div className="panel panel-default">
							{(() => {
								if(this.state.data_loading) {
									return <div className="panel-body details-list text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
								}

								return <div className="panel-body details-list">
									<div className="row">
										{(() => {
											if (this.state.is_admin) {
												return <div className="form-group col-lg-11 col-md-11 col-sm-11 col-xs-12 form-config qr-code-input">
													<span>Device Auth. Token:&nbsp;&nbsp;</span>
													<span className="hellip device-auth-token" id="device_auth_token">{this.state.device_auth_token}</span>
													<span className="input-group-button-copy" onMouseLeave={() => this.removeTooltip()}>
														<ClipboardButton option-text={this.getText} onSuccess={this.onSuccess} ref="copy" data-tip="Click to copy to clipboard." wrapper="span" className="btn-tranparent-bg">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20px" height="20px"><path fill="none" d="M0 0h24v24H0z"/><path d="M16 1H4C3 1 2 2 2 3v14h2V3h12V1zm3 4H8C7 5 6 6 6 7v14c0 1 1 2 2 2h11c1 0 2-1 2-2V7c0-1-1-2-2-2zm0 16H8V7h11v14z"/></svg>
														</ClipboardButton>
													</span>
												</div>;
											}
										})()}
										<div className="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 form-config">
											<label>Device name</label>
											<input type="text" className="form-control" placeholder="Device Name" value={this.state.location_name} onChange={(e) => this.setState({location_name: e.target.value})} />
										</div>
										{(() => {
											if (this.state.is_admin) {
												return <div>
													<div className="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 form-config">
														<label>Device Type</label>
														<select className="form-control" value={this.state.location_type} onChange={(e) => this.setState({location_type: e.target.value})}>
															<option value={0} disabled selected={this.state.location_type == null ? true : false}> Select Device Type</option>
															{(() => {
																let options = Object.keys(this.state.all_locations).map((id) => {
																	return <option value={id}>{this.state.all_locations[id]}</option>;
																});
																return options;
															})()}
														</select>
													</div>
													<div className="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 form-config">
														<label>Group Name</label>
														<MultiSelect
															placeholder={this.state.group_placeholder}
															options={this.state.all_groups}
															selected={this.state.groups_selected}
															callback={(selected) => this.getGroupIds(selected)}
															item_type={'Groups'} />
													</div>
													<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
														<label>City Name</label>
														<select className="form-control" value={this.state.city_id} onChange={(e) => this.setState({city_id: e.target.value})}>
															<option value={0} disabled> Select City</option>
															{(() => {
																let options = Object.keys(this.state.all_cities).map((id) => {
																	return <option value={id}>{this.state.all_cities[id]}</option>;
																});
																return options;
															})()}
														</select>
													</div>
												</div>;
											}
										})()}
										<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
											<label>Device Latitude</label>
											<input type="text" className="form-control" placeholder="Device Location Lat." value={this.state.lat} onChange={(e) => this.setState({lat: e.target.value})} />
										</div>
										<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
											<label>Device Longitude</label>
											<input type="text" className="form-control" placeholder="Device Location Long." value={this.state.long} onChange={(e) => this.setState({long: e.target.value})} />
										</div>
										{(() => {
											if (this.state.is_admin) {
												return <div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 access-center form-config">
													<span className="access">Publicly Accessible</span>
													<div className="onoffswitch">
														<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id="public_access" defaultChecked={this.state.publicly_accessible} onChange={() => this.setState({publicly_accessible: !this.state.publicly_accessible})} />
														<label className="onoffswitch-label" htmlFor="public_access">
															<span className="onoffswitch-inner"></span>
															<span className="onoffswitch-switch"></span>
														</label>
													</div>
												</div>;
											}
										})()}
										{(() => {
											if (this.state.is_admin) {
												return <div className="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 access-center form-config data-push-toggle">
													<span className="access">{this.state.enable_data_push ? 'Disable Data Push' : 'Enable Data Push'}</span>
													<div className="onoffswitch">
														<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id="data_push" defaultChecked={this.state.enable_data_push} onChange={() => this.setState({enable_data_push: !this.state.enable_data_push})} />
														<label className="onoffswitch-label" htmlFor="data_push">
															<span className="onoffswitch-inner"></span>
															<span className="onoffswitch-switch"></span>
														</label>
													</div>
												</div>;
											}
										})()}
									</div>
									<div className="row">
										{(() => {
											if (this.state.is_admin) {
												return <div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 form-config">
													<label>MAC ID</label>
													<input type="text" className="form-control" placeholder="Device MAC ID" value={this.state.device_mac_id} onChange={(e) => this.setState({device_mac_id: e.target.value})} />
												</div>;
											}
										})()}
										<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 form-config">
											<label>Location Full Address</label>
											<input type="text" className="form-control" placeholder="Location Full Address" value={this.state.station_full_address} onChange={(e) => this.setState({station_full_address: e.target.value})} />
										</div>
										{(() => {
											if (this.state.is_admin) {
												return <div>
													<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 form-config">
														<label>Device QR Code</label>
														<input type="text" className="form-control qr-field" id="device_qr_code" value={this.state.device_qr_code} placeholder="Device QR Code." onChange={(e) => this.setState({device_qr_code: e.target.value})} onFocus={(e) => this.enableQrCodeAcception('device_qr_code')} onBlur={(e) => this.disableQrCodeAcception()}/>
														<label htmlFor="device_qr_code" className="qr-code-svg">
															<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 401.994 401.994">
																<path d="M0 401.991h182.724V219.265H0v182.726zm36.542-146.178h109.636v109.352H36.542V255.813z"/>
																<path d="M73.089 292.355h36.544v36.549H73.089zM292.352 365.449h36.553v36.545h-36.553zM365.442 365.449h36.552v36.545h-36.552z"/>
																<path d="M365.446 255.813h-36.542v-36.548H219.265v182.726h36.548V292.355h36.539v36.549h109.639V219.265h-36.545M0 182.728h182.724V0H0v182.728zM36.542 36.542h109.636v109.636H36.542V36.542z"/>
																<path d="M73.089 73.089h36.544v36.547H73.089zM219.265 0v182.728h182.729V0H219.265zm146.181 146.178H255.813V36.542h109.633v109.636z"/>
																<path d="M292.352 73.089h36.553v36.547h-36.553z"/>
															</svg>
														</label>
													</div>
													<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
														<label>Device Firmware Version</label>
														<select className="form-control" value={this.state.device_firmware_version ? this.state.device_firmware_version : 1} onChange={(e) => this.setState({device_firmware_version: e.target.value})}>
															<option value={0} disabled> Select Firmware Version </option>
															{(() => {
																let device_firmware_versions = [];
																if (this.state.device_firmware_versions) {
																	let options = this.state.device_firmware_versions.map((value) => {
																		return <option value={value}>{value}</option>;
																	});
																	return options;
																}
															})()}
														</select>
													</div>
													<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 form-config">
														<label>IoT Board QR Code</label>
														<input type="text" className="form-control" id="iot_board_qr_code" value={this.state.iot_board_qr_code} placeholder="IoT Board QR Code." onChange={(e) => this.setState({iot_board_qr_code: e.target.value})} onFocus={(e) => this.enableQrCodeAcception('iot_board_qr_code')} onBlur={(e) => this.disableQrCodeAcception()}/>
														<label htmlFor="iot_board_qr_code" className="qr-code-svg">
															<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 401.994 401.994">
																<path d="M0 401.991h182.724V219.265H0v182.726zm36.542-146.178h109.636v109.352H36.542V255.813z"/>
																<path d="M73.089 292.355h36.544v36.549H73.089zM292.352 365.449h36.553v36.545h-36.553zM365.442 365.449h36.552v36.545h-36.552z"/>
																<path d="M365.446 255.813h-36.542v-36.548H219.265v182.726h36.548V292.355h36.539v36.549h109.639V219.265h-36.545M0 182.728h182.724V0H0v182.728zM36.542 36.542h109.636v109.636H36.542V36.542z"/>
																<path d="M73.089 73.089h36.544v36.547H73.089zM219.265 0v182.728h182.729V0H219.265zm146.181 146.178H255.813V36.542h109.633v109.636z"/>
																<path d="M292.352 73.089h36.553v36.547h-36.553z"/>
															</svg>
														</label>
													</div>
													<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
														<label>IoT Board Circuit Version</label>
														<select className="form-control" value={this.state.iot_board_version ? this.state.iot_board_version : 1} onChange={(e) => this.setState({iot_board_version: e.target.value})}>
															<option value={0} disabled> Select Circuit Version </option>
															{(() => {
																let iot_board_versions = [];
																if (this.state.iot_board_versions) {
																	let options = this.state.iot_board_versions.map((value) => {
																		return <option value={value}>{value}</option>;
																	});
																	return options;
																}
															})()}
														</select>
													</div>
													{/*<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
														<button className="btn-right green-fill-btn btn-width" onClick={() => this.getQrCode()}>QR</button>
													</div>*/}
													<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 form-config">
														<label>Shield QR Code</label>
														<input type="text" className="form-control" id="shield_qr_code" value={this.state.shield_qr_code} placeholder="Shield QR Code." onChange={(e) => this.setState({shield_qr_code: e.target.value})} onFocus={(e) => this.enableQrCodeAcception('shield_qr_code')} onBlur={(e) => this.disableQrCodeAcception()}/>
														<label htmlFor="shield_qr_code" className="qr-code-svg">
															<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 401.994 401.994">
																<path d="M0 401.991h182.724V219.265H0v182.726zm36.542-146.178h109.636v109.352H36.542V255.813z"/>
																<path d="M73.089 292.355h36.544v36.549H73.089zM292.352 365.449h36.553v36.545h-36.553zM365.442 365.449h36.552v36.545h-36.552z"/>
																<path d="M365.446 255.813h-36.542v-36.548H219.265v182.726h36.548V292.355h36.539v36.549h109.639V219.265h-36.545M0 182.728h182.724V0H0v182.728zM36.542 36.542h109.636v109.636H36.542V36.542z"/>
																<path d="M73.089 73.089h36.544v36.547H73.089zM219.265 0v182.728h182.729V0H219.265zm146.181 146.178H255.813V36.542h109.633v109.636z"/>
																<path d="M292.352 73.089h36.553v36.547h-36.553z"/>
															</svg>
														</label>
													</div>
													<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
														<label>Shield Circuit Version</label>
														<select className="form-control" value={this.state.shield_version ? this.state.shield_version : 1} onChange={(e) => this.setState({shield_version: e.target.value})}>
															<option value={0} disabled> Select Circuit Version </option>
															{(() => {
																let shield_versions = [];
																if (this.state.shield_versions) {
																	let options = this.state.shield_versions.map((value) => {
																		return <option value={value}>{value}</option>;
																	});
																	return options;
																}
															})()}
														</select>
													</div>
													<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 form-config">
														<label>Sensor Board QR Code</label>
														<input type="text" className="form-control" id="sensor_board_qr_code" value={this.state.sensor_board_qr_code} placeholder="Sensor Board QR Code." onChange={(e) => this.setState({sensor_board_qr_code: e.target.value})} onFocus={(e) => this.enableQrCodeAcception('sensor_board_qr_code')} onBlur={(e) => this.disableQrCodeAcception()}/>
														<label htmlFor="sensor_board_qr_code" className="qr-code-svg">
															<svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 401.994 401.994">
																<path d="M0 401.991h182.724V219.265H0v182.726zm36.542-146.178h109.636v109.352H36.542V255.813z"/>
																<path d="M73.089 292.355h36.544v36.549H73.089zM292.352 365.449h36.553v36.545h-36.553zM365.442 365.449h36.552v36.545h-36.552z"/>
																<path d="M365.446 255.813h-36.542v-36.548H219.265v182.726h36.548V292.355h36.539v36.549h109.639V219.265h-36.545M0 182.728h182.724V0H0v182.728zM36.542 36.542h109.636v109.636H36.542V36.542z"/>
																<path d="M73.089 73.089h36.544v36.547H73.089zM219.265 0v182.728h182.729V0H219.265zm146.181 146.178H255.813V36.542h109.633v109.636z"/>
																<path d="M292.352 73.089h36.553v36.547h-36.553z"/>
															</svg>
														</label>
													</div>
													<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
														<label>Sensor Board Circuit Version</label>
														<select className="form-control" value={this.state.sensor_board_version ? this.state.sensor_board_version : 1} onChange={(e) => this.setState({sensor_board_version: e.target.value})}>
															<option value={0} disabled> Select Circuit Version </option>
															{(() => {
																let sensor_board_versions = [];
																if (this.state.sensor_board_versions) {
																	let options = this.state.sensor_board_versions.map((value) => {
																		return <option value={value}>{value}</option>;
																	});
																	return options;
																}
															})()}
														</select>
													</div>
													<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 form-config">
														<label>Sensor No.</label>
														<input type="text" className="form-control" value={this.state.sensor_no} placeholder="Sensor No." onChange={(e) => this.setState({sensor_no: e.target.value})}/>
													</div>
												</div>;
											}
										})()}
									</div>
									{(() => {
										if (this.state.is_admin) {
											return <div className="row">
												<div className="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12 form-config">
													<label>Sim Operator</label>
													<select className="form-control" value={this.state.sim_operator ? this.state.sim_operator : 0} onChange={(e) => this.setState({sim_operator: e.target.value})}>
														<option value={0} disabled>Select Operator</option>
														{(() => {
															let sim_operator = [];
															if (this.state.sim_operators) {
																let options = this.state.sim_operators.map((value) => {
																	return <option value={value}>{value}</option>;
																});
																return options;
															}
														})()}
													</select>
												</div>
												<div className="form-group col-lg-2 col-md-2 col-sm-2 col-xs-12 form-config">
													<label>Sim Type</label>
													<select className="form-control" value={this.state.sim_type ? this.state.sim_type : 0} onChange={(e) => this.setState({sim_type: e.target.value})}>
														<option value={0} disabled>Select Type</option>
														{(() => {
															let sim_types = [];
															if (this.state.sim_types) {
																let options = this.state.sim_types.map((value) => {
																	return <option value={value}>{value}</option>;
																});
																return options;
															}
														})()}
													</select>
												</div>
												<div className="form-group col-lg-3 col-md-3 col-sm-3 col-xs-12 form-config">
													<label>SIM Mobile No.</label>
													<input type="text" className="form-control" value={this.state.sim_mobile_no} placeholder="SIM Mobile No." onChange={(e) => this.setState({sim_mobile_no: e.target.value})}/>
												</div>
												<div className="form-group col-lg-5 col-md-5 col-sm-5 col-xs-12 form-config">
													<label>Sim Serial No.</label>
													<input type="text" className="form-control" value={this.state.sim_sl_no} placeholder="Sim Serial No." onChange={(e) => this.setState({sim_sl_no: e.target.value})}/>
												</div>
											</div>;
										}
									})()}
									{(() => {
										if (this.state.is_admin) {
											return <div className="row">
												<div className="col-lg-2 check-box-width">
													<div className="checkbox">
													<label className="check-box-label line-height">
														<input type="checkbox" name="checkbox" id="checkbox" checked={(this.state.connectivity_gprs === true) ? true : false} onChange={(e) => this.setState({connectivity_gprs: (e.target.checked === true) ? true : false})}/> GPRS
													</label>
													</div>
												</div>
												<div className="col-lg-2 check-box-width">
													<div className="checkbox">
													<label className="check-box-label line-height">
														<input type="checkbox" name="checkbox" id="checkbox" checked={(this.state.connectivity_wifi === true) ? true : false} onChange={(e) => this.setState({connectivity_wifi: (e.target.checked === true) ? true : false})}/> Wifi
													</label>
													</div>
												</div>
												<div className="col-lg-2 check-box-width">
													<div className="checkbox">
														<label className="check-box-label line-height">
														<input type="checkbox" name="checkbox" id="checkbox" checked={(this.state.connectivity_ethernet === true) ? true : false} onChange={(e) => this.setState({connectivity_ethernet: (e.target.checked === true) ? true : false})}/>Ethernet
														</label>
													</div>
												</div>
											</div>;
										}
									})()}
									{(() => {
										if (this.state.is_admin) {
											return <div className="row">
												<div className="col-lg-2 check-box-width">
													<div className="checkbox">
														<label className="check-box-label line-height">
														<input type="checkbox" name="checkbox" id="checkbox" checked={(this.state.device_archive_status === true) ? true : false} onChange={(e) => this.setState({device_archive_status: (e.target.checked === true) ? true : false})}/>Archive Station
														</label>
													</div>
												</div>
											</div>;
										}
									})()}
									{(() => {
										if (this.state.saving_status) {
											return <div className="in-btn-loading"><svg className="loading-spinner" width="16" height="16" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
										} else {
											return <button className="btn-right green-fill-btn btn-width" onClick={() => this.saveDeviceDetails()}>Save</button>;
										}
									})()}
								</div>;
							})()}
						</div>
					</div>
					<ReactTooltip effect="solid" />
				</Scrollbars>
				{/*(() => {
					if ((this.city_name === 'admin') || (this.city_name === '127')) {
					} else {
						return <h3><center>404 - Not Found</center></h3>;
					}
				})()*/}
			</div>
		);
	}
}

DeviceConfigDetails.contextTypes = {
	router: React.PropTypes.object
};