import React from 'react';
import moment from 'moment-timezone';
import ReactTooltip from 'react-tooltip';
import ReactHighcharts from 'react-highcharts';
import { Scrollbars } from 'react-custom-scrollbars';
import { SortableContainer, SortableElement } from 'react-sortable-hoc';

const SortableItem = SortableElement(({item}) => (
	<div className="compare-container">
		{(() => {
			if (item.type == '1' || item.type == null) {
				return <div className="compare-item">
					<Scrollbars autoHide>
						<div className="location-time-container">
							<div className="location-container">
								<div className="icon location"></div>
								<div className="location-name hellip" data-tip={item.name}>{item.name}</div>
								<button className="close" onClick={() => item.remove_station()}>✖</button>
							</div>
							<div className="data-source">Data Source: {item.source}</div>
							<div className="time-container">
								<div className={'icon clock' + ((item.connection_status === 'offline') ? ' offline' : '')}></div>
								<div className="time">{(parseInt(item.time)) ? moment.unix(item.time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm') : 'NA'}</div>
							</div>
						</div>
						<div className="properties-container">
							<div className="aqi-container">
								<div className={'icon aqi-'+item.aqi_range}></div>
								<div className="param-value">
									<div className="aqi-value">{(parseInt(item.time)) ? item.aqi : 'NA'}</div>
									<div className="aqi-text">AQI</div>
								</div>
								<div className="aqi-status">
									<div className={'status aqi-'+item.aqi_range}>{(parseInt(item.time)) ? item.aqi_status : ''}</div>
									{(() => {
										if(parseInt(item.time) && item.params && Object.keys(item.params).length) {
											let aqi_name = item.params.map((param) => {
												if (param.key == item.responsible_param) {
													return <div className = "responsible-param-name" dangerouslySetInnerHTML={{__html: "Pollutant : " + param.name}} />;
												}
											});
											return aqi_name;
										}
									})()}
								</div>
							</div>
						</div>
						{(() => {
							if (item.plot_graph) {
								let hourly_aqis = [];
								item.hourly_aqi_config.series[0].data.map((hourly_aqi) => {
									if(hourly_aqi && !isNaN(hourly_aqi.y)) {
										hourly_aqis.push(parseInt(hourly_aqi.y));
									}
								});

								return(<div className="graph-container">
									<ReactHighcharts config={item.hourly_aqi_config} />
									<div className="graph-right-item">
										<div className="text">
											{Math.min.apply(null, hourly_aqis)}<div className="label">Min</div>
										</div>
										<div className="text">
											{Math.max.apply(null, hourly_aqis)}<div className="label">Max</div>
										</div>
									</div>
								</div>);
							} else {
								return(<div className="graph-container">
									<div className="hourly-aqi-na-error">No data received in last 24 hours.</div>
								</div>);
							}
						})()}
						<div className="panel-container">
							<div className="panel-title">Last 24 Hrs Data</div>
							{(() => {
								let graphs_24hrs = [];
								Object.keys(item.param_plot_graph).map((param, i) => {
									if (item.param_plot_graph[param] && item.param_aqis_config[param]) {
										graphs_24hrs.push(<div className="graph-item" key={i}>
											<div className="graph-left-item">
												<div className="text">{param}</div>
												<div className="text">
													{item.hourly_param_aqis[param].filter(function(val){if(val)return val})[0]}
													<sub>AQI</sub>
												</div>
											</div>
											<ReactHighcharts config={item.param_aqis_config[param]} />
											<div className="graph-right-item">
												<div className="text">
													{Math.min.apply(null, item.hourly_param_aqis[param].filter((value) => value ? true : false))}
													<div className="label">Min</div>
												</div>
												<div className="text">
													{Math.max.apply(null, item.hourly_param_aqis[param].filter((value) => value ? true : false))}
													<div className="label">Max</div>
												</div>
											</div>
										</div>);
									}
								});
								if (graphs_24hrs.length) {
									return(<div className="graph-history-container">{graphs_24hrs}</div>);
								} else {
									return(<div className="hourly-aqi-na-error">No data received in last 24 hours.</div>);
								}
							})()}
						</div>
					</Scrollbars>
					<ReactTooltip effect="solid" />
				</div>;
			} else {
				return <div className="aqua-compare-item">
					<Scrollbars autoHide>
						<div className="aqua-location-time-container">
							<div className="location-container">
								<div className="icon location"></div>
								<div className="location-name hellip" data-tip={item.name}>{item.name}</div>
								<button className="close" onClick={() => item.remove_station()}>✖</button>
							</div>
							<div className="data-source">Data Source: {item.source}</div>
							<div className="time-container">
								<div className={'icon clock' + ((item.data_receive_connection_status === 'offline') ? ' offline' : '')}></div>
								<div className="time">{(parseInt(item.last_data_receive_time)) ? moment.unix(item.last_data_receive_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm') : 'NA'}</div>
							</div>
						</div>
						<div className="parameter-section">
							<div className="latest-data-title">Latest Data</div>
							<div className="parameter-container">
								{(() => {
									if (item) {
										console.log('Data', item);
										if (item.params && Object.keys(item.params).length) {
											let latest_param = item.params.map((value, index) => {
												return <div className="param-container">
													<div className="param-text" dangerouslySetInnerHTML={{__html: value.name }} />
													<div className="param-value-unit">
														<div className="param-value">{item.latest_param_value && item.latest_param_value[value.key] ? item.latest_param_value[value.key] : 0}</div>
														<div className="param-unit" dangerouslySetInnerHTML={{__html: value.unit}} />
													</div>
												</div>;
											});
											return latest_param;
										}
									} else {
										return
											<div className="hourly-aqi-na-error">No data received in last 24 hours.</div>;
									}
								})()}
							</div>
						</div>
					</Scrollbars>
					<ReactTooltip effect="solid" />
				</div>;
			}
		})()}
	</div>
));
	
/**
 * This function is used to show render list.
 * @param  {Object} ({items}) This shows the list.
 * @return {ReactElement}    markup
 */
export const SortableList = SortableContainer(({items}) => {
	console.log('Items:',items);
	return (
		<div className="margin-table">
			<div className="flex">
				{items.map((item, index) =>
					<SortableItem key={index} index={index} item={item} />
				)}
			</div>
		</div>
	);
});