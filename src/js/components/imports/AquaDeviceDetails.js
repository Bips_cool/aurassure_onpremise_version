import React from 'react';
import {Link} from 'react-router';
import moment from 'moment-timezone';
import ReactHighcharts from 'react-highcharts';
import { Scrollbars } from 'react-custom-scrollbars';
import CompareCheck from './CompareCheck';
import StationSearch from './StationSearch';

/**
 * This Class Used for Device details and used in Dashboard page.
 */
export default class AquaDeviceDetails extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			station_id: props.station_id,
			pathname: props.pathname ? props.pathname : '',
			pattern: props.pattern ? props.pattern : '',
			page: props.pattern ? 'analytics' : '',
			compared_station_ids: null,
			station_compared: null,
			parameters_value: {
				'so2': 1.2,
				'no': 11111,
				'PM': 2.1,
				'pm<sub>2.5</sub>': 1.1,
				'spm': 1.2
			},
			parameters_unit: {
				'so2': 'Mg/nm<sub>3</sub>',
				'no': 'ppm',
				'PM': 'Mg/nm<sub>3</sub>',
				'pm<sub>2.5</sub>': 'm<sup>3</sup>',
				'spm': 'Mg/nm<sub>3</sub>'
			}
		};
	}

	/**
	 * This function used to fetch comapre station details.
	 */
	fetch_compared_station_ids() {
		if (this._child) {
			this._child.fetch_compared_station_ids();
		}
	}

	/**
	 * This function used to fetch station data.
	 * @param  {number} id This is the selected station id.
	 */
	fetchStationData(id) {
		this.props.fetchStationData(id);
	}

	/**
	 * This is invoked before rendering when new props or state are being received. This method is not called for the initial render.
	 * @param  {object} nextProps This is to compare this.props with nextProps for that upadte can be skipped.
	 */
	// shouldComponentUpdate(nextProps) {
	// 	console.log('Should component update?');
	// 	return true;
	// }

	/**
	 * This renders the Device details.
	 * @return {ReactElement} markup
	 */
	render() {
		if (!this.props.station_data) {
			return <div className="loading">
				<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} />
				</svg>
			</div>;
		} else {
			let graph_data = [];
			let current_time = moment().unix();
			let previous_time = current_time - 3600;
			let categories = [];
			if (this.props.station_data) {
				this.props.station_data.hourly_aqis.map((hourly_aqi, i) => {
					let color = window.color_code.color_1;
					if(hourly_aqi >= 401) {
						color = window.color_code.color_6;
					} else if(hourly_aqi >= 301) {
						color = window.color_code.color_5;
					} else if(hourly_aqi >= 201) {
						color = window.color_code.color_4;
					} else if(hourly_aqi >= 101) {
						color = window.color_code.color_3;
					} else if(hourly_aqi >= 51) {
						color = window.color_code.color_2;
					}
					graph_data.push({
						y: parseInt(hourly_aqi),
						color: color
					});

					categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));
				});
				categories.reverse();
			}
			graph_data.reverse();
			
			/**
			 * This is a object to plot graph data.
			 * @type {Object}
			 * @property {object} chart This sets chart properties.
			 * @property {object} plotOptions This customize the graph plot settings/options.
			 * @property {object} title This sets the title for the graph.
			 * @property {object} subtitle This sets the subtitle for the graph.
			 * @property {object} xAxis This defines what to show in xAxis.
			 * @property {object} yAxis This defines what to show in yAxis.
			 * @property {object} legend This hides the printing options from graph.
			 * @property {object} tooltip This sets what to show in tooltip.
			 * @property {Array} series This contains data for ploting graph.
			 */
			this.config = {
				chart: {
					type: 'column',
					height: 80,
					width: 230
				},
				plotOptions: {
					series: {
						pointPadding: 0,
						groupPadding: 0
					}
				},
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					title: {
						enabled: true,
						text: 'Past 24 hr Trend',
						style: {
							fontWeight: 'normal'
						}
					},
					type: '',
					lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',      
					labels: {
						enabled: false
					},
					minorTickLength: 0,
					tickLength: 0,
					categories: categories

				},
				yAxis: {
					title: {
						text: 'AQI'
					},
					lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',      
					labels: {
						enabled: false
					},
					minorTickLength: 0,
					tickLength: 0,
					gridLineColor: 'transparent'

				},
				legend: {
					enabled: false
				},
				tooltip: {
					pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b><br/>'
				},
				series: [{
					data: graph_data
				}]
			};

			// console.log('Data', this.props.station_data);
			// console.log('AllDataProps', this.props.station_data);
			// console.log('AllData', this.state);
			// console.log('AllDataparameters_value', this.state.parameters_value);
			var suggestions = [];
			if(this.props.station_data.suggestions) {
				this.props.station_data.suggestions.map(function(suggestion, i) {
					suggestions.push(<li className="panel-item" key={i}>
						<div className={'icon suggestion-' + (i+1)}></div>
						<div className="panel-text">{suggestion}</div>
					</li>);
				}, this);
			} else {
				suggestions.push(<li className="hourly-aqi-na-error">NA</li>);
			}
		}

		return(
			<Scrollbars autoHide>
				<div className="aqua-location-time-container">
					<StationSearch {...this.props} {...this.state} fetchStationData={(id) => this.fetchStationData(id)} />
					<div className="data-source">Data Source: {this.props.source}</div>
					<div className="time-container">
						<div className={'icon clock' + ((this.props.station_data.data_receive_connection_status === 'offline') ? ' offline' : '')}></div>
						<div className="time" id="last_data_time">
							{(() => {
								if(this.props.station_data.last_data_receive_time != 0) {
									return moment.unix(this.props.station_data.last_data_receive_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm');
								} else {
									return 'NA';
								}
							})()}
						</div>
						<CompareCheck {...this.props} ref={(child) => {this._child = child;}} />
					</div>
				</div>
				<div className="aqua-parameter-section">
					<div className="latest-data-title">Latest Data</div>
						{/*<div className="aqi-container">
							<div className={'icon aqi-' + this.props.station_data.aqi_range}></div>
							<div className="aqi-value">
								{(() => {
									if(this.props.station_data.last_data_update_time != 0) {
										return this.props.station_data.aqi;
									} else {
										return 'NA';
									}
								})()}
							</div>
							<div className="aqi-status">
								<div className="aqi-text">Air Quality Index</div>
								{(() => {
									if(this.props.station_data.last_data_update_time != 0) {
										return(
											<div className={'status aqi-' + this.props.station_data.aqi_range}>
												{this.props.station_data.aqi_status}
											</div>
										);
									}
								})()}
							</div>
						</div>*/}
					{(() => {
						if (this.props.station_data.latest_param_value) {
							return <div className="parameter-container">
								{(() => {
									let current_params = {},
										allowed_parameters_in_order = ['pm1', 'pm2.5', 'pm10', 'so2', 'no', 'no2', 'co', 'o3', 'co2', 'o2', 'temperature', 'humidity', 'rain', 'uv', 'light', 'noise', 'voc', 'lint', 'nox', 'ch4', 'lead', 'wspeed', 'press', 'no3', 'ph', 'do', 'nh4', 'orp', 'us_mb', 'cur', 'nh3', 'us_hc04', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'];
									this.props.station_data.params.map((param) => {
										current_params[param.key] = param;
									});
									let latest_param = allowed_parameters_in_order.map((param, i) => {
										if(Object.keys(current_params).indexOf(param) >= 0) {
											return <div className="param-container" key={i}>
												<div className="param-text" dangerouslySetInnerHTML={{__html: current_params[param].name }} />
												<div className="param-value" id={'parameter_value_' + param}>{(parseFloat(this.props.station_data.latest_param_value[param]).toFixed(2))}</div>
												<div className="parameter-unit" dangerouslySetInnerHTML={{__html: ' ' + this.props.station_data.param_units[_.findIndex(this.props.station_data.params, { 'key': param})] }}/>
											</div>;
										}
									});
									return latest_param;
								})()}
							</div>;
						} else {
							return <div className='hourly-aqi-na-error param-error'>No data to show.</div>;
						}
					})()}
				</div>
				{(() => {
					if (this.props.more_info_enabled) {
						return <Link to={'/stations/' + this.props.station_data.id + '/analytics'} className="btn details-link">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 500"><style dangerouslySetInnerHTML={{__html: ".st0{stroke-width:20;stroke-miterlimit:10;}" }} /><circle cx={250} cy={250} r="238.5" fill="#FFFFFF" stroke="#FFFFFF" className="st0" /><path fill="#F6851F" stroke="#F6851F" d="M307.5,273.4c-0.2,0.6-1.1,2.1-2.8,4.5l-12.1,16.6c-35.2,48.9-62.1,73.4-80.6,73.4 c-8.8,0-16.2-3.1-22.1-9.4c-5.9-6.3-8.8-14.1-8.8-23.4c0-10.8,6.5-27.3,19.4-49.4l43.8-74.4c5.8,1.3,10.4,2,13.8,2 c5.1,0,12.2-0.7,21.3-2l-63.7,104.7c-9.2,15.2-13.8,26.8-13.8,35.1c0,8.4,4.4,12.6,13.2,12.6c16.7,0,46-29.7,88-89.1 c0.9-1.9,1.9-2.8,2.8-2.8C306.9,271.7,307.5,272.3,307.5,273.4z M307.4,132.3c0,6.6-2.8,12.7-8.3,18.4c-5.5,5.7-11.6,8.6-18.1,8.6 c-8.4,0-12.6-4.2-12.6-12.6c0-6.2,2.9-12.1,8.7-17.8c5.8-5.7,11.9-8.6,18.2-8.6C303.4,120.3,307.4,124.3,307.4,132.3z" className="st0" /></svg>
							More Info
						</Link>;
					}
				})()}
			</Scrollbars>
		);
	}
}

AquaDeviceDetails.contextTypes = {
	router: React.PropTypes.object
};
