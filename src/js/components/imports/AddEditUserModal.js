import React from 'react';
import ReactDOM from 'react-dom';
import MultiSelect from './MultiSelect';
import { Scrollbars } from 'react-custom-scrollbars';
import _ from 'lodash';

export default class AddEditUserModal extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			first_name: null,
			last_name: null,
			designation: null,
			department: null,
			email: null,
			mobile_no: null,
			password: null,
			cnf_password: null,
			submitting_formdata: false,
			user_role: null,
			user_type: null,
			group_id: props.group_id,
			stations_placeholder: 'Select Stations',
			master_access: false
		};
	}

	closeModal() {
		this.setState({
			first_name: null,
			last_name: null,
			designation: null,
			department: null,
			email: null,
			mobile_no: null,
			password: null,
			cnf_password: null,
			user_role: null,
			user_type: null,
			selected_stations: [],
			master_access: false
		}, () => {this.props.closeModal()});
	}

	componentDidUpdate() {
	}

	componentWillReceiveProps(nextProps) {
		if (!_.isEqual(this.props.temp_user_details, nextProps.temp_user_details)) {
			this.setState({
				current_user_id: nextProps.current_user_id,
				first_name: nextProps.first_name,
				last_name: nextProps.last_name,
				designation: nextProps.designation,
				department: nextProps.department,
				email: nextProps.email,
				mobile_no: nextProps.mobile_no,
				user_role: nextProps.user_role,
				group_id: nextProps.group_id,
				user_type: nextProps.user_type,
				selected_stations: nextProps.selected_stations,
				master_access: nextProps.master_access
			});
			// console.log('Stations To Get', this.state);
		}
		// console.log('nextProps', nextProps);
	}

	addUserDetails() {
		let that = this, error = null;
		if (that.state.first_name == null && that.state.first_name == '' ) {
			error = 'Please enter First Name!';
		} else if (that.state.email == null && that.state.email == '') {
			error = 'Please enter Email ID!';
		} else if (that.state.password == null && that.state.password == '') {
			error = 'Please enter password!';
		} else if (that.state.password != that.state.cnf_password) {
			error = 'Passwords did not match!';
		} else if (that.state.user_role == null && that.state.user_role == '') {
			error = 'Please select User role!';
		} else if (that.state.user_type == null && that.state.user_type == '') {
			error = 'Please select User type!';
		}
		if (error) {
			showPopup('danger',error);
		} else {
			that.setState({ submitting_formdata: true });
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'add_new_user.php', {
				credentials: 'include',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					user_fname: this.state.first_name,
					user_lname: this.state.last_name,
					designation: this.state.designation,
					department: this.state.department,
					user_email: this.state.email,
					user_phone: this.state.mobile_no,
					user_pass: this.state.password,
					user_role: this.state.user_role,
					user_type: this.state.user_type,
					group_id: this.state.group_id,
					selected_stations: this.state.stations_selected,
					master_access: this.state.master_access
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('User Added:', json);
				if (json.status === 'success') {
					showPopup('success','User added successfully');
					that.closeModal();
					that.props.resetUserList();
					that.setState({submitting_formdata: false});
					console.log('User Saved');
				} else {
					showPopup('danger',json.message);
				}
			}).then(function() {
				that.setState({ submitting_formdata: false });
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.setState({ submitting_formdata: false });
			});
		}
	}

	saveUserDetails() {
		let that = this, error = null;
		if (that.state.first_name == null && that.state.first_name == '' ) {
			error = 'Please enter First Name!';
		} else if (that.state.email == null && that.state.email == '') {
			error = 'Please enter Email ID!';
		} else if (that.state.user_role == null && that.state.user_role == '') {
			error = 'Please select User role!';
		} else if (that.state.user_type == null && that.state.user_type == '') {
			error = 'Please select User type!';
		}
		if (error) {
			showPopup('danger',error);
		} else {
			that.setState({ submitting_formdata: true });
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'edit_user_for_group.php', {
				credentials: 'include',
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					user_id: that.state.current_user_id,
					user_fname: this.state.first_name,
					user_lname: this.state.last_name,
					designation: this.state.designation,
					department: this.state.department,
					user_email: this.state.email,
					user_phone: this.state.mobile_no,
					user_pass: this.state.password,
					user_role: this.state.user_role,
					user_type: this.state.user_type,
					group_id: this.state.group_id,
					selected_stations: this.state.stations_selected,
					master_access: this.state.master_access
				})
			}).then(function(response) {
				return response.json();
			}).then(function(json) {
				console.log('User Edited:', json);
				if (json.status === 'success') {
					showPopup('success','User edited successfully');
					that.closeModal();
					that.props.resetUserList();
					that.setState({submitting_formdata: false});
					console.log('User Saved');
				} else {
					showPopup('danger',json.message);
				}
			}).then(function() {
				that.setState({ submitting_formdata: false });
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				that.setState({ submitting_formdata: false });
			});
		}
	}

	getStationIds(values) {
		let that = this;
		console.log('values stations', values);
		
		this.setState({
			stations_selected: values
		});
	}

	setStationList(){
		let that = this,
			all_stations_list = {};
		if (that.props.all_stations && Object.keys(that.props.all_stations).length) {
			that.props.all_stations.map((station, index) => {
				// console.log('key', index);
				all_stations_list[station.id] = station.name;
			});
		}
		console.log('station_lists', all_stations_list);
		that.setState({
			all_stations_list: all_stations_list
		}, () => {
			that.resetSelectedStations();
		});
	}

	resetSelectedStations(){
		let that = this,
			selected = [],
			checked_parameters = [];
		if (that.state.stations_selected) {
			that.state.stations_selected.map((id) => {
				that.props.all_stations.map((station, index) => {
					if(station.id == id && selected.indexOf(id) === -1) {
						selected.push(id);
					}
				});
			});
		}

		console.log('selected', selected);

		that.setState({
			stations_selected: selected
		});
	}

	render() {
		return <div>
			{(() => {
				if (this.props.add_user) {
					return <div className="modal">
						<Scrollbars autoHide>
							<div className="modal-dialog">
								<div className="modal-content">
								<div className="modal-header">
									<button type="button" className="close" onClick={() => this.closeModal()}>
										<svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 21.9 21.9">
												<path fill="black" d="M14.1 11.3c-.2-.2-.2-.5 0-.7l7.5-7.5c.2-.2.3-.5.3-.7s-.1-.5-.3-.7L20.2.3c-.2-.2-.5-.3-.7-.3-.3 0-.5.1-.7.3l-7.5 7.5c-.2.2-.5.2-.7 0L3.1.3C2.9.1 2.6 0 2.4 0s-.5.1-.7.3L.3 1.7c-.2.2-.3.5-.3.7s.1.5.3.7l7.5 7.5c.2.2.2.5 0 .7L.3 18.8c-.2.2-.3.5-.3.7s.1.5.3.7l1.4 1.4c.2.2.5.3.7.3s.5-.1.7-.3l7.5-7.5c.2-.2.5-.2.7 0l7.5 7.5c.2.2.5.3.7.3s.5-.1.7-.3l1.4-1.4c.2-.2.3-.5.3-.7s-.1-.5-.3-.7l-7.5-7.5z"/>
											</svg>
									</button>
									<h4 className="modal-title">{this.props.form_mode == 'edit' ? 'Edit User' : 'Add New User'}</h4>
								</div>
								<div className="modal-body" id="modal_content">
									<div className="col-lg-12 col-md-12 col-sm-12 padding-zero">
										<div className="row">
											<div className="col-lg-6 col-md-6 col-sm-6">
												<label>Name</label>
												<input type="text" className="user-text-box" value={this.state.first_name} placeholder="First" onChange={(e) => this.setState({first_name: e.target.value})}/>
											</div>
											<div className="col-lg-6 col-md-6 col-sm-6">
												<label className="no-label"></label>
												<input type="text" className="user-text-box" value={this.state.last_name} placeholder="Last" onChange={(e) => this.setState({last_name: e.target.value})}/>
											</div>
										</div>
										<div className="row">
											<div className="col-lg-6 col-md-6 col-sm-6">
												<label>Designation</label>
												<input type="text" className="user-text-box" value={this.state.designation} placeholder="" onChange={(e) => this.setState({designation: e.target.value})}/>
											</div>
											<div className="col-lg-6 col-md-6 col-sm-6">
												<label>Department</label>
												<input type="text" className="user-text-box" value={this.state.department} placeholder="" onChange={(e) => this.setState({department: e.target.value})}/>
											</div>
										</div>
										<div className="row">
											<div className="col-lg-6 col-md-6 col-sm-6">
												<label>Email</label>
												<input type="text" className="user-text-box" value={this.state.email} placeholder="user@company.com" onChange={(e) => this.setState({email: e.target.value})}/>
											</div>
											<div className="col-lg-6 col-md-6 col-sm-6">
												<label>Mobile No.</label>
												<input type="text" className="user-text-box" value={this.state.mobile_no} placeholder="9999999999" onChange={(e) => this.setState({mobile_no: e.target.value})}/>
											</div>
										</div>
										{(() => {
											if (this.props.form_mode == 'add') {
												return <div className="row">
													<div className="col-lg-6 col-md-6 col-sm-6">
														<label>Password</label>
														<input type="password" className="user-text-box" value={this.state.password} placeholder="Enter Password" onChange={(e) => this.setState({password: e.target.value})}/>
													</div>
													<div className="col-lg-6 col-md-6 col-sm-6">
														<label className="no-label"></label>
														<input type="password" className="user-text-box" value={this.state.cnf_password} placeholder="Confirm Password" onChange={(e) => this.setState({cnf_password: e.target.value})}/>
													</div>
												</div>;
											}
										})()}
										<div className="row">
											<div className="col-lg-4 col-md-4 col-sm-4 margin-top z-index">
												<label>User Role</label>
												<select className="user-text-box margin-top" value={this.state.user_role} onChange={(e) => this.setState({user_role: parseInt(e.target.value)})}>
													{(() => {
														if (this.props.form_mode == 'add') {
															return <option selected disabled>Select User Role</option>;
														} else {
															return <option disabled>Select User Role</option>;
														}
													})()}
													<option value="0">Normal</option>
													<option value="1">Manager</option>
													<option value="2">Admin</option>
												</select>
											</div>
											<div className="col-lg-4 col-md-4 col-sm-4 margin-top z-index">
												<label>User Type</label>
												<select className="user-text-box margin-top" value={this.state.user_type} onChange={(e) => this.setState({user_type: parseInt(e.target.value)})}>
													{(() => {
														if (this.props.form_mode == 'add') {
															return <option selected disabled>Select User Type</option>;
														} else {
															return <option disabled>Select User Type</option>;
														}
													})()}
													<option value="0">Normal</option>
													<option value="1">City Specific</option>
												</select>
											</div>
										</div>
										<div className="row">
											<div className="col-lg-12 col-md-12 col-sm-12">
												<div className="assign-station">Assign Stations</div>
											</div>
										</div>
										<div className="row">
											<div className="col-lg-6 col-md-6 col-sm-6">
												<label className="label-box">
													<input type="checkbox" className="all-check" checked={this.state.master_access} onChange={(e) => this.setState({master_access: (e.target.checked) ? true : false})}/>Master Access
												</label>
											</div>
										</div>
										{(() => {
											if (!this.state.master_access) {
												return <div className="row">
													<div className="col-lg-8 col-md-8 col-sm-8">
														<MultiSelect
															placeholder={this.state.stations_placeholder}
															options={this.state.all_stations_list}
															selected={this.state.stations_selected}
															callback={(selected) => this.getStationIds(selected)}
															item_type={'Stations'} />
													</div>
												</div>;
											}
										})()}
										{/*<div className="row">
											<div className="col-lg-12 col-md-12 col-sm-12">
												<div className="assign-station">Assign Group</div>
											</div>
										</div>
										<div className="row">
											<div className="col-lg-6 col-md-6 col-sm-6">
												<select className="user-text-box margin-top" value={this.state.group_id} onChange={(e) => this.setState({group_id: parseInt(e.target.value)})}>
													{(() => {
														if (this.props.form_mode == 'add') {
															return <option selected disabled>Select group</option>;
														} else {
															return <option disabled>Select group</option>;
														}
													})()}
													{(() => {
														if (this.props.groups_list && this.props.groups_list.length) {
															let group_list = this.props.groups_list.map((grp, ind) => {
																return <option value={grp.id}>{grp.name}</option>
															});
															return group_list;
														}
													})()}
												</select>
											</div>
										</div>*/}
									</div>
								</div>
								<div className="modal-footer">
									<button type="button" className="btn btn-cancel" onClick={() => this.closeModal()}>Cancel</button>
									{(() => {
										if (this.props.form_mode == 'add') {
											return <button type="button" className="btn btn-primary btn-save" disabled={this.state.submitting_formdata ? true : false} onClick={() => this.addUserDetails()}>Save</button>;
										} else {
											return <button type="button" className="btn btn-primary btn-save" disabled={this.state.submitting_formdata ? true : false} onClick={() => this.saveUserDetails()}>Save</button>;
										}
									})()}
								</div>
								</div>
							</div>
						</Scrollbars>
					</div>;
				}
			})()}
		</div>;
	}
}

AddEditUserModal.contextTypes = {
	router: React.PropTypes.object
};