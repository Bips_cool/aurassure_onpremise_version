import React from 'react';
import moment from 'moment-timezone';
import ReactTooltip from 'react-tooltip';

/**
 * This Class Used for User card.
 */
export default class UserCard extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {Object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = props;
	}

	/**
	 * This renders the User card.
	 * @return {ReactElement} markup
	 */
	render() {
		var cards = [];
		this.state.users.map((user, index) => {
			if (this.props.uid == user.usr_id) {
				cards.push(
					<div className="user-card" key={index}>
						<div className="user-icon">
							<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 409.2 409.2"><path d="M204.6 216.7c50.6 0 91.7-48 91.7-107.4C296.3 27 255.3 2 204.6 2s-91.8 25-91.8 107.3c0 59.3 41 107.4 91.8 107.4zm202.6 158L361 270.5c-2.2-4.8-6-8.8-10.6-11.2L278.6 222c-1.6-1-3.5-.7-5 .3-20.3 15.4-44 23.5-69 23.5s-48.8-8-69-23.5c-1.5-1-3.4-1.2-5-.4l-71.8 37c-4.7 2.4-8.4 6.4-10.5 11.2L2 374.7c-3.2 7.2-2.5 15.4 1.8 22 4.2 6.6 11.5 10.5 19.4 10.5H386c8 0 15-4 19.4-10.5 4.3-6.6 5-14.8 1.8-22z"/></svg>
						</div>
						<div className="user-details">
							<div className="name hellip">{(user.usr_first_name + ' ' + user.usr_middle_name + ' ' + user.usr_last_name).trim()}</div>
							<div className="hellip">
								{((user.usr_designation) ? (user.usr_designation + ', ') : '-') + ((user.usr_department) ? user.usr_department : '')}
							</div>
							<small className="text-success hellip">
								{'Last logged in: ' + ((isNaN(this.state.users_login[user.usr_id])) ? ((this.state.users_login[user.usr_id] != undefined) ? this.state.users_login[user.usr_id] : 'Never') : moment.unix(this.state.users_login[user.usr_id]).tz('Asia/Kolkata').format('DD MMM, HH:mm'))}
							</small>
						</div>
						<div className="access">
							<legend>Access</legend>
							<div className="access-items">
								<div className="access-item">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 258.8 258.8" width="18" height="18"><circle cx="129.4" cy="60" r="60"/><path d="M129.4 150c-60 0-108.8 48.7-108.8 108.8H238c0-60-48.6-108.8-108.6-108.8z"/></svg>
								</div>
								<div className="access-item">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 258.8 258.8" width="18" height="18"><circle cx="129.4" cy="60" r="60"/><path d="M129.4 150c-60 0-108.8 48.7-108.8 108.8H238c0-60-48.6-108.8-108.6-108.8z"/></svg>
								</div>
								<div className="access-item">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 258.8 258.8" width="18" height="18"><circle cx="129.4" cy="60" r="60"/><path d="M129.4 150c-60 0-108.8 48.7-108.8 108.8H238c0-60-48.6-108.8-108.6-108.8z"/></svg>
								</div>
								<div className="access-item">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 258.8 258.8" width="18" height="18"><circle cx="129.4" cy="60" r="60"/><path d="M129.4 150c-60 0-108.8 48.7-108.8 108.8H238c0-60-48.6-108.8-108.6-108.8z"/></svg>
								</div>
							</div>
						</div>
						<div className="actions">
							<legend>Actions</legend>
							<div className="action-items">
								<div className={'action-item' + ((this.state.id == this.state.uid) ? ' hide' : '')} data-tip="Reset Password" onClick={(id, act) => this.props.actionTriggered(this.props.uid, 'reset_pass')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 510 510" width="18" height="18"><path d="M267.8 12.8C178.4 12.8 99.3 61 58.6 135.2L0 76.5v165.8h165.8l-71.5-71.5c33.2-63.7 97-107 173.4-107C372.4 63.8 459 150.3 459 255c0 104.5-86.7 191.3-191.3 191.3-84 0-153-53.6-181-127.6H33c28.2 102 122.6 178.6 234.8 178.6 135 0 242.2-109.7 242.2-242.3 0-132.6-109.6-242.3-242.3-242.3z"/><path d="M263.7 114c-37.4 0-67.7 30.3-67.7 67.7 0 26.4 15.2 49.3 37.3 60.5v30.6s24 2 24 16.6c0 10.3-12.3 14.4-19.4 18.5-3.3 1.5-10.3 5.3-.3 10.5 5.3 3 19 6.3 19 19.5 0 13.4-22.2 19.8-22.2 19.8l29.6 29.5 29.8-29.7V242.2c22-11 37.3-34 37.3-60.5 0-37.4-30.2-67.7-67.5-67.7zm-49 59.5c4.4-23 24.6-40.4 49-40.4 24.3 0 44.5 17.6 49 40.6h-98z"/></svg>
								</div>
								{/*<div className={'action-item' + ((this.state.id == this.state.uid) ? ' hide' : '')} data-tip="Manage Access" onClick={(id, act) => this.props.actionTriggered(this.props.uid, 'user_access')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 46.2 46.2" width="18" height="18"><path d="M44 22.2c-.2-.2-.4-.3-.7-.2l-2 .2c-.5 0-1-.3-1-.8l-.3-2c0-.4-.2-.6-.4-.7-.2-.2-.4-.3-.7-.2l-3 .3c-.8 0-1-.3-1-.8l-.7-2c0-.2 0-.4-.3-.5 0-.2-.4-.2-.6-.2h-1c0 .2-.4 0-.6-.2.2 0 .3 0 .3-.3s-.2-.5-.4-.6l-1.3-2c.5-3.6-1-7.2-4-9.5C22-1.2 15.7-.2 12 4c0 .4-.3.6-.5 1-3.3 0-7 1-9 5.4-2.4 5.3 1 10.8 3.5 14L5 25c-1 .6-1.4 1.8-1 2.8l10.4 17.4c.6 1 2 1.3 2.8.7l6-4 1-1c.2-1 0-1-.2-2L14 22c-.3-.3-.8-.7-1.3-.8-.5 0-1 0-1.5.2L9 22.8c-2.2-2.5-5.4-7.2-3.6-11.2 1.2-2.4 3.2-3.3 5-3.6-1 4 .5 8.2 4 10.7 3 2.3 6.7 2.8 10 1.4l13 10c.4 1 1 1 1.4 0l4.4-1c.6 0 1-1 1-1l.2-4c0-.3-.2-.5-.4-.7zm-32.6 3.3c.5.8.2 2-.6 2.3-.8.5-1.8.2-2.3-.6l-.2-.6.2.2c.3.3.7.4 1 .4.5 0 1-.2 1.2-.5.4-.5.5-1.2.2-1.7v.5zM18.8 9c-1 1.2-2.6 1.5-3.7.6l-1-1.4c1 0 1 .2 1 .2 1 .3 2-.2 2-1 1-.8 0-1.6-1-2 1-.7 2-.7 3 0 1 .8 2 2.6 1 3.6z"/></svg>
								</div>*/}
								<div className="action-item" data-tip="Edit User" onClick={(id, act) => this.props.actionTriggered(this.props.uid, 'edit_user')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 399.1 399.1" width="18" height="18"><circle cx="108.8" cy="73" r="60"/><path d="M192.4 202.2c-20-24-50-39.2-83.6-39.2C48.8 163 0 211.7 0 271.7h123l69.4-69.5zm108-65.5L339 98l60 60-38.5 38.8-60-60zM111 386.2l68.5-20-48.5-48.5-20 68.5zm35.4-95.5L279.2 158l60 60-132.7 132.8-60-60z"/></svg>
								</div>
								<div className={'action-item' + ((this.state.id == this.state.uid) ? ' hide' : '')} data-tip="Remove User" onClick={(id, act) => this.props.actionTriggered(this.props.uid, 'remove_user')}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 374.1 374.1" width="18" height="18"><circle cx="108.8" cy="68" r="60"/><path d="M274.7 167.3c-54.8 0-99.4 44.6-99.4 99.4s44.6 99.5 99.4 99.5 99.4-44.6 99.4-99.5-44.4-99.4-99.2-99.4zm61.5 114.4h-123v-30h123v30zM108.8 158C48.8 158 0 206.7 0 266.7h145.3c0-32.3 12-62 31.6-84.7-18.8-15-42.5-24-68.4-24z"/></svg>
								</div>
							</div>
						</div>
						<ReactTooltip effect="solid" />
					</div>
				);
			}
		});
		return (<div>{cards}</div>);
	}
}