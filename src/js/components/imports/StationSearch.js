import React from 'react';
import {Link} from 'react-router';

/**
 * This Class Used for Station search.
 */
export default class StationSearch extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} params
	 */
	constructor(params) {
		super();
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			source: 'Aurassure',
			show_search_list: null,
			search_term: '',
			station_data: null
		};
		/**
		 * This is used for bind the all stations data.
		 * @type {object}
		 */
		this.retrive_data = this.retrive_data.bind(this);
		this.retrive_data();
	}

	/**
	 * This function is used for serch box to search stations.
	 * @param  {object} e This the user input value in search box.
	 */
	search_list(e) {
		this.setState({search_term: e.trim().toLowerCase()});
	}

	/**
	 * This function sets state to open search list.
	 */
	open_search_list() {
		this.setState({show_search_list: 1});
	}

	/**
	 * This function sets state for toggling search list.
	 */
	toggle_search_list() {
		this.setState({show_search_list: !(this.state.show_search_list)});
	}

	/**
	 * This function used for refresh stations.
	 * @param  {number} id   This is the station id.
	 * @param  {string} name This is the station name.
	 */
	refresh_station(id, name) {
		var that = this;
		if (id != that.props.station_id) {
			// console.log('id', id);
			document.getElementById('location_name').value = name;
			that.props.fetchStationData(id);
			that.context.router.transitionTo('/stations/' + id + ((that.props.page === 'analytics') ? '/analytics' : ''));
			// location.reload();
 		 }
		that.toggle_search_list();
	}

	/**
	 * This function is used to fetch all stations data.
	 */
	retrive_data() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_stations.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations:',json);
			if (json.status === 'success') {
				that.setState({all_stations: json.stations});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This renders the  Station Search list.
	 * @return {ReactElement} markup
	 */
	render() {
		if (!this.state.all_stations) {
			return <center><svg className="loading-spinner" width="16" height="16" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></center>
		} else {
			var stations = [];
			this.state.all_stations.map(function(station, i) {
				if ((this.state.search_term == '') || (station.name).toLowerCase().search(this.state.search_term) >= 0) {
					stations.push(<li className={'hellip' + ((this.props.station_data.id == station.id) ? ' active' : '')} key={i} onClick={() => this.refresh_station(station.id, station.name)}>{station.name}</li>);
				}
			}, this);
			if (!stations.length && (this.state.search_term != '')) {
				stations.push(<li>No results found by term {'"'+this.state.search_term+'"'}.</li>);
			} else if (!stations.length && (this.state.search_term == '')) {
				stations.push(<li onClick={() => this.close_search_list()}>No stations available.</li>);
			}
		}

		return(
			<div className="location-container">
				<div className="icon location"></div>
				{(() => {
					if (this.state.show_search_list) {
						return <input type="text" id="location_name" className="location-name" defaultValue={this.props.station_data.name} onChange={(e) => this.search_list(e.target.value)} onFocus={() => this.open_search_list()} autoFocus />;
					} else {
						return <div className="location-name hellip">{this.props.station_data.name}</div>;
					}
				})()}
				<span className="icon search" onClick={() => this.toggle_search_list()}></span>
				<ul id="searchResults" className={'location-search-result'+ ((this.state.show_search_list) ? '' : ' hide')}>
					{stations}
				</ul>
			</div>
		);
	}
}

StationSearch.contextTypes = {
	router: React.PropTypes.object
};
