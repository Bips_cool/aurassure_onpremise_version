import React from 'react';
import ReactDOM from 'react-dom';

/**
 * This Class Used for filter in Cofiguration page.
 */
export default class ConfigFilter extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			city_id: '0',
			connection: '0',
			sync: '0',
			health: '0',
			data_sending: '0',
			unconfigured: '0',//false
			device_archive_status: 'active',
			no_data_received: '0',//false
			publicly_accessible: '0',
			search_value: '',
			open_filter: false,
			filter_state: true
		};
	}

	/**
	 * This function will apply the filter.
	 */
	applyFilter() {
		this.props.updateFilter({
			city_id: this.state.city_id,
			connection: this.state.connection,
			sync: this.state.sync,
			health: this.state.health,
			data_sending: this.state.data_sending,
			device_archive_status: this.state.device_archive_status,
			unconfigured: this.state.unconfigured,
			no_data_received: this.state.no_data_received,
			publicly_accessible: this.state.publicly_accessible
		});
		if (this.state.city_id !== '0' || this.state.connection !== '0' || this.state.data_sending !== '0' || this.state.health !== '0' || this.state.no_data_received !== '0' || this.state.publicly_accessible !== '0' || this.state.sync !== '0' || this.state.unconfigured !== '0' || this.state.device_archive_status !== '0') {
			this.setState({filter_state: true});
		} else {
			this.setState({filter_state: false});
		}
		this.setState({open_filter: false});
	}

	/**
	 * On change of value in the text field this function will called.
	 * @param  {string} value Input value of the user.
	 */
	searchBoxFilter(value) {
		this.props.searchBoxFilter(value);
		this.setState({search_value: value});
	}

	/**
	 * This resets the modal filter to its default value.
	 */
	resetFilter() {
		this.setState({
			city_id: '0',
			connection: '0',
			sync: '0',
			health: '0',
			data_sending: '0',
			device_archive_status: '0',
			unconfigured: '0',
			no_data_received: '0',
			publicly_accessible: '0'
		});
	}

	/**
	 * This function closes modal on background click of the page body(exclude modal)
	 * @param  {object} event [description]
	 */
	closeFilterOptions(event) {
		const filterOptionsPanel = ReactDOM.findDOMNode(this.refs.filterOptionsPanel);
		if (filterOptionsPanel && !filterOptionsPanel.contains(event.target)) {
			this.setState({ open_filter: false });
		}
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
		window.react_container.addEventListener('click', (event) => this.closeFilterOptions(event));
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		window.react_container.removeEventListener('click', (event) => this.closeFilterOptions(event));
	}

	/**
	 * This renders the search and filter.
	 * @return {ReactElement} markup
	 */
	render() {
		return (<div className="select-filter-options">
			<div className="input-group adv-search" id="adv-search">
				<span className="svg-search">
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#666" d="M15.5 14h-.8l-.3-.3c1-1 1.6-2.6 1.6-4.2C16 6 13 3 9.5 3S3 6 3 9.5 6 16 9.5 16c1.6 0 3-.6 4.2-1.6l.3.3v.8l5 5 1.5-1.5-5-5zm-6 0C7 14 5 12 5 9.5S7 5 9.5 5 14 7 14 9.5 12 14 9.5 14z" /></svg>
				</span>
				<input type="text" className="form-control search-box-group" placeholder="Search for Station name, Id, City or Firmware version" value={this.state.search_value} onChange={(e) => this.searchBoxFilter(e.target.value)}/>
				{(() => {
					if (this.state.open_filter) {
						return <div className="form-horizontal bg-box background-box" ref="filterOptionsPanel">
							<button type="button" className="close close-icon" onClick={(e) => this.setState({open_filter: false})}>&times;</button>
							<fieldset>
								<div className="form-group">
									<label className="col-lg-3 control-label label-font">City:</label>
									<div className="col-lg-5">
										<select className="form-control select-style" value={this.state.city_id} onChange={(e) => this.setState({city_id: e.target.value})}>
											<option value={0}>Any</option>
											{(() => {
												let device_cities = Object.keys(this.props.device_cities).map((key) => {
													return <option value={key}>{this.props.device_cities[key]}</option>;
												});
												return device_cities;
											})()}
										</select>
									</div>
								</div>
								<div className="form-group">
									<label className="col-lg-3 control-label label-font">Connection Status:</label>
									<div className="col-lg-5">
										<select className="form-control select-style" value={this.state.connection} onChange={(e) => this.setState({connection: e.target.value})}>
											<option value={0}>Any</option>
											<option value={'online'}>Online</option>
											<option value={'offline'}>Offline</option>
										</select>
									</div>
								</div>
								<div className="form-group">
									<label className="col-lg-3 control-label label-font">Sync Status:</label>
									<div className="col-lg-5">
										<select className="form-control select-style" value={this.state.sync} onChange={(e) => this.setState({sync: e.target.value})}>
											<option value={0}>Any</option>
											<option value={'in_sync'}>In sync</option>
											<option value={'syncing'}>Syncing</option>
											<option value={'waiting_for_device'}>Waiting for device</option>
										</select>
									</div>
								</div>
								<div className="form-group">
									<label className="col-lg-3 control-label label-font">Health Status:</label>
									<div className="col-lg-5">
										<select className="form-control select-style" value={this.state.health} onChange={(e) => this.setState({health: e.target.value})}>
											{(() => {
												let health_status = Object.keys(this.props.health_status).map((key) => {
													return <option value={key}>{this.props.health_status[key]}</option>;
												});
												return health_status;
											})()}
										</select>
									</div>
								</div>
								<div className="form-group">
									<label className="col-lg-3 control-label label-font">Data Sending:</label>
									<div className="col-lg-5">
										<select className="form-control select-style" value={this.state.data_sending} onChange={(e) => this.setState({data_sending: e.target.value})}>
											<option value={0}>Any</option>
											<option value={'on'}>On</option>
											<option value={'off'}>Off</option>
										</select>
									</div>
								</div>
								<div className="form-group">
									<label className="col-lg-3 control-label label-font label-device-accessible">Publicly Accessible:</label>
									<div className="col-lg-5">
										<select className="form-control select-style" value={this.state.publicly_accessible} onChange={(e) => this.setState({
											publicly_accessible: (e.target.value === 'true') ? true : ((e.target.value === 'false') ? false : e.target.value)
										})}>
											<option value={0}>Any</option>
											<option value={true}>Yes</option>
											<option value={false}>No</option>
										</select>
									</div>
								</div>
								<div className="form-group">
									<label className="col-lg-3 control-label label-font">Station Status:</label>
									<div className="col-lg-5">
										<select className="form-control select-style" value={this.state.device_archive_status} onChange={(e) => this.setState({device_archive_status: e.target.value})}>
											<option value={'active'}>Active</option>
											<option value={'archived'}>Archived</option>
											<option value={0}>Any</option>
										</select>
									</div>
								</div>
								<div className="form-group">
									<label htmlFor="checkbox" className="col-lg-3"></label>
									<div className="col-lg-2 check-box-width">
										<div className="checkbox">
										<label className="check-box-label">
											<input type="checkbox" name="checkbox" id="checkbox" checked={(this.state.unconfigured === false) ? true : false} onChange={(e) => this.setState({unconfigured: (e.target.checked === true) ? false : '0'})}/> Unconfigured
										</label>
										</div>
									</div>
									<div className="col-lg-2 check-box-width">
										<div className="checkbox">
											<label className="check-box-label">
											<input type="checkbox" name="checkbox" id="checkbox" checked={(this.state.no_data_received === false) ? true : false} onChange={(e) => this.setState({no_data_received: (e.target.checked === true) ? false : '0'})}/> No Data Received
											</label>
										</div>
									</div>
								</div>
								<div className="form-group btn-top-margin">
									<div className="col-lg-3 btn-group">
										<button className="btn btn-default btn-margin border-radious" onClick={() => this.resetFilter()}>Reset</button>
										<button className="btn btn-primary border-radious" onClick={() => this.applyFilter()}>Apply</button>
									</div>
								</div>
							</fieldset>
						</div>;
					}
				})()}
				<span className="svg-filter" onClick={(e) => this.setState({open_filter: true})} data-tip={'Click to filter'}>
					<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill={(this.state.open_filter || this.state.filter_state ? '#2673B5' : '#666')} d="M10 18h4v-2h-4v2zM3 6v2h18V6H3zm3 7h12v-2H6v2z"/></svg>
				</span>
			</div>
		</div>);
	}
}

ConfigFilter.contextTypes = {
	router: React.PropTypes.object
};