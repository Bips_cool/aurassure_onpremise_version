import React from 'react';
import {Link} from 'react-router';
import moment from 'moment-timezone';
import ReactHighcharts from 'react-highcharts';
import { Scrollbars } from 'react-custom-scrollbars';
import CompareCheck from './CompareCheck';
import StationSearch from './StationSearch';

/**
 * This Class Used for Device details and used in Dashboard page.
 */
export default class DeviceDetails extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			station_id: props.station_id,
			compared_station_ids: null,
			station_compared: null,
		};
		this.suggestions = true
		this.wind_index = '';
		this.speed_index = '';
		this.pressure_index = '';
	}

	/**
	 * This function used to fetch comapre station details.
	 */
	fetch_compared_station_ids() {
		if (this._child) {
			this._child.fetch_compared_station_ids();
		}
	}

	/**
	 * This function used to fetch station data.
	 * @param  {number} id This is the selected station id.
	 */
	fetchStationData(id) {
		this.props.fetchStationData(id);
	}

	/**
	 * This is invoked before rendering when new props or state are being received. This method is not called for the initial render.
	 * @param  {object} nextProps This is to compare this.props with nextProps for that upadte can be skipped.
	 */
	shouldComponentUpdate(nextProps) {
		console.log('Should component update?');
		return true;
	}

	/**
	 * This renders the Device details.
	 * @return {ReactElement} markup
	 */
	render() {
		if (!this.props.station_data) {
			return <div className="full-height" id="panelView">
				<div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>
			</div>;
		} else {
			let graph_data = [];
			let current_time = moment().unix();
			let previous_time = current_time - 3600;
			let categories = [];
			if (this.props.station_data) {
				// console.log('color_code_props', window.color_code);
				this.props.station_data.hourly_aqis.map((hourly_aqi, i) => {
					let color = window.color_code.color_1;
					if(hourly_aqi.aqi >= 401) {
						color = window.color_code.color_6;
					} else if(hourly_aqi.aqi >= 301) {
						color = window.color_code.color_5;
					} else if(hourly_aqi.aqi >= 201) {
						color = window.color_code.color_4;
					} else if(hourly_aqi.aqi >= 101) {
						color = window.color_code.color_3;
					} else if(hourly_aqi.aqi >= 51) {
						color = window.color_code.color_2;
					}
					graph_data.push({
						y: parseInt(hourly_aqi.aqi),
						param: ((hourly_aqi.param == "" || hourly_aqi.param == null) ? '-' : hourly_aqi.param.toUpperCase()),
						color: color
					});

					categories.push(moment.unix(previous_time - 3600 * i).tz('Asia/Kolkata').format('DD MMM HH') + ' - ' + moment.unix(current_time - 3600 * i).tz('Asia/Kolkata').format('HH'));
				});
				categories.reverse();
			}
			graph_data.reverse();
			
			/**
			 * This is a object to plot graph data.
			 * @type {Object}
			 * @property {object} chart This sets chart properties.
			 * @property {object} plotOptions This customize the graph plot settings/options.
			 * @property {object} title This sets the title for the graph.
			 * @property {object} subtitle This sets the subtitle for the graph.
			 * @property {object} xAxis This defines what to show in xAxis.
			 * @property {object} yAxis This defines what to show in yAxis.
			 * @property {object} legend This hides the printing options from graph.
			 * @property {object} tooltip This sets what to show in tooltip.
			 * @property {Array} series This contains data for ploting graph.
			 */
			this.config = {
				chart: {
					type: 'column',
					height: 80,
					width: 230
				},
				plotOptions: {
					series: {
						pointPadding: 0,
						groupPadding: 0
					}
				},
				title: {
					text: ''
				},
				subtitle: {
					text: ''
				},
				xAxis: {
					title: {
						enabled: true,
						text: 'Past 24 hr Trend',
						style: {
							fontWeight: 'normal'
						}
					},
					type: '',
					lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',      
					labels: {
						enabled: false
					},
					minorTickLength: 0,
					tickLength: 0,
					categories: categories

				},
				yAxis: {
					title: {
						text: 'AQI'
					},
					lineWidth: 0,
					minorGridLineWidth: 0,
					lineColor: 'transparent',      
					labels: {
						enabled: false
					},
					minorTickLength: 0,
					tickLength: 0,
					gridLineColor: 'transparent'

				},
				legend: {
					enabled: false
				},
				tooltip: {
					pointFormat: '<span style="color:{point.color}">AQI <b>{point.y}</b> ({point.param})<br/></span>'
				},
				series: [{
					data: graph_data
				}]
			};
			// console.log('config', this.config);

			if (this.props.station_data && this.props.station_data.latest_param_value) {
				if (this.props.station_data.latest_param_value.wdir || this.props.station_data.latest_param_value.wspeed || this.props.station_data.latest_param_value.press) {
					this.suggestions = false;
					if (this.props.station_data.latest_param_value.wdir) {
						this.wind_index = this.props.station_data.params.findIndex(x => x.key=="wdir");
						this.magneto_config = {
							chart: {
								type: 'gauge',
								height: 145,
								width: 145
							},
							exporting: {
								enabled: false
							},
							credits: {
								enabled: false
							},

							title: {
								text: ''
							},
							tooltip: {
								enabled: false,
								backgroundColor: '#FCFFC5',
								borderColor: '#fff',
								pointFormat: '{series.name}: <b>{point.y}</b><br/>',
								valueSuffix: ' °'
							},
							 plotOptions: {
								series: {
									dataLabels: {
										enabled: false
									}
								}
							},

							pane: {
								startAngle: 0,
								endAngle: 360,
								background: [{
									borderWidth: 1,
									outerRadius: '50%'
								}, {
									backgroundColor: {
										linearGradient: {
											x1: 0,
											y1: 0
										},
										stops: [
											[0, '#FFF'],
											[1, '#333']
										]
									},
									borderWidth: 4,
									outerRadius: '10%'
								}, {
									backgroundColor: {
										linearGradient: {
											x1: 0,
											y1: 0,
										},
										stops: [
											[0, '#333'],
											[1, '#FFF']
										]
									},
									borderWidth: 4,
									outerRadius: '10%'
								}, {
									// default background
								}, {
									backgroundColor: '#DDD',
									borderWidth: 4,
									outerRadius: '10%',
									innerRadius: '8%'
								}]
							},
							// the value axis
							yAxis: [{
								title: {
									text: '',
								},
								min: 0,
								max: 360,
								lineColor: '#333',
								offset: -10,
								tickInterval: 0,
								tickWidth: 2,
								tickPosition: 'outside',
								tickLength: 10,
								tickColor: '#333',
								minorTickInterval: 5,
								minorTickWidth: 1,
								minorTickLength: 10,
								minorTickPosition: 'outside',
								minorTickColor: '#666',
								labels: {
									step: 1,
									distance: -12,
									rotation: 'auto'
								},
							}, {
								title: {
									text: '',
								},
								type: 'category',
								categories: ['N', '' , 'NE', '', 'E', '', 'SE', '', 'S', '', 'SW', '' , 'W', '', 'NW', '', 'N'],
								min: 0,
								max: 16,
								lineColor: '#ddd',
								offset: -20,
								tickInterval: 1,
								tickWidth: 1,
								tickPosition: 'outside',
								tickLength: 20, // =50-10
								tickColor: '#ddd',
								minorTickInterval: 1,
								minorTickWidth: 0,
								minorTickLength: 50,
								minorTickPosition: 'inside',
								minorTickColor: '#0f0',
								labels: {
									step: 1,
									distance: 10,
									rotation: 'auto'
								},
								endOnTick: true,
							}, {
								type: 'number',
								title: {
									text: '',
								},
								labels: {
									enabled: false,
								},
								min: 0,
								max: 12,
								lineColor: '#ddd',
								offset: -50,
								tickInterval: 10,
								tickWidth: 0,
								tickPosition: 'inside',
								tickLength: 45,
								tickColor: '#ddd',
								minorTickWidth: 0,
								endOnTick: false,
							}],

							series: [{
								name: 'East',
								data: [parseFloat(this.props.station_data.latest_param_value.wdir)],
								dial: {
									radius: '60%',
									baseWidth: 10,
									baseLength: '0%',
									rearLength: 0,
									borderWidth: 1,
									borderColor: '#9A0000',
									backgroundColor: '#CC0000',
								}
							}, {
									name: 'West',
								data: [parseFloat(this.props.station_data.latest_param_value.wdir)],
								dial: {
									radius: '-60%',
									baseWidth: 10,
									baseLength: '0%',
									rearLength: 0,
									borderWidth: 1,
									borderColor: '#1B4684',
									backgroundColor: '#3465A4',
								}
							}]
						}
					}

					if (this.props.station_data.latest_param_value.wspeed) {
						this.speed_index = this.props.station_data.params.findIndex(x => x.key=="wspeed");
						this.speedo_meter = {
							chart: {
								type: 'gauge',
								height: 145,
								width: 145
							},

							title: {
								text: ''
							},

							pane: {
								startAngle: -150,
								endAngle: 150,
								background: [{
									backgroundColor: {
										linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
										stops: [
											[0, '#FFF'],
											[1, '#333']
										]
									},
									borderWidth: 0,
									outerRadius: '10%'
								}, {
									backgroundColor: {
										linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1 },
										stops: [
											[0, '#333'],
											[1, '#FFF']
										]
									},
									borderWidth: 1,
									outerRadius: '10%'
								}, {
									// default background
								}, {
									backgroundColor: '#DDD',
									borderWidth: 0,
									outerRadius: '10%',
									innerRadius: '10%'
								}]
							},

							// the value axis
							yAxis: {
								min: 0,
								max: 60,

								minorTickInterval: 'auto',
								minorTickWidth: 1,
								minorTickLength: 10,
								minorTickPosition: 'inside',
								minorTickColor: '#666',

								tickPixelInterval: 30,
								tickWidth: 2,
								tickPosition: 'inside',
								tickLength: 10,
								tickColor: '#666',
								labels: {
									step: 2,
									rotation: 'auto'
								},
								title: {
									text: ''
								},
								plotBands: [{
									from: 0,
									to: 20,
									color: '#55BF3B' // green
								}, {
									from: 20,
									to: 40,
									color: '#DDDF0D' // yellow
								}, {
									from: 40,
									to: 60,
									color: '#DF5353' // red
								}]
							},
							plotOptions: {
								series: {
									dataLabels: {
										enabled: false
									}
								}
							},

							exporting: {
								enabled: false
							},
							credits: {
								enabled: false
							},
							tooltip: {
								enabled: false
							},
							series: [{
								data: [parseFloat(this.props.station_data.latest_param_value.wspeed)]
							}]
						}
					}

					if (this.props.station_data.latest_param_value.press) {
						this.pressure_index = this.props.station_data.params.findIndex(x => x.key=="press");
						this.pressure_config = {
							chart: {
								type: 'gauge',
								alignTicks: false,
								plotBackgroundColor: null,
								plotBackgroundImage: null,
								plotBorderWidth: 0,
								plotShadow: false,
								height: 175,
								width: 175
							},
							title: {
								text: ''
							},
							pane: {
								startAngle: -150,
								endAngle: 150
							},
							yAxis: [{
								min: 720,
								max: 870,
								lineColor: '#339',
								tickColor: '#339',
								minorTickColor: '#339',
								offset: -25,
								lineWidth: 2,
								labels: {
									distance: -20,
									rotation: 'auto'
								},
								tickLength: 5,
								minorTickLength: 5,
								endOnTick: false
							}, {
								min: 960,
								max: 1160,
								tickPosition: 'outside',
								lineColor: '#933',
								lineWidth: 2,
								minorTickPosition: 'outside',
								tickColor: '#933',
								minorTickColor: '#933',
								tickLength: 5,
								minorTickLength: 5,
								labels: {
									distance: 12,
									rotation: 'auto'
								},
								offset: -20,
								endOnTick: false
							}],
							exporting: {
								enabled: false
							},
							credits: {
								enabled: false
							},
							tooltip: {
								enabled: false
							},
							series: [{
								data: [(parseFloat(this.props.station_data.latest_param_value.press) * 0.750062)],
								dataLabels: {
									formatter: function () {
										let mmHg = this.y.toFixed(2),
											mBar = Math.round(mmHg / 0.750062).toFixed(2);
										// return '<span style="color:#339">' + mBar + ' ' + that.props.station_data.param_units[pressure_index]+ '</span><br/>' +'<span style="color:#933">' + mmHg + ' mmHg</span>';
									},
									backgroundColor: {
										linearGradient: {
											x1: 0,
											y1: 0,
											x2: 0,
											y2: 1
										},
										stops: [
											[0, '#DDD'],
											[1, '#FFF']
										]
									}
								}
							}]
						}
					}
				} else {
					this.suggestions = true;
				}
			} else {
				this.suggestions = true;
			}


			console.log('station_data', this.props.station_data);
			var suggestions = [];
			if(this.props.station_data.suggestions) {
				// this.chart = true;
				this.props.station_data.suggestions.map(function(suggestion, i) {
					suggestions.push(<li className="panel-item" key={i}>
						<div className={'icon suggestion-' + (i+1)}></div>
						<div className="panel-text">{suggestion}</div>
					</li>);
				}, this);
			} else {
				suggestions.push(<li className="hourly-aqi-na-error">NA</li>);
			}
		}

		return(
			<div className="full-height" id="panelView">
				<Scrollbars autoHide>
					<div className="location-time-container">
						<StationSearch {...this.props} fetchStationData={(id) => this.fetchStationData(id)} />
						<div className="data-source">Data Source: {this.props.source}</div>
						<div className="time-container">
							<div className={'icon clock' + ((this.props.station_data.connection_status === 'offline') ? ' offline' : '')}></div>
							<div className="time">
								{(() => {
									if(this.props.station_data.last_data_update_time != 0) {
										return moment.unix(this.props.station_data.last_data_update_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm');
									} else {
										return 'NA';
									}
								})()}
							</div>
							<CompareCheck {...this.props} ref={(child) => {this._child = child;}} />
						</div>
					</div>
					<div className="properties-container">
						<div className="aqi-container">
							<div className={'icon aqi-' + this.props.station_data.aqi_range}></div>
							<div className= "param-value">
								<span className="aqi-value">
									{(() => {
										if(this.props.station_data.last_data_update_time != 0) {
											return this.props.station_data.aqi;
										} else {
											return 'NA';
										}
									})()}
								</span>
								<span className="aqi-text">AQI</span>
							</div>
							<div className="aqi-status">
								{(() => {
									if(this.props.station_data.last_data_update_time != 0) {
										return(
											<div className={'status aqi-' + this.props.station_data.aqi_range}>
												{this.props.station_data.aqi_status}
											</div>
										);
									}
								})()}
								{(() => {
									if(this.props.station_data.last_data_update_time != 0 && this.props.station_data.params && Object.keys(this.props.station_data.params).length) {
										let aqi_name = this.props.station_data.params.map((param) => {
											if (param.key == this.props.station_data.responsible_param) {
												return <div className = "responsible-param-name" dangerouslySetInnerHTML={{__html: "Major Pollutant : " + param.name}} />;
											}
										});
										return aqi_name;
									}
								})()}
							</div>
						</div>
					</div>	
					<div className="graph-container-main">
						{(() => {
							let hourly_aqis = [];
							this.props.station_data.hourly_aqis.map((hourly_aqi) => {
								if(hourly_aqi.aqi) {
									hourly_aqis.push(parseInt(hourly_aqi.aqi));
								}
							});
							// console.log('hour', hourly_aqis);

							if(hourly_aqis.length) {
								return(
									<div>
										<ReactHighcharts ref="chart" config={this.config} />
										<div className="graph-right-item">
											<div className="text">
												{Math.min.apply(null, hourly_aqis)}<div className="label">Min</div>
											</div>
											<div className="text">
												{Math.max.apply(null, hourly_aqis)}<div className="label">Max</div>
											</div>
										</div>
									</div>
								);
							} else {
								return(
									<div className="hourly-aqi-na-error">No data to show.</div>
								);
							}
						})()}
					</div>
					<div className="latest-parameter-section">
						<div className="latest-parameter-title">Latest Data</div>
						{(() => {
							if (this.props.station_data.latest_param_value) {
								return <div className="latest-parameter-container">
									{(() => {
										let current_params = {},
											allowed_parameters_in_order = ['pm1', 'pm2.5', 'pm10', 'so2', 'no', 'no2', 'co', 'o3', 'co2', 'o2', 'temperature', 'humidity', 'rain', 'uv', 'light', 'noise', 'voc', 'lint', 'nox', 'ch4', 'lead', 'wspeed', 'press', 'no3', 'ph', 'do', 'nh4', 'orp', 'us_mb', 'cur', 'nh3', 'us_hc04', 'p1', 'p2', 'p3', 'p4', 'p5', 'p6', 'p7', 'p8'];
										this.props.station_data.params.map((param) => {
											current_params[param.key] = param;
										});
										let latest_param = allowed_parameters_in_order.map((param, i) => {
											if(Object.keys(current_params).indexOf(param) >= 0) {
												return <div className="latest-param-container" key={i}>
													<div className="latest-param-text" dangerouslySetInnerHTML={{__html: current_params[param].name }} />
													<div className="latest-param-value">
														<span id={'parameter_value_' + param}>{(parseFloat(this.props.station_data.latest_param_value[param]).toFixed(2))}</span>
														<span dangerouslySetInnerHTML={{__html: ' ' + this.props.station_data.param_units[_.findIndex(this.props.station_data.params, { 'key': param})] }}/>
														{(() => {
															if (param == 'rain') {
																return <span>
																	<span id={'parameter_value_' + param}>{(' / ' + parseFloat(this.props.station_data.latest_param_value[param]*25.4).toFixed(2))}</span>
																	<span dangerouslySetInnerHTML={{__html: ' ' + 'mm' }}/>
																</span>
															}
														})()}
													</div>
												</div>;
											}
										});
										return latest_param;
									})()}
								</div>;
							} else {
								return <div className='latest-param-error'>No data to show.</div>;
							}
						})()}
					</div>
					<Link to={'/stations/' + this.props.station_data.id + '/analytics'} className="btn details-link">
						<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 500 500"><style dangerouslySetInnerHTML={{__html: ".st0{stroke-width:20;stroke-miterlimit:10;}" }} /><circle cx={250} cy={250} r="238.5" fill="#FFFFFF" stroke="#FFFFFF" className="st0" /><path fill="#F6851F" stroke="#F6851F" d="M307.5,273.4c-0.2,0.6-1.1,2.1-2.8,4.5l-12.1,16.6c-35.2,48.9-62.1,73.4-80.6,73.4 c-8.8,0-16.2-3.1-22.1-9.4c-5.9-6.3-8.8-14.1-8.8-23.4c0-10.8,6.5-27.3,19.4-49.4l43.8-74.4c5.8,1.3,10.4,2,13.8,2 c5.1,0,12.2-0.7,21.3-2l-63.7,104.7c-9.2,15.2-13.8,26.8-13.8,35.1c0,8.4,4.4,12.6,13.2,12.6c16.7,0,46-29.7,88-89.1 c0.9-1.9,1.9-2.8,2.8-2.8C306.9,271.7,307.5,272.3,307.5,273.4z M307.4,132.3c0,6.6-2.8,12.7-8.3,18.4c-5.5,5.7-11.6,8.6-18.1,8.6 c-8.4,0-12.6-4.2-12.6-12.6c0-6.2,2.9-12.1,8.7-17.8c5.8-5.7,11.9-8.6,18.2-8.6C303.4,120.3,307.4,124.3,307.4,132.3z" className="st0" /></svg>
						More Info
					</Link>
					{(() => {
						if (this.props.station_data && this.props.station_data.latest_param_value) {
							return <div className="speed-magneto">
								<div className="speed-magneto-chart">
								{(() => {
									if (this.props.station_data.latest_param_value.wspeed) {
										return <ReactHighcharts ref="speed_chart" config={this.speedo_meter} />;
									}
								})()}
								{(() => {
									if (this.props.station_data.latest_param_value.wdir) {
										return <ReactHighcharts ref="magneto_chart" config={this.magneto_config} />;
									}
								})()}
								</div>
								<div className="main-wind-label">
									{(() => {
										if (this.props.station_data.latest_param_value.wspeed) {
											return <div className="wind-label">
												<div className="wind-speed-data" id="wind_speed_data">{this.props.station_data.latest_param_value.wspeed + ' ' + this.props.station_data.param_units[this.speed_index]}</div>
												<div className="wind-speed-label">Wind Speed</div>
											</div>;
										}
									})()}
									{(() => {
										if (this.props.station_data.latest_param_value.wdir) {
											return <div className="wind-label">
												<div className="wind-direction-data" id="wind_direction_data">{parseInt(this.props.station_data.latest_param_value.wdir) + ' ' + this.props.station_data.param_units[this.wind_index]}</div>
												<div className="wind-direction-label">Wind Direction</div>
											</div>;
										}
									})()}
								</div>
							</div>;
						}
					})()}
					{(() => {
						if (this.props.station_data && this.props.station_data.latest_param_value && this.props.station_data.latest_param_value.press) {
							return <div className="pressure-chart">
								<ReactHighcharts ref="pressure_chart" config={this.pressure_config} />
								<div className="pressure-title">
									<div className="pressure-data" id="pressure_data_dashboard">{this.props.station_data.latest_param_value.press + ' ' + this.props.station_data.param_units[this.pressure_index]}</div>
									<div className="pressure-label">Pressure</div>
								</div>
							</div>;
						}
					})()}
					{(() => {
						if (this.suggestions) {
							return <div className="panel-container">
								<div className="panel-title">Suggestions</div>
								<ul className="panel-list">
									{suggestions}
								</ul>
							</div>;
						}
					})()}
				</Scrollbars>
			</div>
		);
	}
}

DeviceDetails.contextTypes = {
	router: React.PropTypes.object
};
