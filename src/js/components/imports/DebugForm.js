import React from 'react';
import moment from 'moment-timezone';
import DateTime from 'react-datetime';

/**
 * This class will return an element having the value of the previous day of the current day.
 * @return {object}
 */
var FromDateTime = React.createClass({
	displayName: 'FromDateTime',
	getInitialState: function() {
		return {fromDate: moment().subtract(1, 'd').add(1, 'm')};
	},
	handleChange: function(date) {
		this.setState({fromDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="from_date_time" dateFormat="DD-MM-YYYY" id="fromTime" timeFormat="HH:mm" defaultValue={that.state.fromDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

/**
 * This class will return an element having the value of the current day.
 * @return {object}
 */
var UptoDateTime = React.createClass({
	displayName: 'UptoDateTime',
	getInitialState: function() {
		return {uptoDate: moment()};
	},
	handleChange: function(date) {
		this.setState({uptoDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="upto_date_time" dateFormat="DD-MM-YYYY" id="upToTime" timeFormat="HH:mm" defaultValue={that.state.uptoDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

/**
 * This Class Used for Device debug form used in device debug page.
 */
export default class DebugForm extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			city_id: props.city_id,
			device_id: props.device_id
		};
		console.log('Device ID:',this.state.device_id);
		this.deviceFilter();
	}

	/**
	 * This function used to set the selected device id in the state.
	 * @param  {number} dev_id This is the selected device's id.
	 */
	changeUrl(dev_id) {
		this.setState({device_id: dev_id});
	}

	/**
	 * This function is used to filter devices as per city selected.
	 * @param  {number} city This is the city id of selected city.
	 */
	deviceFilter(city) {
		/*if (this.props.data_sources) {
			this.dataSources = this.props.data_sources.map((value) => {
				if (city == value.ct_id) {
					return(<option value={value.id}>{value.name}</option>);
				}
			});
			this.setState({city_id: city});
			console.log('City', this.state.city_id);
		}*/
		console.log('City', this.state.city_id);
		this.setState({
			city_id: city ? city : this.props.city_id,
			device_id: 0
		});
	}

	/**
	 * This is invoked before rendering when new props or state are being received. This method is not called for the initial render.
	 * @param  {object} nextProps This is to compare this.props with nextProps for that upadte can be skipped.
	 */
	shouldComponentUpdate(nextProps) {
		console.log(nextProps, this.props);
		if ((nextProps.device_id != this.props.device_id) || (nextProps.city_id != this.props.city_id) || (this.props.time_interval && ((nextProps.time_interval[0] != this.props.time_interval[0]) || (nextProps.time_interval[1] != this.props.time_interval[1])))) {
			this.setState({
				device_id: nextProps.device_id,
				city_id: nextProps.city_id
			});
		}
		return true;
	}

	download_archive_data_of_station() {
		let data_to_be_sent = {
			device_id: this.state.device_id,
			time_interval: [(parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))),(parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')))]
		};
		window.location = '##PR_STRING_REPLACE_API_BASE_PATH##' + 'download_debug_data.php?d='+ JSON.stringify(data_to_be_sent);
	}

	/**
	 * This renders the Debug form for Debug page.
	 * @return {ReactElement} markup
	 */
	render() {
		return (<div className="archive-form">
			<h3 className="archive-title">View Debug Logs</h3>
			<div className="archive-options">
				<div className="option-row">
					<div className="option-label">Data Source</div>
					<div className="option-content">
						<select className="form-control" id="cityID" title="Select City" value={this.state.city_id} onChange={(e) => this.deviceFilter(e.target.value)}>
							<option value={0} disabled>Select City</option>
							{(() => {
								if (this.props.data_sources_city) {
									let dataSourceCities = this.props.data_sources_city.map((value) => {
										return(<option value={value.id}>{value.name}</option>);
									});
									return dataSourceCities;
								}
							})()}
						</select>
						<div className="hyphen table"></div>
						<select className="form-control" id="dataID" title="Select Device" value={this.state.device_id} onChange={(e) => this.changeUrl(e.target.value)}>
							<option value={0} disabled>Select Device</option>
							{(() => {
								if (this.props.data_sources && this.state.city_id) {
									let dataSources = this.props.data_sources.map((value) => {
										if (this.state.city_id == value.ct_id) {
											return(<option value={value.id}>{value.name}</option>);
										}
									});
									return dataSources;
								}
							})()}
						</select>
					</div>
				</div>
				<div className="option-row">
					<div className="option-label">Time Interval</div>
					<div className="option-content">
						<div className="date-time">
							<div className="rdt from_date_time" id="from_time">
								<FromDateTime />
							</div>
						</div>
						<div className="hyphen">-</div>
						<div className="date-time">
							<div className="rdt upto_date_time" id="upto_time">
								<UptoDateTime />
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="archive-btns">
				<button type="button" className="btn btn-primary" onClick={() => this.props.get_device_error_log(this.state.device_id)}>View</button>
				<button type="button" id="download_data_btn" className="btn btn-success" disabled={this.state.city_id > 0 ? false : true} onClick={() => this.download_archive_data_of_station()} >Download</button>
			</div>
		</div>);
	}
}

DebugForm.contextTypes = {
	router: React.PropTypes.object
};