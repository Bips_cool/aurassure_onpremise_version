import React from 'react';
import ReactDOM from 'react-dom';
import moment from 'moment-timezone';
import { DateRange } from 'react-date-range';

export default class Calendar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			calendar_opened: false,
			calendar_time: null,
			selected_dates: 'Today',
			view_mode: (window.innerWidth >= 1024) ? 'desktop' : 'mobile'
		};

		this.today = moment().tz('Asia/Kolkata').format('DD-MM-YYYY');
		this.yesterday = moment().subtract(1, 'day').tz('Asia/Kolkata').format('DD-MM-YYYY');
		this.sixmonth = moment().subtract(180, 'day').tz('Asia/Kolkata').format('DD-MM-YYYY');

		if (this.props.calendar_time) {
			this.calendarTime = {
				fromTime: this.props.calendar_time[0],
				uptoTime : this.props.calendar_time[1]
			};
		} else {
			this.calendarTime = {
				fromTime: this.today,
				uptoTime: this.today
			};
		}
	}

	toggleCalender() {
		this.setState({
			calendar_opened: !this.state.calendar_opened
		});
	}

	closeCalender(event) {
		const calenderModule = ReactDOM.findDOMNode(this.refs.calenderModule);
		if (calenderModule && !calenderModule.contains(event.target)) {
			// console.log('closing modules menu');
			this.setState({
				calendar_opened: false
			});
		}
	}

	componentDidMount() {
		window.react_container.addEventListener('click', (event) => this.closeCalender(event));

		window.addEventListener('resize', () => {
			this.setState({view_mode: (window.innerWidth >= 1024) ? 'desktop' : 'mobile'});
		});
	}

	componentWillUnmount() {
		window.react_container.removeEventListener('click', (event) => this.closeCalender(event));
	}

	componentWillMount() {
		this.setCalendarDates();
	}

	setCalendarDates(pass, date) {
		if (date) {
			this.calendarTime = date;
		}
		console.log('Calendar Time',this.calendarTime);
		let text = '';
		if (this.calendarTime.fromTime === this.calendarTime.uptoTime && this.calendarTime.fromTime === this.today) {
			text = 'Today';
		} else if (this.calendarTime.fromTime === this.calendarTime.uptoTime && this.calendarTime.fromTime === this.yesterday) {
			text = 'Yesterday';
		} else if (this.calendarTime.fromTime === this.calendarTime.uptoTime) {
			text = this.calendarTime.fromTime;
		} else {
			text = this.calendarTime.fromTime + ' - ' + this.calendarTime.uptoTime;
		}
		this.setState({
			calendar_opened: null,
			calendar_time: this.calendarTime,
			selected_dates: text
		});
		if (pass) {
			this.props.changeDate([this.calendarTime.fromTime, this.calendarTime.uptoTime]);
		}
	}

	handleChange(range) {
		this.calendarTime = {
			fromTime: range.startDate.format('DD-MM-YYYY').toString(),
			uptoTime : range.endDate.format('DD-MM-YYYY').toString()
		};
		console.log(this.calendarTime);
	}

	render() {
		return <div>
			<div className="select-calendar"  onClick={() => this.toggleCalender()}>
				<svg xmlns="http://www.w3.org/2000/svg" height="18" width="18" viewBox="0 0 500 353.4"><path d="M479.5 0H428v40h-49V0H121.5v40H72V0H20.6C9.2 0 0 9.2 0 20.5v312.3c0 11.3 9.2 20.5 20.5 20.5h459c11.3 0 20.5-9.2 20.5-20.5V20.5C500 9.2 490.8 0 479.5 0zm-25.8 302.8c0 4-3.2 7.2-7.2 7.2H54c-4 0-7-3.2-7-7.2V87.2c0-4 3-7.2 7-7.2h392.5c4 0 7.2 3.2 7.2 7.2v215.6z"></path><path d="M114.3 133.2h61v44.2h-61zm94.7 0h61v44.2h-61zm-94.7 72h61v44.2h-61zm94.7 0h61v44.2h-61zm94.7-72h61v44.2h-61z"></path>
				</svg>
				{this.state.selected_dates}
			</div>
			{(() => {
				if (this.state.calendar_opened) {
					return <div className="calendar-content" ref="calenderModule">
						<DateRange
							startDate={(this.state.calendar_time) ? moment(this.state.calendar_time.fromTime,'DD-MM-YYYY') : moment()/*.startOf('month')*/}
							endDate={(this.state.calendar_time) ? moment(this.state.calendar_time.uptoTime,'DD-MM-YYYY') : moment()}
							calendars={(this.state.view_mode === 'desktop') ? 3 : 1}
							linkedCalendars={true}
							maxDate={this.today}
							minDate={this.sixmonth}
							ranges={(this.state.view_mode === 'desktop') ? {
								'Today': {
									startDate: function startDate(now) {
										return now;
									},
									endDate: function endDate(now) {
										return now;
									}
								},
								'Yesterday': {
									startDate: function startDate(now) {
										return now.add(-1, 'days');
									},
									endDate: function endDate(now) {
										return now.add(-1, 'days');
									}
								},
								'This Month': {
									startDate: function startDate(now) {
										return now.startOf('month');
									},
									endDate: function endDate(now) {
										return now;
									}
								},
								'This Quarter': {
									startDate: function startDate(now) {
										return now.startOf('quarter');
									},
									endDate: function endDate(now) {
										return now;
									}
								}
							} : {}
							}
							disableDaysAfterToday={true}
							onInit={(range) => this.handleChange(range)}
							onChange={(range) => this.handleChange(range)}
							theme={{
								DateRange: {
									display: 'table',
									float: 'right',
									paddingLeft: 5,
									paddingRight: (this.state.view_mode === 'desktop') ? 0 : 15,
									background: '#f7f7f7'
								},
								Calendar: {
									width: 220,
									color: '#555555',
									background: '#f7f7f7'
								},
								DaySelected: {
									background: '#496DF5'
								},
								PredefinedRanges: {
									paddingTop: 50,
									width: 100,
									background: '#f7f7f7'
								},
								PredefinedRangesItem: {
									color: '#8a8a8a',
									background: '#EEEEEE'
								},
								PredefinedRangesItemActive: {
									color: '#496DF5'
								}
							}} />
						<div className="clearfix"></div>
						<div className="btn-group">
							<button className="btn btn-default" onClick={() => this.toggleCalender()}>Cancel</button>
							<button className="btn btn-primary" onClick={() => this.setCalendarDates(1)}>Apply</button>
						</div>
					</div>;
				} else {
					return null;
				}
			})()}
		</div>;
	}
}

Calendar.contextTypes = {
	router: React.PropTypes.object
};