import React from 'react';
import ReactHighcharts from 'react-highcharts';

/**
 * This Class Used for Compare locations.
 */
export default class CompareLocations extends React.Component {
	/**
	 * This is the Constructor of the page.
	 */
	constructor() {
		super();
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			show_search_list: null,
			loading_data_flag: 1,
			search_term: ''
		};

		this.fetch_stations();
	}

	/**
	 * This function is used for serch box to search stations.
	 * @param  {Object} e This the user input value in search box.
	 */
	search_list(e) {
		this.setState({search_term: e.trim().toLowerCase()});
	}

	/**
	 * This function sets state to open search list.
	 */
	open_search_list() {
		this.setState({show_search_list: 1});
	}

	/**
	 * This function sets state for closing search list and clear user input value in searchbox.
	 */
	close_search_list() {
		this.setState({
			show_search_list: null,
			search_term: ''
		});
		document.getElementById('search').value = '';
	}

	/**
	 * This function is used for fetch compared stations data.
	 */
	fetch_stations() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_data_of_compared_stations_of_user.php', {
			credentials: 'include',
			method: 'POST',
			headers: {'Content-Type': 'application/json'}
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log(json);
			if (json.status === 'success') {
				var allStations = that.props.all_stations;
				json.compared_stations.map(function(c_station) {
					allStations.map(function(station, j) {
						if (c_station.id === station.id) {
							console.info('Removed ['+j+']:',station.id);
							// all_stations.splice(all_stations[j], 1);
							delete allStations[j];
						}
					}, that);
				}, that);
				that.setState({
					loading_data_flag: null,
					compared_stations: json.compared_stations,
					filtered_stations: allStations
				});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function used for add station to compared list.
	 * @param {Number} id This is the station id of selected station for compare.
	 */
	add_station(id) {
		console.log('Add Station:',id);
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'add_station_to_compare_list_of_user.php', {
			credentials: 'include',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				station_id: id
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log(json);
			if (json.status === 'success') {
				document.getElementById('search').value = '';
				var all_stations = that.props.all_stations;
				var compared_stations = that.state.compared_stations;
				all_stations.map(function(station) {
					if (station.id === id) {
						compared_stations.push({
							aqi: station.aqi,
							aqi_range: station.aqi_range,
							id: station.id,
							name: station.name
						});
					}
				}, that);
				console.info(that.props.all_stations);
				compared_stations.map(function(c_station) {
					all_stations.map(function(station, j) {
						if (c_station.id === station.id) {
							delete all_stations[j];
						}
					}, that);
				}, that);
				that.setState({
					compared_stations: compared_stations,
					filtered_stations: all_stations,
					show_search_list: null
				});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function used for remove station from compare list.
	 * @param  {Number} id This is the station id of selected station.
	 */
	remove_station(id) {
		console.log('Remove Station:',id);
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'delete_station_from_compare_list_of_user.php', {
			credentials: 'include',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				station_id: id
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log(json);
			if (json.status === 'success') {
				var compared_stations = that.state.compared_stations;
				compared_stations.map(function(c_station, i) {
					if (c_station.id === id) {
						delete compared_stations[i];
					}
				}, that);
				that.setState({
					compared_stations: compared_stations
				});
				that.props.retrive_data(1);
				// that.fetch_stations();
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This renders the Compared stations.
	 * @return {ReactElement} markup
	 */
	render() {
		console.log('Compared Stations', this.state.compared_stations);
		if (this.state.loading_data_flag) {
			return <div className="loading"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
		}

		if (!this.state.loading_data_flag && this.state.filtered_stations && this.state.compared_stations) {
			var stations = [];
			this.state.filtered_stations.map(function(station, i) {
				if ((this.state.search_term == '') || (station.name).toLowerCase().search(this.state.search_term) >= 0) {
					stations.push(<li className="hellip" key={i}>
						{station.name}<button className="btn btn-success btn-xs pull-right" onClick={() => this.add_station(station.id)}>＋</button>
					</li>);
				}
			}, this);
			if (!stations.length && (this.state.search_term != '')) {
				stations.push(<li>No results found by term {'"'+this.state.search_term+'"'}.</li>);
			} else if (!stations.length && (this.state.search_term == '')) {
				stations.push(<li onClick={() => this.close_search_list()}>No stations available.</li>);
			}

			var compared_stations = [];
			this.state.compared_stations.map(function(station, i) {
				compared_stations.push(<div className="compare-item" key={i}>
					<div className="icon">#{i+1}</div>
					<div className="item-text hellip">{station.name}</div>
					<div className={'item-value aqi-'+station.aqi_range}>{station.aqi} <sub>AQI</sub></div>
					<div className="item-remove" onClick={() => this.remove_station(station.id)}>✖</div>
				</div>);
			}, this);

			var min_value = null,
				min_range = null,
				min_station = '',
				max_value = null,
				max_range = null,
				max_station = '';
			this.state.compared_stations.map(function(station) {
				if (min_value && parseInt(min_value) > parseInt(station.aqi)) {
					min_value = parseInt(station.aqi);
					min_range = station.aqi_range;
					min_station = station.name;
				} else if (!min_value) {
					min_value = parseInt(station.aqi);
					min_range = station.aqi_range;
					min_station = station.name;
				}
				if (max_value && parseInt(max_value) < parseInt(station.aqi)) {
					max_value = parseInt(station.aqi);
					max_range = station.aqi_range;
					max_station = station.name;
				} else if (!max_value) {
					max_value = parseInt(station.aqi);
					max_range = station.aqi_range;
					max_station = station.name;
				}
			}, this);
		}

		let graph_data = [];
		let categories = [];
		if (this.state.compared_stations) {
			this.state.compared_stations.map((stations, i) => {
				let color = window.color_code.color_1;
				if(stations.aqi >= 401) {
					color = window.color_code.color_6;
				} else if(stations.aqi >= 301) {
					color = window.color_code.color_5;
				} else if(stations.aqi >= 201) {
					color = window.color_code.color_4;
				} else if(stations.aqi >= 101) {
					color = window.color_code.color_3;
				} else if(stations.aqi >= 51) {
					color = window.color_code.color_2;
				}
				graph_data.push({
					y: parseInt(stations.aqi),
					color: color
				});
				categories.push(i + 1);
			});
		}

		/**
		 * This is a Object to plot graph data.
		 * @type {Object}
		 * @property {Object} chart This sets chart properties.
		 * @property {Object} plotOptions This customize the graph plot settings/options.
		 * @property {Object} title This sets the title for the graph.
		 * @property {Object} subtitle This sets the subtitle for the graph.
		 * @property {Object} xAxis This defines what to show in xAxis.
		 * @property {Object} yAxis This defines what to show in yAxis.
		 * @property {Object} legend This hides the printing options from graph.
		 * @property {Object} tooltip This sets what to show in tooltip.
		 * @property {Array} series This contains data for ploting graph.
		 */
		this.config = {
			chart: {
				type: 'column',
				height: 150
			},
			plotOptions: {
				series: {
					pointPadding: 0,
					groupPadding: 0
				}
			},
			title: {
				text: ''
			},
			subtitle: {
				text: ''
			},
			xAxis: {
				title: {
					enabled: true,
					text: 'Cities',
					style: {
						fontWeight: 'normal'
					}
				},
				type: 'category',
				categories: categories,
				tickLength: 0
			},
			yAxis: {
				title: {
					text: 'AQI'
				},
				lineWidth: 0,
				minorGridLineWidth: 0,
				lineColor: 'transparent',      
				labels: {
					enabled: false
				},
				minorTickLength: 0,
				tickLength: 0,
				gridLineColor: 'transparent'
			},
			legend: {
				enabled: false
			},
			tooltip: {
				headerFormat: '<span style="font-size:11px">AQI</span><br>',
				pointFormat: '<span style="color:{point.color}"><b>{point.y}</b><br/>'
			},
			series: [{
				data: graph_data
			}]
		};

		return(
			<div className="full-height" id="panelView">
				<div className="location-time-container">
					<div className="location-search-container">
						<div className="icon location"></div>
						<div className="location-search">
							<input className="form-control" id="search" placeholder="Search cities to compare" onChange={(e) => this.search_list(e.target.value)} onFocus={() => this.open_search_list()} />
							<span className="close" onClick={() => this.close_search_list()}>✖</span>
							<ul id="searchResults" className={(this.state.show_search_list) ? '' : 'hide'}>
								{stations}
							</ul>
						</div>
					</div>
				</div>
				<div className={'compare-container' + ((this.state.show_search_list) ? ' hide' : '')}>
					{compared_stations}
				</div>
				<div className={'min-max-container' + ((this.state.show_search_list) ? ' hide' : '')}>
					<div className="min-max-wrapper">
						<div className="max-container">
							<div className="item-text hellip">{max_station}</div>
							<div className={'item-value aqi-'+max_range}>{max_value} <sub>AQI</sub></div>
							<div className="item-label">Max</div>
						</div>
						<div className="min-container">
							<div className="item-text hellip">{min_station}</div>
							<div className={'item-value aqi-'+min_range}>{min_value} <sub>AQI</sub></div>
							<div className="item-label">Min</div>
						</div>
					</div>
				</div>
				<div className={'compare-graph-container' + ((this.state.show_search_list) ? ' hide' : '')}>
					<ReactHighcharts ref="chart" config={this.config} />
				</div>
			</div>
		);
	}
}
