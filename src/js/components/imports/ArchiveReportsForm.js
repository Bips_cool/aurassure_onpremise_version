import React from 'react';
import moment from 'moment-timezone';
import DateTime from 'react-datetime';
import Select from './Select';
import MultiSelect from './MultiSelect';
import _ from 'lodash';

var FromDateTime = React.createClass({
	displayName: 'FromDateTime',
	getInitialState: function() {
		return {fromDate: moment().subtract(1, 'day')};
	},
	handleChange: function(date) {
		this.setState({fromDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="from_date_time" dateFormat="DD-MM-YYYY" timeFormat={false} defaultValue={that.state.fromDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

/**
 * This class will return an element having the value of the current day.
 * @return {object}
 */
var UptoDateTime = React.createClass({
	displayName: 'UptoDateTime',
	getInitialState: function() {
		return {uptoDate: moment()};
	},
	handleChange: function(date) {
		this.setState({uptoDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="upto_date_time" dateFormat="DD-MM-YYYY" timeFormat={false} defaultValue={that.state.uptoDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

export default class ArchiveReportsForm extends React.Component {
	constructor(props) {
		super(props);
		/**
		 * the state of the component
		 * @type {object}
		 * @property {string} station_type_placeholder placeholder for the station type multiselect
		 * @property {string} stations_plceholder placeholder for stations multiselect
		 * @property {array} all_parameters list of all the paramneters
		 * @property {object} all_stations_list list of all the stations 
		 * @property {array} selected_parameters parameters of the selected stations
		 * @property {array} checked_parameters parameters those are selected by the user
		 * @property {array} station_type list of all the station type
		 * @property {array} time_interval 'from date' and 'to date' in unix timestamp
		 * @property {number} avg_time_interval no of seconds of which, average to be done
		 * @property {array} station_type_selected type of station selected
		 * @property {array} data_format type in which the data to be shown 'grid' or 'graph'
		 * @property {object} station_list lsit of station under selected station type
		 * @property {array} stations_selected lsit of station selected
		 */
		this.state = {
			all_stations: null,
			// is_admin: false,
			is_admin: document.getElementById('city_id') ? (document.getElementById('city_id').value == 5 ? true : false) : false,
			stations_placeholder: 'Select Station',
			city_placeholder: 'Select City',
			data_type_check: true,
			conversion_type: 'usepa',
			station_parameters: null,
			city_selected: null
		};
		this.hourly_aqi = {
			'name': 'AQI',
			'key': 'hourly_aqi',
			'unit': ''
		};
		this.wind_rose = {
			'name': 'Wind Rose',
			'key': 'wind_rose',
			'unit': ''
		};
		this.pollution_rose = {
			'name': 'Pollution Rose',
			'key': 'pollution_rose',
			'unit': ''
		};
		this.pollutants = [
			{
				'name': 'PM<sub>2.5</sub>',
				'key': 'pm2.5',
				'unit': ''
			},
			{
				'name': 'PM<sub>10</sub>',
				'key': 'pm10',
				'unit': ''
			},
			{
				'name': 'CO',
				'key': 'co',
				'unit': ''
			},
			{
				'name': 'CO<sub>2</sub>',
				'key': 'co2',
				'unit': ''
			},
			{
				'name': 'NO<sub>2</sub>',
				'key': 'no2',
				'unit': ''
			},
			{
				'name': 'SO<sub>2</sub>',
				'key': 'so2',
				'unit': ''
			},
			{
				'name': 'NH<sub>3</sub>',
				'key': 'nh3',
				'unit': ''
			},
			{
				'name': 'O<sub>3</sub>',
				'key': 'o3',
				'unit': ''
			},
			{
				'name': 'NO',
				'key': 'no',
				'unit': ''
			},
			{
				'name': 'NO<sub>x</sub>',
				'key': 'nox',
				'unit': ''
			}
		];
		this.station_type = null;
		this.all_stations_parameters = [];
		this.enabled_raw_data = true;
		this.error_msg = null;
		this.all_parameters_of_station = [];
	}

	/**
	 * It calls the API to get the details of all stations & set into state.
	 */
	get_all_cities() {
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_cities_for_archive.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include'
		}).then(function(Response) {
			console.log('Response', Response);
			return Response.json();
		}).then(function(json) {
			console.log('All Cities', json);
			if (json.status === 'success') {
				that.setState({
					all_cities: json.cities
				}, () => {
					that.getCities();
				});
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger', 'Unable to load data!');
		});
	}

	getCities() {
		let that = this,
			all_cities_list = {};
		if (that.state.all_cities && Object.keys(that.state.all_cities).length) {
			that.state.all_cities.map((city, index) => {
				console.log('key', index);
				all_cities_list[city.id] = city.name;
			});
		}
		console.log('city_lists', all_cities_list);
		that.setState({
			all_cities_list: all_cities_list
		});
	}

	getCityValue(value) {
		console.log('values city', value);
		let that = this;
		that.setState({
			city_selected: value
		});
	}

	componentDidMount(){
		this.get_details_of_stations();
		this.get_all_cities();	
	}

	changeConversionType(type) {
		let required_parameters = [];
		let required_parameter_keys = [];
		let all_parameters_of_station = [];
		let conversion_type = this.state.conversion_type;
		if (type == 'usepa') {
			conversion_type = type;
			// console.log('conversion_type', type);
		} else {
			conversion_type = type;
			// console.log('conversion_type', type);
		}
		
		this.all_parameters_of_station.map((parameter) => {
			if(document.getElementById('param_'+ parameter.key) && document.getElementById('param_'+ parameter.key).checked) {
				if (conversion_type == 'naqi') {
					if (parameter.key == 'so2' || parameter.key == 'no2' || parameter.key == 'o3') {
						parameter.unit = 'μg / m<sup>3</sup>';
					} else if (parameter.key == 'co') {
						parameter.unit = 'mg / m<sup>3</sup>';
					}
					required_parameters.push(parameter);
					required_parameter_keys.push(parameter.key);
					// console.log('parameter_unit', this.required_parameters);
					// console.log('parameter_keys', this.required_parameter_keys);
				} else {
					if (parameter.key == 'so2' || parameter.key == 'no2' || parameter.key == 'o3') {
						parameter.unit = 'ppb';
					} else if (parameter.key == 'co') {
						parameter.unit = 'ppm';
					}
					required_parameters.push(parameter);
					required_parameter_keys.push(parameter.key);
					// console.log('parameter_keys_else', this.required_parameter_keys);
				}
				// console.log('parameter_required_in_function_addSelectedParameters', this.required_parameters);
			}
		});

		this.setState({
			conversion_type: conversion_type,
			station_parameters: required_parameters
		});
	}

	/**
	 * This function gets an array of station id those are selected from the multi select
	 * @param {Array} values 
	 */
	getStationIds(values) {
		let that = this;
		console.log('values stations', values);
		if (values.length > 1) {
			document.getElementById('raw_data').disabled = true;
			if (document.getElementById('raw_data').checked == true) {
				this.enabled_raw_data = false;
				document.getElementById('data_type_error').innerHTML = 'Please Select Average';
			}
			// document.getElementById('avg_over_time').checked = true;
		} else {
			document.getElementById('raw_data').disabled = false;
			this.enabled_raw_data = true;
			document.getElementById('data_type_error').innerHTML = null;
		}
		
		this.setState({
			stations_selected: values
		},()=> that.setParamsIds());
	}

	/**
	 * This function sets the liste of station being registered under the currently selected station types
	 */
	setStationList(){
		let that = this,
			all_stations_list = {};
		if (that.state.all_stations && Object.keys(that.state.all_stations).length) {
			that.state.all_stations.map((station, index) => {
				// console.log('key', index);
				all_stations_list[station.id] = station.name;
			});
		}
		console.log('station_lists', all_stations_list);
		that.setState({
			all_stations_list: all_stations_list
		}, () => {
			that.resetSelectedStations();
		});
	}

	/**
	 * This function resets the selected stations and checked parameters when ever a station type is unselected
	 * It removes the selected station and checked parametrs of the previously selected selected stations
	 */
	resetSelectedStations(){
		let that = this,
			selected = [],
			checked_parameters = [];
		if (that.state.stations_selected) {
			that.state.stations_selected.map((id) => {
				that.state.all_stations.map((station, index) => {
					if(station.id == id && selected.indexOf(id) === -1) {
						selected.push(id);
					}
				});
			});
		}

		console.log('selected', selected);

		if (that.state.selected_parameters) {
			that.state.selected_parameters.map((id) => {
				that.state.checked_parameters.map((check_id) => {
					if(check_id == id && checked_parameters.indexOf(check_id) === -1)
						checked_parameters.push(check_id);
				});
			});
		}

		that.setState({
			stations_selected: selected,
			checked_parameters: checked_parameters
		});
	}

	/**
	 * This function sets all the parameters in the exists in the currently selected stations 
	 */
	setParamsIds(){
		let that = this;
		// console.log(station_ids);
		let rain_box = document.getElementById('param_rain'),
			aqi_box = document.getElementById('param_hourly_aqi'),
			wind_box = document.getElementById('param_wind_rose'),
			pollution_box = document.getElementById('param_pollution_rose');
		let new_parameters = [];
		if (!new_parameters.includes('hourly_aqi')) {
			new_parameters.push('hourly_aqi');
		}
		this.state.all_stations.map((station) => {
			if(this.state.stations_selected && this.state.stations_selected.length && this.state.stations_selected.includes(station.id)) {
				if (_.some(station.parameters, {key: 'wspeed'}) && _.some(station.parameters, {key: 'wdir'})) {
					if (!new_parameters.includes('wind_rose')) {
						new_parameters.push('wind_rose');
					}
				}
				if (_.some(station.parameters, {key: 'wdir'}) && (_.some(station.parameters, {key: 'pm2.5'}) || _.some(station.parameters, {key: 'pm10'}) || _.some(station.parameters, {key: 'no2'}) || _.some(station.parameters, {key: 'o3'}) || _.some(station.parameters, {key: 'co'}) || _.some(station.parameters, {key: 'so2'}))) {
					if (!new_parameters.includes('pollution_rose')) {
						new_parameters.push('pollution_rose');
					}
				}
			}
		});
		that.state.all_stations.map((station) => {
			that.state.stations_selected.map((id) => {
				if(station.id == id){
					// console.log(id, station);
					// console.log('looking here' + station.name);
					station.parameters.map((params)=>{
						// console.log('params', params);
						if(new_parameters.indexOf(params.key) === -1){
							// console.log('params_key', params.key);
							new_parameters.push(params.key);
						}
					});
				}
			});
		});

		if (that.state.data_type_check && document.getElementById('raw_data').checked) {
			console.log('entered into is_admin');
			if (new_parameters.indexOf('hourly_aqi') !== -1) {
				new_parameters.splice(new_parameters.indexOf('hourly_aqi'), 1);
			}
			if (new_parameters.indexOf('wind_rose') !== -1) {
				new_parameters.splice(new_parameters.indexOf('wind_rose'), 1);
			}
			if (new_parameters.indexOf('pollution_rose') !== -1) {
				new_parameters.splice(new_parameters.indexOf('pollution_rose'), 1);
			}
			if (!this.state.is_admin) {
				if (new_parameters.indexOf('rain') !== -1) {
					new_parameters.splice(new_parameters.indexOf('rain'), 1);
				}
			}
		}

		if(this.state.stations_selected.length === 0){
			new_parameters = null;
		}

		console.log('selected new params',new_parameters);

		this.setState({
			selected_parameters: new_parameters,
			checked_parameters: new_parameters
		},()=>{
			this.resetSelectedStations();
			this.addSelectedParameters();
		});
	}

	/**
	 * This one sets and unsets the selected and unselected parameters respectively
	 * @param {Boolean} status 
	 * @param {String} id 
	 */
	addSelectedParameters(status, key) {
		let selected_parameters = this.state.checked_parameters, all_parameters_of_station = [];
		let rain_box = document.getElementById('param_rain'),
			aqi_box = document.getElementById('param_hourly_aqi'),
			wind_box = document.getElementById('param_wind_rose'),
			pollution_box = document.getElementById('param_pollution_rose');
		console.log('status', status);
		console.log('key', key);
		if (status && selected_parameters && selected_parameters.indexOf(key) === -1) {
			selected_parameters.push(key);
		} else if (selected_parameters && selected_parameters.indexOf(key) !== -1) {
			selected_parameters.splice(selected_parameters.indexOf(key), 1);
		}

		if (!(_.some(all_parameters_of_station, {key: 'hourly_aqi'}))) {
			all_parameters_of_station.push(this.hourly_aqi);
		}
		this.state.all_stations.map((station) => {
			if(this.state.stations_selected && this.state.stations_selected.length && this.state.stations_selected.includes(station.id)) {
				if (_.some(station.parameters, {key: 'wspeed'}) && _.some(station.parameters, {key: 'wdir'})) {
					if (!(_.some(all_parameters_of_station, {key: 'wind_rose'}))) {
						all_parameters_of_station.push(this.wind_rose);
					}
				}
				if (_.some(station.parameters, {key: 'wdir'}) && (_.some(station.parameters, {key: 'pm2.5'}) || _.some(station.parameters, {key: 'pm10'}) || _.some(station.parameters, {key: 'no2'}) || _.some(station.parameters, {key: 'o3'}) || _.some(station.parameters, {key: 'co'}) || _.some(station.parameters, {key: 'so2'}))) {
					if (!(_.some(all_parameters_of_station, {key: 'pollution_rose'}))) {
						all_parameters_of_station.push(this.pollution_rose);
					}
				}
			}
		});

		this.state.all_stations.map((station) => {
			if(this.state.stations_selected && this.state.stations_selected.length && this.state.stations_selected.includes(station.id)) {
				station.parameters.map((params) => {
					if (!(_.some(all_parameters_of_station, {key: params.key}))) {
						all_parameters_of_station.push(params);
					}
				});
			}
		});
		this.all_parameters_of_station = all_parameters_of_station;

		let checked_all = false;
		// console.log('all_parameters_of_station'+ all_parameters_of_station.length+ ' => ', all_parameters_of_station);
		// console.log('selected_parameters' + selected_parameters.length + ' => ', selected_parameters);
		if (selected_parameters && selected_parameters.length) {
			if (this.state.is_admin) {
				if (this.state.data_type_check && aqi_box && wind_box && pollution_box) {
					// console.log('data_type_check ==> 1');
					checked_all =  all_parameters_of_station.length-3 === selected_parameters.length ? true : false;
				} else if (this.state.data_type_check && aqi_box && wind_box) {
					// console.log('data_type_check ==> 2');
					checked_all =  all_parameters_of_station.length-2 === selected_parameters.length ? true : false;
				} else if (this.state.data_type_check && aqi_box ) {
					// console.log('data_type_check ==> 3');
					checked_all =  all_parameters_of_station.length-1 === selected_parameters.length ? true : false;
				} else {
					// console.log('data_type_check ==> 4');
					checked_all =  all_parameters_of_station.length === selected_parameters.length ? true : false;
				}
			} else {
				if (this.state.data_type_check && rain_box && aqi_box && wind_box && pollution_box) {
					// console.log('data_type_check_else ==> 1');
					checked_all = all_parameters_of_station.length-4 === selected_parameters.length ? true : false;
				} else if (this.state.data_type_check && rain_box && aqi_box && wind_box) {
					// console.log('data_type_check_else ==> 2');
					checked_all = all_parameters_of_station.length-3 === selected_parameters.length ? true : false;
				} else if (this.state.data_type_check && rain_box && aqi_box) {
					// console.log('data_type_check_else ==> 3');
					checked_all = all_parameters_of_station.length-2 === selected_parameters.length ? true : false;
				} else if (this.state.data_type_check && aqi_box) {
					// console.log('data_type_check_else ==> 4');
					checked_all = all_parameters_of_station.length-1 === selected_parameters.length ? true : false;
				} else if (this.state.data_type_check) {
					// console.log('data_type_check_else ==> 5');
					checked_all = all_parameters_of_station.length === selected_parameters.length ? true : false;
				} else {
					checked_all = all_parameters_of_station.length === selected_parameters.length ? true : false;
				}
			}
		}

		this.setState({
			checked_parameters: selected_parameters
		}, () => {
			console.log('checked parameters', this.state.checked_parameters);
		});
		document.getElementById('all_params').checked = checked_all;
	}

	/**
	 * It calls the API to get the details of all stations & set into state.
	 */
	get_details_of_stations() {
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_all_station_details_for_archive_page.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json();
		}).then(function(json) {
			console.log('All Stations Data', json);
			if (json.status === 'success') {
				that.setState({
					all_stations: json.stations,
					all_parameters: json.all_parameters
				}, () => {
					that.setStationList();
				});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger', 'Unable to load data!');
		});
	}

	/**
	 * This function validate the required data to be sent to API for viewing & download.
	 * @return {Boolean|Object} This will return either Boolean false value or the verified object of data for API.
	 */
	validateData() {
		let that = this;
		let required_params = [];
		let from_time = moment(document.querySelector('.from_date_time .form-control').value, "DD-MM-YYYY").unix(),
			upto_time = moment(document.querySelector('.upto_date_time .form-control').value, "DD-MM-YYYY").unix();
		let err_msg;
		if (document.getElementById('city_select') && document.getElementById('city_select').checked == true) {
			if (this.state.city_selected != null) {
				if ((parseInt(from_time) < parseInt(upto_time)) &&
					(
						(document.getElementById('view_data_as_grid').checked === true) ||
						(document.getElementById('view_data_as_graph').checked === true)
					)) {
					return {
						city_id: that.state.city_selected,
						from_time: document.querySelector('.from_date_time .form-control').value,
						upto_time: document.querySelector('.upto_date_time .form-control').value,
						data_type: that.state.data_type_check,
						duration: document.querySelector('#duration').value,
						conversion_type: that.state.conversion_type
					};
				} else {
					err_msg = 'Invalid Input!';
					showPopup('danger', err_msg);
					return false;
				}
			} else {
				err_msg = 'Select a city from list';
				showPopup('danger', err_msg);
				return false;
			}
		} else {
			that.all_parameters_of_station.map((parameter) => {
				if(document.getElementById('param_'+ parameter.key) && document.getElementById('param_'+ parameter.key).checked) {
					if (that.state.conversion_type == 'naqi') {
						if (parameter.key == 'so2' || parameter.key == 'no2' || parameter.key == 'o3') {
							parameter.unit = 'μg / m<sup>3</sup>';
						} else if (parameter.key == 'co') {
							parameter.unit = 'mg / m<sup>3</sup>';
						}
						required_params.push(parameter);
						// console.log('parameter_unit', that.required_params);
					} else {
						if (parameter.key == 'so2' || parameter.key == 'no2' || parameter.key == 'o3') {
							parameter.unit = 'ppb';
						} else if (parameter.key == 'co') {
							parameter.unit = 'ppm';
						}
						required_params.push(parameter);
					}
				}
			});
			console.log('station_parameters', required_params);
			that.setState({station_parameters: required_params});
				console.log('Time Interval' + from_time + '=> ', upto_time);
			if ((parseInt(from_time) < parseInt(upto_time)) &&
				(
					(document.getElementById('view_data_as_grid').checked === true) ||
					(document.getElementById('view_data_as_graph').checked === true)
				) &&
				(that.state.stations_selected && that.state.stations_selected.length)
			) {
				console.log('Station List', that.state.stations_selected);
				console.log('Checked Parametes', that.state.checked_parameters);
				console.log('From Time', from_time);
				console.log('Upto time', upto_time);
				console.log('data_type', that.state.data_type_check);
				console.log('duration', document.querySelector('#duration').value);
				console.log('conversion_type', that.state.conversion_type);
				return {
					station_ids: that.state.stations_selected,
					parameters: that.state.checked_parameters,
					from_time: document.querySelector('.from_date_time .form-control').value,
					upto_time: document.querySelector('.upto_date_time .form-control').value,
					data_type: that.state.data_type_check,
					duration: document.querySelector('#duration').value,
					conversion_type: that.state.conversion_type,
					station_parameters: required_params
				};
			} else {
				err_msg = 'Invalid Input!';
				if (that.state.stations_selected && !that.state.stations_selected.length) {
					// document.getElementById('view_data_btn').disabled = true;
					// document.getElementById('download_data_btn').disabled = true;
					err_msg = 'Please select a Station from list!';
				} else if (that.state.checked_parameters && !that.state.checked_parameters.length) {
					// document.getElementById('view_data_btn').disabled = true;
					// document.getElementById('download_data_btn').disabled = true;
					err_msg = 'Please select atleast 1 Parameter!';
				} else if (!that.enabled_raw_data && (document.getElementById('raw_data').checked == true)) {
					// document.getElementById('view_data_btn').disabled = true;
					// document.getElementById('download_data_btn').disabled = true;
					err_msg = 'Please select another data view type';
				} else {
					// document.getElementById('view_data_btn').disabled = false;
					// document.getElementById('download_data_btn').disabled = false;
					// document.getElementById('archive_error').innerHTML = null;
					err_msg = 'Something went wrong!';
				}
				showPopup('danger', err_msg);
				return false;
			}
			console.log('Station List', that.state.stations_selected);
			console.log('Checked Parametes', that.state.checked_parameters);
			console.log('Time Interval', that.state.time_interval);
			console.log('avg_time_interval', that.state.avg_time_interval);
			console.log('Data Format', that.state.data_format);
		}
	}

	/**
	 * This function calls the the parent funtion once the form is submitted
	 */
	get_archive_data() {
		let data_to_be_posted = this.validateData();
		console.log('data_to_be_posted', data_to_be_posted);
		if(data_to_be_posted) {
			this.props.getReport(data_to_be_posted);
		}
	}

	/**
	 * Checks all parameters
	 */
	checkAllParams(status) {
		console.log('entered into checkAllParams', status);
		let selected_parameters = [];
		let all_parameters_of_station = [];
		let rain_box = "rain", aqi_box = 'hourly_aqi', wind_box = 'wind_rose', pollution_box = 'pollution_rose';

		all_parameters_of_station.push(this.hourly_aqi);
		this.state.all_stations.map((station) => {
			if(this.state.stations_selected && this.state.stations_selected.length && this.state.stations_selected.includes(station.id)) {
				if (_.some(station.parameters, {key: 'wspeed'}) && _.some(station.parameters, {key: 'wdir'})) {
					if (!(_.some(all_parameters_of_station, {key: 'wind_rose'}))) {
						all_parameters_of_station.push(this.wind_rose);
					}
				}
				if (_.some(station.parameters, {key: 'wdir'}) && (_.some(station.parameters, {key: 'pm2.5'}) || _.some(station.parameters, {key: 'pm10'}) || _.some(station.parameters, {key: 'no2'}) || _.some(station.parameters, {key: 'o3'}) || _.some(station.parameters, {key: 'co'}) || _.some(station.parameters, {key: 'so2'}))) {
					if (!(_.some(all_parameters_of_station, {key: 'pollution_rose'}))) {
						all_parameters_of_station.push(this.pollution_rose);
					}
				}
			}
		});
		this.state.all_stations.map((station) => {
			if(this.state.stations_selected && this.state.stations_selected.length && this.state.stations_selected.includes(station.id)) {
				station.parameters.map((params) => {
					if (!(_.some(all_parameters_of_station, {key: params.key}))) {
						all_parameters_of_station.push(params);
					}
				});
			}
		});
		if (this.state.data_type_check) {
			all_parameters_of_station = all_parameters_of_station.filter(function(param){ 
				return param.key != aqi_box; 
			});
			all_parameters_of_station = all_parameters_of_station.filter(function(param){ 
				return param.key != wind_box; 
			});
			all_parameters_of_station = all_parameters_of_station.filter(function(param){ 
				return param.key != pollution_box; 
			});
		}
		if (!this.state.is_admin) {
			if (this.state.data_type_check) {
				all_parameters_of_station = all_parameters_of_station.filter(function(param){ 
					return param.key != rain_box; 
				});
			}
		}
		console.log('all_parameters_of_station', all_parameters_of_station);
		if (status) {
			all_parameters_of_station.map((params)=>{
				console.log('params', params);
				if (status && selected_parameters.indexOf(params.key) === -1) {
					selected_parameters.push(params.key);
				} else if (selected_parameters.indexOf(params.key) !== -1) {
					selected_parameters.splice(selected_parameters.indexOf(params.key), 1);
				}
				// selected_parameters.push(params.key);
			});
		} else {
			selected_parameters = [];
		}

		all_parameters_of_station.map((parameter) => {
			if (document.getElementById('param_'+ parameter.key)) {
				document.getElementById('param_'+ parameter.key).checked = status ? true : false;
			}
		});

		this.setState({checked_parameters: selected_parameters});
	}

	changeDataType(type) {
		let selected_parameters = this.state.checked_parameters,
			data_type_check = this.state.data_type_check;
		let rain_box = document.getElementById('param_rain'),
			aqi_box = document.getElementById('param_hourly_aqi'),
			wind_box = document.getElementById('param_wind_rose'),
			pollution_box = document.getElementById('param_pollution_rose');
		if (type === 'raw_data') {
			data_type_check = true;
			this.enabled_raw_data = true;
		} else {
			data_type_check = false;
			this.enabled_raw_data = true;
			document.getElementById('data_type_error').innerHTML = null;
		}
		if (aqi_box) {
			if (this.station_type == '2') {
				document.getElementById('param_hourly_aqi').setAttribute("disabled", true);
				if (selected_parameters.indexOf('hourly_aqi') !== -1) {
					selected_parameters.splice(selected_parameters.indexOf('hourly_aqi'), 1);
				}
			} else {
				if (data_type_check) {
					document.getElementById('param_hourly_aqi').setAttribute("disabled", true);
					if (selected_parameters.indexOf('hourly_aqi') !== -1) {
						selected_parameters.splice(selected_parameters.indexOf('hourly_aqi'), 1);
					}
					// console.log('hiiif');
				} else {
					document.getElementById('param_hourly_aqi').removeAttribute("disabled");
					// console.log('hiielse');
				}
			}
		}
		if (wind_box) {
			if (this.station_type == '2') {
				document.getElementById('param_wind_rose').setAttribute("disabled", true);
				if (selected_parameters.indexOf('wind_rose') !== -1) {
					selected_parameters.splice(selected_parameters.indexOf('wind_rose'), 1);
				}
			} else {
				if (data_type_check) {
					document.getElementById('param_wind_rose').setAttribute("disabled", true);
					if (selected_parameters.indexOf('wind_rose') !== -1) {
						selected_parameters.splice(selected_parameters.indexOf('wind_rose'), 1);
					}
					// console.log('hiiif');
				} else {
					document.getElementById('param_wind_rose').removeAttribute("disabled");
					// console.log('hiielse');
				}
			}
		}
		if (pollution_box) {
			if (this.station_type == '2') {
				document.getElementById('param_pollution_rose').setAttribute("disabled", true);
				if (selected_parameters.indexOf('pollution_rose') !== -1) {
					selected_parameters.splice(selected_parameters.indexOf('pollution_rose'), 1);
				}
			} else {
				if (data_type_check) {
					document.getElementById('param_pollution_rose').setAttribute("disabled", true);
					if (selected_parameters.indexOf('pollution_rose') !== -1) {
						selected_parameters.splice(selected_parameters.indexOf('pollution_rose'), 1);
					}
					// console.log('hiiif');
				} else {
					document.getElementById('param_pollution_rose').removeAttribute("disabled");
					// console.log('hiielse');
				}
			}
		}
		if (!this.state.is_admin && rain_box) {
			if (rain_box && data_type_check) {
				document.getElementById('param_rain').setAttribute("disabled", true);
				if (selected_parameters.indexOf('rain') !== -1) {
					selected_parameters.splice(selected_parameters.indexOf('rain'), 1);
				}
				// console.log('hiiif');
			} else {
				document.getElementById('param_rain').removeAttribute("disabled");
				// console.log('hiielse');
			}
		} else {
			if (rain_box) {
				document.getElementById('param_rain').removeAttribute("disabled");
			}
			// console.log('disabled else');
		}
		this.setState({data_type_check : data_type_check}, () => {this.addSelectedParameters()});	
	}

	/**
	 * It redirect the page to an API to download the required file.
	 */
	download_archive_data_of_station() {
		let data_type;
		let data_to_be_posted = this.validateData();
		if (data_to_be_posted['data_type']) {
			data_type = 'raw_data';
		} else {
			data_type = 'avg_over_time';
		}
		if (data_to_be_posted) {
			let data_to_be_sent = {
				city_id: data_to_be_posted['city_id'],
				station_id: data_to_be_posted['station_ids'],
				parameters: data_to_be_posted['parameters'],
				from_time: document.querySelector('.from_date_time .form-control').value,
				upto_time: document.querySelector('.upto_date_time .form-control').value,
				data_type: data_type,
				duration: document.querySelector('#duration').value,
				conversion_type: data_to_be_posted['conversion_type']
			};
			// console.log('data_to_be_posted', data_to_be_posted);
			window.location = '##PR_STRING_REPLACE_API_BASE_PATH##' + 'download_archive_data.php?d='+ JSON.stringify(data_to_be_sent);
		}
	}

	changeDataSource(source) {
		// console.log('source', source);
		if (source == 'city') {
			document.getElementById('raw_data').disabled = true;
			if (document.getElementById('raw_data').checked == true) {
				document.getElementById('data_type_error').innerHTML = 'Please Select Average';
				this.enabled_raw_data = false;
			}
			// console.log('city', document.getElementById('city_select'));
			if (document.getElementById('city_select')) {
				document.getElementById('city_select').checked = true;
			} 
			if (document.getElementById('station_select')) {
				document.getElementById('station_select').checked = false;
			}
			this.setState({
				stations_selected: [],
				selected_parameters: [],
				checked_parameters: []
			});
		} else {
			document.getElementById('raw_data').disabled = false;
			document.getElementById('data_type_error').innerHTML = null;
			this.enabled_raw_data = true;
			// console.log('station', document.getElementById('station_select'));
			if (document.getElementById('city_select')) {
				document.getElementById('city_select').checked = false;
			} 
			if (document.getElementById('station_select')) {
				document.getElementById('station_select').checked = true;
			}
			this.setState({
				city_selected: null
			});
		}
	}

	render() {
		return <div className="archive-form">
			<h3 className="archive-title">Select your requirements</h3>
			<div className="archive-options">
				<div className="option-row">
					<div className="option-label">Station</div>
					<div className="option-content">
						{(() => {
							if (this.state.all_cities && this.state.all_cities.length) {
								return <div className="data-source">
									{(() => {
										if (this.state.all_cities_list && Object.keys(this.state.all_cities_list).length) {
											return <div className="station-area" onClick={() => this.changeDataSource('city')}>
												<input type="radio" name="source_select" id="city_select" />
												<div className="label-city-select">
													<Select
														placeholder={this.state.city_selected == null ? this.state.city_placeholder : this.state.city_placeholder}
														options={this.state.all_cities_list}
														selected={this.state.city_selected}
														callback={(selected) => this.getCityValue(selected)}
														item_type={'Stations'} />
												</div>
											</div>;
										}
									})()}
									{(() => {
										if (this.state.all_stations_list && Object.keys(this.state.all_stations_list).length) {
											return <div className="station-area" onClick={() => this.changeDataSource('station')}>
												<input type="radio" name="source_select" id="station_select" />
													<div className="label-multi-stations-select" >
														<MultiSelect
															placeholder={this.state.stations_placeholder}
															options={this.state.all_stations_list}
															selected={this.state.stations_selected}
															callback={(selected) => this.getStationIds(selected)}
															item_type={'Stations'} />
													</div>
											</div>;
										}
									})()}
								</div>;
							} else {
								if (this.state.all_stations_list && Object.keys(this.state.all_stations_list).length) {
									return <div className="station-area">
										<MultiSelect
											placeholder={this.state.stations_placeholder}
											options={this.state.all_stations_list}
											selected={this.state.stations_selected}
											callback={(selected) => this.getStationIds(selected)}
											item_type={'Stations'} />
									</div>;
								}
							}
						})()}
						
					</div>
				</div>
				<div className="option-row">
					<div className="option-label">
						Parameters
						{(() => {
							if (this.state.all_stations && this.state.all_stations.length) {
								return <div className="option">
									<input type="checkbox" id={'all_params'} defaultChecked onChange={(e) => this.checkAllParams(e.target.checked)} />
									<label htmlFor={'all_params'}>Select All</label>
								</div>;
							}
						})()}
					</div>
					{(() => {
						let parameters = [];
						if (this.state.city_selected != null) {
							parameters = [];
							return <div className="option-content">Cann't choose parameter for city</div>;
						} else {
							if (this.state.selected_parameters != null) {
								console.log("selected parameters in render ",this.state.selected_parameters);
								console.log("checked parameters in render ",this.state.checked_parameters);
								if (this.state.stations_selected && this.state.stations_selected.length) {
									parameters = [];
									parameters.push(this.hourly_aqi);
								}
								this.state.all_stations.map((station) => {
									if(this.state.stations_selected && this.state.stations_selected.length && this.state.stations_selected.includes(station.id)) {
										if (_.some(station.parameters, {key: 'wspeed'}) && _.some(station.parameters, {key: 'wdir'})) {
											if (!(_.some(parameters, {key: 'wind_rose'}))) {
												parameters.push(this.wind_rose);
											}
										}
										if (_.some(station.parameters, {key: 'wdir'}) && (_.some(station.parameters, {key: 'pm2.5'}) || _.some(station.parameters, {key: 'pm10'}) || _.some(station.parameters, {key: 'no2'}) || _.some(station.parameters, {key: 'o3'}) || _.some(station.parameters, {key: 'co'}) || _.some(station.parameters, {key: 'so2'}))) {
											if (!(_.some(parameters, {key: 'pollution_rose'}))) {
												parameters.push(this.pollution_rose);
											}
										}
									}
								});
								if (this.state.all_stations && this.state.all_stations.length) {
									this.state.all_stations.map((station) => {
										if(this.state.stations_selected && this.state.stations_selected.length && this.state.stations_selected.includes(station.id)) {
											station.parameters.map((param) => {
												if (!(_.some(parameters, {key: param.key}))) {
													parameters.push(param);
												}
												// console.log('param_key', param.key);
											});
											this.station_type = station.type;
											console.log('station_type', this.station_type);
										}
									});
								}
								if (parameters && parameters.length) {
									console.log('parameters', parameters);
									let parameter_options = parameters.map((parameter, i) => {
										if (parameter.key == 'hourly_aqi') {
											if (this.station_type == '2') {
												return null;
											} else {
												return (
													<div className="option" key={i}>
														<input type="checkbox" id={'param_hourly_aqi'} defaultChecked checked={this.state.checked_parameters.indexOf(parameter.key) >= 0} onChange={(e) =>this.addSelectedParameters(e.target.checked, parameter.key)} disabled={this.state.data_type_check ? true : false} />
														<label htmlFor={'param_'+parameter.key} dangerouslySetInnerHTML={{__html: parameter.name}} />
													</div>
												);
											}
										} else if (parameter.key == 'wind_rose') {
											if (this.station_type == '2') {
												return null;
											} else {
												return (
													<div className="option" key={i}>
														<input type="checkbox" id={'param_wind_rose'} defaultChecked checked={this.state.checked_parameters.indexOf(parameter.key) >= 0} onChange={(e) =>this.addSelectedParameters(e.target.checked, parameter.key)} disabled={this.state.data_type_check ? true : false} />
														<label htmlFor={'param_'+parameter.key} dangerouslySetInnerHTML={{__html: parameter.name}} />
													</div>
												);
											}
										} else if (parameter.key == 'pollution_rose') {
											if (this.station_type == '2') {
												return null;
											} else {
												return (
													<div className="option" key={i}>
														<input type="checkbox" id={'param_pollution_rose'} defaultChecked checked={this.state.checked_parameters.indexOf(parameter.key) >= 0} onChange={(e) =>this.addSelectedParameters(e.target.checked, parameter.key)} disabled={this.state.data_type_check ? true : false} />
														<label htmlFor={'param_'+parameter.key} dangerouslySetInnerHTML={{__html: parameter.name}} />
													</div>
												);
											}
										} else {
											return(
												<div className="option" key={i}>
													<input type="checkbox" id={'param_'+parameter.key} defaultChecked checked={this.state.checked_parameters.indexOf(parameter.key) >= 0} onChange={(e) =>this.addSelectedParameters(e.target.checked, parameter.key)} disabled={(this.state.is_admin ? false : (parameter.key == 'rain' && (this.state.is_admin || this.state.data_type_check) ? true : false))} />
													<label htmlFor={'param_'+parameter.key} dangerouslySetInnerHTML={{__html: parameter.name}} />
												</div>
											);
										}
									});
									return(
										<div className="option-content">
											{parameter_options.filter(Boolean)}
										</div>
									);
								} else {
									return <div className="option-content">
										{'No data has been received from the device yet.'}
									</div>;
								}
								
								// let	params_list = this.state.selected_parameters.map((key) => {
								// 	return <div className="option">
								// 		<input type="checkbox" id={'param_'+key} checked={this.state.checked_parameters.indexOf(key) >= 0} onChange={(e) =>this.addSelectedParameters(e.target.checked, key)} />
								// 		<label htmlFor={'param_'+key} dangerouslySetInnerHTML={{ __html: this.state.all_parameters[key].name }} />
								// 	</div>;
								// });
								// return params_list;
							}else{
								return <div className="option-content">Select a station first</div>;
							}
						}
					})()}
				</div>
				<div className="option-row">
					<div className="option-label">Time Interval</div>
					<div className="option-content">
						<div className="date-time">
							<FromDateTime />
						</div>
						<div className="hyphen">-</div>
						<div className="date-time">
							<UptoDateTime />
						</div>
					</div>
				</div>
				<div className="option-row">
					<div className="option-label">Data Type</div>
					<div className="option-content">
						<div className="option">
							<input name="data-type" type="radio" id="raw_data" defaultChecked onClick={() => this.changeDataType('raw_data')}/>
							<label htmlFor="raw_data">Raw Data</label>
						</div>
						<div className="option lg">
							<input name="data-type" type="radio" id="avg_over_time" onClick={() => this.changeDataType('avg_over_time')} />
							<label htmlFor="avg_over_time">Average over time based</label>
							<select className="form-control" title="Select Time" defaultValue={1} id="duration">
								<option value={'1'}>1 Hour</option>
								<option value={'8'}>8 Hour</option>
								<option value={'24'}>24 Hour</option>
							</select>
						</div>
						<div className="data-type-option-select-error">
							<div className="data-type-error-text" id="data_type_error"></div>
						</div>
					</div>
				</div>
				<div className="option-row">
					<div className="option-label">View Data Format</div>
					<div className="option-content">
						<div className="option">
							<input type="checkbox" id="view_data_as_grid" defaultChecked />
							<label htmlFor="view_data_as_grid">Grid</label>
						</div>
						<div className="option">
							<input type="checkbox" id="view_data_as_graph" defaultChecked />
							<label htmlFor="view_data_as_graph">Graph</label>
						</div>
					</div>
				</div>
				<div className="option-row">
					<div className="option-label">Conversion Type</div>
					<div className="option-content">
						<div className="option">
							<input type="radio" name="conversion_factor" id="view_data_as_usepa" defaultChecked onClick={() => this.changeConversionType('usepa')} />
							<label htmlFor="view_data_as_usepa">USEPA</label>
						</div>
						<div className="option">
							<input type="radio" name="conversion_factor" id="view_data_as_naqi" onClick={() => this.changeConversionType('naqi')} />
							<label htmlFor="view_data_as_naqi">NAQI</label>
						</div>
					</div>
				</div>
			</div>
			<div className="archive-btns">
				<button type="button" id="view_data_btn" className="btn btn-primary" onClick={() => this.get_archive_data()} disabled={this.enabled_raw_data ? false : true}>View</button>
				<button type="button" id="download_data_btn" className="btn btn-success" onClick={() => this.download_archive_data_of_station()} disabled={this.enabled_raw_data ? false : true} >Download</button>
			</div>
			<div className="text-danger text-center" id="archive_error"></div>
		</div>;
	}
}

ArchiveReportsForm.contextTypes = {
	router: React.PropTypes.object
};