import React from 'react';
import moment from 'moment-timezone';
import ReactTooltip from 'react-tooltip';

/**
 * This Class Used for Compare check.
 */
export default class CompareCheck extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			props: props,
			compared_station_ids: null,
			station_compared: null
		};

		this.fetch_compared_station_ids();
	}

	/**
	 * This function is to refresh station data.
	 */
	refresh_station() {
		var that = this;
		var compared = that.state.compared_station_ids.indexOf(parseInt(that.props.station_id));
		that.setState({
			station_compared: (compared > -1) ? 1 : null,
			station_id: that.props.station_id
		});
	}

	/**
	 * This function is used to fetch compared station list.
	 */
	fetch_compared_station_ids() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_station_compare_list_of_user.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include'
		}).then(function(Response) {
			return Response.json()
		}).then(function(json) {
			console.log('Compared Stations:',json);
			if(json.status === 'success') {
				if (json.station_ids != null) {
					let station_ids = json.station_ids.map(Number);
					let compared = station_ids.indexOf(parseInt(that.props.station_id));
					that.setState({
						compared_station_ids: station_ids,
						station_compared: (compared > -1) ? 1 : null
					});
				}
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function is to add station to compare list.
	 */
	add_station_to_compare() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'add_station_to_compare_list_of_user.php', {
			credentials: 'include',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				station_id: parseInt(that.props.station_id)
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('Add Response:',json);
			if (json.status === 'success') {
				that.fetch_compared_station_ids();
				showPopup('success','Station added to compare list.');
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function is to remove station to compare list.
	 */
	remove_station_from_compare() {
		var that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'delete_station_from_compare_list_of_user.php', {
			credentials: 'include',
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({
				station_id: parseInt(that.props.station_id)
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('Remove Response:',json);
			if (json.status === 'success') {
				that.fetch_compared_station_ids();
				showPopup('success','Station removed from compare list.');
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	/**
	 * This function is used to toggle compare checkbox.
	 */
	toggle_station_to_compare() {
		var that = this;
		if (that.state.station_compared) {
			that.remove_station_from_compare();
		} else {
			that.add_station_to_compare();
		}
	}

	/**
	 * This renders the Compare check.
	 * @return {ReactElement} markup
	 */
	render() {
		return (<div className="compare-btn-container">
			{(() => {
				if (this.state.station_compared) {
					return React.createElement('input',{type: 'checkbox', id: 'check_compare', defaultChecked: (this.state.station_compared) ? true : false});
				}
			})()}
			<label className="compare-btn" htmlFor="check_compare" onClick={() => this.toggle_station_to_compare()}>
				<span className="custom-checkbox"></span>Compare
			</label>
		</div>);
	}
}