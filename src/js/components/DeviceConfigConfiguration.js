import React from 'react';
import { findDOMNode } from 'react-dom';
import NavLink from './NavLink';
import ReactTooltip from 'react-tooltip';
import { Scrollbars } from 'react-custom-scrollbars';
import io from 'socket.io-client';
import { Link } from 'react-router';

/**
 * This Class Used for Configuration of devices.
 */
export default class DeviceConfigConfiguration extends React.Component {
	/**
	 * This is the Constructor of the page.
	 * @param {object} props
	 */
	constructor(props) {
		super(props);
		/**
		 * This sets the initial state for the page.
		 * @type {Object}
		 */
		this.state = {
			is_admin: document.getElementById('city_id') ? (document.getElementById('city_id').value == 5 ? true : false) : false,
			device_id: props.params.device_id ? props.params.device_id : 1,
			data_loading: 1,
			status: null,
			device_qr_code: null,
			data_sampling_interval: null,
			data_transmission_interval: null,
			wifi_pass: null,
			wifi_ssid: null,
			modem_type: 0,
			fs_ip: null,
			fs_port: null,
			ss_ip: null,
			ss_port: null,
			device_ip: null,
			subnet_mask: null,
			default_gateway: null,
			dns_server: null,
			enable_save_transmission: true,
			enable_save_sampling: true,
			allow_configurations_success_msg: false
		};

		/**
		 * This is used for City name.
		 * @type {string}
		 */
		this.city_name = window.location.host.split('.')[0];
	}

	/**
	 * This function saves the user edited configurations.
	 */
	saveDeviceConfiguration() {
		this.socket.emit('save_device_configurations', JSON.stringify({
			device_qr_code: this.state.device_qr_code,
			status: this.state.status,
			device_id: this.state.device_id,
			configurations: {
				data_transmission_interval: this.state.data_transmission_interval,
				data_sampling_interval: this.state.data_sampling_interval,
				modem_type: this.state.modem_type,
				wifi_ssid: this.state.wifi_ssid,
				wifi_pass: this.state.wifi_pass,
				fs_ip: this.state.fs_ip,
				fs_port: this.state.fs_port,
				ss_ip: this.state.ss_ip,
				ss_port: this.state.ss_port,
				device_ip: this.state.device_ip,
				subnet_mask: this.state.subnet_mask,
				default_gateway: this.state.default_gateway,
				dns_server: this.state.dns_server
			}
		}));
	}

	/**
	 * This funtion used for validating the fields (number or not).
	 * @param  {number} target value of the field.
	 * @param  {string} key    name of the field from api side is used as key.
	 */
	dataValidationCheckConfig(target,key) {
		switch(key) {
			case 'data_transmission_interval':
				this.setState({data_transmission_interval: target.value});
				if (target.value >= 0 && !isNaN(target.value)) {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12';
					this.setState({enable_save_transmission: true});
				} else {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 error';
					this.setState({enable_save_transmission: false});
				}
				break;
			case 'data_sampling_interval':
				this.setState({data_sampling_interval: target.value});
				if (target.value >= 0 && !isNaN(target.value)) {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12';
					this.setState({enable_save_sampling: true});
				} else {
					target.parentElement.className = 'form-group col-lg-6 col-md-6 col-sm-6 col-xs-12 error';
					this.setState({enable_save_sampling: false});
				}
				break;
		}
		// console.log('Target', target);
	}

	readCookie(key) {
		let result;
		return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
	}

	/**
	 * This method is called right after when an instance of a component is being created and inserted into the DOM.
	 */
	componentDidMount() {
			document.title = 'Device Configuration - Aurassure';

			/**
			 * Socket is used to sync the data between client and server in real time.
			 */
			this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##', {
				query: {
					s_token: this.readCookie('PHPSESSID')
				}
			});
			this.socket.on('connect', () => {
				this.socket.emit('connect_dashboard_to_socket');
				console.log('connected to socket');
			});

			this.socket.on('dashboard_successfully_connected', () => {
				this.socket.emit('get_device_configurations', JSON.stringify({
					device_id: this.state.device_id
				}));
			});

			this.socket.on('update_device_configurations', (payload) => {
				let device_configuration = JSON.parse(payload);
				console.log('update_device_configurations', device_configuration);
				if (this.state.device_id == device_configuration.device_id) {
					let saved_configurations = {
						data_sampling_interval: device_configuration.configurations ? device_configuration.configurations.data_sampling_interval : this.state.data_sampling_interval,
						data_transmission_interval: device_configuration.configurations ? device_configuration.configurations.data_transmission_interval : this.state.data_transmission_interval,
						modem_type: device_configuration.configurations ? device_configuration.configurations.modem_type : this.state.modem_type,
						wifi_pass: device_configuration.configurations ? device_configuration.configurations.wifi_pass : this.state.wifi_pass,
						wifi_ssid: device_configuration.configurations ? device_configuration.configurations.wifi_ssid : this.state.wifi_ssid,
						fs_ip: device_configuration.configurations ? device_configuration.configurations.fs_ip : this.state.fs_ip,
						fs_port: device_configuration.configurations ? device_configuration.configurations.fs_port : this.state.fs_port,
						ss_ip: device_configuration.configurations ? device_configuration.configurations.ss_ip : this.state.ss_ip,
						ss_port: device_configuration.configurations ? device_configuration.configurations.ss_port : this.state.ss_port,
						device_ip: device_configuration.configurations ? device_configuration.configurations.device_ip : this.state.device_ip,
						subnet_mask: device_configuration.configurations ? device_configuration.configurations.subnet_mask : this.state.subnet_mask,
						default_gateway: device_configuration.configurations ? device_configuration.configurations.default_gateway : this.state.default_gateway,
						dns_server: device_configuration.configurations ? device_configuration.configurations.dns_server : this.state.dns_server
					};
					this.setState({
						device_qr_code: device_configuration.device_qr_code ? device_configuration.device_qr_code : this.state.device_qr_code,
						status: device_configuration.status ? device_configuration.status : this.state.status,
						device_id: device_configuration.device_id,
						data_sampling_interval: device_configuration.configurations ? device_configuration.configurations.data_sampling_interval : this.state.data_sampling_interval,
						data_transmission_interval: device_configuration.configurations ? device_configuration.configurations.data_transmission_interval : this.state.data_transmission_interval,
						modem_type: device_configuration.configurations ? device_configuration.configurations.modem_type : this.state.modem_type,
						wifi_pass: device_configuration.configurations ? device_configuration.configurations.wifi_pass : this.state.wifi_pass,
						wifi_ssid: device_configuration.configurations ? device_configuration.configurations.wifi_ssid : this.state.wifi_ssid,
						fs_ip: device_configuration.configurations ? device_configuration.configurations.fs_ip : this.state.fs_ip,
						fs_port: device_configuration.configurations ? device_configuration.configurations.fs_port : this.state.fs_port,
						ss_ip: device_configuration.configurations ? device_configuration.configurations.ss_ip : this.state.ss_ip,
						ss_port: device_configuration.configurations ? device_configuration.configurations.ss_port : this.state.ss_port,
						device_ip: device_configuration.configurations ? device_configuration.configurations.device_ip : this.state.device_ip,
						subnet_mask: device_configuration.configurations ? device_configuration.configurations.subnet_mask : this.state.subnet_mask,
						default_gateway: device_configuration.configurations ? device_configuration.configurations.default_gateway : this.state.default_gateway,
						dns_server: device_configuration.configurations ? device_configuration.configurations.dns_server : this.state.dns_server,
						modem_types: {
							1: 'GPRS',
							2: 'WIFI',
							3: 'Ethernet'
						},
						data_loading: null,
						saved_configurations: saved_configurations
					});
					if (this.state.allow_configurations_success_msg) {
						showPopup('success', 'Configurations saved successfully!');
					} else {
						this.setState({
							allow_configurations_success_msg: true
						});
					}
				}
			});

			this.socket.on('update_location_details_in_dashboard', (payload) => {
				console.log('update_location_details_in_dashboard', JSON.parse(payload));
				if (JSON.parse(payload).device_id == this.state.device_id) {
					this.setState(JSON.parse(payload));
				}
			});
		// if ((this.city_name === 'admin') || (this.city_name === '127')) {

		// } else {
		// 	document.title = '404 Not Found - Aurassure';
		// }
	}

	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		this.socket.close();
	}

	/**
	 * This is invoked immediately after updating occurs. This method is not called for the initial render.
	 * @param  {object} prevProps This is the Previous saved props.
	 * @param  {object} prevState This is the Previous saved state.
	 */
	componentDidUpdate(prevProps, prevState) {
		// console.log(this.state.saved_configurations);
		if (this.state.saved_configurations) {
			if (
				((this.state.saved_configurations.data_sampling_interval == this.state.data_sampling_interval) && 
				(this.state.saved_configurations.data_transmission_interval == this.state.data_transmission_interval) && (this.state.saved_configurations.modem_type == this.state.modem_type) &&
				(this.state.saved_configurations.wifi_ssid == this.state.wifi_ssid) && 
				(this.state.saved_configurations.wifi_pass == this.state.wifi_pass) && 
				(this.state.saved_configurations.fs_ip == this.state.fs_ip) && 
				(this.state.saved_configurations.fs_port == this.state.fs_port) &&
				(this.state.saved_configurations.ss_ip == this.state.ss_ip) && 
				(this.state.saved_configurations.ss_port == this.state.ss_port) &&
				(this.state.saved_configurations.device_ip == this.state.device_ip) &&
				(this.state.saved_configurations.subnet_mask == this.state.subnet_mask) &&
				(this.state.saved_configurations.default_gateway == this.state.default_gateway) &&
				(this.state.saved_configurations.dns_server == this.state.dns_server)) ||
				(!this.state.enable_save_transmission) ||
				(!this.state.enable_save_sampling)
			) {
				findDOMNode(this.refs.saveConfig).setAttribute("disabled", true);
			} else {
				findDOMNode(this.refs.saveConfig).removeAttribute("disabled");
			}
		}
		ReactTooltip.rebuild();
	}

	/**
	 * This renders the Device Configuration page.
	 * @return {ReactElement} markup
	 */
	render() {
		return(
			<div className="full-page-container device-config-container">
				<NavLink active="configure" />
				<Scrollbars autoHide>
					<div className="col-lg-8 col-md-10 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-1 device-details-list">
						<h4 className="title-text">
							<Link to={'/device-config'} className="back-arrow pull-left">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 491.1 491.1" width="24" height="24"><path fill="#CCC" d="M111.85,208.25l192.8-192.8c20.6-20.6,54-20.6,74.6,0s20.6,54,0,74.6l-155.5,155.5l155.5,155.5c20.6,20.6,20.6,54,0,74.6 s-54,20.6-74.6,0l-192.8-192.8C91.25,262.25,91.25,228.85,111.85,208.25z"/></svg>
							</Link>
							<span className={'device-name' + ((this.state.status == "data_off") ? ' data-off' : ((this.state.status == "online") ? ' active' : ''))}>Device: {this.state.device_qr_code}</span>
						</h4>
						<hr className="separator" />
						<div className="tab-container">
							<Link to={'/device-config/' + this.state.device_id + '/details'} className="tab-item">Details</Link>
							<Link to={'/device-config/' + this.state.device_id + '/configuration'} className="tab-item active">Configuration</Link>
							<Link to={'/device-config/' + this.state.device_id + '/calibration'} className="tab-item">Calibration</Link>
						</div>
						<div className="panel panel-default">
							<div className="panel-body details-list">
								{(() => {
									if(this.state.data_loading) {
										return<div className="text-center"><svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
									}
									return <div>
										<div className="row">
											<div className="form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 form-config">
												<label>Modem Type</label>
												<select className="form-control" value={this.state.modem_type} onChange={(e) => this.setState({modem_type: e.target.value})}>
													<option value={0} selected={((!this.state.modem_type || this.state.modem_type == 0) ? true : false)} disabled>Select Operator</option>
													{(() => {
														if (this.state.modem_types) {
															let options = Object.keys(this.state.modem_types).map((index) => {
																return <option value={index}>{this.state.modem_types[index]}</option>;
															});
															return options;
														}
													})()}
												</select>
											</div>
										</div>
										<div className= "row">
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>Data Transmission interval</label>
												<input type="text" className="form-control" id="data_transmission_interval" placeholder="Data Transmission interval" value={this.state.data_transmission_interval} onChange={(e) => this.dataValidationCheckConfig(e.target, 'data_transmission_interval')} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>Data Sampling Interval</label>
												<input type="text" className="form-control" id="data_sampling_interval" placeholder="Data Sampling Interval" value={this.state.data_sampling_interval} onChange={(e) => this.dataValidationCheckConfig(e.target, 'data_sampling_interval')} />
											</div>
										</div>
										<div className="row">
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>WiFi SSID</label>
												<input type="text" className="form-control" placeholder="WiFi SSID" value={this.state.wifi_ssid} onChange={(e) =>this.setState({wifi_ssid: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>WiFi Password</label>
												<input type="text" className="form-control" placeholder="WiFi Password" value={this.state.wifi_pass} onChange={(e) => this.setState({wifi_pass: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>First Server IP</label>
												<input type="text" className="form-control" placeholder="First Server IP" value={this.state.fs_ip} onChange={(e) =>this.setState({fs_ip: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>First Server Port</label>
												<input type="text" className="form-control" placeholder="First Server Port" value={this.state.fs_port} onChange={(e) => this.setState({fs_port: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>Secondary Server IP</label>
												<input type="text" className="form-control" placeholder="Secondary Server IP" value={this.state.ss_ip} onChange={(e) =>this.setState({ss_ip: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>Secondary Server Port</label>
												<input type="text" className="form-control" placeholder="Secondary Server Port" value={this.state.ss_port} onChange={(e) => this.setState({ss_port: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>Device IP</label>
												<input type="text" className="form-control" placeholder="Device IP" value={this.state.device_ip} onChange={(e) => this.setState({device_ip: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>Subnet Mask</label>
												<input type="text" className="form-control" placeholder="Subnet Mask" value={this.state.subnet_mask} onChange={(e) => this.setState({subnet_mask: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>Default Gateway</label>
												<input type="text" className="form-control" placeholder="Default Gateway" value={this.state.default_gateway} onChange={(e) => this.setState({default_gateway: e.target.value})} />
											</div>
											<div className="form-group col-lg-6 col-md-6 col-sm-6 col-xs-12">
												<label>DNS Server</label>
												<input type="text" className="form-control" placeholder="DNS Server" value={this.state.dns_server} onChange={(e) => this.setState({dns_server: e.target.value})} />
											</div>
										</div>
									</div>;
								})()}
								{(() => {
									if (!this.state.data_loading) {
										return <button className="btnn btn-left green-fill-btn btn-width" onClick={() => this.saveDeviceConfiguration()} id ="save_config" ref="saveConfig">Save</button>;
									}
									else {
										return <button className="btnn btn-left green-fill-btn btn-width invisible">Save</button>;
									}
								})()}
							</div>
						</div>
					</div>
					<ReactTooltip effect="solid" />
				</Scrollbars>
				{/*(() => {
					if ((this.city_name === 'admin') || (this.city_name === '127')) {
					} else {
						return <h3><center>404 - Not Found</center></h3>;
					}
				})()*/}
			</div>
		);
	}
}

DeviceConfigConfiguration.contextTypes = {
	router: React.PropTypes.object
};