import React from 'react';
import { HashRouter as Router, Match } from 'react-router';

import Dashboard from './components/Dashboard';
import StationDetailedAnalytics from './components/StationDetailedAnalytics';
import StationAnalyticsGraph from './components/StationAnalyticsGraph';
import StationAnalyticsHourlyGraph from './components/StationAnalyticsHourlyGraph';
import Analytics from "./components/Analytics";
import ArchiveReports from './components/ArchiveReports';
import ManageUsers from './components/ManageUsers';
import Compare from './components/Compare';
import DeviceConfiguration from './components/DeviceConfiguration';
import DeviceConfigDetails from './components/DeviceConfigDetails';
import DeviceConfigConfiguration from './components/DeviceConfigConfiguration';
import DeviceConfigCalibration from './components/DeviceConfigCalibration';
import DeviceDebug from './components/DeviceDebug';
import Settings from './components/Settings';
import Notifications from './components/Notifications';

export default class App extends React.Component {
	constructor() {
		super();
		this.state = {
			access: {
				// city_is_premium: document.getElementById('city_is_premium').value
			}
		};

		var tout = null;
		if (!window.showPopup) {
			window.showPopup = (type, message, timeout) => {
				if (!timeout) {timeout=5000;}
				document.getElementById('popup_alert').className = 'alert alert-'+type+' active';
				document.getElementById('popup_alert_msg').innerHTML = message;
				tout = setTimeout(() => {
					hidePopup();
				}, timeout);
				console.log('Message:',message);
			};
		}
		if (!window.hidePopup) {
			window.hidePopup = () => {
				document.getElementById('popup_alert').className = 'alert';
				clearTimeout(tout);
			};
		}

		window.saveStationId = (id) => {
			window.stationId = (id) ? parseInt(id) : 1;
			console.info(stationId);
		};
		saveStationId();

		window.color_code= {
			color_1: '#00b050',
			color_2: '#92d050',
			color_3: '#ffff00',
			color_4: '#ff9900',
			color_5: '#ff0000',
			color_6: '#c00000'
		};

		this.baseName = '';
		this.routes = [
			{
				pattern: '/stations/:station_id',
				exactly: true,
				component: Dashboard
			},
			{
				pattern: '/stations/:station_id/',
				exactly: true,
				component: Dashboard
			},
			{
				pattern: '/stations/:station_id/analytics',
				exactly: true,
				component: Analytics
			},
			{
				pattern: '/stations/:station_id/analytics/',
				exactly: true,
				component: Analytics
			},
			{
				pattern: '/stations/:station_id/raw-data-snap',
				exactly: true,
				component: StationAnalyticsGraph
			},
			{
				pattern: '/stations/:station_id/raw-data-snap/',
				exactly: true,
				component: StationAnalyticsGraph
			},,
			{
				pattern: '/stations/:station_id/avg-data-snap',
				exactly: true,
				component: StationAnalyticsHourlyGraph
			},
			{
				pattern: '/stations/:station_id/avg-data-snap/',
				exactly: true,
				component: StationAnalyticsHourlyGraph
			},
			{
				pattern: '/archive',
				exactly: true,
				component: ArchiveReports
			},
			{
				pattern: '/archive/',
				exactly: true,
				component: ArchiveReports
			},
			{
				pattern: '/compare',
				component: Compare
			},
			{
				pattern: '/compare/',
				component: Compare
			},
			{
				pattern: '/users',
				component: ManageUsers
			},
			{
				pattern: '/users/',
				component: ManageUsers
			},
			{
				pattern: '/device-config',
				exactly: true,
				component: DeviceConfiguration
			},
			{
				pattern: '/device-config/',
				exactly: true,
				component: DeviceConfiguration
			},
			{
				pattern: '/device-config/:device_id',
				exactly: true,
				component: DeviceConfigDetails
			},
			{
				pattern: '/device-config/:device_id/',
				exactly: true,
				component: DeviceConfigDetails
			},
			{
				pattern: '/device-config/:device_id/details',
				exactly: true,
				component: DeviceConfigDetails
			},
			{
				pattern: '/device-config/:device_id/details/',
				exactly: true,
				component: DeviceConfigDetails
			},
			{
				pattern: '/device-config/:device_id/configuration',
				exactly: true,
				component: DeviceConfigConfiguration
			},
			{
				pattern: '/device-config/:device_id/configuration/',
				exactly: true,
				component: DeviceConfigConfiguration
			},
			{
				pattern: '/device-config/:device_id/calibration',
				exactly: true,
				component: DeviceConfigCalibration
			},
			{
				pattern: '/device-config/:device_id/calibration/',
				exactly: true,
				component: DeviceConfigCalibration
			},
			{
				pattern: '/debug',
				exactly: true,
				component: DeviceDebug
			},
			{
				pattern: '/debug/',
				exactly: true,
				component: DeviceDebug
			},
			{
				pattern: '/debug/:device_id',
				exactly: true,
				component: DeviceDebug
			},
			{
				pattern: '/debug/:device_id/',
				exactly: true,
				component: DeviceDebug
			},
			{
				pattern: '/notifications',
				exactly: true,
				component: Notifications
			},
			{
				pattern: '/notifications/',
				exactly: true,
				component: Notifications
			}
		];

		this.MatchWithSubRoutes = (route) => (
			<Match {...route} render={(props) => (
				// pass the sub-routes down to keep nesting
				<route.component {...props} routes={route.routes} />
			)}/>
		);
	}

	render() {
		return (
			<div>
				<Router basename={this.baseName} >
					<div>
						<div className="alert alert-success" id="popup_alert" onClick={() => hidePopup()}>
							<span className="close hidden-sm hidden-xs" onClick={() => hidePopup()}>&times;</span>
							<div id="popup_alert_msg"></div>
						</div>
						{
							this.routes.map((route, i) => (
								<this.MatchWithSubRoutes key={i} {...route} />
							))
						}
					</div>
				</Router>
			</div>
		);
	}
}
