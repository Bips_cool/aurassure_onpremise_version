<?php
//detect user agent
require_once('/var/www/phoenixrobotix/_includes/vendor/mobiledetect/mobiledetectlib/Mobile_Detect.php');
$user_agent_detector = new Mobile_Detect();
$_ROUTER['user_agent'] = 'desktop';
if($user_agent_detector->isMobile()) {
	$_ROUTER['user_agent'] = 'mobile';
} elseif($user_agent_detector->isTablet()) {
	$_ROUTER['user_agent'] = 'tablet';
}

$ROUTER['base_source_directory'] = '/var/www/aurassure/live/';

if($_ROUTER['user_agent'] == 'desktop') {
	require_once($ROUTER['base_source_directory'].'desktop.php');
} else {
	require_once($ROUTER['base_source_directory'].'mobile.php');
}
