## v1.3.1 (2018-02-19)
###Bugfixes
1. Suggestions were not coming when changing to station having parameter wind-direction/wind-speed/pressure in Dashboard page.

## v1.3.0 (2018-02-18)
###Features
1. Latest parameter values shown in dashboard.
2. Alerts for Offline devices added.
3. Few modifications in frontend styles.

## v1.2.0 (2018-02-03)
###Features
1. Focus the map by zooming to the particular city.
2. Changes in AQI colors as per CPCB standards.
3. URLs modified for serving through HTTPS connection.
4. React Router is changing accourding to the Deployment process.
5. A key file is used to Stage/Deploy the Website through FTP.
6. Integrity applied for CDN files using SHA-384 encryption.
7. Few options in calibration page are not accessible for non-admin users.
### Bugfixes
1. Few bugs fixed in calibration page.
2. Fixed styling of compare page.

## v1.1.0 (2017-12-18)
###Features
1. Release of v1.1.0.
2. Added AQI Major Pollutant(Responsible Pollutant) shown in Dashbord, Analytics and Reports.
3. Added Wind Rose And Pollution Rose in Reports.
4. Added three new graphs (Magneto Meter, Pressure Meter and Speedometer) in Dashboard and Analytics page.
### Bugfixes
1. Corrected AQI For Stations while changing duration in Average Reports.
2. Few Bugs fixed from Back-end for Reports page.
3. Timestamps not appearing properly in Reports with grid-view.

## v1.0.0 (2017-12-09)
###Features
1. Release of v1.0.0.
2. Includes Application Dashboard for Aurassure Infra along with preliminary dashboard for Aurassure Aqua.